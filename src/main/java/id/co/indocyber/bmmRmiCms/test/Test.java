package id.co.indocyber.bmmRmiCms.test;

import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.service.GlobalSvc;
import java.util.List;

import org.springframework.context.ConfigurableApplicationContext;
import org.springframework.context.support.ClassPathXmlApplicationContext;

public class Test {
	public static void main(String args[]) {
		ConfigurableApplicationContext ctx = 
				new ClassPathXmlApplicationContext("/META-INF/spring/app-config.xml");
//		MAddressTypeDao dao = ctx.getBean(MAddressTypeDao.class);
//		System.out.println(dao.findAll());
//		ctx.close();
		
		GlobalSvc svc = ctx.getBean(GlobalSvc.class);
//		System.err.println("Hasilllll = ");
//		List<Integer> list = svc.getAllMyTeam(10);
//		int no = 1;
//		for(Integer i : list){
//			System.out.println(no + ". "+i);
//			no++;
//		}
//		
//		MobEmployeeSvc svc2 = ctx.getBean(MobEmployeeSvc.class);
//		System.out.println("");
//		System.err.println("Data :");
//		int num = 1;
//		List<MobEmployee> list2 = svc2.findManagedEmployees(list);
//		for(MobEmployee e :  list2){
//			System.out.println(num+". "+e.getFullname());
//			num++;
//		}
		
		System.err.println("Hasilll");
		List<MUserDto> listDto = svc.listTesting();
		for(MUserDto d : listDto){
			System.out.println(d.getFullname());
		}
	}
}
