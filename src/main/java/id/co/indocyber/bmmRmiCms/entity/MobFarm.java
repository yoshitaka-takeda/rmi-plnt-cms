package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name = "mob_farm")
@NamedQuery(name = "MobFarm.findAll", query = "SELECT mf FROM MobFarm mf")
@IdClass(MobFarmPK.class)
public class MobFarm implements Serializable {
	private static final long serialVersionUID = -5778365524484767090L;

	@Id
	@Column(name = "farmer_id")
	private Integer farmerId;

	@Id
	@Column(name = "farm_code")
	private String farmCode;

	@Column(name = "field_area")
	private Double fieldArea;

	@Column(name = "village")
	private String village;
	
	@Column(name = "sub_district")
	private String subDistrict;
	
	@Column(name = "district")
	private String district;
	
	@Column(name = "province")
	private String province;

	@Column(name = "latitude")
	private Double latitude;
	
	@Column(name = "longitude")
	private Double longitude;
	
	@Column(name = "farm_main_type")
	private String farmMainType;
	
	@Column(name = "farm_sub_type")
	private String farmSubType;
	
	@Column(name = "reference_code")
	private String referenceCode;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "is_active")
	private Boolean isActive;
	
	@Column(name = "processed_by")
	private String processedBy;
	
	@Column(name = "processed_date")
	private Timestamp processedDate;

	public Integer getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(Integer farmerId) {
		this.farmerId = farmerId;
	}

	public String getFarmCode() {
		return farmCode;
	}

	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}

	public Double getFieldArea() {
		return fieldArea;
	}

	public void setFieldArea(Double fieldArea) {
		this.fieldArea = fieldArea;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getSubDistrict() {
		return subDistrict;
	}

	public void setSubDistrict(String subDistrict) {
		this.subDistrict = subDistrict;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public String getFarmMainType() {
		return farmMainType;
	}

	public void setFarmMainType(String farmMainType) {
		this.farmMainType = farmMainType;
	}

	public String getFarmSubType() {
		return farmSubType;
	}

	public void setFarmSubType(String farmSubType) {
		this.farmSubType = farmSubType;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getProcessedBy() {
		return processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public Timestamp getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(Timestamp processedDate) {
		this.processedDate = processedDate;
	}
}
