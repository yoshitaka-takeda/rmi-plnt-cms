package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name = "mob_paddy_field")
@NamedQuery(name = "MobPaddyField.findAll", query = "SELECT mpf FROM MobPaddyField mpf")
public class MobPaddyField implements Serializable {
	private static final long serialVersionUID = -5778365524484767090L;

	@Id
	@Column(name = "id")
	private Integer id;
	
	@Column(name = "farmer_id")
	private Integer farmerId;

	@Column(name = "farm_code")
	private String farmCode;

	@Column(name = "paddy_field_code")
	private String paddyFieldCode;
	
	@Column(name = "line_num")
	private Integer lineNum;
	
	@Column(name = "field_area")
	private Double fieldArea;
	
	@Column(name = "latitude")
	private Double latitude;
	
	@Column(name = "longitude")
	private Double longitude;
	
	@Column(name = "weigh_estimation")
	private Double weighEstimation;
	
	@Column(name = "sugarcane_type")
	private String sugarcaneType;
	
	@Column(name = "age_of_sugarcane")
	private String ageOfSugarcane;
	
	@Column(name = "planting_date")
	private Date plantingDate;
	
	@Column(name = "sugarcane_species")
	private String sugarcaneSpecies;
	
	@Column(name = "sugarcane_score")
	private Double sugarcaneScore;
	
	@Column(name = "sugarcane_qty")
	private Double sugarcaneQty;
	
	@Column(name = "planting_period")
	private String plantingPeriod;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "reference_code")
	private String referenceCode;
	
	@Column(name = "is_active")
	private Boolean isActive;
	
	@Column(name = "processed_by")
	private String processedBy;
	
	@Column(name = "processed_date")
	private Timestamp processedDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(Integer farmerId) {
		this.farmerId = farmerId;
	}

	public String getFarmCode() {
		return farmCode;
	}

	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}

	public String getPaddyFieldCode() {
		return paddyFieldCode;
	}

	public void setPaddyFieldCode(String paddyFieldCode) {
		this.paddyFieldCode = paddyFieldCode;
	}

	public Integer getLineNum() {
		return lineNum;
	}

	public void setLineNum(Integer lineNum) {
		this.lineNum = lineNum;
	}

	public Double getFieldArea() {
		return fieldArea;
	}

	public void setFieldArea(Double fieldArea) {
		this.fieldArea = fieldArea;
	}

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public Double getWeighEstimation() {
		return weighEstimation;
	}

	public void setWeighEstimation(Double weighEstimation) {
		this.weighEstimation = weighEstimation;
	}

	public String getSugarcaneType() {
		return sugarcaneType;
	}

	public void setSugarcaneType(String sugarcaneType) {
		this.sugarcaneType = sugarcaneType;
	}

	public String getAgeOfSugarcane() {
		return ageOfSugarcane;
	}

	public void setAgeOfSugarcane(String ageOfSugarcane) {
		this.ageOfSugarcane = ageOfSugarcane;
	}

	public Date getPlantingDate() {
		return plantingDate;
	}

	public void setPlantingDate(Date plantingDate) {
		this.plantingDate = plantingDate;
	}

	public String getSugarcaneSpecies() {
		return sugarcaneSpecies;
	}

	public void setSugarcaneSpecies(String sugarcaneSpecies) {
		this.sugarcaneSpecies = sugarcaneSpecies;
	}

	public Double getSugarcaneScore() {
		return sugarcaneScore;
	}

	public void setSugarcaneScore(Double sugarcaneScore) {
		this.sugarcaneScore = sugarcaneScore;
	}

	public Double getSugarcaneQty() {
		return sugarcaneQty;
	}

	public void setSugarcaneQty(Double sugarcaneQty) {
		this.sugarcaneQty = sugarcaneQty;
	}

	public String getPlantingPeriod() {
		return plantingPeriod;
	}

	public void setPlantingPeriod(String plantingPeriod) {
		this.plantingPeriod = plantingPeriod;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getProcessedBy() {
		return processedBy;
	}

	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}

	public Timestamp getProcessedDate() {
		return processedDate;
	}

	public void setProcessedDate(Timestamp processedDate) {
		this.processedDate = processedDate;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobPaddyField other = (MobPaddyField) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
}
