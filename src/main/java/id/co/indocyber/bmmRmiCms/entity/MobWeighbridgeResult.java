package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.SimpleDateFormat;

import javax.persistence.*;

@Entity
@Table(name = "mob_weighbridge_result")
@NamedQuery(name = "MobWeighbridgeResult.findAll", query = "SELECT mwr FROM MobWeighbridgeResult mwr")
public class MobWeighbridgeResult implements Serializable {
	private static final long serialVersionUID = -5778365524484767090L;
	
	@Transient
	private SimpleDateFormat sdf = new SimpleDateFormat("yyyy-MM-dd");
	
	@Transient
	private Timestamp tsIn;
	
	@Transient
	private Timestamp tsOut;
	
	@Id
	@Column(name = "id")
	@GeneratedValue(strategy=GenerationType.IDENTITY)
	private Integer id;

	@Column(name = "document_num")
	private Integer documentNum;

	@Column(name = "document_type")
	private String documentType;

	@Column(name = "mitra_code")
	private String mitraCode;

	@Column(name = "mitra_name")
	private String mitraName;

	@Column(name = "vehicle_no")
	private String vehicleNo;

	@Column(name = "vehicle_type")
	private String vehicleType;

	@Column(name = "driver_id")
	private String driverId;
	
	@Column(name = "driver_name")
	private String driverName;

	@Column(name = "farmer_code")
	private String farmerCode;

	@Column(name = "farmer_name")
	private String farmerName;
	
	@Column(name = "contract_no_1")
	private String contractNo1;
	
	@Column(name = "contract_no_2")
	private String contractNo2;
	
	@Column(name = "time_in")
	private String timeIn;

	@Column(name = "date_in")
	private Date dateIn;

	@Column(name = "time_out")
	private String timeOut;

	@Column(name = "date_out")
	private Date dateOut;
	
	@Column(name = "status")
	private String status;
	
	@Column(name = "monitoringPos")
	private String monitoringPos;

	@Column(name = "weigh_bruto")
	private Double weighBruto;
	
	@Column(name = "weigh_netto")
	private Double weighNetto;
	
	@Column(name = "weigh_vehicle")
	private Double weighVehicle;
	
	@Column(name = "qty_weigho")
	private Double qtyWeigho;
	
	@Column(name = "rafaksi")
	private Double rafaksi;
	
	@Column(name = "total_paid")
	private Double totalPaid;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDocumentNum() {
		return documentNum;
	}

	public void setDocumentNum(Integer documentNum) {
		this.documentNum = documentNum;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getMitraCode() {
		return mitraCode;
	}

	public void setMitraCode(String mitraCode) {
		this.mitraCode = mitraCode;
	}

	public String getMitraName() {
		return mitraName;
	}

	public void setMitraName(String mitraName) {
		this.mitraName = mitraName;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getFarmerCode() {
		return farmerCode;
	}

	public void setFarmerCode(String farmerCode) {
		this.farmerCode = farmerCode;
	}

	public String getFarmerName() {
		return farmerName;
	}

	public void setFarmerName(String farmerName) {
		this.farmerName = farmerName;
	}

	public String getContractNo1() {
		return contractNo1;
	}

	public void setContractNo1(String contractNo1) {
		this.contractNo1 = contractNo1;
	}

	public String getContractNo2() {
		return contractNo2;
	}

	public void setContractNo2(String contractNo2) {
		this.contractNo2 = contractNo2;
	}

	public String getTimeIn() {
		return timeIn;
	}

	public void setTimeIn(String timeIn) {
		this.timeIn = timeIn;
	}

	public Date getDateIn() {
		return dateIn;
	}

	public void setDateIn(Date dateIn) {
		this.dateIn = dateIn;
	}

	public String getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(String timeOut) {
		this.timeOut = timeOut;
	}

	public Date getDateOut() {
		return dateOut;
	}

	public void setDateOut(Date dateOut) {
		this.dateOut = dateOut;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMonitoringPos() {
		return monitoringPos;
	}

	public void setMonitoringPos(String monitoringPos) {
		this.monitoringPos = monitoringPos;
	}

	public Double getWeighBruto() {
		return weighBruto;
	}

	public void setWeighBruto(Double weighBruto) {
		this.weighBruto = weighBruto;
	}

	public Double getWeighNetto() {
		return weighNetto;
	}

	public void setWeighNetto(Double weighNetto) {
		this.weighNetto = weighNetto;
	}

	public Double getWeighVehicle() {
		return weighVehicle;
	}

	public void setWeighVehicle(Double weighVehicle) {
		this.weighVehicle = weighVehicle;
	}

	public Double getQtyWeigho() {
		return qtyWeigho;
	}

	public void setQtyWeigho(Double qtyWeigho) {
		this.qtyWeigho = qtyWeigho;
	}

	public Double getRafaksi() {
		return rafaksi;
	}

	public void setRafaksi(Double rafaksi) {
		this.rafaksi = rafaksi;
	}

	public Double getTotalPaid() {
		return totalPaid;
	}

	public void setTotalPaid(Double totalPaid) {
		this.totalPaid = totalPaid;
	}

	public String getDriverId() {
		return driverId;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}

	public Timestamp getTsIn() {
		return tsIn;
	}

	public void setTsIn(Timestamp tsIn) {
		this.tsIn = tsIn;
	}

	public Timestamp getTsOut() {
		return tsOut;
	}

	public void setTsOut(Timestamp tsOut) {
		this.tsOut = tsOut;
	}
	
	@PostLoad
	public void setTime(){
		try{
			String tsIn = sdf.format(dateIn).concat(" ").concat(timeIn).concat(":00");
			this.tsIn = Timestamp.valueOf(tsIn);
		}
		catch(NullPointerException e){
			this.tsIn = null;
		}
		try{
			String tsOut = sdf.format(dateOut).concat(" ").concat(timeOut).concat(":00");
			this.tsOut = Timestamp.valueOf(tsOut);
		}
		catch(NullPointerException e){
			this.tsOut = null;
		}
	}
}
