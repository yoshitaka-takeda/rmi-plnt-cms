package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;

public class MobScheduleVisitScoringPK implements Serializable {
	private static final long serialVersionUID = -5778365524484767090L;

	private Integer scheduleVisitDetailId;
	private String paramCode;

	public Integer getScheduleVisitDetailId() {
		return scheduleVisitDetailId;
	}

	public void setScheduleVisitDetailId(Integer scheduleVisitDetailId) {
		this.scheduleVisitDetailId = scheduleVisitDetailId;
	}

	public String getParamCode() {
		return paramCode;
	}

	public void setParamCode(String paramCode) {
		this.paramCode = paramCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((paramCode == null) ? 0 : paramCode.hashCode());
		result = prime
				* result
				+ ((scheduleVisitDetailId == null) ? 0 : scheduleVisitDetailId
						.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobScheduleVisitScoringPK other = (MobScheduleVisitScoringPK) obj;
		if (paramCode == null) {
			if (other.paramCode != null)
				return false;
		} else if (!paramCode.equals(other.paramCode))
			return false;
		if (scheduleVisitDetailId == null) {
			if (other.scheduleVisitDetailId != null)
				return false;
		} else if (!scheduleVisitDetailId.equals(other.scheduleVisitDetailId))
			return false;
		return true;
	}
}
