package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name = "mob_spta")
@NamedQuery(name = "MobSpta.findAll", query = "SELECT spta FROM MobSpta spta")
public class MobSpta implements Serializable {
	private static final long serialVersionUID = -5778365524484767090L;

	@Id
	@Column(name = "spta_num")
	private Integer sptaNum;

	@Column(name = "farmer_code")
	private String farmerCode;
	
	@Column(name = "qty")
	private Double qty;

	@Column(name = "status")
	private String status;

	@Column(name = "vehicle_no")
	private String vehicleNo;
	
	@Column(name = "driver_id")
	private Integer driverId;
	
	@Column(name = "vehicle_type")
	private Integer vehicleType;
	
	@Column(name = "document_date")
	private Date documentDate;
	
	@Column(name = "expired_date")
	private Date expiredDate;
	
	@Column(name = "rendemen")
	private Double rendemen;
	
	@Column(name = "brix")
	private Double brix;
	
	@Column(name = "pol")
	private Double pol;
	
	@Column(name = "source")
	private String source;
	
	@Column(name = "base_entry")
	private Integer baseEntry;
	
	@Column(name = "base_line")
	private Integer baseLine;

	@Column(name = "reference_code")
	private String referenceCode;
	
	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Timestamp createdDate;
	
	@Column(name = "modified_by")
	private String modifiedBy;

	@Column(name = "modified_date")
	private Timestamp modifiedDate;
	
	//- NEW
	@Column(name = "contract_no1")
	private String contractNo1;
	
	@Column(name = "contract_no2")
	private String contractNo2;

	@Column(name = "farmer_type")
	private String farmerType;
	
	@Column(name = "farm_code")
	private String farmCode;
	
	@Column(name = "coop_name")
	private String coopName;
	
	@Column(name = "monitoring_pos")
	private String monitoringPos;
	
	@Column(name = "loading_sugarcane_latitude")
	private Double loadingSugarcaneLatitude;
	
	@Column(name = "loading_sugarcane_longitude")
	private Double loadingSugarcaneLongitude;
	
	@Column(name = "loading_sugarcane_date")
	private Timestamp loadingSugarcaneDate;
	
	@Column(name = "loading_sugarcane_by")
	private String loadingSugarcaneBy;
	
	@Column(name = "destination_latitude")
	private Double destinationLatitude;
	
	@Column(name = "destination_longitude")
	private Double destinationLongitude;
	
	@Column(name = "destination_arrived_date")
	private Timestamp destinationDate;
	
	@Column(name = "destination_arrived_by")
	private String destinationBy;
	
	@Column(name = "exit_latitude")
	private Double exitLatitude;
	
	@Column(name = "exit_longitude")
	private Double exitLongitude;
	
	@Column(name = "exit_date")
	private Timestamp exitDate;
	
	@Column(name = "exit_by")
	private String exitBy;
	
	@Column(name = "reason_for_cancel")
	private String reasonForCancel;
	
	public Integer getSptaNum() {
		return sptaNum;
	}

	public void setSptaNum(Integer sptaNum) {
		this.sptaNum = sptaNum;
	}

	public String getFarmerCode() {
		return farmerCode;
	}

	public void setFarmerCode(String farmerCode) {
		this.farmerCode = farmerCode;
	}

	public Double getQty() {
		return qty;
	}

	public void setQty(Double qty) {
		this.qty = qty;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public Integer getDriverId() {
		return driverId;
	}

	public void setDriverId(Integer driverId) {
		this.driverId = driverId;
	}

	public Integer getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(Integer vehicleType) {
		this.vehicleType = vehicleType;
	}

	public Date getDocumentDate() {
		return documentDate;
	}

	public void setDocumentDate(Date documentDate) {
		this.documentDate = documentDate;
	}

	public Date getExpiredDate() {
		return expiredDate;
	}

	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}

	public Double getRendemen() {
		return rendemen;
	}

	public void setRendemen(Double rendemen) {
		this.rendemen = rendemen;
	}

	public Double getBrix() {
		return brix;
	}

	public void setBrix(Double brix) {
		this.brix = brix;
	}

	public Double getPol() {
		return pol;
	}

	public void setPol(Double pol) {
		this.pol = pol;
	}

	public String getSource() {
		return source;
	}

	public void setSource(String source) {
		this.source = source;
	}

	public Integer getBaseEntry() {
		return baseEntry;
	}

	public void setBaseEntry(Integer baseEntry) {
		this.baseEntry = baseEntry;
	}

	public Integer getBaseLine() {
		return baseLine;
	}

	public void setBaseLine(Integer baseLine) {
		this.baseLine = baseLine;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getContractNo1() {
		return contractNo1;
	}

	public void setContractNo1(String contractNo1) {
		this.contractNo1 = contractNo1;
	}

	public String getContractNo2() {
		return contractNo2;
	}

	public void setContractNo2(String contractNo2) {
		this.contractNo2 = contractNo2;
	}

	public String getFarmerType() {
		return farmerType;
	}

	public void setFarmerType(String farmerType) {
		this.farmerType = farmerType;
	}

	public String getFarmCode() {
		return farmCode;
	}

	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}

	public String getCoopName() {
		return coopName;
	}

	public void setCoopName(String coopName) {
		this.coopName = coopName;
	}

	public String getMonitoringPos() {
		return monitoringPos;
	}

	public void setMonitoringPos(String monitoringPos) {
		this.monitoringPos = monitoringPos;
	}

	public Double getLoadingSugarcaneLatitude() {
		return loadingSugarcaneLatitude;
	}

	public void setLoadingSugarcaneLatitude(Double loadingSugarcaneLatitude) {
		this.loadingSugarcaneLatitude = loadingSugarcaneLatitude;
	}

	public Double getLoadingSugarcaneLongitude() {
		return loadingSugarcaneLongitude;
	}

	public void setLoadingSugarcaneLongitude(Double loadingSugarcaneLongitude) {
		this.loadingSugarcaneLongitude = loadingSugarcaneLongitude;
	}

	public Timestamp getLoadingSugarcaneDate() {
		return loadingSugarcaneDate;
	}

	public void setLoadingSugarcaneDate(Timestamp loadingSugarcaneDate) {
		this.loadingSugarcaneDate = loadingSugarcaneDate;
	}

	public String getLoadingSugarcaneBy() {
		return loadingSugarcaneBy;
	}

	public void setLoadingSugarcaneBy(String loadingSugarcaneBy) {
		this.loadingSugarcaneBy = loadingSugarcaneBy;
	}

	public Double getDestinationLatitude() {
		return destinationLatitude;
	}

	public void setDestinationLatitude(Double destinationLatitude) {
		this.destinationLatitude = destinationLatitude;
	}

	public Double getDestinationLongitude() {
		return destinationLongitude;
	}

	public void setDestinationLongitude(Double destinationLongitude) {
		this.destinationLongitude = destinationLongitude;
	}

	public Timestamp getDestinationDate() {
		return destinationDate;
	}

	public void setDestinationDate(Timestamp destinationDate) {
		this.destinationDate = destinationDate;
	}

	public String getDestinationBy() {
		return destinationBy;
	}

	public void setDestinationBy(String destinationBy) {
		this.destinationBy = destinationBy;
	}

	public Double getExitLatitude() {
		return exitLatitude;
	}

	public void setExitLatitude(Double exitLatitude) {
		this.exitLatitude = exitLatitude;
	}

	public Double getExitLongitude() {
		return exitLongitude;
	}

	public void setExitLongitude(Double exitLongitude) {
		this.exitLongitude = exitLongitude;
	}

	public Timestamp getExitDate() {
		return exitDate;
	}

	public void setExitDate(Timestamp exitDate) {
		this.exitDate = exitDate;
	}

	public String getExitBy() {
		return exitBy;
	}

	public void setExitBy(String exitBy) {
		this.exitBy = exitBy;
	}

	public String getReasonForCancel() {
		return reasonForCancel;
	}

	public void setReasonForCancel(String reasonForCancel) {
		this.reasonForCancel = reasonForCancel;
	}
}
