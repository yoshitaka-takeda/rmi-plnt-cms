package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name = "mob_mapping_farmer_employee")
@NamedQuery(name = "MobMappingFarmerEmployee.findAll", query = "SELECT map FROM MobMappingFarmerEmployee map")
@IdClass(MobMappingFarmerEmployeePK.class)
public class MobMappingFarmerEmployee implements Serializable {
	private static final long serialVersionUID = -5778365524484767090L;

	@Id
	@Column(name = "farmer_id")
	private Integer farmerId;
	
	@Id
	@Column(name = "employee_id")
	private Integer employeeId;
	
	@Column(name = "is_active")
	private Boolean isActive;
	
	@Column(name = "created")
	private String createdBy;
	
	@Column(name = "created_date")
	private Timestamp createdDate;

	public Integer getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(Integer farmerId) {
		this.farmerId = farmerId;
	}

	public Integer getEmployeeId() {
		return employeeId;
	}

	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
}
