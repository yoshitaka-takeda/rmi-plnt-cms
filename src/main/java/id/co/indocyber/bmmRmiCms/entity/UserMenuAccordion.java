package id.co.indocyber.bmmRmiCms.entity;

public class UserMenuAccordion {
	private String title;
	private String url;
	private String code;

	public UserMenuAccordion() {
	}

	public UserMenuAccordion(String title) {
		this.title = title;
	}
	
	public UserMenuAccordion(String title, String url){
		this.title = title;
		this.url = url;
	}
	
	public UserMenuAccordion(String title, String url, String code){
		this.title = title;
		this.url = url;
		this.code = code;
	}

	public String getTitle() {
		return title;
	}

	public void setTitle(String title) {
		this.title = title;
	}

	public String getUrl() {
		return url;
	}

	public void setUrl(String url) {
		this.url = url;
	}

	public String getCode() {
		return code;
	}

	public void setCode(String code) {
		this.code = code;
	}
}
