package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name = "mob_stand")
@NamedQuery(name = "MobStand.findAll", query = "SELECT ms FROM MobStand ms")
public class MobStand implements Serializable {
	private static final long serialVersionUID = -5778365524484767090L;

	@Id
	@Column(name = "visit_no")
	private String visitNo;
	
	@Column(name = "date_entry")
	private Timestamp dateEntry;
	
	@Column(name = "farm_area")
	private Double farmArea;
	
	@Column(name = "tonnage_estimation")
	private Double tonnageEstimation;
	
	@Column(name = "last_stand_tonnage")
	private Double lastStandTonnage;
	
	@Column(name = "last_stand_area")
	private Double lastStandArea;
	
	@Column(name = "stand_tonnage")
	private Double StandTonnage;
	
	@Column(name = "stand_area")
	private Double StandArea;
	
	@Column(name = "activity_text")
	private String activityText;
	
	@Column(name = "is_active")
	private Boolean isActive;
	
	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Timestamp createdDate;

	public String getVisitNo() {
		return visitNo;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

	public Timestamp getDateEntry() {
		return dateEntry;
	}

	public void setDateEntry(Timestamp dateEntry) {
		this.dateEntry = dateEntry;
	}

	public Double getFarmArea() {
		return farmArea;
	}

	public void setFarmArea(Double farmArea) {
		this.farmArea = farmArea;
	}

	public Double getTonnageEstimation() {
		return tonnageEstimation;
	}

	public void setTonnageEstimation(Double tonnageEstimation) {
		this.tonnageEstimation = tonnageEstimation;
	}

	public Double getLastStandTonnage() {
		return lastStandTonnage;
	}

	public void setLastStandTonnage(Double lastStandTonnage) {
		this.lastStandTonnage = lastStandTonnage;
	}

	public Double getLastStandArea() {
		return lastStandArea;
	}

	public void setLastStandArea(Double lastStandArea) {
		this.lastStandArea = lastStandArea;
	}

	public Double getStandTonnage() {
		return StandTonnage;
	}

	public void setStandTonnage(Double standTonnage) {
		StandTonnage = standTonnage;
	}

	public Double getStandArea() {
		return StandArea;
	}

	public void setStandArea(Double standArea) {
		StandArea = standArea;
	}

	public String getActivityText() {
		return activityText;
	}

	public void setActivityText(String activityText) {
		this.activityText = activityText;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
}
