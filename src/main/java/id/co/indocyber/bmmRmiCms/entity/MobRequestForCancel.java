package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name = "mob_request_for_cancel")
@NamedQuery(name = "MobRequestForCancel.findAll", query = "SELECT rfc FROM MobRequestForCancel rfc")
public class MobRequestForCancel implements Serializable {
	private static final long serialVersionUID = -5778365524484767090L;

	@Id
	@Column(name = "id")
	private Integer id;

	@Column(name = "spta_num")
	private Integer sptaNum;

	@Column(name = "location_for_cancel")
	private String locationForCancel;
	
	@Column(name = "reason_for_cancel")
	private String reasonForCancel;

	@Column(name = "task_activity")
	private String taskActivity;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Timestamp createdDate;

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getSptaNum() {
		return sptaNum;
	}

	public void setSptaNum(Integer sptaNum) {
		this.sptaNum = sptaNum;
	}

	public String getReasonForCancel() {
		return reasonForCancel;
	}

	public void setReasonForCancel(String reasonForCancel) {
		this.reasonForCancel = reasonForCancel;
	}

	public String getTaskActivity() {
		return taskActivity;
	}

	public void setTaskActivity(String taskActivity) {
		this.taskActivity = taskActivity;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getLocationForCancel() {
		return locationForCancel;
	}

	public void setLocationForCancel(String locationForCancel) {
		this.locationForCancel = locationForCancel;
	}
}
