package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;

public class MobMappingFarmerEmployeePK implements Serializable {
	private static final long serialVersionUID = -5778365524484767090L;

	private Integer farmerId;
	private Integer employeeId;
	
	public Integer getFarmerId() {
		return farmerId;
	}
	public void setFarmerId(Integer farmerId) {
		this.farmerId = farmerId;
	}
	public Integer getEmployeeId() {
		return employeeId;
	}
	public void setEmployeeId(Integer employeeId) {
		this.employeeId = employeeId;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((employeeId == null) ? 0 : employeeId.hashCode());
		result = prime * result
				+ ((farmerId == null) ? 0 : farmerId.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobMappingFarmerEmployeePK other = (MobMappingFarmerEmployeePK) obj;
		if (employeeId == null) {
			if (other.employeeId != null)
				return false;
		} else if (!employeeId.equals(other.employeeId))
			return false;
		if (farmerId == null) {
			if (other.farmerId != null)
				return false;
		} else if (!farmerId.equals(other.farmerId))
			return false;
		return true;
	}
}
