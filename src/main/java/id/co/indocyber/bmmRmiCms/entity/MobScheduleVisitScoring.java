package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name = "mob_schedule_visit_scoring")
@NamedQuery(name = "MobScheduleVisitScoring.findAll", 
	query = "SELECT visitScore FROM MobScheduleVisitScoring visitScore")
@IdClass(MobScheduleVisitScoringPK.class)
public class MobScheduleVisitScoring implements Serializable {
	private static final long serialVersionUID = -5778365524484767090L;

	@Id
	@Column(name = "schedule_visit_detail_id")
	private Integer scheduleVisitDetailId;

	@Id
	@Column(name = "param_code")
	private String paramCode;

	@Column(name = "param_value")
	private String paramValue;
	
	@Column(name = "criteria")
	private String criteria;
	
	@Column(name = "weight")
	private Double weight;

	@Column(name = "score")
	private Double score;
	
	@Column(name = "notes")
	private String notes;
	
	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Timestamp createdDate;

	public Integer getScheduleVisitDetailId() {
		return scheduleVisitDetailId;
	}

	public void setScheduleVisitDetailId(Integer scheduleVisitDetailId) {
		this.scheduleVisitDetailId = scheduleVisitDetailId;
	}

	public String getParamCode() {
		return paramCode;
	}

	public void setParamCode(String paramCode) {
		this.paramCode = paramCode;
	}

	public String getParamValue() {
		return paramValue;
	}

	public void setParamValue(String paramValue) {
		this.paramValue = paramValue;
	}

	public String getCriteria() {
		return criteria;
	}

	public void setCriteria(String criteria) {
		this.criteria = criteria;
	}

	public Double getWeight() {
		return weight;
	}

	public void setWeight(Double weight) {
		this.weight = weight;
	}

	public Double getScore() {
		return score;
	}

	public void setScore(Double score) {
		this.score = score;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
}
