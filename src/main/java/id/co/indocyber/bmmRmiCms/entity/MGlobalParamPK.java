package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;

public class MGlobalParamPK implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8361545543716243372L;
	private String valueKey;
	private String longText;
	public String getValueKey() {
		return valueKey;
	}
	public void setValueKey(String valueKey) {
		this.valueKey = valueKey;
	}
	public String getLongText() {
		return longText;
	}
	public void setLongText(String longText) {
		this.longText = longText;
	}
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((longText == null) ? 0 : longText.hashCode());
		result = prime * result
				+ ((valueKey == null) ? 0 : valueKey.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MGlobalParamPK other = (MGlobalParamPK) obj;
		if (longText == null) {
			if (other.longText != null)
				return false;
		} else if (!longText.equals(other.longText))
			return false;
		if (valueKey == null) {
			if (other.valueKey != null)
				return false;
		} else if (!valueKey.equals(other.valueKey))
			return false;
		return true;
	}
	
	public MGlobalParamPK() {
		
	}
	
	public MGlobalParamPK(String longText, String valueKey) {
		this.longText = longText;
		this.valueKey = valueKey;
	}
}
