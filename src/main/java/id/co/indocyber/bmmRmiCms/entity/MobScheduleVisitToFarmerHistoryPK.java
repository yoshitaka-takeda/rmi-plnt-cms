package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;

public class MobScheduleVisitToFarmerHistoryPK implements Serializable {
	private static final long serialVersionUID = -7903594474522672561L;

	private String visitNo;
	private Integer farmerId;
	private Integer assignTo;

	public String getVisitNo() {
		return visitNo;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

	public Integer getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(Integer farmerId) {
		this.farmerId = farmerId;
	}

	public Integer getAssignTo() {
		return assignTo;
	}

	public void setAssignTo(Integer assignTo) {
		this.assignTo = assignTo;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((assignTo == null) ? 0 : assignTo.hashCode());
		result = prime * result
				+ ((farmerId == null) ? 0 : farmerId.hashCode());
		result = prime * result + ((visitNo == null) ? 0 : visitNo.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobScheduleVisitToFarmerHistoryPK other = (MobScheduleVisitToFarmerHistoryPK) obj;
		if (assignTo == null) {
			if (other.assignTo != null)
				return false;
		} else if (!assignTo.equals(other.assignTo))
			return false;
		if (farmerId == null) {
			if (other.farmerId != null)
				return false;
		} else if (!farmerId.equals(other.farmerId))
			return false;
		if (visitNo == null) {
			if (other.visitNo != null)
				return false;
		} else if (!visitNo.equals(other.visitNo))
			return false;
		return true;
	}
}
