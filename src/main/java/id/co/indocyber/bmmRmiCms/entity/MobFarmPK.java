package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;

public class MobFarmPK implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8361545543716243372L;
	private Integer farmerId;
	private String farmCode;

	public MobFarmPK() {
	}

	public MobFarmPK(Integer farmerId, String farmCode) {
		this.farmerId = farmerId;
		this.farmCode = farmCode;
	}



	public Integer getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(Integer farmerId) {
		this.farmerId = farmerId;
	}

	public String getFarmCode() {
		return farmCode;
	}

	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}

	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result
				+ ((farmCode == null) ? 0 : farmCode.hashCode());
		result = prime * result
				+ ((farmerId == null) ? 0 : farmerId.hashCode());
		return result;
	}

	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobFarmPK other = (MobFarmPK) obj;
		if (farmCode == null) {
			if (other.farmCode != null)
				return false;
		} else if (!farmCode.equals(other.farmCode))
			return false;
		if (farmerId == null) {
			if (other.farmerId != null)
				return false;
		} else if (!farmerId.equals(other.farmerId))
			return false;
		return true;
	}
}
