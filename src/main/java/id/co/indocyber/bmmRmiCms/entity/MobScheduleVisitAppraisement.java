package id.co.indocyber.bmmRmiCms.entity;

import java.io.Serializable;
import java.sql.Timestamp;

import javax.persistence.*;

@Entity
@Table(name = "mob_schedule_visit_appraisement")
@NamedQuery(name = "MobScheduleVisitAppraisement.findAll", 
	query = "SELECT visitApp FROM MobScheduleVisitAppraisement visitApp")
@IdClass(MobScheduleVisitAppraisementPK.class)
public class MobScheduleVisitAppraisement implements Serializable {
	private static final long serialVersionUID = -5778365524484767090L;

	@Id
	@Column(name = "schedule_visit_detail_id")
	private Integer scheduleVisitDetailId;

	@Id
	@Column(name = "period_name")
	private String periodName;

	@Column(name = "current_sugarcane_height")
	private Double currentSugarcaneHeight;
	
	@Column(name = "cutdown_sugarcane_height")
	private Double cutdownSugarcaneHeight;
	
	@Column(name = "sugarcane_stem_weight")
	private Double sugarcaneStemWeight;
	
	@Column(name = "weight_per_sugarcane")
	private Double weightPerSugarcane;
	
	@Column(name = "number_of_stem_per_leng")
	private Integer numberOfStemPerLeng;
	
	@Column(name = "leng_factor_per_HA")
	private Integer lengFactorPerHa;
	
	@Column(name = "number_of_sugarcane_per_HA")
	private Double numberOfSugarcanePerHa;
	
	@Column(name = "prod_per_hectar_KU")
	private Double prodPerHectarKu;
	
	@Column(name = "amount_of_production")
	private Double amountOfProduction;
	
	@Column(name = "cutting_schedule")
	private String cuttingSchedule;
	
	@Column(name = "avg_brix_number")
	private Double avgBrixNumber;
	
	@Column(name = "rendemen_estimation")
	private Double rendemenEstimation;
	
	@Column(name = "ku_hablur_per_ha")
	private Double kuHablurPerHa;
	
	@Column(name = "amount_of_hablur")
	private Double amountOfHablur;
	
	@Column(name = "number_of_sugarcane_KU")
	private Double numberOfSugarcaneKu;
	
	@Column(name = "appraisement")
	private Double appraisement;
	
	@Column(name = "is_active")
	private Boolean isActive;

	@Column(name = "created_by")
	private String createdBy;

	@Column(name = "created_date")
	private Timestamp createdDate;

	public Integer getScheduleVisitDetailId() {
		return scheduleVisitDetailId;
	}

	public void setScheduleVisitDetailId(Integer scheduleVisitDetailId) {
		this.scheduleVisitDetailId = scheduleVisitDetailId;
	}

	public String getPeriodName() {
		return periodName;
	}

	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}

	public Double getCurrentSugarcaneHeight() {
		return currentSugarcaneHeight;
	}

	public void setCurrentSugarcaneHeight(Double currentSugarcaneHeight) {
		this.currentSugarcaneHeight = currentSugarcaneHeight;
	}

	public Double getCutdownSugarcaneHeight() {
		return cutdownSugarcaneHeight;
	}

	public void setCutdownSugarcaneHeight(Double cutdownSugarcaneHeight) {
		this.cutdownSugarcaneHeight = cutdownSugarcaneHeight;
	}

	public Double getSugarcaneStemWeight() {
		return sugarcaneStemWeight;
	}

	public void setSugarcaneStemWeight(Double sugarcaneStemWeight) {
		this.sugarcaneStemWeight = sugarcaneStemWeight;
	}

	public Integer getNumberOfStemPerLeng() {
		return numberOfStemPerLeng;
	}

	public void setNumberOfStemPerLeng(Integer numberOfStemPerLeng) {
		this.numberOfStemPerLeng = numberOfStemPerLeng;
	}

	public Integer getLengFactorPerHa() {
		return lengFactorPerHa;
	}

	public void setLengFactorPerHa(Integer lengFactorPerHa) {
		this.lengFactorPerHa = lengFactorPerHa;
	}

	public Double getNumberOfSugarcanePerHa() {
		return numberOfSugarcanePerHa;
	}

	public void setNumberOfSugarcanePerHa(Double numberOfSugarcanePerHa) {
		this.numberOfSugarcanePerHa = numberOfSugarcanePerHa;
	}

	public Double getNumberOfSugarcaneKu() {
		return numberOfSugarcaneKu;
	}

	public void setNumberOfSugarcaneKu(Double numberOfSugarcaneKu) {
		this.numberOfSugarcaneKu = numberOfSugarcaneKu;
	}

	public Double getAppraisement() {
		return appraisement;
	}

	public void setAppraisement(Double appraisement) {
		this.appraisement = appraisement;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public Double getWeightPerSugarcane() {
		return weightPerSugarcane;
	}

	public void setWeightPerSugarcane(Double weightPerSugarcane) {
		this.weightPerSugarcane = weightPerSugarcane;
	}

	public Double getProdPerHectarKu() {
		return prodPerHectarKu;
	}

	public void setProdPerHectarKu(Double prodPerHectarKu) {
		this.prodPerHectarKu = prodPerHectarKu;
	}

	public Double getAmountOfProduction() {
		return amountOfProduction;
	}

	public void setAmountOfProduction(Double amountOfProduction) {
		this.amountOfProduction = amountOfProduction;
	}

	public String getCuttingSchedule() {
		return cuttingSchedule;
	}

	public void setCuttingSchedule(String cuttingSchedule) {
		this.cuttingSchedule = cuttingSchedule;
	}

	public Double getAvgBrixNumber() {
		return avgBrixNumber;
	}

	public void setAvgBrixNumber(Double avgBrixNumber) {
		this.avgBrixNumber = avgBrixNumber;
	}

	public Double getRendemenEstimation() {
		return rendemenEstimation;
	}

	public void setRendemenEstimation(Double rendemenEstimation) {
		this.rendemenEstimation = rendemenEstimation;
	}

	public Double getKuHablurPerHa() {
		return kuHablurPerHa;
	}

	public void setKuHablurPerHa(Double kuHablurPerHa) {
		this.kuHablurPerHa = kuHablurPerHa;
	}

	public Double getAmountOfHablur() {
		return amountOfHablur;
	}

	public void setAmountOfHablur(Double amountOfHablur) {
		this.amountOfHablur = amountOfHablur;
	}
}
