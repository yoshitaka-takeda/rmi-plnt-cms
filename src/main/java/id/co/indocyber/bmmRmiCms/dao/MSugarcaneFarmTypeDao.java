package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MSugarcaneFarmType;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MSugarcaneFarmTypeDao extends JpaRepository<MSugarcaneFarmType, String>{

}
