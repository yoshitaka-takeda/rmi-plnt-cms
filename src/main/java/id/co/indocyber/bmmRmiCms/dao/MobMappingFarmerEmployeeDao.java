package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobMappingFarmerEmployee;
import id.co.indocyber.bmmRmiCms.entity.MobMappingFarmerEmployeePK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobMappingFarmerEmployeeDao extends JpaRepository<MobMappingFarmerEmployee, MobMappingFarmerEmployeePK>{
	@Query("select map.farmerId from MobMappingFarmerEmployee map where map.employeeId = :empId "
			+ "and map.isActive is true")
	List<Integer> findAssignedFarmerIds(@Param("empId")Integer employeeId);
	
	@Query("select distinct map.farmerId from MobMappingFarmerEmployee map where map.employeeId in (:empIds) "
			+ "and map.isActive is true")
	List<Integer> findAssignedFarmerIdsMultiple(@Param("empIds")List<Integer> managedEmployees);

	@Query("select map from MobMappingFarmerEmployee map where map.employeeId = :empId")
	List<MobMappingFarmerEmployee> findExisting(@Param("empId")Integer employeeId);

	@Query("select map.employeeId from MobMappingFarmerEmployee map where map.farmerId = :farmerId")
	List<Integer> findMyEmployees(@Param("farmerId")Integer farmerId);

	// Alternative delete method, karena entah kenapa ngga nge-delete
	@Modifying
	@Query(value="delete from mob_mapping_farmer_employee where employee_id = :empId", nativeQuery=true)
	void deleteExisting(@Param("empId")Integer id);
}
