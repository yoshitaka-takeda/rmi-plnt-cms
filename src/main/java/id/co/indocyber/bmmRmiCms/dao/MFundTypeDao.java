package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MFundType;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MFundTypeDao extends JpaRepository<MFundType, String>{

}
