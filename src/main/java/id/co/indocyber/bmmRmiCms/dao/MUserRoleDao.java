package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MUserRole;
import id.co.indocyber.bmmRmiCms.entity.MUserRolePK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MUserRoleDao extends JpaRepository<MUserRole, MUserRolePK>{
	@Query("select ur from MUserRole ur where ur.email = :email and ur.isActive is true")
	public MUserRole findUserRole(@Param("email")String email);

	@Query("select ur from MUserRole ur where ur.email = :email and ur.moduleApps = 'Web'")
	public MUserRole getCmsAccess(@Param("email")String email);
}
