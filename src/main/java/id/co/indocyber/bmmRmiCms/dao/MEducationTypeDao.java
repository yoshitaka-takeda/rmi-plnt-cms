package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MEducationType;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MEducationTypeDao extends JpaRepository<MEducationType, String>{

}
