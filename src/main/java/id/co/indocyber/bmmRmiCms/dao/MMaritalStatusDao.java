package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MMaritalStatus;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MMaritalStatusDao extends JpaRepository<MMaritalStatus, String>{

}
