package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobFarm;
import id.co.indocyber.bmmRmiCms.entity.MobFarmPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobFarmDao extends JpaRepository<MobFarm, MobFarmPK>{
	@Query("select farm from MobFarm farm where farm.farmerId = :farmerId")
	public List<MobFarm> findFarmByFarmer(@Param("farmerId")Integer farmerId);

	@Query("select count(farm) from MobFarm farm where farm.farmerId = :farmerId")
	public Long farmCount(@Param("farmerId")Integer farmerId);
}
