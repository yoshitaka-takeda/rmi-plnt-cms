package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitScoring;
import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitScoringPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobScheduleVisitScoringDao extends
		JpaRepository<MobScheduleVisitScoring, MobScheduleVisitScoringPK> {
	@Query("select visitScore from MobScheduleVisitScoring visitScore where "
			+ "visitScore.scheduleVisitDetailId = :visitDetailId")
	public List<MobScheduleVisitScoring> getHistory(@Param("visitDetailId")Integer visitDetailId);
}
