package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MPlantingPeriod;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MPlantingPeriodDao extends JpaRepository<MPlantingPeriod, String>{

}
