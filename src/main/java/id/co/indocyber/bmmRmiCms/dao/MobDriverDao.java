package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobDriver;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobDriverDao extends JpaRepository<MobDriver, Integer>{
	@Query("select drv from MobDriver drv where drv.id like concat('%',:arg,'%') or "
			+ "drv.fullname like concat('%',:arg,'%') or "
			+ "drv.phoneNum like concat('%',:arg,'%') or drv.address like concat('%',:arg,'%') "
			+ "or date_format(drv.birthday,'%d-%M-%Y') like concat('%',:arg,'%')")
	public List<MobDriver> findByArg(@Param("arg")String arg, Pageable pageable);
	
	@Query("select count(drv) from MobDriver drv where drv.id like concat('%',:arg,'%') or "
			+ "drv.fullname like concat('%',:arg,'%') or "
			+ "drv.phoneNum like concat('%',:arg,'%') or drv.address like concat('%',:arg,'%') "
			+ "or date_format(drv.birthday,'%d-%M-%Y') like concat('%',:arg,'%')")
	public Long countByArg(@Param("arg")String arg);

	@Query("select drv from MobDriver drv where lower(drv.fullname) = lower(:name)")
	public List<MobDriver> findByName(@Param("name")String fullname);

	@Query("select drv from MobDriver drv where lower(drv.email) = lower(:email)")
	public List<MobDriver> findByEmail(@Param("email")String email);
	
	@Query("select drv from MobDriver drv where drv.id not in ("
			+ "select user.personId from MUser user, MUserRole role where "
			+ "user.email = role.email and role.roleId = 3 and user.isActive is true)")
	public List<MobDriver> findAllNoUser();
}
