package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MobStand;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MobStandDao extends JpaRepository<MobStand, String>{

}
