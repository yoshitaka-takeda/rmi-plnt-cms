package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MMenuRole;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MMenuRoleDao extends JpaRepository<MMenuRole, Integer>{
	@Query("select mr, menu "
			+ "from MMenuRole mr, MMenu menu where mr.menuCode = menu.menuCode and menu.isActive is true "
			+ "and mr.roleId = :roleId and menu.appType = :module "
			+ "and menu.menuCode not in ('SJ','SJ-1','SJ-2','SPT','SPT-1','SPT-2','MD-8','MD-9','MD-10')"
			+ "and mr.isView = 1 order by mr.menuCode")
	public List<Object[]> getAccessPrivs(@Param("roleId")Integer roleId, @Param("module")String module);
	
	@Query("select mr, menu "
			+ "from MMenuRole mr, MMenu menu where mr.menuCode = menu.menuCode and menu.isActive is true "
			+ "and mr.roleId = :roleId and menu.appType = :module "
			+ "order by mr.menuCode")
	public List<Object[]> getAccessPrivsForMaster(@Param("roleId")Integer roleId, @Param("module")String module);
}
