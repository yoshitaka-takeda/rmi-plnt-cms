package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MGender;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MGenderDao extends JpaRepository<MGender, String>{

}
