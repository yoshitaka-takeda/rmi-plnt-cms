package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MRole;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MRoleDao extends JpaRepository<MRole, Integer>{
	@Query("select role from MRole role where lower(role.roleName) = lower(:roleName)")
	List<MRole> findByName(@Param("roleName")String roleName);

	@Query("select role from MRole role where role.roleName like concat('%',:arg,'%') or "
			+ "role.description like concat('%',:arg,'%')")
	List<MRole> findByArg(@Param("arg")String arg, Pageable pageable);
	
	@Query("select count(role) from MRole role where role.roleName like concat('%',:arg,'%') or "
			+ "role.description like concat('%',:arg,'%')")
	Long countByArg(@Param("arg")String arg);
}
