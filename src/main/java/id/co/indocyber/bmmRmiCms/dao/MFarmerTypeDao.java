package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MFarmerType;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MFarmerTypeDao extends JpaRepository<MFarmerType, String>{

}
