package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MFarmFieldsType;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MFarmFieldsTypeDao extends JpaRepository<MFarmFieldsType, String>{

}
