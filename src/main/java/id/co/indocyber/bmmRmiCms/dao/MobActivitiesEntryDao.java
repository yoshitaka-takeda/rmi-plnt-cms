package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobActivitiesEntry;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobActivitiesEntryDao extends JpaRepository<MobActivitiesEntry, Integer>{
	// ngga pake global param dulu, karena belum ada...
	@Query("select mae, e from MobActivitiesEntry mae, MobEmployee e where "
			+ "mae.employeeId = e.id and mae.employeeId in (:ids) and("
			+ "e.fullname like concat('%',:arg,'%') or "
			+ "mae.activityInfo like concat('%',:arg,'%') or "
			+ "mae.location like concat('%',:arg,'%'))")
	public List<Object[]> findAllInMyTeam(@Param("ids")List<Integer> ids, @Param("arg")String arg, 
			Pageable pageable);
	
	@Query("select mae, e from MobActivitiesEntry mae, MobEmployee e where "
			+ "mae.employeeId = e.id and("
			+ "e.fullname like concat('%',:arg,'%') or "
			+ "mae.activityInfo like concat('%',:arg,'%') or "
			+ "mae.location like concat('%',:arg,'%'))")
	public List<Object[]> findAll(@Param("arg")String arg, 
			Pageable pageable);
	
	@Query("select count(mae) from MobActivitiesEntry mae, MobEmployee e where "
			+ "mae.employeeId = e.id and mae.employeeId in (:ids) and("
			+ "e.fullname like concat('%',:arg,'%') or "
			+ "mae.activityInfo like concat('%',:arg,'%') or "
			+ "mae.location like concat('%',:arg,'%'))")
	public Long countAllInMyTeam(@Param("ids")List<Integer> ids, @Param("arg")String arg);
	
	@Query("select count(mae) from MobActivitiesEntry mae, MobEmployee e where "
			+ "mae.employeeId = e.id and("
			+ "e.fullname like concat('%',:arg,'%') or "
			+ "mae.activityInfo like concat('%',:arg,'%') or "
			+ "mae.location like concat('%',:arg,'%'))")
	public Long countAll(@Param("arg")String arg);
}
