package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MVehicleType;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MVehicleTypeDao extends JpaRepository<MVehicleType, Integer>{

}
