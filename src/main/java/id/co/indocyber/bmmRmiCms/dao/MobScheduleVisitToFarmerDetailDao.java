package id.co.indocyber.bmmRmiCms.dao;

import java.sql.Date;
import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitToFarmerDetail;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobScheduleVisitToFarmerDetailDao extends
		JpaRepository<MobScheduleVisitToFarmerDetail, Integer> {
	
	@Query("select visitD from MobScheduleVisitToFarmerDetail visitD where visitD.visitNo = :visitNo "
			+ "and visitD.isActive is true")
	List<MobScheduleVisitToFarmerDetail> findActiveVisitDetail(
			@Param("visitNo") String visitNo);
	
	@Query("select visitD from MobScheduleVisitToFarmerDetail visitD where visitD.visitNo = :visitNo ")
	List<MobScheduleVisitToFarmerDetail> findVisitDetail(
			@Param("visitNo") String visitNo);

	@Query("select visitD from MobScheduleVisitToFarmerDetail visitD where "
			+ "visitD.visitNo = :visitNo and visitD.farmCode = :farmCode "
			+ "and visitD.paddyFieldCode = :fieldCode")
	MobScheduleVisitToFarmerDetail findExisting(
			@Param("visitNo") String visitNo,
			@Param("farmCode") String farmCode,
			@Param("fieldCode") String paddyFieldCode);

	@Query(value = "select visitH.visit_no 'visit no', farmer.id 'Farmer', "
			+ "emp.fullname 'Assigned Employee', farm.village, visitD.farm_code, visitD.paddy_field_code, visitD.id, "
			+ "scoring.*, visitH.task_activity 'status', visitH.notes 'visit notes', visitD.latitude, visitD.longitude,"
			+ "visitH.visit_date, visitH.visit_expired_date from "
			+ "mob_schedule_visit_to_farmer_header visitH inner join "
			+ "mob_schedule_visit_to_farmer_detail visitD on visitH.visit_no = visitD.visit_no left join "
			+ "mob_schedule_visit_scoring scoring on visitD.id = scoring.schedule_visit_detail_id inner join "
			+ "mob_farm farm on visitD.farm_code = farm.farm_code inner join "
			+ "mob_farmer farmer on farm.farmer_id = farmer.id inner join "
			+ "mob_employee emp on emp.id = visitH.assign_to "
			+ "where (visitH.visit_expired_date >= :startDate and visitH.visit_date <= :endDate) "
			+ "and visitH.task_activity in ('00','01') and visitH.assign_to in (:ids)"
			+ "order by visitH.visit_no, farmer.fullname, emp.fullname, farm.village, scoring.schedule_visit_detail_id", 
			nativeQuery = true)
	List<Object[]> findResults(@Param("startDate") Date startDate,
			@Param("endDate") Date endDate, @Param("ids")List<Integer> ids);

	@Query(value = "select visitH.visit_no 'visit no', farmer.id 'Farmer', "
			+ "emp.fullname 'Assigned Employee', farm.village, visitD.farm_code, visitD.paddy_field_code, visitD.id, "
			+ "scoring.*, visitH.task_activity 'status', visitH.notes 'visit notes', visitD.latitude, visitD.longitude,"
			+ "visitH.visit_date, visitH.visit_expired_date from "
			+ "mob_schedule_visit_to_farmer_header visitH inner join "
			+ "mob_schedule_visit_to_farmer_detail visitD on visitH.visit_no = visitD.visit_no left join "
			+ "mob_schedule_visit_scoring scoring on visitD.id = scoring.schedule_visit_detail_id inner join "
			+ "mob_farm farm on visitD.farm_code = farm.farm_code inner join "
			+ "mob_farmer farmer on farm.farmer_id = farmer.id inner join "
			+ "mob_employee emp on emp.id = visitH.assign_to "
			+ "where (visitH.visit_expired_date >= :startDate and visitH.visit_date <= :endDate) "
			+ "and visitH.task_activity in ('00','01')"
			+ "order by visitH.visit_no, farmer.fullname, emp.fullname, farm.village, scoring.schedule_visit_detail_id", 
			nativeQuery = true)
	List<Object[]> findResultsAdmin(@Param("startDate") Date startDate, @Param("endDate") Date endDate);
}
