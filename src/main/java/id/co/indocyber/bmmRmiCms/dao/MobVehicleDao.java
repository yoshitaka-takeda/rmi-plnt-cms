package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobVehicle;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobVehicleDao extends JpaRepository<MobVehicle, String>{
	@Query("select mv from MobVehicle mv where mv.vehicleNo like concat('%',:arg,'%') or "
			+ "mv.productionYear like concat('%',:arg,'%') or "
			+ "coalesce((select vt.vehicleTypeName from MVehicleType vt where vt.id = mv.vehicleType),'') "
			+ "like concat('%',:arg,'%') or mv.capacity like concat('%',:arg,'%')")
	public List<MobVehicle> findByArg(@Param("arg")String arg, Pageable pageable);
	
	@Query("select count(mv) from MobVehicle mv where mv.vehicleNo like concat('%',:arg,'%') or "
			+ "mv.productionYear like concat('%',:arg,'%') or "
			+ "coalesce((select vt.vehicleTypeName from MVehicleType vt where vt.id = mv.vehicleType),'') "
			+ "like concat('%',:arg,'%') or mv.capacity like concat('%',:arg,'%')")
	public Long countByArg(@Param("arg")String arg);
}
