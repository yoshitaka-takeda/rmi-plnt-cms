package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MGlobalParam;
import id.co.indocyber.bmmRmiCms.entity.MGlobalParamPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MGlobalParamDao extends JpaRepository<MGlobalParam, MGlobalParamPK>{
	@Query("select param.longText from MGlobalParam param where param.valueKey = :id")
	public List<String> getOptions(@Param("id")String id);
}
