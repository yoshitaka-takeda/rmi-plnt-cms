package id.co.indocyber.bmmRmiCms.dao;

import java.sql.Date;
import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobSpta;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobSptaDao extends JpaRepository<MobSpta, Integer>{
	@Query("select spta from MobSpta spta where (spta.expiredDate>=:startDate and spta.documentDate<=:endDate) and "
			+ "(spta.sptaNum like concat('%',:arg,'%') or "
			+ "coalesce((select farmer.fullname from MobFarmer farmer where farmer.id = spta.farmerCode),'') like concat('%',:arg,'%') or "
			+ "coalesce((select driver.fullname from MobDriver driver where driver.id = spta.driverId),'') like concat('%',:arg,'%') or "
			+ "coalesce(spta.vehicleNo,'') like concat('%',:arg,'%') or "
			+ "(case spta.status when 'O' then 'Open' when 'I' then 'In Weighbridge' when 'A' then 'Approved' "
			+ "when 'L' then 'Batal' when 'T' then 'Tapping' when 'M' then 'MBS' when 'C' then 'Closed' when 'E' then 'Expired' "
			+ "when 'R' then 'Timbang Keluar' else '' end) like concat('%',:arg,'%') or "
			+ "coalesce((select vt.vehicleTypeName from MVehicleType vt where vt.id = spta.vehicleType),'') like concat('%',:arg,'%') "
			+ "or spta.qty like concat('%',:arg,'%') "
			+ "or coalesce((select farmer.fullname from MobFarmer farmer where spta.farmerCode = farmer.referenceCode),'') like concat('%',:arg,'%') "
			+ "or coalesce((select farmer.village from MobFarmer farmer where spta.farmerCode = farmer.referenceCode),'') like concat('%',:arg,'%') "
			+ "or spta.farmerCode like concat('%',:arg,'%'))")
	List<MobSpta> findByArg(@Param("arg")String arg, 
			@Param("startDate")Date startDate, @Param("endDate")Date endDate, Pageable pageable);
	
	@Query("select count(spta) from MobSpta spta where (spta.expiredDate>=:startDate and spta.documentDate<=:endDate) and "
			+ "(spta.sptaNum like concat('%',:arg,'%') or "
			+ "coalesce((select farmer.fullname from MobFarmer farmer where farmer.id = spta.farmerCode),'') like concat('%',:arg,'%') or "
			+ "coalesce((select driver.fullname from MobDriver driver where driver.id = spta.driverId),'') like concat('%',:arg,'%') or "
			+ "coalesce(spta.vehicleNo,'') like concat('%',:arg,'%') or "
			+ "(case spta.status when 'O' then 'Open' when 'I' then 'In Weighbridge' when 'A' then 'Approved' "
			+ "when 'L' then 'Batal' when 'T' then 'Tapping' when 'M' then 'MBS' when 'C' then 'Closed' when 'E' then 'Expired' "
			+ "when 'R' then 'Timbang Keluar' else '' end) like concat('%',:arg,'%') or "
			+ "coalesce((select vt.vehicleTypeName from MVehicleType vt where vt.id = spta.vehicleType),'') like concat('%',:arg,'%') "
			+ "or spta.qty like concat('%',:arg,'%') "
			+ "or coalesce((select farmer.fullname from MobFarmer farmer where spta.farmerCode = farmer.referenceCode),'') like concat('%',:arg,'%') "
			+ "or coalesce((select farmer.village from MobFarmer farmer where spta.farmerCode = farmer.referenceCode),'') like concat('%',:arg,'%') "
			+ "or spta.farmerCode like concat('%',:arg,'%'))")
	Long countByArg(@Param("arg")String arg, @Param("startDate")Date startDate, @Param("endDate")Date endDate);
}
