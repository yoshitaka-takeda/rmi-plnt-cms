package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MReligion;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MReligionDao extends JpaRepository<MReligion, String>{

}
