package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitOtherActivities;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobScheduleVisitOtherActivitiesDao extends
		JpaRepository<MobScheduleVisitOtherActivities, String> {
	@Query("select visitAct from MobScheduleVisitOtherActivities visitAct where "
			+ "visitAct.visitNo = :visitNo")
	public List<MobScheduleVisitOtherActivities> findActivities(@Param("visitNo")String visitNo);
}
