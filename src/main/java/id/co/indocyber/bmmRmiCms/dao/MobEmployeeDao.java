package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobEmployee;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobEmployeeDao extends JpaRepository<MobEmployee, Integer>{
	@Query("select emp.id from MobEmployee emp")
	List<Integer> findAllIds();
	
	@Query("select emp from MobEmployee emp where emp.manager in (:id)")
	List<MobEmployee> findManagedEmployees(@Param("id")List<Integer> id);
	
	@Query("select emp.fullname from MobEmployee emp where emp.manager = :id ")
	List<String> findDirectManagedNames(@Param("id")Integer id);
	
	@Query("select emp, d.roleName from MobEmployee emp, MUser b, MUserRole c, MRole d, MobMappingFarmerEmployee e "
			+ "where "
			+ "	emp.id = b.personId "
			+ "	and b.email = c.email "
			+ "	and e.employeeId = emp.id "
			+ "	and c.roleId = d.id "
			+ "	and emp.id in (:id) "
			+ "	and e.farmerId = :farmerId ")
	List<Object[]> findManagedEmployeesById(@Param("id")List<Integer> id, @Param("farmerId")int farmerId);
	
	@Query("select d.roleName from MobEmployee emp, MUser b, MUserRole c, MRole d "
			+ "where "
			+ "	emp.id = b.personId "
			+ "	and b.email = c.email "
			+ "	and c.roleId = d.id "
			+ "	and emp.id = :id and c.roleId not in (2,3)")
	List<String> getRoleName(@Param("id")Integer id);

	@Query("select emp from MobEmployee emp where lower(emp.fullname) = lower(:name)")
	List<MobEmployee> findByName(@Param("name")String fullname);

	@Query("select emp from MobEmployee emp where lower(emp.email) = lower(:email)")
	List<MobEmployee> findByEmail(@Param("email")String email);
	
	@Query("select emp from MobEmployee emp where lower(emp.phoneNum) = lower(:phone)")
	List<MobEmployee> findByPhone(@Param("phone")String phone);
	
	@Query("select emp.manager from MobEmployee emp where emp.id = :employeeId")
	Integer getMyManager(@Param("employeeId")Integer employeeId);
	
	@Query("select emp from MobEmployee emp where emp.fullname like concat('%',:arg,'%') or "
			+ "emp.phoneNum like concat('%',:arg,'%') or emp.address like concat('%',:arg,'%') or "
			+ "emp.id like concat('%',:arg,'%') "
			+ "or date_format(emp.birthday,'%d-%M-%Y') like concat('%',:arg,'%') or "
			+ "coalesce((select emp2.fullname from MobEmployee emp2 where emp2.id = emp.manager), '') like concat('%',:arg,'%') or "
			+ "("
			+ "select count(d.roleName) from MobEmployee emp3, MUser b, MUserRole c, MRole d "
			+ "where "
			+ "	emp.id = b.personId "
			+ "	and b.email = c.email "
			+ "	and c.roleId = d.id "
			+ "	and emp3.id = emp.id and c.roleId not in (2,3) "
			+ " and d.roleName like concat ('%',:arg,'%')"
			+ ")>0")
	List<MobEmployee> findByArg(@Param("arg")String arg, Pageable pageRequest);
	
	@Query("select count(emp) from MobEmployee emp where emp.fullname like concat('%',:arg,'%') or "
			+ "emp.phoneNum like concat('%',:arg,'%') or emp.address like concat('%',:arg,'%') or "
			+ "emp.id like concat('%',:arg,'%') "
			+ "or date_format(emp.birthday,'%d-%M-%Y') like concat('%',:arg,'%') or "
			+ "coalesce((select emp2.fullname from MobEmployee emp2 where emp2.id = emp.manager), '') like concat('%',:arg,'%') or "
			+ "("
			+ "select count(d.roleName) from MobEmployee emp3, MUser b, MUserRole c, MRole d "
			+ "where "
			+ "	emp.id = b.personId "
			+ "	and b.email = c.email "
			+ "	and c.roleId = d.id "
			+ "	and emp3.id = emp.id and c.roleId not in (2,3) "
			+ " and d.roleName like concat ('%',:arg,'%')"
			+ ")>0")
	Long countByArg(@Param("arg")String arg);

	@Query("select emp from MobEmployee emp where emp.id not in ("
			+ "select user.personId from MUser user, MUserRole role where "
			+ "user.email = role.email and role.roleId not in (2,3) and user.isActive is true)")
	List<MobEmployee> findAllNoUser();

	@Modifying
	@Query(value="update mob_employee set manager = :newManager where manager = :prevManager",nativeQuery=true)
	void updateManager(@Param("prevManager")Integer prevManager, @Param("newManager")Integer newManager);
}
