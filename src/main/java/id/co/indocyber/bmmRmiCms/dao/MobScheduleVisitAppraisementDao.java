package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;
import java.util.Set;

import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitAppraisement;
import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitAppraisementPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobScheduleVisitAppraisementDao
		extends
		JpaRepository<MobScheduleVisitAppraisement, MobScheduleVisitAppraisementPK> {
	@Query(value = "select farm.sub_district 'Kecamatan', farm.district 'Desa', paddy.field_area 'Luas', "
			+ "farmer.fullname 'Pemilik', paddy.paddy_field_code 'Nomor Register', app.period_name 'Bulan Tanam', "
			+ "app.current_sugarcane_height 'Tinggi Sekarang', app.cutdown_sugarcane_height 'Tinggi Ditebang', "
			+ "app.sugarcane_stem_weight 'Berat Batang / m', app.weight_per_sugarcane 'Berat / Batang', "
			+ "app.number_of_stem_per_leng 'Jumlah Batang / leng', app.leng_factor_per_HA 'Jumlah leng / Ha', "
			+ "app.number_of_sugarcane_per_HA 'Total Batang / Ha', app.prod_per_hectar_KU 'Prod / Hektar (Ku)', app.amount_of_production 'Jumlah produksi', "
			+ "app.cutting_schedule 'Periode Tebang', app.avg_brix_number 'Brix', app.rendemen_estimation 'Rendemen', "
			+ "app.ku_hablur_per_ha 'Hablur / Ha', app.amount_of_hablur 'Total Hablur', visitD.latitude 'lat', visitD.longitude 'long' "
			+ "from mob_schedule_visit_appraisement app inner join "
			+ "mob_schedule_visit_to_farmer_detail visitD on visitD.id = app.schedule_visit_detail_id inner join "
			+ "mob_schedule_visit_to_farmer_header visitH on visitH.visit_no = visitD.visit_no inner join "
			+ "mob_farmer farmer on farmer.id = visitH.farmer_id inner join "
			+ "mob_farm farm on farm.farmer_id = visitH.farmer_id and farm.farm_code = visitD.farm_code inner join "
			+ "mob_paddy_field paddy on paddy.farm_code = farm.farm_code and paddy.paddy_field_code = visitD.paddy_field_code inner join "
			+ "m_sugarcane_type sugartype on sugartype.code = paddy.sugarcane_type "
			+ "where visitD.id in (:ids)", nativeQuery = true)
	public List<Object[]> getAppraisement(@Param("ids") Set<Integer> visitIds);
}
