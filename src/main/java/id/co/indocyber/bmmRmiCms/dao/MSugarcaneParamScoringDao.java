package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MSugarcaneParamScoring;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MSugarcaneParamScoringDao extends JpaRepository<MSugarcaneParamScoring, Integer>{
	@Query("select sps from MSugarcaneParamScoring sps where "
			+ "sps.paramCode like concat('%',:arg,'%') "
			+ "or sps.paramValue like concat('%',:arg,'%') "
			+ "or sps.criteria like concat('%',:arg,'%') "
			+ "or sps.weight like concat('%',:arg,'%')")
	public List<MSugarcaneParamScoring> findByArg(@Param("arg")String arg, Pageable pageable);
	
	@Query("select count(sps) from MSugarcaneParamScoring sps where "
			+ "sps.paramCode like concat('%',:arg,'%') "
			+ "or sps.paramValue like concat('%',:arg,'%') "
			+ "or sps.criteria like concat('%',:arg,'%') "
			+ "or sps.weight like concat('%',:arg,'%')")
	public Long countByArg(@Param("arg")String arg);

	@Query("select sum(sps.weight) from MSugarcaneParamScoring sps where sps.isActive is true")
	public Double getSum();

	@Query("select sps from MSugarcaneParamScoring sps where sps.isActive is true")
	public List<MSugarcaneParamScoring> findAllActive();
}
