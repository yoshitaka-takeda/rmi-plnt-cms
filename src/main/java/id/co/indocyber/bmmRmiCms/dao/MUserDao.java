package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MUser;
import id.co.indocyber.bmmRmiCms.entity.MUserRole;

import org.springframework.data.domain.Pageable;
import org.springframework.data.domain.Sort;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Modifying;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MUserDao extends JpaRepository<MUser, String>{
	@Query("select user, role.roleName "
			+ "from MUser user, MUserRole ur, MRole role where "
			+ "role.id = ur.roleId and user.email = ur.email")
	public List<Object[]> getAll();
	@Query("select user, role.roleName "
			+ "from MUser user, MUserRole ur, MRole role where "
			+ "role.id = ur.roleId and user.email = ur.email")
	public List<Object[]> getAll(Pageable pageable);
	@Query("select user, role.roleName "
			+ "from MUser user, MUserRole ur, MRole role where "
			+ "role.id = ur.roleId and user.email = ur.email")
	public List<Object[]> getAll(Sort sort);
	
	@Query("select user from MUser user where (lower(user.email) = lower(:user) "
			+ "or lower(user.username) = lower(:user) or user.phoneNum = :user)")
	public MUser findByUsernameOrEmail(@Param("user")String usernameOrEmail);

	@Query("select user, role.roleName "
			+ "from MUser user, MUserRole ur, MRole role where "
			+ "role.id = ur.roleId and user.email = ur.email "
			+ "and (user.fullname like concat('%',:arg,'%') or "
			+ "user.email like concat('%',:arg,'%') or "
			+ "role.roleName like concat('%',:arg,'%') or "
			+ "user.imei like concat('%',:arg,'%') or "
			+ "user.personId like concat('%',:arg,'%') or "
			+ "user.phoneNum like concat('%',:arg,'%'))")
	public List<Object[]> findByArg(@Param("arg")String arg, Pageable pageable);
	
	@Query("select count(user) from MUser user, MUserRole ur, MRole role where "
			+ "role.id = ur.roleId and user.email = ur.email "
			+ "and (user.fullname like concat('%',:arg,'%') or "
			+ "user.email like concat('%',:arg,'%') or "
			+ "role.roleName like concat('%',:arg,'%') or "
			+ "user.imei like concat('%',:arg,'%') or "
			+ "user.personId like concat('%',:arg,'%') or "
			+ "user.phoneNum like concat('%',:arg,'%'))")
	public Long countByArg(@Param("arg")String arg);
	
	@Query("select user from MUser user, MUserRole ur where user.email = ur.email and "
			+ "ur.roleId not in(2,3) and user.personId = :id")
	public MUser findEmployee(@Param("id")Integer id);
	
	@Query("select user from MUser user, MUserRole ur where user.email = ur.email and "
			+ "ur.roleId = 3 and user.personId = :id")
	public MUser findDriver(@Param("id")Integer id);
	
	@Query("select a from MUserRole a, MUser b, MRole c "
			+ "where "
			+ "a.email = b.email "
			+ "and a.roleId = c.id "
			+ "and b.username like :username")
	public MUserRole findUserLogin(@Param("username")String username);
	
	@Query("select user.token from MUser user, MUserRole ur where user.email = ur.email and "
			+ "ur.roleId not in(2,3) and user.personId = :id")
	public String findEmployeeToken(@Param("id")Integer id);
	
	@Modifying
	@Query(value="delete from m_user where username=:id", nativeQuery=true)
	public void deleteByUsername(@Param("id")String username);
	
	@Modifying
	@Query(value="update m_user set email = :email where username=:id", nativeQuery=true)
	public void updateEmail(@Param("id")String username, @Param("email")String email);
}
