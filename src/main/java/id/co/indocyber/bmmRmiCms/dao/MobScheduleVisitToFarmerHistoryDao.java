package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitToFarmerHistory;
import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitToFarmerHistoryPK;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobScheduleVisitToFarmerHistoryDao extends
		JpaRepository<MobScheduleVisitToFarmerHistory, MobScheduleVisitToFarmerHistoryPK> {
	@Query("select visitHist from MobScheduleVisitToFarmerHistory visitHist where "
			+ "visitHist.visitNo = :visitNo")
	public List<MobScheduleVisitToFarmerHistory> getHistory(@Param("visitNo")String visitNo);

	@Query("select visitHist, emp.fullname from MobScheduleVisitToFarmerHistory visitHist, "
			+ "MobEmployee emp where "
			+ "visitHist.assignTo = emp.id and visitHist.visitNo = :visitNo")
	public List<Object[]> getHistoryWithName(@Param("visitNo")String visitNo);

	@Query("select count(visitHist)>0 from MobScheduleVisitToFarmerHistory visitHist where "
			+ "visitHist.visitNo = :visitNo and visitHist.taskAction = '04'")
	public Boolean hasBeenDelegated(@Param("visitNo")String visitNo);
}
