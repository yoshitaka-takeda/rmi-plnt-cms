package id.co.indocyber.bmmRmiCms.dao;

import id.co.indocyber.bmmRmiCms.entity.MAddressType;

import org.springframework.data.jpa.repository.JpaRepository;

public interface MAddressTypeDao extends JpaRepository<MAddressType, String>{

}
