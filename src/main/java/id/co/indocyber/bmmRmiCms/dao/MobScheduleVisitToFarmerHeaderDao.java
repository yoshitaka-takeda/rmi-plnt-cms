package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitToFarmerHeader;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobScheduleVisitToFarmerHeaderDao extends
		JpaRepository<MobScheduleVisitToFarmerHeader, String> {
	@Query("select count(visitH) from MobScheduleVisitToFarmerHeader visitH where "
			+ "month(visitH.createdDate) = month(current_date()) "
			+ "and year(visitH.createdDate) = year(current_date())")
	Integer getCurrentMonthYearVisitCount();

	@Query("select visitH from MobScheduleVisitToFarmerHeader visitH "
			+ "where visitH.taskActivity in ('02','03') "
			+ "and visitH.assignTo in(:list)")
	List<MobScheduleVisitToFarmerHeader> findAllPending(
			@Param("list") List<Integer> list, Pageable pageable);
	
	@Query("select count(visitH) from MobScheduleVisitToFarmerHeader visitH "
			+ "where visitH.taskActivity in ('02','03') and visitH.assignTo in(:list)")
	Long countAllPending(@Param("list") List<Integer> list);
	
	@Query("select visitH from MobScheduleVisitToFarmerHeader visitH where visitH.taskActivity in ('02','03') "
			+ "and visitH.assignTo in (:list) "
			+ "and ("
			+ "visitH.visitNo like concat('%',:arg,'%') or "
			+ "(select fr.fullname from MobFarmer fr where visitH.farmerId = fr.id) like concat('%',:arg,'%') or "
			+ "(select emp.fullname from MobEmployee emp where visitH.assignTo = emp.id) like concat('%',:arg,'%') or "
			+ "(select farm.village from MobFarm farm where farm.farmCode = ("
			+ "select distinct visitD.farmCode from MobScheduleVisitToFarmerDetail visitD where visitD.visitNo = visitH.visitNo and visitD.isActive is true)"
			+ ") like concat('%',:arg,'%') or "
			+ "date_format(visitH.visitDate,'%d-%m-%Y') like concat('%',:arg,'%') or "
			+ "date_format(visitH.visitExpiredDate,'%d-%m-%Y') like concat('%',:arg,'%'))")
	List<MobScheduleVisitToFarmerHeader> findAllByArg(
			@Param("list") List<Integer> list, @Param("arg")String arg, Pageable pageRequest);
	
	@Query("select count(visitH) from MobScheduleVisitToFarmerHeader visitH where visitH.taskActivity in ('02','03') "
			+ "and visitH.assignTo in (:list) "
			+ "and ("
			+ "visitH.visitNo like concat('%',:arg,'%') or "
			+ "(select fr.fullname from MobFarmer fr where visitH.farmerId = fr.id) like concat('%',:arg,'%') or "
			+ "(select emp.fullname from MobEmployee emp where visitH.assignTo = emp.id) like concat('%',:arg,'%') or "
			+ "(select farm.village from MobFarm farm where farm.farmCode = ("
			+ "select distinct visitD.farmCode from MobScheduleVisitToFarmerDetail visitD where visitD.visitNo = visitH.visitNo and visitD.isActive is true)"
			+ ") like concat('%',:arg,'%') or "
			+ "date_format(visitH.visitDate,'%d-%m-%Y') like concat('%',:arg,'%') or "
			+ "date_format(visitH.visitExpiredDate,'%d-%m-%Y') like concat('%',:arg,'%'))")
	Long countAllByArg(@Param("list") List<Integer> list, @Param("arg")String arg);

}
