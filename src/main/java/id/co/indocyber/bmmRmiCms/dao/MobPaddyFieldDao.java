package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobPaddyField;

import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobPaddyFieldDao extends JpaRepository<MobPaddyField, Integer>{
	@Query("select mpf from MobPaddyField mpf where mpf.farmCode = :farmCode and mpf.farmerId = :farmerId")
	public List<MobPaddyField> findFieldsByFarmCode(@Param("farmerId")Integer farmerId,
			@Param("farmCode")String farmCode);

	@Query("select mpf from MobPaddyField mpf where mpf.farmCode = :farmCode and mpf.farmerId = :farmerId "
			+ "and mpf.paddyFieldCode in (:fieldCodes)")
	public List<MobPaddyField> findSelectedPaddyFields(@Param("farmerId")Integer farmerId,
			@Param("farmCode")String farmCode, @Param("fieldCodes")List<String> paddyFieldCodes);
	
	@Query("select mpf.fieldArea from MobPaddyField mpf where "
			+ "mpf.farmCode = :farmCode and mpf.farmerId = :farmerId "
			+ "and mpf.paddyFieldCode = :paddyFieldCode)")
	public Double findFieldArea(@Param("farmerId")Integer farmerId, @Param("farmCode")String farmCode,
			@Param("paddyFieldCode")String paddyFieldCode);
}
