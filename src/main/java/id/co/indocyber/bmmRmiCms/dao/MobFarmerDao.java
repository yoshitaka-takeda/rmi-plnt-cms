package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobFarmer;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobFarmerDao extends JpaRepository<MobFarmer, Integer>{
	@Query("select farmer from MobFarmer farmer where farmer.id like concat('%',:arg,'%') or "
			+ "farmer.referenceCode like concat('%',:arg,'%') or "
			+ "farmer.fullname like concat('%',:arg,'%') or "
			+ "farmer.phoneNum like concat('%',:arg,'%') or farmer.address like concat('%',:arg,'%') or "
			+ "date_format(farmer.birthday,'%d-%M-%Y') like concat('%',:arg,'%')")
	List<MobFarmer> findByArg(@Param("arg")String arg, Pageable pageRequest);
	
	@Query("select count(farmer) from MobFarmer farmer where farmer.id like concat('%',:arg,'%') or "
			+ "farmer.referenceCode like concat('%',:arg,'%') or "
			+ "farmer.fullname like concat('%',:arg,'%') or "
			+ "farmer.phoneNum like concat('%',:arg,'%') or farmer.address like concat('%',:arg,'%') or "
			+ "date_format(farmer.birthday,'%d-%M-%Y') like concat('%',:arg,'%')")
	Long countByArg(@Param("arg")String arg);

	@Query("select farmer from MobFarmer farmer where farmer.id not in "
			+ "(select map.farmerId from MobMappingFarmerEmployee map where map.isActive is true)")
	List<MobFarmer> findAllUnmapped();

	@Query("select farmer from MobFarmer farmer where farmer.id in "
			+ "(select map.farmerId from MobMappingFarmerEmployee map where map.isActive is true and map.employeeId = :id) "
			+ "and farmer.id not in (select map.farmerId from MobMappingFarmerEmployee map where map.isActive is true and map.employeeId = :empId)")
	List<MobFarmer> findAllUnmappedFromHead(@Param("id")int id,@Param("empId")int empId);

	@Query("select farmer from MobFarmer farmer where farmer.id not in ("
			+ "select user.personId from MUser user, MUserRole role where "
			+ "user.email = role.email and role.roleId = 2  and user.isActive is true)")
	List<MobFarmer> findAllNoUser();
	
	@Query("select farmer from MobFarmer farmer where farmer.referenceCode = :referenceCode")
	MobFarmer findByReference(@Param("referenceCode") String farmerCode);
}
