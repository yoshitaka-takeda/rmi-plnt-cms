package id.co.indocyber.bmmRmiCms.dao;

import java.sql.Date;
import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobWeighbridgeResult;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobWeighbridgeResultDao extends JpaRepository<MobWeighbridgeResult, Integer>{
	@Query("select wRes from MobWeighbridgeResult wRes where (wRes.dateOut>=:startDate and wRes.dateIn<=:endDate) and ("
			+ "wRes.documentNum like concat('%',:arg,'%') or "
			+ "concat(coalesce(wRes.contractNo1,''),' - ',coalesce(wRes.contractNo2,'')) like concat('%',:arg,'%') or "
			+ "coalesce(wRes.farmerName,'') like concat('%',:arg,'%') or "
			+ "coalesce(wRes.driverName,'') like concat('%',:arg,'%') or "
			+ "concat(date_format(wRes.dateIn,'%d-%m-%Y'),' ',wRes.timeIn) like concat('%',:arg,'%') or "
			+ "concat(date_format(wRes.dateOut,'%d-%m-%Y'),' ',wRes.timeOut) like concat('%',:arg,'%') or "
			+ "coalesce(wRes.qtyWeigho, 0) like concat('%',:arg,'%') or "
			+ "coalesce(wRes.totalPaid,0) like concat('%',:arg,'%') or "
			+ "coalesce(wRes.status,'') like concat('%',:arg,'%') or "
			+ "coalesce((select vt.vehicleTypeName from MVehicleType vt where vt.id = cast(wRes.vehicleType as int)),'') like concat('%',:arg,'%'))")
	List<MobWeighbridgeResult> findByArg(@Param("arg")String arg,@Param("startDate")Date startDate,@Param("endDate")Date endDate, Pageable pageRequest);
	
	@Query("select count(wRes) from MobWeighbridgeResult wRes where (wRes.dateOut>=:startDate and wRes.dateIn<=:endDate) and ("
			+ "wRes.documentNum like concat('%',:arg,'%') or "
			+ "concat(coalesce(wRes.contractNo1,''),' - ',coalesce(wRes.contractNo2,'')) like concat('%',:arg,'%') or "
			+ "coalesce(wRes.farmerName,'') like concat('%',:arg,'%') or "
			+ "coalesce(wRes.driverName,'') like concat('%',:arg,'%') or "
			+ "concat(date_format(wRes.dateIn,'%d-%m-%Y'),' ',wRes.timeIn) like concat('%',:arg,'%') or "
			+ "concat(date_format(wRes.dateOut,'%d-%m-%Y'),' ',wRes.timeOut) like concat('%',:arg,'%') or "
			+ "coalesce(wRes.qtyWeigho, 0) like concat('%',:arg,'%') or "
			+ "coalesce(wRes.totalPaid,0) like concat('%',:arg,'%') or "
			+ "coalesce(wRes.status,'') like concat('%',:arg,'%') or "
			+ "coalesce((select vt.vehicleTypeName from MVehicleType vt where vt.id = cast(wRes.vehicleType as int)),'') like concat('%',:arg,'%'))")
	Long countByArg(@Param("arg")String arg,@Param("startDate")Date startDate,@Param("endDate")Date endDate);
}
