package id.co.indocyber.bmmRmiCms.dao;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MobRequestForCancel;

import org.springframework.data.domain.Pageable;
import org.springframework.data.jpa.repository.JpaRepository;
import org.springframework.data.jpa.repository.Query;
import org.springframework.data.repository.query.Param;

public interface MobRequestForCancelDao extends JpaRepository<MobRequestForCancel, Integer>{
	@Query("select rfc from MobRequestForCancel rfc "
		+ "where (select spta.status from MobSpta spta where spta.sptaNum = rfc.sptaNum) is not 'L' and ("
		+ "rfc.sptaNum like concat('%',:arg,'%') or "
		+ "rfc.reasonForCancel like concat('%',:arg,'%') or rfc.locationForCancel like concat('%',:arg,'%') or "
		+ "rfc.createdBy like concat('%',:arg,'%') or date_format(rfc.createdDate,'%d-%m-%Y') like concat('%',:arg,'%') or "
		+ "(case rfc.taskActivity when '00' then 'Selesai' when '01' then 'Batal' when '02' then 'Pending' "
		+ "when '03' then 'Dalam Proses' when '04' then 'Didelegasikan' else 'Kadaluarsa' end) like concat('%',:arg,'%'))")
	List<MobRequestForCancel> findAllByArg(@Param("arg")String arg, Pageable pageRequest);

	@Query("select count(rfc) from MobRequestForCancel rfc "
		+ "where (select spta.status from MobSpta spta where spta.sptaNum = rfc.sptaNum) is not 'L' and ("
		+ "rfc.sptaNum like concat('%',:arg,'%') or "
		+ "rfc.reasonForCancel like concat('%',:arg,'%') or rfc.locationForCancel like concat('%',:arg,'%') or "
		+ "rfc.createdBy like concat('%',:arg,'%') or date_format(rfc.createdDate,'%d-%m-%Y') like concat('%',:arg,'%') or "
		+ "(case rfc.taskActivity when '00' then 'Selesai' when '01' then 'Batal' when '02' then 'Pending' "
		+ "when '03' then 'Dalam Proses' when '04' then 'Didelegasikan' else 'Kadaluarsa' end) like concat('%',:arg,'%'))")
	Long countAllByArg(@Param("arg")String arg);
}
