package id.co.indocyber.bmmRmiCms.dto;

import id.co.indocyber.bmmRmiCms.entity.MAddressType;
import id.co.indocyber.bmmRmiCms.entity.MEducationType;
import id.co.indocyber.bmmRmiCms.entity.MGender;
import id.co.indocyber.bmmRmiCms.entity.MMaritalStatus;
import id.co.indocyber.bmmRmiCms.entity.MReligion;
import id.co.indocyber.bmmRmiCms.entity.MobDriver;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.Period;

public class MobDriverDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 3414311527774827535L;
	private Integer id;
	private String fullname;
	private Date birthday;
	private Integer age;
	private String placeOfBirthday;
	private MAddressType addressType;
	private String address;
	private String rtrw;
	private String village;
	private String subDistrict;
	private String district;
	private String city;
	private String province;
	private String zipcode;
	private String phoneNum;
	private String email;
	private MMaritalStatus maritalStatus;
	private MGender gender;
	private MReligion religion;
	private MEducationType lastEducation;
	private String ktp;
	private String driverType;
	private String referenceCode;
	private Boolean isActive;
	private String createdBy;
	private Timestamp createdDate;
	private String modifiedBy;
	private Timestamp modifiedDate;

	public MobDriverDto(MobDriver x) {
		this.address = x.getAddress();
		this.birthday = x.getBirthday();
		this.city = x.getCity();
		this.district = x.getDistrict();
		this.email = x.getEmail();
		this.fullname = x.getFullname();
		this.id = x.getId();
		this.isActive = x.getIsActive();
		this.ktp = x.getKtp();
		this.phoneNum = x.getPhoneNum()==null?"":
			x.getPhoneNum().startsWith("628")?
					x.getPhoneNum().replaceFirst("628", "08"): x.getPhoneNum();
		this.placeOfBirthday = x.getPlaceOfBirthday();
		this.createdBy = x.getCreatedBy();
		this.createdDate = x.getCreatedDate();
		this.modifiedBy = x.getModifiedBy();
		this.modifiedDate = x.getModifiedDate();
		this.province = x.getProvince();
		this.referenceCode = x.getReferenceCode();
		this.rtrw = x.getRtrw();
		this.subDistrict= x.getSubDistrict();
		this.village = x.getVillage();
		this.zipcode = x.getZipcode();
		this.driverType = x.getDriverType();
		if(this.birthday!=null){
			this.age = Period.between(this.birthday.toLocalDate(), LocalDate.now()).getYears();
		}
	}
	
	public MobDriverDto(){
		//default DB value
		this.driverType = "Petani";
	}

	public void reCalculateAge(){
		if(this.birthday!=null){
			this.age = Period.between(this.birthday.toLocalDate(), LocalDate.now()).getYears();
		}
	}

	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public String getFullname() {
		return fullname;
	}

	public void setFullname(String fullname) {
		this.fullname = fullname;
	}

	public Date getBirthday() {
		return birthday;
	}

	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}

	public Integer getAge() {
		return age;
	}

	public void setAge(Integer age) {
		this.age = age;
	}

	public String getPlaceOfBirthday() {
		return placeOfBirthday;
	}

	public void setPlaceOfBirthday(String placeOfBirthday) {
		this.placeOfBirthday = placeOfBirthday;
	}

	public MAddressType getAddressType() {
		return addressType;
	}

	public void setAddressType(MAddressType addressType) {
		this.addressType = addressType;
	}

	public String getAddress() {
		return address;
	}

	public void setAddress(String address) {
		this.address = address;
	}

	public String getRtrw() {
		return rtrw;
	}

	public void setRtrw(String rtrw) {
		this.rtrw = rtrw;
	}

	public String getVillage() {
		return village;
	}

	public void setVillage(String village) {
		this.village = village;
	}

	public String getSubDistrict() {
		return subDistrict;
	}

	public void setSubDistrict(String subDistrict) {
		this.subDistrict = subDistrict;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public String getCity() {
		return city;
	}

	public void setCity(String city) {
		this.city = city;
	}

	public String getProvince() {
		return province;
	}

	public void setProvince(String province) {
		this.province = province;
	}

	public String getZipcode() {
		return zipcode;
	}

	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}

	public String getPhoneNum() {
		return phoneNum;
	}

	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}

	public String getEmail() {
		return email;
	}

	public void setEmail(String email) {
		this.email = email;
	}

	public MMaritalStatus getMaritalStatus() {
		return maritalStatus;
	}

	public void setMaritalStatus(MMaritalStatus maritalStatus) {
		this.maritalStatus = maritalStatus;
	}

	public MGender getGender() {
		return gender;
	}

	public void setGender(MGender gender) {
		this.gender = gender;
	}

	public MReligion getReligion() {
		return religion;
	}

	public void setReligion(MReligion religion) {
		this.religion = religion;
	}

	public MEducationType getLastEducation() {
		return lastEducation;
	}

	public void setLastEducation(MEducationType lastEducation) {
		this.lastEducation = lastEducation;
	}

	public String getKtp() {
		return ktp;
	}

	public void setKtp(String ktp) {
		this.ktp = ktp;
	}

	public String getReferenceCode() {
		return referenceCode;
	}

	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}

	public String getDriverType() {
		return driverType;
	}

	public void setDriverType(String driverType) {
		this.driverType = driverType;
	}
}