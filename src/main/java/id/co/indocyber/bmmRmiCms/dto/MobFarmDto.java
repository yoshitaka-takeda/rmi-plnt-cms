package id.co.indocyber.bmmRmiCms.dto;

import id.co.indocyber.bmmRmiCms.entity.MFarmFieldsType;
import id.co.indocyber.bmmRmiCms.entity.MSugarcaneFarmType;
import id.co.indocyber.bmmRmiCms.entity.MobFarm;

import java.io.Serializable;
import java.sql.Timestamp;

public class MobFarmDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8974126541154776163L;
	private Integer farmerId;
	private String farmCode;
	private Double fieldArea;
	private String address;
	private Double latitude;
	private Double longitude;
	private MFarmFieldsType farmMainType;
	private MSugarcaneFarmType farmSubType; // SugarcaneType?
	private String referenceCode;
	private String status;
	private Boolean isActive;
	private String village;
	private String processedBy;
	private Timestamp processedDate;

	public Integer getFarmerId() {
		return farmerId;
	}
	public void setFarmerId(Integer farmerId) {
		this.farmerId = farmerId;
	}
	public String getFarmCode() {
		return farmCode;
	}
	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}
	public Double getFieldArea() {
		return fieldArea;
	}
	public void setFieldArea(Double fieldArea) {
		this.fieldArea = fieldArea;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public MFarmFieldsType getFarmMainType() {
		return farmMainType;
	}
	public void setFarmMainType(MFarmFieldsType farmMainType) {
		this.farmMainType = farmMainType;
	}
	public MSugarcaneFarmType getFarmSubType() {
		return farmSubType;
	}
	public void setFarmSubType(MSugarcaneFarmType farmSubType) {
		this.farmSubType = farmSubType;
	}
	public String getReferenceCode() {
		return referenceCode;
	}
	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getProcessedBy() {
		return processedBy;
	}
	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}
	public Timestamp getProcessedDate() {
		return processedDate;
	}
	public void setProcessedDate(Timestamp processedDate) {
		this.processedDate = processedDate;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	
	public MobFarmDto(){
		
	}
	
	public MobFarmDto(MobFarm entity){
		this.address = (entity.getSubDistrict()==null?"":entity.getSubDistrict())
				+"\n"+(entity.getDistrict()==null?"":entity.getDistrict())
				+"\n"+(entity.getProvince()==null?"":entity.getProvince());
		this.farmCode = entity.getFarmCode();
		this.farmerId = entity.getFarmerId();
		this.fieldArea = entity.getFieldArea();
		this.isActive = entity.getIsActive();
		this.latitude = entity.getLatitude();
		this.longitude = entity.getLongitude();
		this.processedBy = entity.getProcessedBy();
		this.processedDate = entity.getProcessedDate();
		this.referenceCode = entity.getReferenceCode();
		this.status = entity.getStatus();
		this.village = entity.getVillage();
	}
}
