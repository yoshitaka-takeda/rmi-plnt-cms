package id.co.indocyber.bmmRmiCms.dto;

import java.io.Serializable;

public class MobScheduleVisitAppraisementDto implements Serializable {
	private static final long serialVersionUID = -5778365524484767090L;

	private Integer scheduleVisitDetailId;
	private String subDistrict;
	private String district;
	private Double fieldArea;
	private String farmer;
	private String paddyFieldCode;
	private String periodName;
	private Double currentSugarcaneHeight;
	private Double cutdownSugarcaneHeight;
	private Double sugarcaneStemWeight;
	private Double weightPerSugarcane;
	private Integer numberOfStemPerLeng;
	private Integer lengFactorPerHa;
	private Double numberOfSugarcanePerHa;
	private Double prodPerHectarKu;
	private Double amountOfProduction;
	private String cuttingSchedule;
	private Double avgBrixNumber;
	private Double rendemenEstimation;
	private Double kuHablurPerHa;
	private Double amountOfHablur;
	private Double appraisement;
	private Double latitude, longitude;

	public Double getLatitude() {
		return latitude;
	}

	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}

	public Double getLongitude() {
		return longitude;
	}

	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}

	public MobScheduleVisitAppraisementDto() {

	}

	public Integer getScheduleVisitDetailId() {
		return scheduleVisitDetailId;
	}

	public void setScheduleVisitDetailId(Integer scheduleVisitDetailId) {
		this.scheduleVisitDetailId = scheduleVisitDetailId;
	}

	public String getSubDistrict() {
		return subDistrict;
	}

	public void setSubDistrict(String subDistrict) {
		this.subDistrict = subDistrict;
	}

	public String getDistrict() {
		return district;
	}

	public void setDistrict(String district) {
		this.district = district;
	}

	public Double getFieldArea() {
		return fieldArea;
	}

	public void setFieldArea(Double fieldArea) {
		this.fieldArea = fieldArea;
	}

	public String getFarmer() {
		return farmer;
	}

	public void setFarmer(String farmer) {
		this.farmer = farmer;
	}

	public String getPaddyFieldCode() {
		return paddyFieldCode;
	}

	public void setPaddyFieldCode(String paddyFieldCode) {
		this.paddyFieldCode = paddyFieldCode;
	}

	public String getPeriodName() {
		return periodName;
	}

	public void setPeriodName(String periodName) {
		this.periodName = periodName;
	}

	public Double getCurrentSugarcaneHeight() {
		return currentSugarcaneHeight;
	}

	public void setCurrentSugarcaneHeight(Double currentSugarcaneHeight) {
		this.currentSugarcaneHeight = currentSugarcaneHeight;
	}

	public Double getCutdownSugarcaneHeight() {
		return cutdownSugarcaneHeight;
	}

	public void setCutdownSugarcaneHeight(Double cutdownSugarcaneHeight) {
		this.cutdownSugarcaneHeight = cutdownSugarcaneHeight;
	}

	public Double getSugarcaneStemWeight() {
		return sugarcaneStemWeight;
	}

	public void setSugarcaneStemWeight(Double sugarcaneStemWeight) {
		this.sugarcaneStemWeight = sugarcaneStemWeight;
	}

	public Double getWeightPerSugarcane() {
		return weightPerSugarcane;
	}

	public void setWeightPerSugarcane(Double weightPerSugarcane) {
		this.weightPerSugarcane = weightPerSugarcane;
	}

	public Integer getNumberOfStemPerLeng() {
		return numberOfStemPerLeng;
	}

	public void setNumberOfStemPerLeng(Integer numberOfStemPerLeng) {
		this.numberOfStemPerLeng = numberOfStemPerLeng;
	}

	public Integer getLengFactorPerHa() {
		return lengFactorPerHa;
	}

	public void setLengFactorPerHa(Integer lengFactorPerHa) {
		this.lengFactorPerHa = lengFactorPerHa;
	}

	public Double getNumberOfSugarcanePerHa() {
		return numberOfSugarcanePerHa;
	}

	public void setNumberOfSugarcanePerHa(Double numberOfSugarcanePerHa) {
		this.numberOfSugarcanePerHa = numberOfSugarcanePerHa;
	}

	public Double getProdPerHectarKu() {
		return prodPerHectarKu;
	}

	public void setProdPerHectarKu(Double prodPerHectarKu) {
		this.prodPerHectarKu = prodPerHectarKu;
	}

	public Double getAmountOfProduction() {
		return amountOfProduction;
	}

	public void setAmountOfProduction(Double amountOfProduction) {
		this.amountOfProduction = amountOfProduction;
	}

	public String getCuttingSchedule() {
		return cuttingSchedule;
	}

	public void setCuttingSchedule(String cuttingSchedule) {
		this.cuttingSchedule = cuttingSchedule;
	}

	public Double getAvgBrixNumber() {
		return avgBrixNumber;
	}

	public void setAvgBrixNumber(Double avgBrixNumber) {
		this.avgBrixNumber = avgBrixNumber;
	}

	public Double getRendemenEstimation() {
		return rendemenEstimation;
	}

	public void setRendemenEstimation(Double rendemenEstimation) {
		this.rendemenEstimation = rendemenEstimation;
	}

	public Double getKuHablurPerHa() {
		return kuHablurPerHa;
	}

	public void setKuHablurPerHa(Double kuHablurPerHa) {
		this.kuHablurPerHa = kuHablurPerHa;
	}

	public Double getAmountOfHablur() {
		return amountOfHablur;
	}

	public void setAmountOfHablur(Double amountOfHablur) {
		this.amountOfHablur = amountOfHablur;
	}

	public Double getAppraisement() {
		return appraisement;
	}

	public void setAppraisement(Double appraisement) {
		this.appraisement = appraisement;
	}
}
