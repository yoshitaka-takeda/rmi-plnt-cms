package id.co.indocyber.bmmRmiCms.dto;

import id.co.indocyber.bmmRmiCms.entity.MAddressType;
import id.co.indocyber.bmmRmiCms.entity.MEducationType;
import id.co.indocyber.bmmRmiCms.entity.MFarmFieldsType;
import id.co.indocyber.bmmRmiCms.entity.MFarmerType;
import id.co.indocyber.bmmRmiCms.entity.MFundType;
import id.co.indocyber.bmmRmiCms.entity.MGender;
import id.co.indocyber.bmmRmiCms.entity.MMaritalStatus;
import id.co.indocyber.bmmRmiCms.entity.MReligion;
import id.co.indocyber.bmmRmiCms.entity.MobFarmer;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.Period;

import org.springframework.util.StringUtils;

public class MobFarmerDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -3729405518216535120L;
	private Integer id;
	private Integer age;
	private String fullname;
	private Date birthday;
	private String placeOfBirthday;
	private MAddressType addressType;
	private String address;
	private String rtrw;
	private String village;
	private String subDistrict;
	private String district;
	private String city;
	private String province;
	private String zipcode;
	private String phoneNum;
	private String email;
	private MMaritalStatus status;
	private MGender gender;
	private MReligion religion;
	private MFarmFieldsType farmType;
	private MFarmerType farmerType;
	private MEducationType lastEducation;
	private Double fieldTotal;
	private MFundType fundType;
	private String businessPartnerCode;
	private String ktp;
	private String npwp;
	private String referenceCode;
	private Boolean isActive;
	private String processedBy;
	private Timestamp processedDate;
	private String modifiedBy;
	private Timestamp modifiedDate;

	
	
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getPlaceOfBirthday() {
		return placeOfBirthday;
	}
	public void setPlaceOfBirthday(String placeOfBirthday) {
		this.placeOfBirthday = placeOfBirthday;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getRtrw() {
		return rtrw;
	}
	public void setRtrw(String rtrw) {
		this.rtrw = rtrw;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public Double getFieldTotal() {
		return fieldTotal;
	}
	public void setFieldTotal(Double fieldTotal) {
		this.fieldTotal = fieldTotal;
	}
	public String getBusinessPartnerCode() {
		return businessPartnerCode;
	}
	public void setBusinessPartnerCode(String businessPartnerCode) {
		this.businessPartnerCode = businessPartnerCode;
	}
	public String getReferenceCode() {
		return referenceCode;
	}
	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getProcessedBy() {
		return processedBy;
	}
	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}
	public Timestamp getProcessedDate() {
		return processedDate;
	}
	public void setProcessedDate(Timestamp processedDate) {
		this.processedDate = processedDate;
	}
	public MAddressType getAddressType() {
		return addressType;
	}
	public void setAddressType(MAddressType addressType) {
		this.addressType = addressType;
	}
	public MMaritalStatus getStatus() {
		return status;
	}
	public void setStatus(MMaritalStatus status) {
		this.status = status;
	}
	public MGender getGender() {
		return gender;
	}
	public void setGender(MGender gender) {
		this.gender = gender;
	}
	public MReligion getReligion() {
		return religion;
	}
	public void setReligion(MReligion religion) {
		this.religion = religion;
	}
	public MFarmFieldsType getFarmType() {
		return farmType;
	}
	public void setFarmType(MFarmFieldsType farmType) {
		this.farmType = farmType;
	}
	public MFarmerType getFarmerType() {
		return farmerType;
	}
	public void setFarmerType(MFarmerType farmerType) {
		this.farmerType = farmerType;
	}
	public MEducationType getLastEducation() {
		return lastEducation;
	}
	public void setLastEducation(MEducationType lastEducation) {
		this.lastEducation = lastEducation;
	}
	public MFundType getFundType() {
		return fundType;
	}
	public void setFundType(MFundType fundType) {
		this.fundType = fundType;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getSubDistrict() {
		return subDistrict;
	}
	public void setSubDistrict(String subDistrict) {
		this.subDistrict = subDistrict;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getZipcode() {
		return zipcode;
	}
	public void setZipcode(String zipcode) {
		this.zipcode = zipcode;
	}
	public String getKtp() {
		return ktp;
	}
	public void setKtp(String ktp) {
		this.ktp = ktp;
	}
	public String getNpwp() {
		return npwp;
	}
	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	
	public MobFarmerDto(){
		
	}
	
	public MobFarmerDto(MobFarmer entity){
		this.address = entity.getAddress();
		this.birthday = entity.getBirthday();
		this.businessPartnerCode = entity.getBusinessPartnerCode();
		this.city = entity.getCity();
		this.email = entity.getEmail();
		this.fieldTotal = entity.getFieldTotal();
		this.fullname = entity.getFullname();
		this.id = entity.getId();
		this.isActive = entity.getIsActive();
		
		if(!StringUtils.isEmpty(entity.getPhoneNum()) && entity.getPhoneNum().startsWith("628"))
			this.phoneNum = entity.getPhoneNum().replace("628", "08");
		else this.phoneNum = entity.getPhoneNum();
		
		this.placeOfBirthday = entity.getPlaceOfBirthday();
		this.processedBy = entity.getProcessedBy();
		this.processedDate = entity.getProcessedDate();
		this.referenceCode = entity.getReferenceCode();
		this.rtrw = entity.getRtrw();
		this.village = entity.getVillage();
		this.subDistrict = entity.getSubDistrict();
		this.district = entity.getDistrict();
		this.province = entity.getProvince();
		this.zipcode = entity.getZipcode();
		this.ktp = entity.getKtp();
		this.npwp = entity.getNpwp();
		if(this.birthday!=null){
			this.age = Period.between(this.birthday.toLocalDate(), LocalDate.now()).getYears();
		}
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Timestamp getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
}
