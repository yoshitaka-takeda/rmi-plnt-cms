package id.co.indocyber.bmmRmiCms.dto;

import java.sql.Date;
import java.sql.Timestamp;

public class MobWeighbridgeResultDto {
	private Integer id;
	private Integer documentNum;
	private String documentType;
	private String mitraCode;
	private String mitraName;
	private String vehicleNo;
	private String vehicleType;
	private String driverId;
	private String driverName;
	private String farmerCode;
	private String farmerName;
	private String contractNo1;
	private String contractNo2;
	private String timeIn;
	private Date dateIn;
	private String timeOut;
	private Date dateOut;
	private String status;
	private String monitoringPos;
	private Double weighBruto;
	private Double weighNetto;
	private Double weighVehicle;
	private Double qtyWeigho;
	private Double rafaksi;
	private Double totalPaid;
	private Timestamp tsIn;
	private Timestamp tsOut;
	
	public Integer getId() {
		return id;
	}

	public void setId(Integer id) {
		this.id = id;
	}

	public Integer getDocumentNum() {
		return documentNum;
	}

	public void setDocumentNum(Integer documentNum) {
		this.documentNum = documentNum;
	}

	public String getDocumentType() {
		return documentType;
	}

	public void setDocumentType(String documentType) {
		this.documentType = documentType;
	}

	public String getMitraCode() {
		return mitraCode;
	}

	public void setMitraCode(String mitraCode) {
		this.mitraCode = mitraCode;
	}

	public String getMitraName() {
		return mitraName;
	}

	public void setMitraName(String mitraName) {
		this.mitraName = mitraName;
	}

	public String getVehicleNo() {
		return vehicleNo;
	}

	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}

	public String getVehicleType() {
		return vehicleType;
	}

	public void setVehicleType(String vehicleType) {
		this.vehicleType = vehicleType;
	}

	public String getDriverName() {
		return driverName;
	}

	public void setDriverName(String driverName) {
		this.driverName = driverName;
	}

	public String getFarmerCode() {
		return farmerCode;
	}

	public void setFarmerCode(String farmerCode) {
		this.farmerCode = farmerCode;
	}

	public String getFarmerName() {
		return farmerName;
	}

	public void setFarmerName(String farmerName) {
		this.farmerName = farmerName;
	}

	public String getContractNo1() {
		return contractNo1;
	}

	public void setContractNo1(String contractNo1) {
		this.contractNo1 = contractNo1;
	}

	public String getContractNo2() {
		return contractNo2;
	}

	public void setContractNo2(String contractNo2) {
		this.contractNo2 = contractNo2;
	}

	public String getTimeIn() {
		return timeIn;
	}

	public void setTimeIn(String timeIn) {
		this.timeIn = timeIn;
	}

	public Date getDateIn() {
		return dateIn;
	}

	public void setDateIn(Date dateIn) {
		this.dateIn = dateIn;
	}

	public String getTimeOut() {
		return timeOut;
	}

	public void setTimeOut(String timeOut) {
		this.timeOut = timeOut;
	}

	public Date getDateOut() {
		return dateOut;
	}

	public void setDateOut(Date dateOut) {
		this.dateOut = dateOut;
	}

	public String getStatus() {
		return status;
	}

	public void setStatus(String status) {
		this.status = status;
	}

	public String getMonitoringPos() {
		return monitoringPos;
	}

	public void setMonitoringPos(String monitoringPos) {
		this.monitoringPos = monitoringPos;
	}

	public Double getWeighBruto() {
		return weighBruto;
	}

	public void setWeighBruto(Double weighBruto) {
		this.weighBruto = weighBruto;
	}

	public Double getWeighNetto() {
		return weighNetto;
	}

	public void setWeighNetto(Double weighNetto) {
		this.weighNetto = weighNetto;
	}

	public Double getWeighVehicle() {
		return weighVehicle;
	}

	public void setWeighVehicle(Double weighVehicle) {
		this.weighVehicle = weighVehicle;
	}

	public Double getQtyWeigho() {
		return qtyWeigho;
	}

	public void setQtyWeigho(Double qtyWeigho) {
		this.qtyWeigho = qtyWeigho;
	}

	public Double getRafaksi() {
		return rafaksi;
	}

	public void setRafaksi(Double rafaksi) {
		this.rafaksi = rafaksi;
	}

	public Double getTotalPaid() {
		return totalPaid;
	}

	public void setTotalPaid(Double totalPaid) {
		this.totalPaid = totalPaid;
	}
	
	public Timestamp getTsIn() {
		return tsIn;
	}

	public void setTsIn(Timestamp tsIn) {
		this.tsIn = tsIn;
	}

	public Timestamp getTsOut() {
		return tsOut;
	}

	public void setTsOut(Timestamp tsOut) {
		this.tsOut = tsOut;
	}

	public String getDriverId() {
		return driverId;
	}

	public void setDriverId(String driverId) {
		this.driverId = driverId;
	}
}