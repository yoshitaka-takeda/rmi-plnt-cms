package id.co.indocyber.bmmRmiCms.dto;

import id.co.indocyber.bmmRmiCms.entity.MobFarm;
import id.co.indocyber.bmmRmiCms.entity.MobFarmer;
import id.co.indocyber.bmmRmiCms.entity.MobPaddyField;
import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitToFarmerHeader;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.List;

public class MobScheduleVisitToFarmerHeaderDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = -2378402841033477047L;
	private String visitNo;
	private Date visitDate;
	private Date visitExpiredDate;
	private MobFarmer farmer;
	private MobEmployeeDto assignedEmployee;
	private MobFarm farm;
	private List<MobPaddyField> paddyFields;
	private String notes;
	private Date estimateHarvestTime;
	private String planText;
	private String evaluation;
	private String taskActivity;
	private Boolean isActive;
	private String createdBy;
	private Timestamp createdDate;
	private String modifiedBy;
	private Timestamp modifiedDate;
	private Boolean adminDelegable;
	
	private Integer farmerId;
	private Integer assignTo;
	
	public String getVisitNo() {
		return visitNo;
	}

	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}

	public Date getVisitDate() {
		return visitDate;
	}

	public void setVisitDate(Date visitDate) {
		this.visitDate = visitDate;
	}

	public Date getVisitExpiredDate() {
		return visitExpiredDate;
	}

	public void setVisitExpiredDate(Date visitExpiredDate) {
		this.visitExpiredDate = visitExpiredDate;
	}

	public MobFarmer getFarmer() {
		return farmer;
	}

	public void setFarmer(MobFarmer farmer) {
		this.farmer = farmer;
	}

	public MobEmployeeDto getAssignedEmployee() {
		return assignedEmployee;
	}

	public void setAssignedEmployee(MobEmployeeDto assignedEmployee) {
		this.assignedEmployee = assignedEmployee;
	}

	public String getNotes() {
		return notes;
	}

	public void setNotes(String notes) {
		this.notes = notes;
	}

	public Date getEstimateHarvestTime() {
		return estimateHarvestTime;
	}

	public void setEstimateHarvestTime(Date estimateHarvestTime) {
		this.estimateHarvestTime = estimateHarvestTime;
	}

	public String getPlanText() {
		return planText;
	}

	public void setPlanText(String planText) {
		this.planText = planText;
	}

	public String getEvaluation() {
		return evaluation;
	}

	public void setEvaluation(String evaluation) {
		this.evaluation = evaluation;
	}

	public String getTaskActivity() {
		return taskActivity;
	}

	public void setTaskActivity(String taskActivity) {
		this.taskActivity = taskActivity;
	}

	public Boolean getIsActive() {
		return isActive;
	}

	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}

	public String getCreatedBy() {
		return createdBy;
	}

	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}

	public Timestamp getCreatedDate() {
		return createdDate;
	}

	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}

	public String getModifiedBy() {
		return modifiedBy;
	}

	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}

	public Timestamp getModifiedDate() {
		return modifiedDate;
	}

	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	public MobFarm getFarm() {
		return farm;
	}

	public void setFarm(MobFarm farm) {
		this.farm = farm;
	}

	public List<MobPaddyField> getPaddyFields() {
		return paddyFields;
	}

	public void setPaddyFields(List<MobPaddyField> paddyFields) {
		this.paddyFields = paddyFields;
	}

	public MobScheduleVisitToFarmerHeaderDto() {
		this.adminDelegable = true;
		this.paddyFields = new ArrayList<>();
	}
	
	public Integer getFarmerId() {
		return farmerId;
	}

	public void setFarmerId(Integer farmerId) {
		this.farmerId = farmerId;
	}

	public Integer getAssignTo() {
		return assignTo;
	}

	public void setAssignTo(Integer assignTo) {
		this.assignTo = assignTo;
	}
	
	public MobScheduleVisitToFarmerHeaderDto(MobScheduleVisitToFarmerHeader entity) {
		this.createdBy = entity.getCreatedBy();
		this.createdDate = entity.getCreatedDate();
		this.estimateHarvestTime = entity.getEstimateHarvestTime();
		this.evaluation = entity.getEvaluation();
		this.isActive = entity.getIsActive();
		this.modifiedBy = entity.getModifiedBy();
		this.modifiedDate = entity.getModifiedDate();
		this.notes = entity.getNotes();
		this.planText = entity.getPlanText();
		this.taskActivity = entity.getTaskActivity();
		this.visitDate = entity.getVisitDate();
		this.visitExpiredDate = entity.getVisitExpiredDate();
		this.visitNo = entity.getVisitNo();
		this.farmerId = entity.getFarmerId();
		this.assignTo = entity.getAssignTo();
	}

	public Boolean getAdminDelegable() {
		return adminDelegable;
	}

	public void setAdminDelegable(Boolean adminDelegable) {
		this.adminDelegable = adminDelegable;
	}
}
