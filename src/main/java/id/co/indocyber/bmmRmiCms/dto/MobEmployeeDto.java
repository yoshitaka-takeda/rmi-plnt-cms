package id.co.indocyber.bmmRmiCms.dto;

import id.co.indocyber.bmmRmiCms.entity.MAddressType;
import id.co.indocyber.bmmRmiCms.entity.MEducationType;
import id.co.indocyber.bmmRmiCms.entity.MGender;
import id.co.indocyber.bmmRmiCms.entity.MMaritalStatus;
import id.co.indocyber.bmmRmiCms.entity.MReligion;
import id.co.indocyber.bmmRmiCms.entity.MobEmployee;

import java.io.Serializable;
import java.sql.Date;
import java.sql.Timestamp;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDate;
import java.time.Period;

import org.springframework.util.StringUtils;

public class MobEmployeeDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 5668781588895580835L;
	private static SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	private Integer id;
	private String fullname;
	private Date birthday;
	private Integer age;
	private String placeOfBirthday;
	private MAddressType addressType;
	private String address;
	private String rtrw;
	private String village;
	private String subDistrict;
	private String district;
	private String city;
	private String province;
	private String zipCode;
	private String phoneNum;
	private String email;
	private MMaritalStatus maritalStatus;
	private MGender gender;
	private MReligion religion;
	private MEducationType lastEducation;
	private String nipeg;
	private Date jointDate;
	private Date endDate;
	private MobEmployee manager;
	private Integer prevManager;
	private String ktp;
	private String npwp;
	private String areaName;
	private String referenceCode;
	private Boolean isActive;
	private String createdBy;
	private Timestamp createdDate;
	private String modifiedBy;
	private Timestamp modifiedDate;
	private Integer intManager;
	
	private String employeePosition;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public Date getBirthday() {
		return birthday;
	}
	public void setBirthday(Date birthday) {
		this.birthday = birthday;
	}
	public String getPlaceOfBirthday() {
		return placeOfBirthday;
	}
	public void setPlaceOfBirthday(String placeOfBirthday) {
		this.placeOfBirthday = placeOfBirthday;
	}
	public MAddressType getAddressType() {
		return addressType;
	}
	public void setAddressType(MAddressType addressType) {
		this.addressType = addressType;
	}
	public String getAddress() {
		return address;
	}
	public void setAddress(String address) {
		this.address = address;
	}
	public String getRtrw() {
		return rtrw;
	}
	public void setRtrw(String rtrw) {
		this.rtrw = rtrw;
	}
	public String getVillage() {
		return village;
	}
	public void setVillage(String village) {
		this.village = village;
	}
	public String getSubDistrict() {
		return subDistrict;
	}
	public void setSubDistrict(String subDistrict) {
		this.subDistrict = subDistrict;
	}
	public String getDistrict() {
		return district;
	}
	public void setDistrict(String district) {
		this.district = district;
	}
	public String getCity() {
		return city;
	}
	public void setCity(String city) {
		this.city = city;
	}
	public String getProvince() {
		return province;
	}
	public void setProvince(String province) {
		this.province = province;
	}
	public String getZipCode() {
		return zipCode;
	}
	public void setZipCode(String zipCode) {
		this.zipCode = zipCode;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public MMaritalStatus getMaritalStatus() {
		return maritalStatus;
	}
	public void setMaritalStatus(MMaritalStatus maritalStatus) {
		this.maritalStatus = maritalStatus;
	}
	public MGender getGender() {
		return gender;
	}
	public void setGender(MGender gender) {
		this.gender = gender;
	}
	public MReligion getReligion() {
		return religion;
	}
	public void setReligion(MReligion religion) {
		this.religion = religion;
	}
	public MEducationType getLastEducation() {
		return lastEducation;
	}
	public void setLastEducation(MEducationType lastEducation) {
		this.lastEducation = lastEducation;
	}
	public String getNipeg() {
		return nipeg;
	}
	public void setNipeg(String nipeg) {
		this.nipeg = nipeg;
	}
	public Date getJointDate() {
		return jointDate;
	}
	public void setJointDate(Date jointDate) {
		this.jointDate = jointDate;
	}
	public Date getEndDate() {
		return endDate;
	}
	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}
	public MobEmployee getManager() {
		return manager;
	}
	public void setManager(MobEmployee manager) {
		this.manager = manager;
	}
	public String getKtp() {
		return ktp;
	}
	public void setKtp(String ktp) {
		this.ktp = ktp;
	}
	public String getNpwp() {
		return npwp;
	}
	public void setNpwp(String npwp) {
		this.npwp = npwp;
	}
	public String getReferenceCode() {
		return referenceCode;
	}
	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Timestamp getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public Integer getAge() {
		return age;
	}
	public void setAge(Integer age) {
		this.age = age;
	}
	public Integer getPrevManager() {
		return prevManager;
	}
	public void setPrevManager(Integer prevManager) {
		this.prevManager = prevManager;
	}
	
	public String getAreaName() {
		return areaName;
	}
	public void setAreaName(String areaName) {
		this.areaName = areaName;
	}
	public MobEmployeeDto() {
		try {
			this.endDate = new Date(sdf.parse("31-12-9999").getTime());
		} catch (ParseException e) {
			this.endDate = new Date(java.util.Date.from(Instant.now()).getTime());
		}
	}
	
	public MobEmployeeDto(MobEmployee entity) {
		this.address = entity.getAddress();
		this.birthday = entity.getBirthday();
		this.city = entity.getCity();
		this.createdBy = entity.getCreatedBy();
		this.createdDate = entity.getCreatedDate();
		this.district = entity.getDistrict();
		this.email = entity.getEmail();
		this.endDate = entity.getEndDate();
		this.fullname = entity.getFullname();
		this.id = entity.getId();
		this.jointDate = entity.getJointDate();
		this.ktp = entity.getKtp();
		this.modifiedBy = entity.getModifiedBy();
		this.modifiedDate = entity.getModifiedDate();
		this.nipeg = entity.getNipeg();
		this.npwp = entity.getNpwp();
		
		if(!StringUtils.isEmpty(entity.getPhoneNum()) && entity.getPhoneNum().startsWith("628"))
			this.phoneNum = entity.getPhoneNum().replace("628", "08");
		else this.phoneNum = entity.getPhoneNum();
		
		this.placeOfBirthday = entity.getPlaceOfBirthday();
		this.province = entity.getProvince();
		this.referenceCode = entity.getReferenceCode();
		this.rtrw = entity.getRtrw();
		this.subDistrict = entity.getSubDistrict();
		this.village = entity.getVillage();
		this.zipCode = entity.getZipCode();
		this.isActive = entity.getIsActive();
		this.areaName = entity.getAreaName();
		this.intManager = entity.getManager();
		if(this.birthday!=null){
			this.age = Period.between(this.birthday.toLocalDate(), LocalDate.now()).getYears();
		}
	}
	
	public void reCalculateAge(){
		if(this.birthday!=null){
			this.age = Period.between(this.birthday.toLocalDate(), LocalDate.now()).getYears();
		}
	}
	
	public void setJointDate(String strJoinDate) throws ParseException{
		this.jointDate = new Date(sdf.parse(strJoinDate).getTime());
	}
	public String getEmployeePosition() {
		return employeePosition;
	}
	public void setEmployeePosition(String employeePosition) {
		this.employeePosition = employeePosition;
	}
	
	@Override
	public int hashCode() {
		final int prime = 31;
		int result = 1;
		result = prime * result + ((id == null) ? 0 : id.hashCode());
		return result;
	}
	@Override
	public boolean equals(Object obj) {
		if (this == obj)
			return true;
		if (obj == null)
			return false;
		if (getClass() != obj.getClass())
			return false;
		MobEmployeeDto other = (MobEmployeeDto) obj;
		if (id == null) {
			if (other.id != null)
				return false;
		} else if (!id.equals(other.id))
			return false;
		return true;
	}
	
	public void setEndDate(String strEndDate) throws ParseException{
		this.endDate = new Date(sdf.parse(strEndDate).getTime());
	}
	
	public Integer getIntManager() {
		return intManager;
	}
	public void setIntManager(Integer intManager) {
		this.intManager = intManager;
	}
	
}