package id.co.indocyber.bmmRmiCms.dto;

import java.io.Serializable;

import org.springframework.util.StringUtils;

import id.co.indocyber.bmmRmiCms.entity.MobDriver;
import id.co.indocyber.bmmRmiCms.entity.MobEmployee;
import id.co.indocyber.bmmRmiCms.entity.MobFarmer;

public class MobIdentityDto implements Serializable{
	/**
	 * 
	 */
	private static final long serialVersionUID = -262603240508472562L;
	private Integer id;
	private String fullname;
	private String nipeg;
	private String referenceCode;
	private String phoneNum;
	private String email;
	private String desa;
	
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public String getFullname() {
		return fullname;
	}
	public void setFullname(String fullname) {
		this.fullname = fullname;
	}
	public String getNipeg() {
		return nipeg;
	}
	public void setNipeg(String nipeg) {
		this.nipeg = nipeg;
	}
	public String getReferenceCode() {
		return referenceCode;
	}
	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}
	public String getPhoneNum() {
		return phoneNum;
	}
	public void setPhoneNum(String phoneNum) {
		this.phoneNum = phoneNum;
	}
	public MobIdentityDto(){
		
	}
	
	public MobIdentityDto(MobEmployee emp){
		this.id = emp.getId();
		this.fullname = emp.getFullname();
		this.nipeg = emp.getNipeg();
		this.referenceCode = emp.getReferenceCode();
		if(!StringUtils.isEmpty(emp.getPhoneNum()) && emp.getPhoneNum().startsWith("628"))
			this.phoneNum = emp.getPhoneNum().replace("628", "08");
		else this.phoneNum = emp.getPhoneNum();
		this.email = emp.getEmail();
		this.desa = emp.getVillage();
	}
	
	public MobIdentityDto(MobFarmer farmer){
		this.id = farmer.getId();
		this.fullname = farmer.getFullname();
		this.nipeg = null;
		this.referenceCode = farmer.getReferenceCode();
		if(!StringUtils.isEmpty(farmer.getPhoneNum()) && farmer.getPhoneNum().startsWith("628"))
			this.phoneNum = farmer.getPhoneNum().replace("628", "08");
		else this.phoneNum = farmer.getPhoneNum();
		this.email = farmer.getEmail();
		this.desa = farmer.getVillage();
	}
	
	public MobIdentityDto(MobDriver drv){
		this.id = drv.getId();
		this.fullname = drv.getFullname();
		this.nipeg = null;
		this.referenceCode = drv.getReferenceCode();
		if(!StringUtils.isEmpty(drv.getPhoneNum()) && drv.getPhoneNum().startsWith("628"))
			this.phoneNum = drv.getPhoneNum().replace("628", "08");
		else this.phoneNum = drv.getPhoneNum();
		this.email = drv.getEmail();
		this.desa = drv.getVillage();
	}
	public String getEmail() {
		return email;
	}
	public void setEmail(String email) {
		this.email = email;
	}
	public String getDesa() {
		return desa;
	}
	public void setDesa(String desa) {
		this.desa = desa;
	}
	
}
