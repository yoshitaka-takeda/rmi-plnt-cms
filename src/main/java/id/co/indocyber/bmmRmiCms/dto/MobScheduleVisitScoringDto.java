package id.co.indocyber.bmmRmiCms.dto;

import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitScoring;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

public class MobScheduleVisitScoringDto implements Comparable<MobScheduleVisitScoringDto>{
	private String farmVille;
	private Date reportDate;
	private Double fieldArea;
	private Double weightSum;
	private Integer rank;
	private String taskStatus;
	private List<MobScheduleVisitScoring> reportDetails;
	
	public String getFarmVille() {
		return farmVille;
	}
	public void setFarmVille(String farmVille) {
		this.farmVille = farmVille;
	}
	public Date getReportDate() {
		return reportDate;
	}
	public void setReportDate(Date reportDate) {
		this.reportDate = reportDate;
	}
	public Double getFieldArea() {
		return fieldArea;
	}
	public void setFieldArea(Double fieldArea) {
		this.fieldArea = fieldArea;
	}
	public Double getWeightSum() {
		return weightSum;
	}
	public void setWeightSum(Double weightSum) {
		this.weightSum = weightSum;
	}
	public Integer getRank() {
		return rank;
	}
	public void setRank(Integer rank) {
		this.rank = rank;
	}
	public List<MobScheduleVisitScoring> getReportDetails() {
		return reportDetails;
	}
	public void setReportDetails(List<MobScheduleVisitScoring> reportDetails) {
		this.reportDetails = reportDetails;
	}
	public String getTaskStatus() {
		return taskStatus;
	}
	public void setTaskStatus(String taskStatus) {
		this.taskStatus = taskStatus;
	}
	
	public MobScheduleVisitScoringDto() {
		weightSum=0d;
		this.reportDetails = new ArrayList<MobScheduleVisitScoring>();
	}
	@Override
	public int compareTo(MobScheduleVisitScoringDto arg0) {
		Double s1 = weightSum == null ? 0d: weightSum;
		Double s2 = arg0.weightSum == null? 0d: arg0.weightSum;
		if(s1.compareTo(s2)<0)
			return 1;
		else if(s1.compareTo(s2)>0)
			return -1;
		else{
			Double f1 = fieldArea == null ? 0d: fieldArea;
			Double f2 = arg0.fieldArea == null? 0d: arg0.fieldArea;
			if(f1.compareTo(f2)<0)
				return 1;
			else return -1;
		}
	}
}
