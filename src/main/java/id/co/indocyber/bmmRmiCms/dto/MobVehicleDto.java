package id.co.indocyber.bmmRmiCms.dto;

import id.co.indocyber.bmmRmiCms.entity.MVehicleType;
import id.co.indocyber.bmmRmiCms.entity.MobVehicle;

import java.io.Serializable;
import java.sql.Timestamp;

public class MobVehicleDto implements Serializable {
	/**
	 * 
	 */
	private static final long serialVersionUID = 1441542878562539618L;
	private String vehicleNo;
	private MVehicleType vehicleType;
	private Double capacity;
	private String productionYear;
	private Boolean isActive;
	private String createdBy;
	private Timestamp createdDate;
	private String modifiedBy;
	private Timestamp modifiedDate;
	
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public MVehicleType getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(MVehicleType vehicleType) {
		this.vehicleType = vehicleType;
	}
	public Double getCapacity() {
		return capacity;
	}
	public void setCapacity(Double capacity) {
		this.capacity = capacity;
	}
	public String getProductionYear() {
		return productionYear;
	}
	public void setProductionYear(String productionYear) {
		this.productionYear = productionYear;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Timestamp getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	
	public MobVehicleDto() {
		this.isActive=true;
	}
	
	public MobVehicleDto(MobVehicle entity) {
		this.vehicleNo = entity.getVehicleNo();
		this.capacity = entity.getCapacity();
		this.productionYear = entity.getProductionYear();
		this.isActive = entity.getIsActive();
		this.createdBy = entity.getCreatedBy();
		this.createdDate = entity.getCreatedDate();
		this.modifiedBy = entity.getModifiedBy();
		this.modifiedDate = entity.getModifiedDate();
	}
}
