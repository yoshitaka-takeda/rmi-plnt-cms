package id.co.indocyber.bmmRmiCms.dto;

import java.sql.Timestamp;

public class MobScheduleVisitToFarmerHistoryDto {
	private String visitNo;
	private Integer farmerId;
	private Integer assignTo;
	private String assignedName;
	private Timestamp assignDate;
	private String taskAction;
	private Boolean isActive;
	private String createdBy;
	private Timestamp createdDate;
	
	public String getVisitNo() {
		return visitNo;
	}
	public void setVisitNo(String visitNo) {
		this.visitNo = visitNo;
	}
	public Integer getFarmerId() {
		return farmerId;
	}
	public void setFarmerId(Integer farmerId) {
		this.farmerId = farmerId;
	}
	public Integer getAssignTo() {
		return assignTo;
	}
	public void setAssignTo(Integer assignTo) {
		this.assignTo = assignTo;
	}
	public String getAssignedName() {
		return assignedName;
	}
	public void setAssignedName(String assignedName) {
		this.assignedName = assignedName;
	}
	public Timestamp getAssignDate() {
		return assignDate;
	}
	public void setAssignDate(Timestamp assignDate) {
		this.assignDate = assignDate;
	}
	public String getTaskAction() {
		return taskAction;
	}
	public void setTaskAction(String taskAction) {
		this.taskAction = taskAction;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
}
