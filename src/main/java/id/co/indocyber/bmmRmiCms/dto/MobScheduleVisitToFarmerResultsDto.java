package id.co.indocyber.bmmRmiCms.dto;

import id.co.indocyber.bmmRmiCms.entity.MobFarmer;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;
import java.util.Set;

public class MobScheduleVisitToFarmerResultsDto {
	private MobFarmer farmer;
	private String visitId;
	private String staffOnFarm;
	private String village;
	private Date visitDate;
	private Date visitEndDate;
	private Double latitude;
	private Double longitude;
	private Integer farmCount;
	private String taskActivity;
	private String coordString;
	private String notes;
	private List<MobScheduleVisitScoringDto> reports;
	private Set<Integer> visitDetailIds;

	public MobFarmer getFarmer() {
		return farmer;
	}
	public void setFarmer(MobFarmer farmer) {
		this.farmer = farmer;
	}
	public String getStaffOnFarm() {
		return staffOnFarm;
	}
	public void setStaffOnFarm(String staffOnFarm) {
		this.staffOnFarm = staffOnFarm;
	}
	public List<MobScheduleVisitScoringDto> getReports() {
		return reports;
	}
	public void setReports(List<MobScheduleVisitScoringDto> reports) {
		this.reports = reports;
	}
	public Date getVisitDate() {
		return visitDate;
	}
	public void setVisitDate(Date visitDate) {
		this.visitDate = visitDate;
	}
	public Date getVisitEndDate() {
		return visitEndDate;
	}
	public void setVisitEndDate(Date visitEndDate) {
		this.visitEndDate = visitEndDate;
	}
	public void setVillage(String string) {
		this.village = string;	
	}
	public String getVillage() {
		return village;
	}
	public Integer getFarmCount() {
		return farmCount;
	}
	public void setFarmCount(Integer farmCount) {
		this.farmCount = farmCount;
	}
	public MobScheduleVisitToFarmerResultsDto() {
		this.reports = new ArrayList<MobScheduleVisitScoringDto>();
	}
	public Set<Integer> getVisitDetailIds() {
		return visitDetailIds;
	}
	public void setVisitDetailIds(Set<Integer> visitDetailIds) {
		this.visitDetailIds = visitDetailIds;
	}
	public String getVisitId() {
		return visitId;
	}
	public void setVisitId(String visitId) {
		this.visitId = visitId;
	}
	public String getTaskActivity() {
		return taskActivity;
	}
	public void setTaskActivity(String taskActivity) {
		this.taskActivity = taskActivity;
	}
	public String getNotes() {
		return notes;
	}
	public void setNotes(String notes) {
		this.notes = notes;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public String getCoordString() {
		return coordString;
	}
	public void setCoordString(String coordString) {
		this.coordString = coordString;
	}
}
