package id.co.indocyber.bmmRmiCms.dto;

import id.co.indocyber.bmmRmiCms.entity.MVehicleType;
import id.co.indocyber.bmmRmiCms.entity.MobDriver;
import id.co.indocyber.bmmRmiCms.entity.MobFarmer;
import id.co.indocyber.bmmRmiCms.entity.MobSpta;

import java.sql.Date;
import java.sql.Timestamp;

public class MobSptaDto {
	private Integer sptaNum;
	private MobFarmer farmer;
	private Double qty;
	private String status;
	private String vehicleNo;
	private MobDriver driver;
	private MVehicleType vehicleType;
	private Date documentDate;
	private Date expiredDate;
	private Double rendemen;
	private Double brix;
	private Double pol;
	private String source;
	private Integer baseEntry;
	private Integer baseLine;
	private String referenceCode;
	private Boolean isActive;
	private String createdBy;
	private Timestamp createdDate;
	private String modifiedBy;
	private Timestamp modifiedDate;
	
	//-NEW-
	private String contractNo1;
	private String contractNo2;
	private String farmerType;
	private String farmCode;
	private String coopName;
	private String monitoringPos;
	private Double loadingSugarcaneLatitude;
	private Double loadingSugarcaneLongitude;
	private Timestamp loadingSugarcaneDate;
	private String loadingSugarcaneBy;
	private Double destinationLatitude;
	private Double destinationLongitude;
	private Timestamp destinationDate;
	private String destinationBy;
	private Double exitLatitude;
	private Double exitLongitude;
	private Timestamp exitDate;
	private String exitBy;
	private String reasonForCancel;
	
	public Integer getSptaNum() {
		return sptaNum;
	}
	public void setSptaNum(Integer sptaNum) {
		this.sptaNum = sptaNum;
	}
	public MobFarmer getFarmer() {
		return farmer;
	}
	public void setFarmer(MobFarmer farmer) {
		this.farmer = farmer;
	}
	public Double getQty() {
		return qty;
	}
	public void setQty(Double qty) {
		this.qty = qty;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getVehicleNo() {
		return vehicleNo;
	}
	public void setVehicleNo(String vehicleNo) {
		this.vehicleNo = vehicleNo;
	}
	public MobDriver getDriver() {
		return driver;
	}
	public void setDriver(MobDriver driver) {
		this.driver = driver;
	}
	public MVehicleType getVehicleType() {
		return vehicleType;
	}
	public void setVehicleType(MVehicleType vehicleType) {
		this.vehicleType = vehicleType;
	}
	public Date getDocumentDate() {
		return documentDate;
	}
	public void setDocumentDate(Date documentDate) {
		this.documentDate = documentDate;
	}
	public Date getExpiredDate() {
		return expiredDate;
	}
	public void setExpiredDate(Date expiredDate) {
		this.expiredDate = expiredDate;
	}
	public Double getRendemen() {
		return rendemen;
	}
	public void setRendemen(Double rendemen) {
		this.rendemen = rendemen;
	}
	public Double getBrix() {
		return brix;
	}
	public void setBrix(Double brix) {
		this.brix = brix;
	}
	public Double getPol() {
		return pol;
	}
	public void setPol(Double pol) {
		this.pol = pol;
	}
	public String getSource() {
		return source;
	}
	public void setSource(String source) {
		this.source = source;
	}
	public Integer getBaseEntry() {
		return baseEntry;
	}
	public void setBaseEntry(Integer baseEntry) {
		this.baseEntry = baseEntry;
	}
	public Integer getBaseLine() {
		return baseLine;
	}
	public void setBaseLine(Integer baseLine) {
		this.baseLine = baseLine;
	}
	public String getReferenceCode() {
		return referenceCode;
	}
	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getCreatedBy() {
		return createdBy;
	}
	public void setCreatedBy(String createdBy) {
		this.createdBy = createdBy;
	}
	public Timestamp getCreatedDate() {
		return createdDate;
	}
	public void setCreatedDate(Timestamp createdDate) {
		this.createdDate = createdDate;
	}
	public String getModifiedBy() {
		return modifiedBy;
	}
	public void setModifiedBy(String modifiedBy) {
		this.modifiedBy = modifiedBy;
	}
	public Timestamp getModifiedDate() {
		return modifiedDate;
	}
	public void setModifiedDate(Timestamp modifiedDate) {
		this.modifiedDate = modifiedDate;
	}
	public String getContractNo1() {
		return contractNo1;
	}
	public void setContractNo1(String contractNo1) {
		this.contractNo1 = contractNo1;
	}
	public String getContractNo2() {
		return contractNo2;
	}
	public void setContractNo2(String contractNo2) {
		this.contractNo2 = contractNo2;
	}
	public String getFarmerType() {
		return farmerType;
	}
	public void setFarmerType(String farmerType) {
		this.farmerType = farmerType;
	}
	public String getFarmCode() {
		return farmCode;
	}
	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}
	public String getCoopName() {
		return coopName;
	}
	public void setCoopName(String coopName) {
		this.coopName = coopName;
	}
	public String getMonitoringPos() {
		return monitoringPos;
	}
	public void setMonitoringPos(String monitoringPos) {
		this.monitoringPos = monitoringPos;
	}
	public Double getLoadingSugarcaneLatitude() {
		return loadingSugarcaneLatitude;
	}
	public void setLoadingSugarcaneLatitude(Double loadingSugarcaneLatitude) {
		this.loadingSugarcaneLatitude = loadingSugarcaneLatitude;
	}
	public Double getLoadingSugarcaneLongitude() {
		return loadingSugarcaneLongitude;
	}
	public void setLoadingSugarcaneLongitude(Double loadingSugarcaneLongitude) {
		this.loadingSugarcaneLongitude = loadingSugarcaneLongitude;
	}
	public Timestamp getLoadingSugarcaneDate() {
		return loadingSugarcaneDate;
	}
	public void setLoadingSugarcaneDate(Timestamp loadingSugarcaneDate) {
		this.loadingSugarcaneDate = loadingSugarcaneDate;
	}
	public String getLoadingSugarcaneBy() {
		return loadingSugarcaneBy;
	}
	public void setLoadingSugarcaneBy(String loadingSugarcaneBy) {
		this.loadingSugarcaneBy = loadingSugarcaneBy;
	}
	public Double getDestinationLatitude() {
		return destinationLatitude;
	}
	public void setDestinationLatitude(Double destinationLatitude) {
		this.destinationLatitude = destinationLatitude;
	}
	public Double getDestinationLongitude() {
		return destinationLongitude;
	}
	public void setDestinationLongitude(Double destinationLongitude) {
		this.destinationLongitude = destinationLongitude;
	}
	public Timestamp getDestinationDate() {
		return destinationDate;
	}
	public void setDestinationDate(Timestamp destinationDate) {
		this.destinationDate = destinationDate;
	}
	public String getDestinationBy() {
		return destinationBy;
	}
	public void setDestinationBy(String destinationBy) {
		this.destinationBy = destinationBy;
	}
	public Double getExitLatitude() {
		return exitLatitude;
	}
	public void setExitLatitude(Double exitLatitude) {
		this.exitLatitude = exitLatitude;
	}
	public Double getExitLongitude() {
		return exitLongitude;
	}
	public void setExitLongitude(Double exitLongitude) {
		this.exitLongitude = exitLongitude;
	}
	public Timestamp getExitDate() {
		return exitDate;
	}
	public void setExitDate(Timestamp exitDate) {
		this.exitDate = exitDate;
	}
	public String getExitBy() {
		return exitBy;
	}
	public void setExitBy(String exitBy) {
		this.exitBy = exitBy;
	}
	public String getReasonForCancel() {
		return reasonForCancel;
	}
	public void setReasonForCancel(String reasonForCancel) {
		this.reasonForCancel = reasonForCancel;
	}
	
	public MobSptaDto(MobSpta entity) {
		this.sptaNum = entity.getSptaNum();
		this.qty = entity.getQty();
		switch(entity.getStatus()){
		case "O":
			this.status = "Open";
			break;
		case "A":
			this.status = "Approved";
			break;
		case "L":
			this.status = "Batal";
			break;
		case "I":
			this.status = "In Weighbridge";
			break;
		case "T":
			this.status = "Tapping";
			break;
		case "M":
			this.status = "MBS";
			break;
		case "C":
			this.status = "Closed";
			break;
		case "E":
			this.status = "Expired";
			break;
		case "R":
			this.status = "Timbang Keluar";
			break;
		default:
			this.status = "?";
			break;
		}
		this.vehicleNo = entity.getVehicleNo();
		this.documentDate = entity.getDocumentDate();
		this.expiredDate = entity.getExpiredDate();
		this.rendemen = entity.getRendemen();
		this.brix = entity.getBrix();
		this.pol = entity.getPol();
		this.source = entity.getSource();
		this.baseEntry = entity.getBaseEntry();
		this.baseLine = entity.getBaseLine();
		this.referenceCode = entity.getReferenceCode();
		this.isActive = entity.getIsActive();
		this.createdBy = entity.getCreatedBy();
		this.createdDate = entity.getCreatedDate();
		this.modifiedBy = entity.getModifiedBy();
		this.modifiedDate = entity.getModifiedDate();
		
		this.contractNo1 = entity.getContractNo1();
		this.contractNo2 = entity.getContractNo2();
		this.farmerType = entity.getFarmerType();
		this.farmCode = entity.getFarmCode();
		this.coopName = entity.getCoopName();
		this.monitoringPos = entity.getMonitoringPos();
		this.loadingSugarcaneLatitude = entity.getLoadingSugarcaneLatitude();
		this.loadingSugarcaneLongitude = entity.getLoadingSugarcaneLongitude();
		this.loadingSugarcaneDate = entity.getLoadingSugarcaneDate();
		this.loadingSugarcaneBy = entity.getLoadingSugarcaneBy();
		this.destinationLatitude = entity.getDestinationLatitude();
		this.destinationLongitude = entity.getDestinationLongitude();
		this.destinationDate = entity.getDestinationDate();
		this.destinationBy = entity.getDestinationBy();
		this.exitLatitude = entity.getExitLatitude();
		this.exitLongitude = entity.getExitLongitude();
		this.exitDate = entity.getExitDate();
		this.exitBy = entity.getExitBy();
		this.reasonForCancel = entity.getReasonForCancel();
	}
}