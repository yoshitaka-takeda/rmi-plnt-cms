package id.co.indocyber.bmmRmiCms.dto;

import id.co.indocyber.bmmRmiCms.entity.MPlantingPeriod;
import id.co.indocyber.bmmRmiCms.entity.MSugarcaneType;
import id.co.indocyber.bmmRmiCms.entity.MobPaddyField;

import java.sql.Date;
import java.sql.Timestamp;

public class MobPaddyFieldDto {
	private Integer id;
	private Integer farmerId;
	private String farmCode;
	private String paddyFieldCode;
	private Integer lineNum;
	private Double fieldArea;
	private Double latitude;
	private Double longitude;
	private Double weighEstimation;
	private MSugarcaneType sugarcaneType;
	private String ageOfSugarcane;
	private Date plantingDate;
	private String sugarcaneSpecies;
	private Double sugarcaneScore;
	private Double sugarcaneQty;
	private MPlantingPeriod plantingPeriod;
	private String status;
	private String referenceCode;
	private Boolean isActive;
	private String processedBy;
	private Timestamp processedDate;
	public Integer getId() {
		return id;
	}
	public void setId(Integer id) {
		this.id = id;
	}
	public Integer getFarmerId() {
		return farmerId;
	}
	public void setFarmerId(Integer farmerId) {
		this.farmerId = farmerId;
	}
	public String getFarmCode() {
		return farmCode;
	}
	public void setFarmCode(String farmCode) {
		this.farmCode = farmCode;
	}
	public String getPaddyFieldCode() {
		return paddyFieldCode;
	}
	public void setPaddyFieldCode(String paddyFieldCode) {
		this.paddyFieldCode = paddyFieldCode;
	}
	public Integer getLineNum() {
		return lineNum;
	}
	public void setLineNum(Integer lineNum) {
		this.lineNum = lineNum;
	}
	public Double getFieldArea() {
		return fieldArea;
	}
	public void setFieldArea(Double fieldArea) {
		this.fieldArea = fieldArea;
	}
	public Double getLatitude() {
		return latitude;
	}
	public void setLatitude(Double latitude) {
		this.latitude = latitude;
	}
	public Double getLongitude() {
		return longitude;
	}
	public void setLongitude(Double longitude) {
		this.longitude = longitude;
	}
	public Double getWeighEstimation() {
		return weighEstimation;
	}
	public void setWeighEstimation(Double weighEstimation) {
		this.weighEstimation = weighEstimation;
	}
	public MSugarcaneType getSugarcaneType() {
		return sugarcaneType;
	}
	public void setSugarcaneType(MSugarcaneType sugarcaneType) {
		this.sugarcaneType = sugarcaneType;
	}
	public String getAgeOfSugarcane() {
		return ageOfSugarcane;
	}
	public void setAgeOfSugarcane(String ageOfSugarcane) {
		this.ageOfSugarcane = ageOfSugarcane;
	}
	public Date getPlantingDate() {
		return plantingDate;
	}
	public void setPlantingDate(Date plantingDate) {
		this.plantingDate = plantingDate;
	}
	public String getSugarcaneSpecies() {
		return sugarcaneSpecies;
	}
	public void setSugarcaneSpecies(String sugarcaneSpecies) {
		this.sugarcaneSpecies = sugarcaneSpecies;
	}
	public Double getSugarcaneScore() {
		return sugarcaneScore;
	}
	public void setSugarcaneScore(Double sugarcaneScore) {
		this.sugarcaneScore = sugarcaneScore;
	}
	public Double getSugarcaneQty() {
		return sugarcaneQty;
	}
	public void setSugarcaneQty(Double sugarcaneQty) {
		this.sugarcaneQty = sugarcaneQty;
	}
	public MPlantingPeriod getPlantingPeriod() {
		return plantingPeriod;
	}
	public void setPlantingPeriod(MPlantingPeriod plantingPeriod) {
		this.plantingPeriod = plantingPeriod;
	}
	public String getStatus() {
		return status;
	}
	public void setStatus(String status) {
		this.status = status;
	}
	public String getReferenceCode() {
		return referenceCode;
	}
	public void setReferenceCode(String referenceCode) {
		this.referenceCode = referenceCode;
	}
	public Boolean getIsActive() {
		return isActive;
	}
	public void setIsActive(Boolean isActive) {
		this.isActive = isActive;
	}
	public String getProcessedBy() {
		return processedBy;
	}
	public void setProcessedBy(String processedBy) {
		this.processedBy = processedBy;
	}
	public Timestamp getProcessedDate() {
		return processedDate;
	}
	public void setProcessedDate(Timestamp processedDate) {
		this.processedDate = processedDate;
	}
	
	public MobPaddyFieldDto(){
		
	}
	
	public MobPaddyFieldDto(MobPaddyField entity){
		this.ageOfSugarcane = entity.getAgeOfSugarcane();
		this.farmCode = entity.getFarmCode();
		this.farmerId = entity.getFarmerId();
		this.fieldArea = entity.getFieldArea();
		this.id = entity.getId();
		this.isActive = entity.getIsActive();
		this.latitude = entity.getLatitude();
		this.longitude = entity.getLongitude();
		this.lineNum = entity.getLineNum();
		this.paddyFieldCode = entity.getPaddyFieldCode();
		this.plantingDate = entity.getPlantingDate();
		this.processedBy = entity.getProcessedBy();
		this.processedDate = entity.getProcessedDate();
		this.referenceCode = entity.getReferenceCode();
		this.status = entity.getStatus();
		this.sugarcaneQty = entity.getSugarcaneQty();
		this.sugarcaneScore = entity.getSugarcaneScore();
		this.sugarcaneSpecies = entity.getSugarcaneSpecies();
		this.weighEstimation = entity.getWeighEstimation();
	}
}
