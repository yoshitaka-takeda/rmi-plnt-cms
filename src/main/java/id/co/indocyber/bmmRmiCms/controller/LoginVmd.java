package id.co.indocyber.bmmRmiCms.controller;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.entity.MRole;
import id.co.indocyber.bmmRmiCms.entity.MUser;
import id.co.indocyber.bmmRmiCms.entity.MUserRole;
import id.co.indocyber.bmmRmiCms.entity.UserMenuAccordion;
import id.co.indocyber.bmmRmiCms.service.MMenuRoleSvc;
import id.co.indocyber.bmmRmiCms.service.MRoleSvc;
import id.co.indocyber.bmmRmiCms.service.MUserRoleSvc;
import id.co.indocyber.bmmRmiCms.service.MUserSvc;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

import org.springframework.util.StringUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class LoginVmd extends BaseVmd{
	@WireVariable
	MUserSvc mUserSvc;
	
	@WireVariable
	MUserRoleSvc mUserRoleSvc;
	
	@WireVariable
	MRoleSvc mRoleSvc;

	@WireVariable
	MMenuRoleSvc mMenuRoleSvc;

	private String username;
	private String password;
	
	private boolean flagChangePassword;
	private boolean flagLogin = true;

	MUserDto user;
	List<MMenuRoleDto> rolePrivs;
	List<UserMenuAccordion> sidebarMenu;

	int maxWrong;
	int adminRole;
	Date today;
	
	private boolean flagShowPassOld =false;
	private boolean flagHidePassOld = true;

	private boolean flagShowPassNew =false;
	private boolean flagHidePassNew = true;

	private boolean flagShowPassRetype =false;
	private boolean flagHidePassRetype = true;

	private String typePassOld= "password";
	private String typePassNew= "password"; 
	private String typePassRetype = "password";
	
	//for change password
	private String cpUsername;
	private String cpOldPass;
	private String cpNewPass;
	private String cpRetypePass;
	private boolean retypePassConstraint;
	
	private String constraint;
	private String constraintStyle;
	private String constraintTooltip;
	public String getUsername() {
		return username;
	}

	public void setUsername(String username) {
		this.username = username;
	}

	public String getPassword() {
		return password;
	}

	public void setPassword(String password) {
		this.password = password;
	}

	@Init
	public void load() {
		
	}

	@Command("login")
	@NotifyChange({ "username", "password" })
	public void login() {
		if (!StringUtils.isEmpty(username) && !StringUtils.isEmpty(password)) {
			boolean existence;
			try {
				existence = mUserSvc.existence(username);
			} catch (Exception e) {
				existence = false;
			}
			if (existence) {
				user = new MUserDto(mUserSvc.findByUsernameOrEmail(username));
				if (user.getIsActive()) {
					if (mUserSvc.login(user.getUsername(), password, user.getPassword())) {
						MUserRole role = mUserRoleSvc.cmsAccess(user.getEmail());
						if (role!=null){
							MRole r2 = mRoleSvc.findOne(role.getRoleId());
							if (role.getIsActive()&&( r2==null ? false : r2.getIsActive() )) {
								rolePrivs = mMenuRoleSvc.getAccessPrivs(role.getRoleId(), "Web");
								sidebarMenu = new ArrayList<UserMenuAccordion>();
								for (MMenuRoleDto x : rolePrivs) {
									sidebarMenu.add(new UserMenuAccordion(x.getMenuName().toUpperCase()
										, x.getMenuLink()));
								}
								user.setMenuList(sidebarMenu);
								user.setRolePrivs(rolePrivs);
								user.setRoleName(r2.getRoleName());
								if(r2.getId().equals(4)){
									user.setAdmin(true);
								}
								Sessions.getCurrent().setAttribute("user",user);
								Executions.sendRedirect("/index.zul");
							} else {
								Clients.alert(StringConstants.MSG_ROLE_DISABLED, "Warning", "");
							}
						}
						else{
							Clients.alert(StringConstants.MSG_ROLE_UNAUTHORIZED, "Warning", "");
						}
					} else {
						Clients.alert(StringConstants.MSG_INVALID_PASSWORD, "Warning", "");
						
						/*
						int triesLeft = maxWrong - user.getCountInvalidPasswd() - 1;
						if (triesLeft > 0) {
							//Clients.alert(tbSysMessageSvc.getMessage("20003"),"Warning", "");
							
						} else {
							Clients.alert(tbSysMessageSvc.getMessage("20004"),"Warning", "");
						}
						*/
					}
				} else {
					Clients.alert(StringConstants.MSG_USER_DISABLED,"Warning", "");
				}
			} else {
				Clients.alert(StringConstants.MSG_USER_NOT_FOUND, "Warning", "");
			}
		} else {
			Clients.alert(StringConstants.MSG_LOGIN_FILL_ALL, "Warning", "");
		}
	}

	@Command("forgetPass")
	public void forgetPass() {
		Sessions.getCurrent().setAttribute("recover", new MUser());
		Executions.sendRedirect("/forgetPassword/forget1.zul");
	}
	
	@Command
	@NotifyChange({"flagChangePassword","flagLogin"})
	public void changePassForm(){
		setFlagChangePassword(true);
		setFlagLogin(false);
	}

	@Command
	@NotifyChange({"flagChangePassword","flagLogin"})
	public void changeLoginForm(){
		setFlagChangePassword(false);
		setFlagLogin(true);
	}
	
	@Command("changePassword")
	@NotifyChange({"flagIconWarning","msg","flagMsgFree"})
	public void changePassword(){
		try{
			if(mUserSvc.login(cpUsername, cpOldPass)==false){
				showErrorMsgBox("E099");
			}else{
				if(validatePass()){
					setFlagIconWarning(true);
					showConfirm("E098");
				}else{
					showWarningMsgBox("E097");
				}
			}
		}catch(Exception e){
			e.printStackTrace();
			showErrorMsgBox("E096");
		}
	}
	
	@Override
	@Command
	public void yesConfirm(){
		user = new MUserDto(mUserSvc.findByUsernameOrEmail(cpUsername));
		user.setPassword(cpNewPass);
		user.setRehash(true);
		try{
			mUserSvc.save(user);						
			showSuccessMsgBox("E095");
		}catch(Exception e){
			showErrorMsgBox("E096");
			e.printStackTrace();
		}
	}
	
	
	@Command
	@NotifyChange({"retypePassConstraint","constraint","constraintStyle","constraintTooltip","cpNewPass"})
	public void retypePass(@BindingParam("item")String retypePass){
//		String pass2 = StringUtils.isEmpty(pass)?cpNewPass:pass;
		cpNewPass = StringUtils.isEmpty(cpNewPass)?"":cpNewPass;
		cpRetypePass = retypePass;
		if(StringUtils.isEmpty(cpRetypePass)){
		}else{
			if(!cpNewPass.equalsIgnoreCase(cpRetypePass)||!cpRetypePass.equalsIgnoreCase(cpNewPass)){
				System.err.println("luthfi12 "+cpNewPass+" "+cpRetypePass);
				setConstraint("z-icon-times");
				setConstraintStyle("background:none; border:none; color: red; font-size: 16px;cursor: default;");
				setConstraintTooltip("Validasi gagal");
				setRetypePassConstraint(true);
			}else{
				System.err.println("luthfi13 "+cpNewPass+" "+cpRetypePass);
				setConstraint("z-icon-check");
				setConstraintStyle("background:none; border:none; color: green; font-size: 16px; cursor: default;");
				setConstraintTooltip("Validasi berhasil");
				setRetypePassConstraint(true);
			}			
		}
	}
	
	public boolean validatePass(){
		if(cpNewPass.equalsIgnoreCase(cpRetypePass)){
			return true;
		}else{
			return false;
		}
	}

	@Command
	@NotifyChange({"flagShowPassOld","flagHidePassOld","typePassOld"})
	public void showHidePassOld(){
		if(isFlagHidePassOld()){
			setFlagHidePassOld(false);
			setFlagShowPassOld(true);
			setTypePassOld("text");
		}else{
			setFlagHidePassOld(true);
			setFlagShowPassOld(false);			
			setTypePassOld("password");
		}
	}

	@Command
	@NotifyChange({"flagShowPassNew","flagHidePassNew","typePassNew"})
	public void showHidePassNew(){
		if(isFlagHidePassNew()){
			setFlagHidePassNew(false);
			setFlagShowPassNew(true);
			setTypePassNew("text");
		}else{
			setFlagHidePassNew(true);
			setFlagShowPassNew(false);			
			setTypePassNew("password");
		}
	}
	
	@Command
	@NotifyChange({"flagShowPassRetype","flagHidePassRetype","typePassRetype"})
	public void showHidePassRetype(){
		if(isFlagHidePassRetype()){
			setFlagHidePassRetype(false);
			setFlagShowPassRetype(true);
			setTypePassRetype("text");
		}else{
			setFlagHidePassRetype(true);
			setFlagShowPassRetype(false);			
			setTypePassRetype("password");
		}
	}

	public boolean isFlagChangePassword() {
		return flagChangePassword;
	}

	public void setFlagChangePassword(boolean flagChangePassword) {
		this.flagChangePassword = flagChangePassword;
	}

	public boolean isFlagLogin() {
		return flagLogin;
	}

	public void setFlagLogin(boolean flagLogin) {
		this.flagLogin = flagLogin;
	}

	public boolean isFlagShowPassOld() {
		return flagShowPassOld;
	}

	public void setFlagShowPassOld(boolean flagShowPassOld) {
		this.flagShowPassOld = flagShowPassOld;
	}

	public boolean isFlagHidePassOld() {
		return flagHidePassOld;
	}

	public void setFlagHidePassOld(boolean flagHidePassOld) {
		this.flagHidePassOld = flagHidePassOld;
	}

	public boolean isFlagShowPassNew() {
		return flagShowPassNew;
	}

	public void setFlagShowPassNew(boolean flagShowPassNew) {
		this.flagShowPassNew = flagShowPassNew;
	}

	public boolean isFlagHidePassNew() {
		return flagHidePassNew;
	}

	public void setFlagHidePassNew(boolean flagHidePassNew) {
		this.flagHidePassNew = flagHidePassNew;
	}

	public boolean isFlagShowPassRetype() {
		return flagShowPassRetype;
	}

	public void setFlagShowPassRetype(boolean flagShowPassRetype) {
		this.flagShowPassRetype = flagShowPassRetype;
	}

	public boolean isFlagHidePassRetype() {
		return flagHidePassRetype;
	}

	public void setFlagHidePassRetype(boolean flagHidePassRetype) {
		this.flagHidePassRetype = flagHidePassRetype;
	}

	public String getTypePassOld() {
		return typePassOld;
	}

	public void setTypePassOld(String typePassOld) {
		this.typePassOld = typePassOld;
	}

	public String getTypePassNew() {
		return typePassNew;
	}

	public void setTypePassNew(String typePassNew) {
		this.typePassNew = typePassNew;
	}

	public String getTypePassRetype() {
		return typePassRetype;
	}

	public void setTypePassRetype(String typePassRetype) {
		this.typePassRetype = typePassRetype;
	}

	public String getCpOldPass() {
		return cpOldPass;
	}

	public void setCpOldPass(String cpOldPass) {
		this.cpOldPass = cpOldPass;
	}

	public String getCpNewPass() {
		return cpNewPass;
	}

	public void setCpNewPass(String cpNewPass) {
		this.cpNewPass = cpNewPass;
	}

	public String getCpUsername() {
		return cpUsername;
	}

	public void setCpUsername(String cpUsername) {
		this.cpUsername = cpUsername;
	}

	public String getCpRetypePass() {
		return cpRetypePass;
	}

	public void setCpRetypePass(String cpRetypePass) {
		this.cpRetypePass = cpRetypePass;
	}

	public boolean isRetypePassConstraint() {
		return retypePassConstraint;
	}

	public void setRetypePassConstraint(boolean retypePassConstraint) {
		this.retypePassConstraint = retypePassConstraint;
	}

	public String getConstraint() {
		return constraint;
	}

	public void setConstraint(String constraint) {
		this.constraint = constraint;
	}

	public String getConstraintStyle() {
		return constraintStyle;
	}

	public void setConstraintStyle(String constraintStyle) {
		this.constraintStyle = constraintStyle;
	}

	public String getConstraintTooltip() {
		return constraintTooltip;
	}

	public void setConstraintTooltip(String constraintTooltip) {
		this.constraintTooltip = constraintTooltip;
	}

}
