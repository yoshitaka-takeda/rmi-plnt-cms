package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MobIdentityDto;
import id.co.indocyber.bmmRmiCms.entity.MRole;
import id.co.indocyber.bmmRmiCms.entity.MUserRole;
import id.co.indocyber.bmmRmiCms.service.MRoleSvc;
import id.co.indocyber.bmmRmiCms.service.MUserRoleSvc;
import id.co.indocyber.bmmRmiCms.service.MUserSvc;
import id.co.indocyber.bmmRmiCms.service.MobIdentitySvc;
import id.co.indocyber.bmmRmiCms.tools.DeepCopy;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Textbox;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class UMUserVmd extends BaseVmd {
	@WireVariable
	MUserSvc mUserSvc;

	@WireVariable
	MRoleSvc mRoleSvc;
	
	@WireVariable
	MUserRoleSvc mUserRoleSvc;
	
	@WireVariable
	MobIdentitySvc mobIdentitySvc;
	
	String urlIndex = "/userManagement/user/index.zul";
	String urlEdit = "/userManagement/user/edit.zul";
	String refresh = "/userManagement/user/ref.zul";

	private String prevPass;

	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;

	private MUserDto user, editUser, selectedUser = new MUserDto();
	private MRole role;
	private MobIdentityDto identity, prevIdentity;
	private String module;
	private List<String> moduleList;
	private List<MUserDto> userList, userListTemp;
	private List<MRole> roleList, fRoleList = new ArrayList<>();
	private List<MobIdentityDto> idList, fIdList = new ArrayList<>();
	private int noUrut = 1;

	private boolean freezeId, freezeProfile;
	private boolean flagIsActive = true;
	private int editIndex = -1, prevRole = 0;
	private Boolean editMode;
	private String pageTitle;
	
	
	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private boolean superAdmin;
	private Integer recordCount = 1;
	private String arg, sArg;
	
	private String sortBy = "";
	private boolean sortAsc = false;
	
	private String passUser;

	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public MUserDto getEditUser() {
		return editUser;
	}

	public void setEditUser(MUserDto editUser) {
		this.editUser = editUser;
	}

	public MRole getRole() {
		return role;
	}

	public void setRole(MRole role) {
		this.role = role;
	}

	public List<MUserDto> getUserList() {
		return userList;
	}

	public void setUserList(List<MUserDto> userList) {
		this.userList = userList;
	}

	public List<MRole> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<MRole> roleList) {
		this.roleList = roleList;
	}

	public List<MRole> getfRoleList() {
		return fRoleList;
	}

	public void setfRoleList(List<MRole> fRoleList) {
		this.fRoleList = fRoleList;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public boolean isSuperAdmin() {
		return superAdmin;
	}

	public void setSuperAdmin(boolean superAdmin) {
		this.superAdmin = superAdmin;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public String getArg() {
		return arg;
	}

	public void setArg(String arg) {
		this.arg = arg;
	}

	public List<MobIdentityDto> getfIdList() {
		return fIdList;
	}

	public void setfIdList(List<MobIdentityDto> fIdList) {
		this.fIdList = fIdList;
	}
	
	public MobIdentityDto getIdentity() {
		return identity;
	}

	public void setIdentity(MobIdentityDto identity) {
		this.identity = identity;
	}
	
	public String getModule() {
		return module;
	}

	public void setModule(String module) {
		this.module = module;
	}

	public List<String> getModuleList() {
		return moduleList;
	}

	public void setModuleList(List<String> moduleList) {
		this.moduleList = moduleList;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	@Init
	@NotifyChange({ "btnAdd", "btnDel", "btnEdit", "btnDl","arg"})
	public void load() {
		try{			
			editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
			user = (MUserDto) Sessions.getCurrent().getAttribute("user");
			roleList = mRoleSvc.findAll();
//			userListTemp = mUserSvc.findAll(pageNo, "modifiedDate", false);
			userList = mUserSvc.findAll(pageNo, "modifiedDate", false);
			recordCount = mUserSvc.countAll();
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
			
			moduleList = Arrays.asList(new String[]{"Mobile","Web"});

			for (MMenuRoleDto x : user.getRolePrivs()) {
				if ("UM-1".equals(x.getMenuCode())) {
					btnAdd = !x.getIsAdd();
					btnDel = !x.getIsDelete();
					btnDl = !x.getIsDownload();
					btnEdit = !x.getIsEdit();
					break;
				}
			}
			arg = null;	
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command
	@NotifyChange("selectedUser")
	public void ubahStatusAktif(@BindingParam("dto") MUserDto dto){
		System.err.println("password = "+dto.getPassword());
		showConfirm("E002");
		setFlagIconWarning(true);
		selectedUser = new MUserDto();
		selectedUser = dto;
		selectedUser.setRehash(false);
		selectedUser.setIsActive(false);
		selectedUser.setModifiedBy(user.getFullname());
		selectedUser.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange("selectedUser")
	public void ubahStatusNonAktif(@BindingParam("dto") MUserDto dto){
		System.err.println("password = "+dto.getPassword());
		showConfirm("E003");
		setFlagIconWarning(true);
		selectedUser = new MUserDto();
		selectedUser = dto;
		selectedUser.setRehash(false);
		selectedUser.setIsActive(true);
		selectedUser.setModifiedBy(user.getFullname());
		selectedUser.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Override
	@Command
	@NotifyChange({"recordCount","userList","maxPage"})
	public void yesConfirm(){
		try{
			mUserSvc.save(selectedUser);
			flagConfirmMsg = false;
			showSuccessMsgBox("E000");
			//reload list
			if (search) {
				recordCount = mUserSvc.countByArg(sArg);
				userList = mUserSvc.findByArg(sArg, pageNo, 
						StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			} else {
				recordCount = mUserSvc.countAll();
				userList = mUserSvc.findAll(pageNo, 
						StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			}
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
			BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command
	@NotifyChange("selectedUser")
	public void clearImei(@BindingParam("dto") MUserDto dto){
		if(dto.getImei() != null){
			showConfirm("E007");
			selectedUser = (MUserDto)DeepCopy.deepCopy(dto);
			selectedUser.setPassword("rmi2019");
			selectedUser.setRehash(true);
			selectedUser.setImei(null);
			selectedUser.setModifiedBy(user.getFullname());
			selectedUser.setModifiedDate(new Timestamp(System.currentTimeMillis()));
		}else{
			showWarningMsgBox("E008");
		}
		
	}
	
	@Command("back")
	public void back() {
		if (activeCheck()) {
			editMode = false;
		}
	}

	@Command("add")
	@NotifyChange({"editMode","editUser","freezeId","identity","role","fRoleList",
		"fIdList","module","fRoleList","fIdList","freezeProfile","flagIsActive","passUser"})
	public void add() {
		if(activeCheck()){
			editMode = true;
			editUser = new MUserDto();
			passUser = "";
			editIndex = -1;
			prevPass = null;
			flagIsActive = true;
			
			freezeId = false;
			freezeProfile = true;
			
			identity = null;
			prevIdentity = null;
			
			role = null;
			prevRole = 0;
			
			module = null;
			fRoleList.clear();
			fIdList.clear();
			
			if(roleList==null){
				roleList = mRoleSvc.findAll();
			}
			
		}
	}

	@Command("edit")
	@NotifyChange({"editMode","editUser","freezeId","freezeProfile",
		"identity","role","fRoleList","fIdList","module","flagIsActive","passUser"})
	public void edit(@BindingParam("item") MUserDto edit) {
		if(activeCheck()){
			if (edit.getUsername() == null)
				Messagebox.show("Pilih data yang akan dirubah!", "Message", 0, null);
			else {
				editMode = true;
				flagIsActive = false;
				editUser = (MUserDto)DeepCopy.deepCopy(edit);
				passUser = edit.getPassword();
				editIndex = userList.indexOf(edit);
				prevPass = edit.getPassword();
				
				MUserRole ur = mUserRoleSvc.findUserRole(edit.getEmail());
				freezeId = true;
				freezeProfile = false;
				
				role = mRoleSvc.findOne(ur.getRoleId());
				prevRole = ur.getRoleId();
				
				identity = mobIdentitySvc.findIdentity(role.getId(), edit.getPersonId());
				prevIdentity = (MobIdentityDto) DeepCopy.deepCopy(identity);
				
				newIdentity(null);
				
				fRoleList.clear();
				fIdList.clear();
				module = ur.getModuleApps();
				
				if(roleList==null){
					roleList = mRoleSvc.findAll();
				}
			}
		}
	}
	
	@Command("save")
	@NotifyChange({"editUser","maxPage","recordCount","userList","editMode",
		"btnAdd", "btnDel", "btnEdit", "btnDl","arg","roleList", "flagIsActive"})
	public void save(@BindingParam("btn")Component btn) {
		if(activeCheck()){
			if(!StringUtils.isEmpty(getPassUser()) && !getPassUser().equals(
					StringUtils.isEmpty(editUser.getPassword())?"":editUser.getPassword())){
				editUser.setPassword(getPassUser());
				editUser.setRehash(true);
			}
			// Database design is not final, but one does what one can...
			boolean existence;
			try {
				existence = mUserSvc.existence(editUser.getUsername());
			} catch (Exception e) {
				existence = false;
			}
			String fullErrMsg = StringConstants.MSG_FORM_TEMPLATE;
			int error = 0;
			if (StringUtils.isEmpty(editUser.getEmail())) {
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Email");
			} else if (!(editUser.getEmail()
					.matches("[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})"))) {
				error++;
				fullErrMsg += StringConstants.MSG_FORM_EMAIL_INVALID;
			} else if (mUserSvc.existsEmail(editUser.getUsername(), editUser.getEmail())) {
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_ID_EXISTS, "Email User");
			}
			if (StringUtils.isEmpty(editUser.getUsername())) {
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "User Id");
			} else if (!editUser.getUsername().matches("[a-zA-Z0-9_.]+")) {
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_ILLEGAL_CHAR, "User Id");
			} else if (existence && editUser.getCreatedDate()==null) {
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_ID_EXISTS, "User Id");
			}
			if (role == null) {
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Role");
			}
			if(identity==null) {
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Profil");
			}
			if (StringUtils.isEmpty(editUser.getPassword())) {
				error++;
				fullErrMsg += String.format(
						StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Password");
			}
			if (!StringUtils.isEmpty(editUser.getPassword()) && 
					(prevPass == null || !editUser.getPassword().equals(prevPass))) {
				if (!StringUtils.isEmpty(editUser.getUsername())
						&& editUser.getPassword().compareTo(editUser.getUsername()) == 0) {
					error++;
					fullErrMsg += StringConstants.MSG_FORM_PASSWORD_EQUALS_USERNAME;
				}
			}
			if(role!=null && !role.getId().equals(2)){
				if(StringUtils.isEmpty(editUser.getPhoneNum())) {
					error++;
					fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Mobile Phone Number");
				} else if (!editUser.getPhoneNum().matches("[0-9]+") || 
						!( editUser.getPhoneNum().startsWith("08") ||editUser.getPhoneNum().startsWith("628") )) {
					error++;
					fullErrMsg += String.format(StringConstants.MSG_FORM_PHONE_NUMERIC_INVALID, 
							"Mobile Phone Number");
				} else if (mUserSvc.existsMobile(editUser.getUsername(),editUser.getPhoneNum())) {
					error++;
					fullErrMsg += String.format(StringConstants.MSG_FORM_ID_EXISTS, "Mobile Phone User");
				}
			}
			if(StringUtils.isEmpty(module)){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Module");
			}
			if (error > 0) {
				Messagebox.show(fullErrMsg, "Message", 0, null);
			} else {
				editUser.setRoleName(role.getRoleName());
				if(identity!=null){
					editUser.setNipeg(identity.getNipeg());
					editUser.setPersonId(identity.getId());
					editUser.setFullname(identity.getFullname());
					//editUser.setPrevEmail(editUser.getEmail());
					//editUser.setReferenceCode(identity.getReferenceCode()); <- some farmers have long reference codes
				}
				
				if (prevPass == null || !editUser.getPassword().equals(prevPass)) {
					editUser.setRehash(true);
				}
				
				if(editUser.getCreatedDate()==null){
					editUser.setCreatedBy(user.getUsername());
					editUser.setCreatedDate(Timestamp.from(
							OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
					
					if(userList.size()==25){
						userList.remove(24); 
						//userList.add(0,editUser);
					}
					//userList.add(0,editUser);
					recordCount++;
					maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
				}
				editUser.setModifiedBy(user.getUsername());
				editUser.setModifiedDate(Timestamp.from(
						OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
				
				if(editUser.getPrevEmail()!=null && !editUser.getPrevEmail().equals(editUser.getEmail())){
					MUserRole uR = mUserRoleSvc.findUserRole(editUser.getPrevEmail());
					mUserRoleSvc.remove(uR);
				}
				
				mUserSvc.save(editUser);
				mobIdentitySvc.updateIdentity(role.getId(), 
						identity.getId(), editUser.getPhoneNum());
				
				MUserRole uR = mUserRoleSvc.findUserRole(editUser.getEmail());
				if(uR==null){
					uR = new MUserRole();
					uR.setIsActive(editUser.getIsActive());
					uR.setCreatedBy(user.getUsername());
					uR.setCreatedDate(Timestamp.from(
						OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
				}
				else if(!uR.getRoleId().equals(role.getId()) || !uR.getModuleApps().equals(module))
				{
					mUserRoleSvc.remove(uR);
				}
				uR.setEmail(editUser.getEmail());
				uR.setModuleApps(module);
				uR.setRoleId(role.getId());
				uR.setModifiedBy(user.getUsername());
				uR.setModifiedDate(Timestamp.from(
						OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
				
				mUserRoleSvc.save(uR);
				
				editMode = false;
				flagIsActive = true;
				load();
				if(editIndex==-1){
					showSuccessMsgBox("E005");
				}
				else{
					showSuccessMsgBox("E100");
				}
			}
		}
	}

	@Command("src")
	@NotifyChange({ "arg", "userList", "pageNo", "maxPage", "recordCount","sArg"})
	public void search() {
		if(activeCheck()){
			search = true;
			pageNo = 1;
			sArg = String.valueOf(StringUtils.isEmpty(arg)?"":arg);
			userList = mUserSvc.findByArg(sArg, pageNo,
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			recordCount = mUserSvc.countByArg(sArg);
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
			arg="";
		}
	}
	
	@Command("del")
	public void delete(@BindingParam("item") final MUserDto del) {
		if(activeCheck()){
			if (del.getUsername() == null)
				Messagebox.show("Pilih data yang akan dihapus", "Message", 0, null);
			else {
				if (!btnDel) {
					Messagebox.show(
						"Apakah anda ingin menonaktifkan user "+del.getUsername()+" ?", "Perhatian",
						new Button[] { Button.YES, Button.NO }, Messagebox.QUESTION, Button.NO,
						new EventListener<Messagebox.ClickEvent>() {
							@Override
							public void onEvent(ClickEvent event)throws Exception {
								if (Messagebox.ON_YES.equals(event.getName())) {
									if(activeCheck()){
										mUserSvc.disable(del.getUsername());
										userList.get(userList.indexOf(del)).setIsActive(false);
										BindUtils.postNotifyChange(null, null, UMUserVmd.this, "userList");
										Clients.showNotification(del.getUsername()+ " berhasil dinonaktifkan",
											Clients.NOTIFICATION_TYPE_INFO, null, null, 5000);
									}
								}
							}
						}
					);
				} else {
					Clients.alert(StringConstants.MSG_NO_DEL_ACCESS, "Warning", "");
				}
			}
		}
	}

	@Command("switchPage")
	@NotifyChange({ "pageNo", "userList", "resetUser","userListTemp","noUrut"})
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}

			if (search) {
				userList = mUserSvc.findByArg(sArg, pageNo,
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			} else {
				userList = mUserSvc.findAll(pageNo, 
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			}
		}
	}

	@Command("reload")
	@NotifyChange({"recordCount","userList","maxPage"})
	public void reload(){
		if (search) {
			recordCount = mUserSvc.countByArg(sArg);
			userList = mUserSvc.findByArg(sArg, pageNo, 
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
		} else {
			recordCount = mUserSvc.countAll();
			userList = mUserSvc.findAll(pageNo, 
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
		}
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
	}
	
	@Command("sort")
	@NotifyChange({"userList"})
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}

			if (search) {
				userList = mUserSvc.findByArg(sArg, pageNo, sortBy, sortAsc);
			} else {
				userList = mUserSvc.findAll(pageNo, sortBy, sortAsc);
			}
		}
	}
	
	@Command("refListR")
	@NotifyChange("fRoleList")
	public void refListR(@BindingParam("src") String src) {
		fRoleList.clear();
		src = StringUtils.isEmpty(src)? "":src;
		for (MRole x : roleList) {
			if (x.getRoleName().toLowerCase().contains(src.toLowerCase())) {
				fRoleList.add(x);
			}
		}
	}
	
	@Command("refListId")
	@NotifyChange("fIdList")
	public void refListE(@BindingParam("src") String src) {
		fIdList.clear();
		src = StringUtils.isEmpty(src)? "":src;
		for (MobIdentityDto x : idList) {
			if (x.getFullname().toLowerCase().contains(src.toLowerCase())||
					(StringUtils.isEmpty(x.getReferenceCode())?"":x.getReferenceCode()).contains(src.toLowerCase())||
					(StringUtils.isEmpty(x.getDesa())?"":x.getDesa()).toLowerCase().contains(src.toLowerCase())) {
				fIdList.add(x);
			}
		}
	}
	
	@Command("newIdentity")
	@NotifyChange({"fIdList","freezeProfile","identity"})
	public void newIdentity(@BindingParam("bb")Component bb){
		idList = mobIdentitySvc.getIdentities(role.getId());
		
		if(prevIdentity!=null){
			switch(role.getId()){
			case 2:
			case 3:
				if(role.getId().equals(prevRole)){
					idList.add(prevIdentity);
				}
				break;
			default:
				if(prevRole!=2 && prevRole!=3){
					idList.add(prevIdentity);
				}
			}
		}
		
		fIdList.clear();
		
		if(role.getRoleName() != null){
			freezeProfile = false;
		}else{
			freezeProfile = true;
		}
		
		if(bb!=null){
			identity = null;
			((Bandbox)bb.getParent().getParent().getParent()).close();
		}
	}
		
	@Command
	public void loadListRole(@BindingParam("tb")Textbox x1){
		fRoleList.clear();
		fRoleList.addAll(roleList);
		x1.focus();
		BindUtils.postNotifyChange(null, null, this, "fRoleList");
	}
	
	@Command
	public void loadListProfile(@BindingParam("tb")Textbox x2){
		fIdList.clear();
		fIdList.addAll(idList);
		x2.focus();
		BindUtils.postNotifyChange(null, null, this, "fIdList");
	}
	
	@Command
	@NotifyChange({"editUser","passUser","module"})
	public void syncCredentials(@BindingParam("bb")Bandbox bb){
		editUser.setEmail(identity.getEmail());
		editUser.setPhoneNum(identity.getPhoneNum());
		passUser = "rmi2019";
		if(!role.getId().equals(2)&&!role.getId().equals(3)) module = "Web";
		else module = "Mobile";
		bb.close();
	}

	public boolean isFreezeProfile() {
		return freezeProfile;
	}

	public void setFreezeProfile(boolean freezeProfile) {
		this.freezeProfile = freezeProfile;
	}

	public MUserDto getSelectedUser() {
		return selectedUser;
	}

	public void setSelectedUser(MUserDto selectedUser) {
		this.selectedUser = selectedUser;
	}

	public boolean isFlagIsActive() {
		return flagIsActive;
	}

	public void setFlagIsActive(boolean flagIsActive) {
		this.flagIsActive = flagIsActive;
	}

	public String getPassUser() {
		return passUser;
	}

	public void setPassUser(String passUser) {
		this.passUser = passUser;
	}

	public List<MUserDto> getUserListTemp() {
		return userListTemp;
	}

	public void setUserListTemp(List<MUserDto> userListTemp) {
		this.userListTemp = userListTemp;
	}

	public int getNoUrut() {
		return noUrut;
	}

	public void setNoUrut(int noUrut) {
		this.noUrut = noUrut;
	}


}
