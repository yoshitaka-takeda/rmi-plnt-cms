package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MobWeighbridgeResultDto;
import id.co.indocyber.bmmRmiCms.service.MobWeighbridgeResultSvc;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.util.StringUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ReportTimbangVmd extends BaseVmd {
	@WireVariable
	MobWeighbridgeResultSvc mobWeighbridgeResultSvc;

	String urlIndex = "/hasiltimbang/index.zul";
	String refresh = "/hasiltimbang/ref.zul";
	
	private MUserDto user;

	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;

	private MobWeighbridgeResultDto viewWeighResult;
	private List<MobWeighbridgeResultDto> weighResultList;

	private boolean freezeId;
	private Boolean editMode;
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private Integer recordCount = 1;
	private String arg,sArg;
	
	private String sortBy = "";
	private boolean sortAsc = true;

	private Date startDate, sStart, endDate, sEnd;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	
	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public MobWeighbridgeResultDto getViewWeighResult() {
		return viewWeighResult;
	}

	public void setViewWeighResult(MobWeighbridgeResultDto viewWeighResult) {
		this.viewWeighResult = viewWeighResult;
	}

	public List<MobWeighbridgeResultDto> getWeighResultList() {
		return weighResultList;
	}

	public void setWeighResultList(List<MobWeighbridgeResultDto> weighResultList) {
		this.weighResultList = weighResultList;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public String getArg() {
		return arg;
	}

	public void setArg(String arg) {
		this.arg = arg;
	}
	
	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Init
	@NotifyChange({ "btnAdd", "btnDel", "btnEdit", "btnDl" })
	public void load() {
		editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
		user = (MUserDto) Sessions.getCurrent().getAttribute("user");
		//superAdmin = (Integer) Sessions.getCurrent().getAttribute("superAdmin") == 1;
		weighResultList = mobWeighbridgeResultSvc.findAll(pageNo, "id", true);
		recordCount = mobWeighbridgeResultSvc.countAll();
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);

		startDate = new Date(new java.util.Date().getTime());
		endDate = new Date(startDate.getTime());
		
		for (MMenuRoleDto x : user.getRolePrivs()) {
			if ("SP-2".equals(x.getMenuCode())) {
				btnAdd = !x.getIsAdd();
				btnDel = !x.getIsDelete();
				btnDl = !x.getIsDownload();
				btnEdit = !x.getIsEdit();
				break;
			}
		}
	}

	@Command("back")
	public void back() {
		if (activeCheck()) {
			editMode=false;
		}
	}

	@Command("view")
	@NotifyChange({"editMode","viewWeighResult"})
	public void edit(@BindingParam("item") MobWeighbridgeResultDto edit) {
		if(activeCheck()){
			if (edit.getDocumentNum() == null)
				Messagebox.show("Pilih data yang akan dilihat!", "Message", 0, null);
			else {
				editMode = true;
				viewWeighResult = edit;
			}
		}
	}

	/*
	private boolean validatePassword(String passwd) {
		int chr = 0, spec = 0, digit = 0, length;
		length = passwd.length();
		for (char y : passwd.toCharArray()) {
			if (Character.isAlphabetic(y)) {
				chr++;
			} else if (Character.isDigit(y)) {
				digit++;
			} else
				spec++;
		}
		return chr >= minChar && spec >= minSpec && digit >= minDigit
				&& length >= minLength;
	}
	*/

	@Command("src")
	@NotifyChange({ "arg", "weighResultList", "pageNo", "maxPage", "recordCount"})
	public void search() {
		if(activeCheck()){
			search = true;
			pageNo = 1;
			
			sStart = Date.valueOf(startDate.toLocalDate());
			sEnd = Date.valueOf(endDate.toLocalDate());
			
			sArg = String.valueOf(StringUtils.isEmpty(arg)?"":arg);
			weighResultList = mobWeighbridgeResultSvc.findByArg(sArg, sStart, sEnd, pageNo,
					StringUtils.isEmpty(sortBy) ? "id" : sortBy, sortAsc);
			recordCount = mobWeighbridgeResultSvc.countByArg(sArg, sStart, sEnd);
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
			arg = "";
		}
	}

	@Command("reload")
	@NotifyChange({"recordCount","weighResultList","maxPage"})
	public void reload() throws ParseException{
		if (search) {
			recordCount = mobWeighbridgeResultSvc.countByArg(sArg, sStart==null?new Date(sdf.parse("01-01-1970").getTime()):sStart, 
					sEnd==null?new Date(sdf.parse("31-12-9999").getTime()):sEnd);
			weighResultList = mobWeighbridgeResultSvc.findByArg(sArg, sStart==null?new Date(sdf.parse("01-01-1970").getTime()):sStart, 
					sEnd==null?new Date(sdf.parse("31-12-9999").getTime()):sEnd, pageNo, 
					StringUtils.isEmpty(sortBy) ? "id" : sortBy, sortAsc);
		} else {
			recordCount = mobWeighbridgeResultSvc.countAll();
			weighResultList = mobWeighbridgeResultSvc.findAll(pageNo, 
					StringUtils.isEmpty(sortBy) ? "id" : sortBy, sortAsc);
		}
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
	}
	
	@Command("switchPage")
	@NotifyChange({ "pageNo", "weighResultList" })
	public void switchPage(@BindingParam("action") String action) throws ParseException {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}

			if (search) {
				weighResultList = mobWeighbridgeResultSvc.findByArg(sArg, sStart==null?new Date(sdf.parse("01-01-1970").getTime()):sStart, 
						sEnd==null?new Date(sdf.parse("31-12-9999").getTime()):sEnd, pageNo,
					StringUtils.isEmpty(sortBy) ? "id" : sortBy, sortAsc);
			} else {
				weighResultList = mobWeighbridgeResultSvc.findAll(pageNo, 
					StringUtils.isEmpty(sortBy) ? "id" : sortBy, sortAsc);
			}
		}
	}

	@Command("sort")
	@NotifyChange({"weighResultList"})
	public void sort(@BindingParam("col") String column) throws ParseException {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}

			if (search) {
				weighResultList = mobWeighbridgeResultSvc.findByArg(sArg, sStart==null?new Date(sdf.parse("01-01-1970").getTime()):sStart, 
						sEnd==null?new Date(sdf.parse("31-12-9999").getTime()):sEnd, pageNo, sortBy, sortAsc);
			} else {
				weighResultList = mobWeighbridgeResultSvc.findAll(pageNo, sortBy, sortAsc);
			}
		}
	}
	
	/*
	@Command("dl")
	public void download() {
		if(activeCheck()){
			List<TbMUserExportDto> export = new ArrayList<>();
			List<MUserDto> full;
			if (superAdmin) {
				if (aiosearch){
					full = mUserSvc.findAllSelectedSimple(aios, recordCount,
						StringUtils.isEmpty(sortBy) ? "idUser" : sortBy, sortAsc);
				} else if (search) {
					full = mUserSvc.findAllSelected(userId, state, name, noHp,
						selectedCust.getIdCust() == null ? "" : selectedCust.getIdCust().toString(), 
						imei, selectedRole, selectApp,recordCount,
						StringUtils.isEmpty(sortBy) ? "idUser" : sortBy, sortAsc);
				} else {
					full = mUserSvc.findAllDl(StringUtils.isEmpty(sortBy) ? "idUser" : sortBy, sortAsc);
				}
			} else {
				if (aiosearch){
					full = mUserSvc.findAllSelectedSimple(aios, user.getIdCompany(), recordCount,
						StringUtils.isEmpty(sortBy) ? "idUser" : sortBy, sortAsc);
				} else if (search) {
					full = mUserSvc.findAllSelected(userId, state, name, noHp,
						selectedCust.getIdCust() == null ? "" : selectedCust.getIdCust().toString(), 
						imei, selectedRole, selectApp,user.getIdCompany(), recordCount,
						StringUtils.isEmpty(sortBy) ? "idUser" : sortBy, sortAsc);
				} else {
					full = mUserSvc.findAllSelectCompanyDl(user.getIdCompany(),
						recordCount,StringUtils.isEmpty(sortBy) ? "idUser" : sortBy, sortAsc);
				}
			}
			
			for (MUserDto x : full) {
				export.add(new TbMUserExportDto(x));
			}
			serve(Sessions.getCurrent().getAttribute("saveTempDir") + "Users", export.toArray());
		}
	}
	*/
}
