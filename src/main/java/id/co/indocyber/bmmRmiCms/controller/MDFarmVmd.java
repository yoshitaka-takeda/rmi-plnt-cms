package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MobFarmDto;
import id.co.indocyber.bmmRmiCms.dto.MobPaddyFieldDto;
import id.co.indocyber.bmmRmiCms.entity.MobFarmer;
import id.co.indocyber.bmmRmiCms.service.MobFarmSvc;
import id.co.indocyber.bmmRmiCms.service.MobFarmerSvc;
import id.co.indocyber.bmmRmiCms.service.MobPaddyFieldSvc;

import java.util.ArrayList;
import java.util.List;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class MDFarmVmd extends BaseVmd {
	@WireVariable
	MobFarmSvc mobFarmSvc;
	@WireVariable
	MobFarmerSvc mobFarmerSvc;
	@WireVariable
	MobPaddyFieldSvc mobPaddyFieldSvc;

	String urlIndex = "/master/farm/index.zul";
	String refresh = "/master/farm/ref.zul";
	
	private MUserDto user;

	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;

	private MobFarmer selectFarmer;
	private List<MobFarmer> farmerList, fFarmerList = new ArrayList<>();
	private List<MobFarmDto> farmList;
	private List<MobPaddyFieldDto> fieldList;

	private Boolean editMode, search = false;
	private String pageTitle;

	private Integer farmerId;
	private String selectFarm;
	
	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public List<MobFarmDto> getFarmList() {
		return farmList;
	}

	public void setFarmList(List<MobFarmDto> farmList) {
		this.farmList = farmList;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}
	
	public MobFarmer getSelectFarmer() {
		return selectFarmer;
	}

	public void setSelectFarmer(MobFarmer selectFarmer) {
		this.selectFarmer = selectFarmer;
	}

	public List<MobFarmer> getfFarmerList() {
		return fFarmerList;
	}

	public void setfFarmerList(List<MobFarmer> fFarmerList) {
		this.fFarmerList = fFarmerList;
	}

	public List<MobPaddyFieldDto> getFieldList() {
		return fieldList;
	}

	public void setFieldList(List<MobPaddyFieldDto> fieldList) {
		this.fieldList = fieldList;
	}
	
	public Boolean getSearch() {
		return search;
	}

	public void setSearch(Boolean search) {
		this.search = search;
	}

	public String getSelectFarm() {
		return selectFarm;
	}

	public void setSelectFarm(String selectFarm) {
		this.selectFarm = selectFarm;
	}

	@Init
	@NotifyChange({ "btnAdd", "btnDel", "btnEdit", "btnDl" })
	public void load() {
		editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
		user = (MUserDto) Sessions.getCurrent().getAttribute("user");
		//superAdmin = (Integer) Sessions.getCurrent().getAttribute("superAdmin") == 1;
		farmerList = mobFarmerSvc.findAll();
		fFarmerList.addAll(farmerList);
		
		for (MMenuRoleDto x : user.getRolePrivs()) {
			if ("MD-6".equals(x.getMenuCode())) {
				btnAdd = !x.getIsAdd();
				btnDel = !x.getIsDelete();
				btnDl = !x.getIsDownload();
				btnEdit = !x.getIsEdit();
				break;
			}
		}
	}
	
	@Command("viewFarm")
	@NotifyChange({"farmList","search","fieldList"})
	public void viewFarm() {
		if(activeCheck()){
			if(selectFarmer!=null){
				search = true;
				farmerId = selectFarmer.getId();
				farmList = mobFarmSvc.findFarmDtoByFarmer(farmerId);
				fieldList = null;
			}
			else{
				showWarningMsgBox("E012");
				setFlagIconWarning(true);
				return;
			}
		}
	}
	
	@Command("viewFields")
	@NotifyChange({"fieldList","selectFarm"})
	public void viewFields(@BindingParam("farmCode")String farmCode){
		if(activeCheck()){
			selectFarm = farmCode;
			fieldList = mobPaddyFieldSvc.findPaddyFieldDtoByFarmCode(farmerId, selectFarm);
		}
	}
	
	@Command("refFarmer")
	@NotifyChange("fFarmerList")
	public void refFarmer(@BindingParam("src")String src){
		fFarmerList.clear();
		for(MobFarmer x: farmerList){
			if(x.getFullname().toLowerCase().contains(src.toLowerCase()) || 
					x.getId().toString().contains(src.toLowerCase())){
				fFarmerList.add(x);
			}
		}
	}
}
