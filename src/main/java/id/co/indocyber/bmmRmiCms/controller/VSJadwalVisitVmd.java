package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MobEmployeeDto;
import id.co.indocyber.bmmRmiCms.dto.MobScheduleVisitToFarmerHeaderDto;
import id.co.indocyber.bmmRmiCms.dto.MobScheduleVisitToFarmerHistoryDto;
import id.co.indocyber.bmmRmiCms.entity.MUserRole;
import id.co.indocyber.bmmRmiCms.entity.MobFarm;
import id.co.indocyber.bmmRmiCms.entity.MobFarmer;
import id.co.indocyber.bmmRmiCms.entity.MobPaddyField;
import id.co.indocyber.bmmRmiCms.service.GlobalSvc;
import id.co.indocyber.bmmRmiCms.service.MUserSvc;
import id.co.indocyber.bmmRmiCms.service.MobEmployeeSvc;
import id.co.indocyber.bmmRmiCms.service.MobFarmSvc;
import id.co.indocyber.bmmRmiCms.service.MobFarmerSvc;
import id.co.indocyber.bmmRmiCms.service.MobPaddyFieldSvc;
import id.co.indocyber.bmmRmiCms.service.MobScheduleVisitSvc;
import id.co.indocyber.bmmRmiCms.tools.RestResponse;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

import java.io.IOException;
import java.io.InputStream;
import java.net.HttpURLConnection;
import java.net.MalformedURLException;
import java.net.URL;
import java.net.URLConnection;
import java.sql.Date;
import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.time.format.DateTimeFormatter;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import java.util.Scanner;

import org.apache.http.client.ClientProtocolException;
import org.apache.http.client.methods.CloseableHttpResponse;
import org.apache.http.client.methods.HttpGet;
import org.apache.http.impl.client.CloseableHttpClient;
import org.apache.http.impl.client.HttpClients;
import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Bandbox;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Window;

import com.fasterxml.jackson.databind.ObjectMapper;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VSJadwalVisitVmd extends BaseVmd {
	@WireVariable
	MobScheduleVisitSvc mobScheduleVisitSvc;
	@WireVariable
	MobFarmerSvc mobFarmerSvc;
	@WireVariable
	MobEmployeeSvc mobEmployeeSvc;
	@WireVariable
	MobFarmSvc mobFarmSvc;
	@WireVariable
	MobPaddyFieldSvc mobPaddyFieldSvc;
	@WireVariable
	GlobalSvc globalSvc;
	@WireVariable
	MUserSvc mUserSvc;
	
	String urlIndex = "/visit/jadwal/index.zul";
	String refresh = "/visit/jadwal/ref.zul";
	
	Window tempwindow;
	
	private final CloseableHttpClient httpClient = HttpClients.createDefault();
	
	private MUserDto user;
	private MUserRole userLogin;
	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;
	
	private boolean flagDisabledPetugas;
	private boolean flagDisabledFam;
	
	private int editIndex=-1;

	private MobScheduleVisitToFarmerHeaderDto editHeader;
	private List<MobScheduleVisitToFarmerHeaderDto> headerList;
	
	private List<Integer> listId = new ArrayList<>();
	
	private boolean freezeId;
	private Boolean editMode, reAssign, reasoning, vHist;
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private Integer recordCount = 1;
	private String arg, sArg;
	
	private String sortBy = "";
	private boolean sortAsc = false;
	
	private List<MobFarmer> farmerList, fFarmerList = new ArrayList<>();
	private List<MobEmployeeDto> employeeList, fEmployeeList, reAssignEmployees, fReAssignEmployees;
	private List<MobFarm> farmList, fFarmList;
	private List<MobPaddyField> padList;
	private List<MobScheduleVisitToFarmerHistoryDto> visitHistory;
	
	private MobEmployeeDto selectEmployee;
	
	// Visit no. generation
	private String[] roman = new String[]{"I","II","III","IV","V","VI","VII","VIII","IX","X","XI","XII"};

	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public MobScheduleVisitToFarmerHeaderDto getEditHeader() {
		return editHeader;
	}

	public void setEditHeader(MobScheduleVisitToFarmerHeaderDto editHeader) {
		this.editHeader = editHeader;
	}

	public List<MobScheduleVisitToFarmerHeaderDto> getHeaderList() {
		return headerList;
	}

	public void setHeaderList(List<MobScheduleVisitToFarmerHeaderDto> headerList) {
		this.headerList = headerList;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public String getArg() {
		return arg;
	}

	public void setArg(String arg) {
		this.arg = arg;
	}
	
	public List<MobFarmer> getfFarmerList() {
		return fFarmerList;
	}

	public void setfFarmerList(List<MobFarmer> fFarmerList) {
		this.fFarmerList = fFarmerList;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public List<MobFarm> getfFarmList() {
		return fFarmList;
	}

	public void setfFarmList(List<MobFarm> fFarmList) {
		this.fFarmList = fFarmList;
	}

	public List<MobPaddyField> getPadList() {
		return padList;
	}

	public void setPadList(List<MobPaddyField> padList) {
		this.padList = padList;
	}

	public List<MobEmployeeDto> getEmployeeList() {
		return employeeList;
	}

	public void setEmployeeList(List<MobEmployeeDto> employeeList) {
		this.employeeList = employeeList;
	}

	public List<MobEmployeeDto> getfEmployeeList() {
		return fEmployeeList;
	}

	public void setfEmployeeList(List<MobEmployeeDto> fEmployeeList) {
		this.fEmployeeList = fEmployeeList;
	}

	public List<MobEmployeeDto> getReAssignEmployees() {
		return reAssignEmployees;
	}

	public void setReAssignEmployees(List<MobEmployeeDto> reAssignEmployees) {
		this.reAssignEmployees = reAssignEmployees;
	}

	public List<MobEmployeeDto> getfReAssignEmployees() {
		return fReAssignEmployees;
	}

	public void setfReAssignEmployees(List<MobEmployeeDto> fReAssignEmployees) {
		this.fReAssignEmployees = fReAssignEmployees;
	}

	public MobEmployeeDto getSelectEmployee() {
		return selectEmployee;
	}

	public void setSelectEmployee(MobEmployeeDto selectEmployee) {
		this.selectEmployee = selectEmployee;
	}
	
	public Boolean getReAssign() {
		return reAssign;
	}

	public void setReAssign(Boolean reAssign) {
		this.reAssign = reAssign;
	}

	public List<Integer> getListId() {
		return listId;
	}

	public void setListId(List<Integer> listId) {
		this.listId = listId;
	}

	public boolean isFlagDisabledPetugas() {
		return flagDisabledPetugas;
	}

	public void setFlagDisabledPetugas(boolean flagDisabledPetugas) {
		this.flagDisabledPetugas = flagDisabledPetugas;
	}

	public boolean isFlagDisabledFam() {
		return flagDisabledFam;
	}

	public void setFlagDisabledFam(boolean flagDisabledFam) {
		this.flagDisabledFam = flagDisabledFam;
	}
	
	public Boolean getReasoning() {
		return reasoning;
	}

	public void setReasoning(Boolean reasoning) {
		this.reasoning = reasoning;
	}
	
	public Boolean getvHist() {
		return vHist;
	}

	public void setvHist(Boolean vHist) {
		this.vHist = vHist;
	}
	
	public List<MobScheduleVisitToFarmerHistoryDto> getVisitHistory() {
		return visitHistory;
	}

	public void setVisitHistory(
			List<MobScheduleVisitToFarmerHistoryDto> visitHistory) {
		this.visitHistory = visitHistory;
	}
	
	public MUserDto getUser() {
		return user;
	}

	public void setUser(MUserDto user) {
		this.user = user;
	}

	@Init
	@NotifyChange({ "btnAdd", "btnDel", "btnEdit", "btnDl","listId" })
	public void load() {
		editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
		user = (MUserDto) Sessions.getCurrent().getAttribute("user");
		if(user.getUsername()!=null){
			userLogin = globalSvc.findUserLogin(user.getUsername());
		}
		//menampilkan jumlah anggota team
		listId = globalSvc.getAllMyTeam(user.getPersonId());
		
		//superAdmin = (Integer) Sessions.getCurrent().getAttribute("superAdmin") == 1;
		List<Integer> newListId = new ArrayList<>();
		newListId = listId;
		newListId.add(user.getPersonId());
		headerList = mobScheduleVisitSvc.findAllVisitHeaders(newListId, pageNo, "modifiedDate", false);
		recordCount = mobScheduleVisitSvc.countAllVisitHeaders(newListId);
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
		
		for (MMenuRoleDto x : user.getRolePrivs()) {
			if ("VS-1".equals(x.getMenuCode())) {
				btnAdd = !x.getIsAdd();
				btnDel = !x.getIsDelete();
				btnDl = !x.getIsDownload();
				btnEdit = !x.getIsEdit();
				break;
			}
		}
	}

	@Command("back")
	public void back() {
		if (activeCheck()) {
			editMode=false;
		}
	}

	@Command("add")
	@NotifyChange({"editMode", "editHeader", "padList", "freezeId","employeeList",
		"flagDisabledFam","flagDisabledPetugas"})
	public void add() {
		if(activeCheck()){
			editMode = true;
			editHeader = new MobScheduleVisitToFarmerHeaderDto();
			editIndex = -1;
			
			if(farmerList==null){
				List<Integer> newListId = new ArrayList<>();
				newListId = listId;
				newListId.add(user.getPersonId());
				farmerList = mobFarmerSvc.deepFindManagedFarmers(newListId);
			}
			if(padList!=null) padList.clear();
			
			freezeId=false;
			flagDisabledFam = true;
			flagDisabledPetugas = true;
		}
	}

	@Command("edit")
	@NotifyChange({"editMode", "editHeader", "padList", "freezeId"})
	public void edit(@BindingParam("item") MobScheduleVisitToFarmerHeaderDto edit) {
		if(activeCheck()){
			if (edit.getVisitNo() == null)
				Messagebox.show("Pilih data yang akan dirubah!", "Message", 0, null);
			else {
				editMode = true;
				editHeader = edit;
				editIndex = headerList.indexOf(edit);
				
				if(farmerList==null){
					List<Integer> newListId = new ArrayList<>();
					newListId = listId;
					newListId.add(user.getPersonId());
					farmerList = mobFarmerSvc.deepFindManagedFarmers(listId);
				}
				
				employeeList = mobEmployeeSvc.findManagedEmployees(listId, editHeader.getFarmer().getId());
				employeeList.addAll(mobEmployeeSvc.findMyManager(
						user.getPersonId(), user.getRoleName(), editHeader.getFarmer().getId()));
				
				farmList = mobFarmSvc.findFarmByFarmer(editHeader.getFarmer().getId());
				if(fFarmList==null) { 
					fFarmList = new ArrayList<>();
				}
				
				padList = mobPaddyFieldSvc.findPaddyFieldByFarmCode(edit.getFarmer().getId(), 
						edit.getFarm().getFarmCode());
				
				freezeId=true;
			}
		}
	}

	@Command("save")
	@NotifyChange({"editHeader","editMode","headerList","maxPage","recordCount"})
	public void save(@BindingParam("btn")Component btn) {
		if(activeCheck()){
			String fullErrMsg = StringConstants.MSG_FORM_TEMPLATE;
			int error = 0;
			if(editHeader.getVisitDate()==null){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Tgl Kunjungan");
			}
			else if(editHeader.getVisitExpiredDate()!=null && 
					editHeader.getVisitDate().after(editHeader.getVisitExpiredDate())){
				error++;
				fullErrMsg += String.format(
					StringConstants.MSG_FORM_DATEDIFF_NEGATIVE, "Tgl Kunjungan", "Tgl Akhir Kunjungan");
			}
			if(editHeader.getFarmer()==null){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Petani");
			}
			if(editHeader.getFarm()==null){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Kebun");
			}
			if(editHeader.getPaddyFields()==null || editHeader.getPaddyFields().size()==0){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Petak-Petak");
			}
			if(editHeader.getAssignedEmployee()==null){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Petugas");
			}
			if (error > 0) {
				Messagebox.show(fullErrMsg, "Message", 0, null);
			} else {
				editHeader.setAssignTo(editHeader.getAssignedEmployee().getId());
				if(StringUtils.isEmpty(editHeader.getVisitNo())){
					if(headerList.size()==25){
						headerList.remove(24);
					}
					
					recordCount++;
					maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
					
					editHeader.setVisitNo(generateVisitNo(mobScheduleVisitSvc.getVisitCount()+1));
					editHeader.setCreatedBy(user.getUsername());
					editHeader.setCreatedDate(Timestamp.from(
						OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
					editHeader.setIsActive(true);
					editHeader.setTaskActivity("02");
				}
				if(editIndex!=-1){
					headerList.remove(editIndex);
				}
				headerList.add(0,editHeader);
				
				if(editHeader.getVisitExpiredDate()==null){
					Calendar c = Calendar.getInstance();
					c.setTime(editHeader.getVisitDate());
					c.set(Calendar.DAY_OF_MONTH, c.get(Calendar.DAY_OF_MONTH)+1);
					editHeader.setVisitExpiredDate(new Date(c.getTime().getTime()));
				}
				editHeader.setModifiedBy(user.getUsername());
				editHeader.setModifiedDate(
					Timestamp.from(OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
				mobScheduleVisitSvc.save(editHeader);
				
				try {
					notifyEmployee(mUserSvc.getEmployeeToken(
						editHeader.getAssignedEmployee().getId()), editHeader.getVisitNo());
				} catch (IOException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
				
				editMode = false;
				((Window)btn.getParent().getParent()).setVisible(false);
				showSuccessMsgBox("E014");
//				Clients.showNotification("Data sukses disimpan",
//					Clients.NOTIFICATION_TYPE_INFO, null, null, 5000);
			}
		}
	}
	
	@Command
	@NotifyChange("reAssign")
	public void saveReAssign(){
		try{
			Integer previous = editHeader.getAssignTo();
			editHeader.setAssignTo(selectEmployee.getId());
			editHeader.setAdminDelegable(false);
			editHeader.setModifiedBy(user.getUsername());
			editHeader.setModifiedDate(
					Timestamp.from(OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
			mobScheduleVisitSvc.saveReAssign(editHeader, previous);
			reAssign = false;
			try {
				notifyEmployee(mUserSvc.getEmployeeToken(
					editHeader.getAssignTo()), editHeader.getVisitNo());
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
			showSuccessMsgBox("E013");
		}catch(Exception e){
			e.printStackTrace();
		}
	}

	@Command("src")
	@NotifyChange({ "arg","headerList", "pageNo", "maxPage", "recordCount"})
	public void search() {
		if(activeCheck()){
			search = true;
			pageNo = 1;
			sArg = StringUtils.isEmpty(arg)?"":arg;
			List<Integer> newListId = new ArrayList<>();
			newListId = listId;
			newListId.add(user.getPersonId());
			headerList = mobScheduleVisitSvc.findVisitHeadersByArg(newListId, sArg, pageNo,
				StringUtils.isEmpty(sortBy) ? "createdDate" : sortBy, sortAsc);
			recordCount = mobScheduleVisitSvc.countVisitHeadersByArg(newListId, sArg);
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
			arg="";
		}
	}
	
	@Command("del")
	@NotifyChange({"editHeader","reasoning"})
	public void delete(@BindingParam("item") final MobScheduleVisitToFarmerHeaderDto del) {
		if(activeCheck()){
			if (del.getVisitNo() == null)
				Messagebox.show("Pilih data yang akan dihapus", "Message", 0, null);
			else {
				if (!btnDel) {
					editHeader = del;
					reasoning = true;
				} else {
					Clients.alert(StringConstants.MSG_NO_DEL_ACCESS, "Warning", "");
				}
			}
		}
	}

	@Command("confirmCancel")
	public void confirmCancel(@BindingParam("btn")Component btn) {
		mobScheduleVisitSvc.cancel(editHeader);
		//headerList.get(headerList.indexOf(editHeader)).setIsActive(false);
		headerList.remove(headerList.indexOf(editHeader));
		BindUtils.postNotifyChange(null, null, VSJadwalVisitVmd.this, "headerList");
		((Window)btn.getParent().getParent()).setVisible(false);
		Clients.showNotification("Rencana visit dibatalkan",
			Clients.NOTIFICATION_TYPE_INFO, null, null, 5000);
	}
	
	@Command("switchPage")
	@NotifyChange({ "pageNo", "headerList" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}
			
			
			List<Integer> newListId = new ArrayList<>();
			newListId = listId;
			newListId.add(user.getPersonId());
			if (search) {
				headerList = mobScheduleVisitSvc.findVisitHeadersByArg(newListId, sArg, pageNo,
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			} else {
				headerList = mobScheduleVisitSvc.findAllVisitHeaders(newListId, pageNo, 
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			}
		}
	}

	@Command("reload")
	@NotifyChange({"recordCount","headerList","maxPage"})
	public void reload(){
		List<Integer> newListId = new ArrayList<>();
		newListId = listId;
		newListId.add(user.getPersonId());
		if (search) {
			recordCount = mobScheduleVisitSvc.countVisitHeadersByArg(newListId, sArg);
			headerList = mobScheduleVisitSvc.findVisitHeadersByArg(newListId, sArg, pageNo,
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
		} else {
			recordCount = mobScheduleVisitSvc.countAllVisitHeaders(newListId);
			headerList = mobScheduleVisitSvc.findAllVisitHeaders(newListId, pageNo, 
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
		}
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
	}

	@Command("sort")
	@NotifyChange({"headerList"})
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}
			
			List<Integer> newListId = new ArrayList<>();
			newListId = listId;
			newListId.add(user.getPersonId());
			if (search) {
				headerList = mobScheduleVisitSvc.findVisitHeadersByArg(newListId,sArg, pageNo, sortBy, sortAsc);
			} else {
				headerList = mobScheduleVisitSvc.findAllVisitHeaders(newListId, pageNo, sortBy, sortAsc);
			}
		}
	}
	
	@Command("refFarmer")
	@NotifyChange("fFarmerList")
	public void refFarmer(@BindingParam("src")String src){
		if(fFarmerList==null) fFarmerList = new ArrayList<>();
		fFarmerList.clear();
		if(farmerList!=null){
			for(MobFarmer x: farmerList){
				if(x.getFullname().toLowerCase().contains(src)){
					fFarmerList.add(x);
				}
			}
		}
	}
	
	@Command("refEmployee")
	@NotifyChange("fEmployeeList")
	public void refEmployee(@BindingParam("src")String src){
		if(fEmployeeList==null) fEmployeeList = new ArrayList<>();
		fEmployeeList.clear();
		if(employeeList!=null){
			for(MobEmployeeDto x: employeeList){
				if(x.getFullname().toLowerCase().contains(src)){
					fEmployeeList.add(x);
				}
			}
		}
	}
	
	@Command("refReEmployee")
	@NotifyChange("fReAssignEmployees")
	public void refReEmployee(@BindingParam("src")String src){
		if(fReAssignEmployees==null) fReAssignEmployees = new ArrayList<>();
		fReAssignEmployees.clear();
		if(reAssignEmployees!=null){
			for(MobEmployeeDto x: reAssignEmployees){
				if(x.getFullname().toLowerCase().contains(src)){
					fReAssignEmployees.add(x);
				}
			}
		}
	}
	
	@Command("getEmployees")
	@NotifyChange({"employeeList", "fEmployeeList", 
		"fFarmList", "padList", "editHeader", "flagDisabledPetugas", "flagDisabledFam"})
	public void getEmployees(@BindingParam("bb")Component bb){
		try{
			employeeList = mobEmployeeSvc.findManagedEmployees(listId, editHeader.getFarmer().getId());
			//employeeList.addAll(mobEmployeeSvc.findMyManager(
			//		user.getPersonId(), user.getRoleName(), editHeader.getFarmer().getId()));
			
			if(fEmployeeList==null) fEmployeeList = new ArrayList<>();
			fEmployeeList.clear(); 
			editHeader.setAssignedEmployee(null);
			
			farmList = mobFarmSvc.findFarmByFarmer(editHeader.getFarmer().getId());
			if(fFarmList==null) fFarmList = new ArrayList<>(); 
			fFarmList.clear();
			editHeader.setFarm(null);
			
			if(padList!=null) padList.clear();
			if(editHeader.getPaddyFields()!=null) editHeader.getPaddyFields().clear();
			
			flagDisabledPetugas = false;
			flagDisabledFam = false;
			
			((Bandbox)bb.getParent().getParent().getParent()).close();
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	
	@Command("getPaddyList")
	@NotifyChange({"padList", "editHeader"})
	public void getPaddyList(@BindingParam("bb")Component bb){
		padList = mobPaddyFieldSvc.findPaddyFieldByFarmCode(editHeader.getFarmer().getId(), 
				editHeader.getFarm().getFarmCode());
		editHeader.getPaddyFields().clear();
		if(bb!=null) ((Bandbox)bb.getParent().getParent().getParent()).close();
	}
	
	@Command("refFarm")
	@NotifyChange("fFarmList")
	public void refFarm(@BindingParam("src")String src){
		if(fFarmList==null) fFarmList = new ArrayList<>();
		fFarmList.clear();
		for(MobFarm x: farmList){
			if(x.getFarmCode().toLowerCase().contains(src)||
				x.getVillage().toLowerCase().contains(src)||
				x.getSubDistrict().toLowerCase().contains(src)||
				x.getDistrict().toLowerCase().contains(src)){
				fFarmList.add(x);
			}
		}
	}
	
	@Command("reassign")
	@NotifyChange({"fReAssignEmployees","selectEmployee","reAssign","reAssignEmployees","editHeader"})
	public void reassign(@BindingParam("item") MobScheduleVisitToFarmerHeaderDto edit){
		if(fReAssignEmployees!=null) fReAssignEmployees.clear();
		selectEmployee = null;
		editIndex = headerList.indexOf(edit);
		editHeader = edit;
		
		List<Integer> tempId = globalSvc.getAllMyTeam(editHeader.getAssignedEmployee().getId());
		
		int farmerId = editHeader.getFarmer()==null?null:editHeader.getFarmer().getId();
		reAssignEmployees = mobEmployeeSvc.findManagedEmployees(tempId, farmerId);
		//reAssignEmployees.addAll(mobEmployeeSvc.findMyManager(edit.getAssignTo(), farmerId));
		if(reAssignEmployees==null || reAssignEmployees.size()==0){
			Messagebox.show("Tidak ada employee yang bisa di reassign", "Message", 0, null);
		}
		else{
			reAssign = true;
		}
	}
	
	@Command
	@NotifyChange({"visitHistory","vHist","editHeader"})
	public void history(@BindingParam("item") final MobScheduleVisitToFarmerHeaderDto hdr) {
		if(activeCheck()){
			editHeader = hdr;
			visitHistory = mobScheduleVisitSvc.getHistory(hdr.getVisitNo());
			vHist = true;
		}
	}
	
	@Command
	public void loadListStaffOnFarm(){
		System.err.println("List ID");
		int no = 1;
		for(Integer i : listId){
			System.out.println(no +". "+i);
			no++;
		}
		int farmerId = editHeader.getFarmer().getId()==null?null:editHeader.getFarmer().getId();
		fReAssignEmployees = new ArrayList<>(reAssignEmployees); 
				//mobEmployeeSvc.findManagedEmployees(globalSvc.getAllMyTeam(editHeader.getAssignedEmployee().getId()), farmerId);
		if(editHeader.getAssignTo().equals(user.getPersonId())){
			for(MobEmployeeDto x: fReAssignEmployees){
				if(x.getId().equals(user.getPersonId())){
					fReAssignEmployees.remove(x);
					break;
				}
			}
		}
		BindUtils.postNotifyChange(null, null, this, "fReAssignEmployees");
	}
	
	@Command
	@NotifyChange("fEmployeeList")
	public void loadListEmployee(){
		if(fEmployeeList != null){
			fEmployeeList.clear();
		}
		if(employeeList != null){
			fEmployeeList.addAll(employeeList);
		}
	}
	
	@Command
	@NotifyChange("fFarmerList")
	public void loadListPetani(){
		if(fFarmerList != null){
			fFarmerList.clear();
		}
		if(farmerList != null){
			fFarmerList.addAll(farmerList);
		}
	}
	
	@Command
	@NotifyChange("fFarmList")
	public void loadListFarm(){
		if(fFarmList != null){
			fFarmList.clear();
		}
		if(farmList != null){
			fFarmList.addAll(farmList);
		}else{
			System.err.println("farmList is null");
		}
	}
	
	@Command
	@NotifyChange("flagIconWarning")
	public void confirmAssign(@BindingParam("btn")Component btn){
		showConfirm("E015",editHeader.getFarm().getFarmCode(),editHeader.getFarmer().getFullname(),
				LocalDate.now().format(DateTimeFormatter.ofPattern("dd-MM-yyyy")));
		tempwindow = ((Window)btn.getParent().getParent());
		setFlagIconWarning(true);
	}
	
	@Override
	@Command
	@NotifyChange({"recordCount","headerList","maxPage"})
	public void yesConfirm(){
		try{
			editHeader.setTaskActivity("03");
			
			flagConfirmMsg = false;
			showSuccessMsgBox("E000");
			mobScheduleVisitSvc.saveAmbil(editHeader);
			//reload list
			
			//menampilkan jumlah anggota team
			listId = globalSvc.getAllMyTeam(user.getPersonId());
			
			//superAdmin = (Integer) Sessions.getCurrent().getAttribute("superAdmin") == 1;
			List<Integer> newListId = new ArrayList<>();
			newListId = listId;
			newListId.add(user.getPersonId());
			
			headerList = mobScheduleVisitSvc.findAllVisitHeaders(newListId, pageNo, "modifiedDate", true);
			recordCount = mobScheduleVisitSvc.countAllVisitHeaders(newListId);
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);

			tempwindow.setVisible(false);
			BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
		}catch(Exception e){
			e.printStackTrace();
			showErrorMsgBox("E001");			
		}
	}
	
	@Override
	@Command
	@NotifyChange({ "flagIconCek", "flagIconWarning", "flagIconError","flagMsgFree","flagConfirmMsg" })
	public void OK(){
		flagIconWarning = false;
		flagIconCek = false;
		flagIconError = false;
		flagMsgFree = false;
		flagConfirmMsg = false;
		BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
		BindUtils.postNotifyChange(null, null, this, "flagIconCek");
		BindUtils.postNotifyChange(null, null, this, "flagIconError");
		BindUtils.postNotifyChange(null, null, this, "flagIconWarning");
		BindUtils.postNotifyChange(null, null, this, "flagMsgFree");
		
		if(tempwindow!=null) tempwindow.setVisible(false);
	}
	
	public String generateVisitNo(Integer lastVisit){
		String[] numParts = new String[3];
		LocalDate now = LocalDate.now();
		
		numParts[0] = lastVisit.toString();
		if(numParts[0].length()<4){
			for(int i=numParts[0].length(); i<4; i++){
				numParts[0] = "0"+numParts[0];
			}
		}
		numParts[1] = roman[now.getMonthValue()-1];
		numParts[2] = Integer.toString(now.getYear());
		return String.join("/", numParts);
	}
	
	public MUserRole getUserLogin() {
		return userLogin;
	}

	public void setUserLogin(MUserRole userLogin) {
		this.userLogin = userLogin;
	}
	
	public void notifyEmployee(String token, String visitNo) throws ClientProtocolException, IOException {
		HttpGet request = new HttpGet("http://103.66.69.20/rmi-api/notifVisit");
        request.addHeader("token", token);
        request.addHeader("noVisit", visitNo);
        
        CloseableHttpResponse resp = httpClient.execute(request);
        resp.close();
    }
}
