package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MobVehicleDto;
import id.co.indocyber.bmmRmiCms.entity.MVehicleType;
import id.co.indocyber.bmmRmiCms.service.MiscellaneousSvc;
import id.co.indocyber.bmmRmiCms.service.MobVehicleSvc;
import id.co.indocyber.bmmRmiCms.tools.DeepCopy;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Window;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class MDKendaraanVmd extends BaseVmd {
	@WireVariable
	MobVehicleSvc mobVehicleSvc;
	@WireVariable
	MiscellaneousSvc miscellaneousSvc;

	String urlIndex = "/master/kendaraan/index.zul";
	String refresh = "/master/kendaraan/ref.zul";
	
	private MUserDto user;

	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;
	
	private int editIndex=-1;

	private MobVehicleDto editVehicle;
	private List<MobVehicleDto> vehicleList;
	private List<MVehicleType> vehicleTypes;
	
	private boolean freezeId;
	private Boolean editMode;
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private Integer recordCount = 1;
	private String arg,sArg;
	
	private String sortBy = "";
	private boolean sortAsc = false;
	
	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public MobVehicleDto getEditVehicle() {
		return editVehicle;
	}

	public void setEditVehicle(MobVehicleDto editVehicle) {
		this.editVehicle = editVehicle;
	}

	public List<MobVehicleDto> getVehicleList() {
		return vehicleList;
	}

	public void setVehicleList(List<MobVehicleDto> vehicleList) {
		this.vehicleList = vehicleList;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public String getArg() {
		return arg;
	}

	public void setArg(String arg) {
		this.arg = arg;
	}
	
	public List<MVehicleType> getVehicleTypes() {
		return vehicleTypes;
	}

	public void setVehicleTypes(List<MVehicleType> vehicleTypes) {
		this.vehicleTypes = vehicleTypes;
	}

	@Init
	@NotifyChange({ "btnAdd", "btnDel", "btnEdit", "btnDl" })
	public void load() {
		editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
		user = (MUserDto) Sessions.getCurrent().getAttribute("user");
		//superAdmin = (Integer) Sessions.getCurrent().getAttribute("superAdmin") == 1;
		vehicleList = mobVehicleSvc.findAll(pageNo, "modifiedDate", false);
		recordCount = mobVehicleSvc.countAll();
		
		vehicleTypes = miscellaneousSvc.getVehicleTypes();
		
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
		
		for (MMenuRoleDto x : user.getRolePrivs()) {
			if ("MD-3".equals(x.getMenuCode())) {
				btnAdd = !x.getIsAdd();
				btnDel = !x.getIsDelete();
				btnDl = !x.getIsDownload();
				btnEdit = !x.getIsEdit();
				break;
			}
		}
	}

	@Command("back")
	public void back() {
		if (activeCheck()) {
			editMode=false;
		}
	}

	@Command("add")
	@NotifyChange({"editMode","editVehicle"})
	public void add() {
		if(activeCheck()){
			editMode = true;
			editVehicle = new MobVehicleDto();
			editIndex = -1;
		}
	}

	@Command("edit")
	@NotifyChange({"editMode","editVehicle"})
	public void edit(@BindingParam("item") MobVehicleDto edit) {
		if(activeCheck()){
			if (edit.getVehicleNo() == null)
				Messagebox.show("Pilih data yang akan dirubah!", "Message", 0, null);
			else {
				editMode = true;
				editVehicle = (MobVehicleDto)DeepCopy.deepCopy(edit);
				editIndex = vehicleList.indexOf(edit);
			}
		}
	}

	@Command("save")
	@NotifyChange({"editVehicle","editMode","vehicleList","maxPage","recordCount"})
	public void save(@BindingParam("btn")Component btn) {
		if(activeCheck()){
			// Database design is not final, but one does what one can...
			boolean existence;
			try {
				existence = mobVehicleSvc.existence(editVehicle.getVehicleNo());
			} catch (Exception e) {
				existence = false;
			}
			String fullErrMsg = StringConstants.MSG_FORM_TEMPLATE;
			int error = 0;
			if(existence && editVehicle.getCreatedDate()==null){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_ID_EXISTS, "No. Polisi");
			}
			if(editVehicle.getCapacity().compareTo(0d)<0){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_NUMERIC_NEGATIVE, "Kapasitas");
			}
			if(!editVehicle.getProductionYear().matches("[0-9]+")){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_PHONE_NUMERIC_INVALID, "Tahun Pembuatan");
			}
			
			if (error > 0) {
				Messagebox.show(fullErrMsg, "Message", 0, null);
			} else {
				if(editVehicle.getCreatedDate()==null){
					editVehicle.setCreatedBy(user.getUsername());
					editVehicle.setCreatedDate(Timestamp.from(
						OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
					
					if(vehicleList.size()==25){
						vehicleList.remove(24);
					}
					
					recordCount++;
					maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
				}
				if(editIndex!=-1){
					vehicleList.remove(editIndex);
				}
				vehicleList.add(0,editVehicle);
				editVehicle.setModifiedBy(user.getUsername());
				editVehicle.setModifiedDate(Timestamp.from(
						OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
				mobVehicleSvc.save(editVehicle);
				editMode = false;
				
				if(editIndex==-1){
					showSuccessMsgBox("E104");
				}
				else{
					showSuccessMsgBox("E100");
				}
				
				load();
			}
		}
	}

	@Command("src")
	@NotifyChange({"arg", "vehicleList", "pageNo", "maxPage", "recordCount"})
	public void search() {
		if(activeCheck()){
			search = true;
			pageNo = 1;
			
			sArg = String.valueOf(StringUtils.isEmpty(arg)?"":arg);
			vehicleList = mobVehicleSvc.findByArg(sArg, pageNo,
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			recordCount = mobVehicleSvc.countByArg(sArg);
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
			arg="";
		}
	}
	
	@Command("del")
	public void delete(@BindingParam("item") final MobVehicleDto del) {
		if(activeCheck()){
			if (del.getVehicleNo() == null)
				Messagebox.show("Pilih data yang akan dihapus", "Message", 0, null);
			else {
				if (!btnDel) {
					Messagebox.show(
						"Apakah anda ingin menonaktifkan kendaraan "+del.getVehicleNo()+" ?", "Perhatian",
						new Button[] { Button.YES, Button.NO }, Messagebox.QUESTION, Button.NO,
						new EventListener<Messagebox.ClickEvent>() {
							@Override
							public void onEvent(ClickEvent event)throws Exception {
								if (Messagebox.ON_YES.equals(event.getName())) {
									if(activeCheck()){
										mobVehicleSvc.disable(del.getVehicleNo());
										vehicleList.get(vehicleList.indexOf(del)).setIsActive(false);
										BindUtils.postNotifyChange(null, null, MDKendaraanVmd.this, "vehicleList");
										Clients.showNotification("Parameter berhasil dinonaktifkan",
											Clients.NOTIFICATION_TYPE_INFO, null, null, 5000);
									}
								}
							}
						}
					);
				} else {
					Clients.alert(StringConstants.MSG_NO_DEL_ACCESS, "Warning", "");
				}
			}
		}
	}

	@Command("switchPage")
	@NotifyChange({ "pageNo", "vehicleList" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}

			if (search) {
				vehicleList = mobVehicleSvc.findByArg(sArg, pageNo,
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			} else {
				vehicleList = mobVehicleSvc.findAll(pageNo, 
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			}
		}
	}

	@Command("sort")
	@NotifyChange({"vehicleList"})
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}

			if (search) {
				vehicleList = mobVehicleSvc.findByArg(sArg, pageNo, sortBy, sortAsc);
			} else {
				vehicleList = mobVehicleSvc.findAll(pageNo, sortBy, sortAsc);
			}
		}
	}
	
	@Command("reload")
	@NotifyChange({"recordCount","vehicleList","maxPage"})
	public void reload(){
		if (search) {
			recordCount = mobVehicleSvc.countByArg(sArg);
			vehicleList = mobVehicleSvc.findByArg(sArg, pageNo, 
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
		} else {
			recordCount = mobVehicleSvc.countAll();
			vehicleList = mobVehicleSvc.findAll(pageNo, 
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
		}
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
	}
	
	@Command
	@NotifyChange("flagIconWarning")
	public void ubahStatusAktif(@BindingParam("dto") MobVehicleDto dto){
		showConfirm("E002");
		setFlagIconWarning(true);
		editVehicle = new MobVehicleDto();
		editVehicle = dto;
		editVehicle.setIsActive(false);
		editVehicle.setModifiedBy(user.getFullname());
		editVehicle.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange("flagIconWarning")
	public void ubahStatusNonAktif(@BindingParam("dto") MobVehicleDto dto){
		showConfirm("E003");
		setFlagIconWarning(true);
		editVehicle = new MobVehicleDto();
		editVehicle = dto;
		editVehicle.setIsActive(true);
		editVehicle.setModifiedBy(user.getFullname());
		editVehicle.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}

	@Override
	@Command
	@NotifyChange({"recordCount","vehicleList","maxPage","maxScore","vehicleTypes"})
	public void yesConfirm(){
		try{
			mobVehicleSvc.save(editVehicle);
			flagConfirmMsg = false;
			showSuccessMsgBox("E000");
			//reload list
			vehicleList = mobVehicleSvc.findAll(pageNo, "modifiedDate", false);
			recordCount = mobVehicleSvc.countAll();
			
			vehicleTypes = miscellaneousSvc.getVehicleTypes();
			
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);

			BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
		}catch(Exception e){
			e.printStackTrace();
			showErrorMsgBox("E001");			
		}
	}
}
