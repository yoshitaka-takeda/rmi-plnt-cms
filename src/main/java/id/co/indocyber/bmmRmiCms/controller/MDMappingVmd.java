package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MobEmployeeDto;
import id.co.indocyber.bmmRmiCms.entity.MobFarmer;
import id.co.indocyber.bmmRmiCms.service.MobEmployeeSvc;
import id.co.indocyber.bmmRmiCms.service.MobFarmerSvc;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class MDMappingVmd extends BaseVmd {
	@WireVariable
	MobFarmerSvc mobFarmerSvc;
	@WireVariable
	MobEmployeeSvc mobEmployeeSvc;

	String urlIndex = "/master/mapping/index.zul";
	String refresh = "/master/mapping/ref.zul";
	
	private MUserDto user;

	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;

	private MobEmployeeDto selectEmployee, sSelectEmployee;
	private List<MobEmployeeDto> empList, fEmpList = new ArrayList<>();
	
	// U -> from unassignedList, A-> from assignedList
	private Set<MobFarmer> selectFarmerA, selectFarmerU;
	private List<MobFarmer> farmerListA, farmerListU, 
		fFarmerA = new ArrayList<>(), fFarmerU = new ArrayList<>();

	private boolean freezeId;
	private Boolean editMode;
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private Integer recordCount = 1;
	private String arg;
	
	private boolean saved = true;

	private Integer empId;
	private String bawahan;
		
	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public MobEmployeeDto getSelectEmployee() {
		return selectEmployee;
	}

	public void setSelectEmployee(MobEmployeeDto selectEmployee) {
		this.selectEmployee = selectEmployee;
	}

	public Set<MobFarmer> getSelectFarmerA() {
		return selectFarmerA;
	}

	public void setSelectFarmerA(Set<MobFarmer> selectFarmerA) {
		this.selectFarmerA = selectFarmerA;
	}

	public Set<MobFarmer> getSelectFarmerU() {
		return selectFarmerU;
	}

	public void setSelectFarmerU(Set<MobFarmer> selectFarmerU) {
		this.selectFarmerU = selectFarmerU;
	}

	public List<MobFarmer> getfFarmerA() {
		return fFarmerA;
	}

	public void setfFarmerA(List<MobFarmer> fFarmerA) {
		this.fFarmerA = fFarmerA;
	}

	public List<MobFarmer> getfFarmerU() {
		return fFarmerU;
	}

	public void setfFarmerU(List<MobFarmer> fFarmerU) {
		this.fFarmerU = fFarmerU;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public String getArg() {
		return arg;
	}

	public void setArg(String arg) {
		this.arg = arg;
	}
	
	public List<MobEmployeeDto> getfEmpList() {
		return fEmpList;
	}

	public void setfEmpList(List<MobEmployeeDto> fEmpList) {
		this.fEmpList = fEmpList;
	}
	
	public Integer getEmpId() {
		return empId;
	}

	public void setEmpId(Integer empId) {
		this.empId = empId;
	}

	public MobEmployeeDto getsSelectEmployee() {
		return sSelectEmployee;
	}

	public void setsSelectEmployee(MobEmployeeDto sSelectEmployee) {
		this.sSelectEmployee = sSelectEmployee;
	}

	public String getBawahan() {
		return bawahan;
	}

	public void setBawahan(String bawahan) {
		this.bawahan = bawahan;
	}

	@Init
	@NotifyChange({ "btnAdd", "btnDel", "btnEdit", "btnDl" })
	public void load() {
		editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
		user = (MUserDto) Sessions.getCurrent().getAttribute("user");
		//superAdmin = (Integer) Sessions.getCurrent().getAttribute("superAdmin") == 1;
		empList = mobEmployeeSvc.findAllDto();
		fEmpList.addAll(empList);
		
		selectFarmerA = new HashSet<>();
		selectFarmerU = new HashSet<>();
		
		for (MMenuRoleDto x : user.getRolePrivs()) {
			if ("MD-7".equals(x.getMenuCode())) {
				btnAdd = !x.getIsAdd();
				btnDel = !x.getIsDelete();
				btnDl = !x.getIsDownload();
				btnEdit = !x.getIsEdit();
				break;
			}
		}
	}

	@Command("getMapping")
	@NotifyChange({"fFarmerA","fFarmerU","selectFarmerA","selectFarmerU","empId","flagIconWarning"
		,"sSelectEmployee","bawahan"})
	public void checkSave() {
		if(selectEmployee==null||selectEmployee.getId()==null){
			showWarningMsgBox("E011");
			setFlagIconWarning(true);
			return;
		}		
		if(activeCheck()){
			if(saved){
				loadMapping();
			}
			else{
				Messagebox.show("Apakah Anda mau simpan perubahan assign farmer?", "Perhatian",
						new Button[] { Button.YES, Button.NO },
						Messagebox.QUESTION, Button.NO,
						new EventListener<Messagebox.ClickEvent>() {
							@Override
							public void onEvent(ClickEvent event) throws Exception {
								if (Messagebox.ON_YES.equals(event.getName())) {
									save();
									loadMapping();
								}
								else if (Messagebox.ON_NO.equals(event.getName())) {
									loadMapping();
								}
							}
						}
					);
			}
		}
	}

	@NotifyChange({"fFarmerA","fFarmerU","selectFarmerA","selectFarmerU","empId","sSelectEmployee","bawahan"})
	public void loadMapping(){
		if(selectEmployee.getId()==null){
			showWarningMsgBox("Tidak ada petugas yang dipilih!");
			return;
		}
		empId = selectEmployee.getId();
		sSelectEmployee = selectEmployee;
		bawahan = String.join(", ",mobEmployeeSvc.findDirectManagedNames(selectEmployee.getId()));
		farmerListA = mobFarmerSvc.findAssignedFarmers(empId);
		if(selectEmployee.getManager()==null||selectEmployee.getManager().getId()==0){
			farmerListU = mobFarmerSvc.findAllUnassigned();			
		}else{
			farmerListU = mobFarmerSvc.findAllUnassignedFromHead(selectEmployee.getManager().getId(), empId);						
		}
		
		fFarmerA.clear();
		fFarmerA.addAll(farmerListA);
		fFarmerU.clear();
		fFarmerU.addAll(farmerListU);
		
		selectFarmerA = null;
		selectFarmerU = null;
		
		BindUtils.postNotifyChange(null, null, MDMappingVmd.this, "empId");
		BindUtils.postNotifyChange(null, null, MDMappingVmd.this, "fFarmerA");
		BindUtils.postNotifyChange(null, null, MDMappingVmd.this, "fFarmerU");
		BindUtils.postNotifyChange(null, null, MDMappingVmd.this, "selectFarmerA");
		BindUtils.postNotifyChange(null, null, MDMappingVmd.this, "selectFarmerU");
		
		saved = true;
	}
	
	@Command("addFarmerToList")
	@NotifyChange({ "fFarmerA","fFarmerU","farmerListA","farmerListU","selectFarmerU" })
	public void addFarmerToList() {
		if (activeCheck()) {
			if (selectFarmerU != null && selectFarmerU.size()>0) {
				//int x = farmerListU.indexOf(selectFarmerU);
				//int y = fFarmerU.indexOf(selectFarmerU);
				farmerListA.addAll(selectFarmerU);
				fFarmerA.addAll(selectFarmerU);
				farmerListU.removeAll(selectFarmerU);
				fFarmerU.removeAll(selectFarmerU);
					
				selectFarmerU.clear();
				saved = false;
				
			}
		}
	}

	@Command("removeFarmerFromList")
	@NotifyChange({ "fFarmerA","fFarmerU","farmerListA","farmerListU","selectFarmerU" })
	public void removeFarmerFromList() {
		if (activeCheck()) {
			if (selectFarmerA != null && selectFarmerA.size()>0) {
				farmerListU.addAll(selectFarmerA);
				fFarmerU.addAll(selectFarmerA);
				farmerListA.removeAll(selectFarmerA);
				fFarmerA.removeAll(selectFarmerA);
					
				selectFarmerA.clear();
				saved = false;
				
			}
		}
	}
	
	@Command("dcAddFarmerToList")
	@NotifyChange({ "fFarmerA","fFarmerU","farmerListA","farmerListU","selectFarmerU" })
	public void dcAddFarmerToList(@BindingParam("farmer")MobFarmer farmer) {
		if (activeCheck()) {
			if (farmer != null) {
				int x = farmerListU.indexOf(farmer);
				int y = fFarmerU.indexOf(farmer);
				if (x != -1) {
					farmerListA.add(farmer);
					fFarmerA.add(farmer);
					farmerListU.remove(x);
					fFarmerU.remove(y);
					
					selectFarmerU = null;
					saved = false;
				}
			}
		}
	}

	@Command("dcRemoveFarmerFromList")
	@NotifyChange({ "fFarmerA","fFarmerU","farmerListA","farmerListU","selectFarmerU" })
	public void dcRemoveFarmerFromList(@BindingParam("farmer")MobFarmer farmer) {
		if (activeCheck()) {
			if (farmer != null) {
				int x = farmerListA.indexOf(farmer);
				int y = fFarmerA.indexOf(farmer);
				
				if (x != -1) {
					farmerListU.add(farmer);
					fFarmerU.add(farmer);
					farmerListA.remove(x);
					fFarmerA.remove(y);
					
					selectFarmerA = null;
					saved = false;
				}
			}
		}
	}

	@Command("srcU")
	@NotifyChange({ "fFarmerU", "selectFarmerU" })
	public void srcU(@BindingParam("src")String src){
		if (activeCheck()) {
			if(fFarmerU!=null)fFarmerU.clear();
			if(src!=null){
				for(MobFarmer x: farmerListU){
					if(x.getFullname().toLowerCase().contains(src.toLowerCase())|| 
							x.getReferenceCode().contains(src.toLowerCase())||
							x.getVillage().toLowerCase().contains(src.toLowerCase())){
						fFarmerU.add(x);
					}
				}
			}
			selectFarmerU = null;
		}
	}
	
	@Command("srcA")
	@NotifyChange({ "fFarmerA", "selectFarmerA" })
	public void srcA(@BindingParam("src")String src){
		if (activeCheck()) {
			if(fFarmerA!=null)fFarmerA.clear();
			if(src!=null){
				for(MobFarmer x: farmerListA){
					if(x.getFullname().toLowerCase().contains(src.toLowerCase())|| 
							x.getReferenceCode().contains(src.toLowerCase())||
							x.getVillage().toLowerCase().contains(src.toLowerCase())){
						fFarmerA.add(x);
					}
				}
			}
			selectFarmerA = null;
		}
	}
	
	@Command("refEmp")
	@NotifyChange({ "fEmpList" })
	public void refEmp(@BindingParam("src")String src){
		if (activeCheck()) {
			if(fEmpList!=null)fEmpList.clear();
			if(src!=null){
				for(MobEmployeeDto x: empList){
					if(x.getFullname().toLowerCase().contains(src.toLowerCase()) || 
							x.getId().toString().contains(src.toLowerCase()) ||
							(StringUtils.isEmpty(x.getEmployeePosition())?"":x.getEmployeePosition()).toLowerCase().contains(src.toLowerCase()) || 
							(x.getManager()==null?"":x.getManager().getFullname()).toLowerCase().contains(src.toLowerCase())){
						fEmpList.add(x);
					}
				}
			}
		}
	}
	
	@Command("save")
	public void save(){
		saved = true;
		List<Integer> farmerIds = new ArrayList<>();
		for(MobFarmer x: farmerListA){
			farmerIds.add(x.getId());
		}
		mobFarmerSvc.saveAssignments(empId, farmerIds, user.getUsername(), 
				Timestamp.from(OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
		
		showSuccessMsgBox("E105");
	}

}
