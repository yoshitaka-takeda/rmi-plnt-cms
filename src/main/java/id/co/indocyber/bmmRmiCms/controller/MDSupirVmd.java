package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MobDriverDto;
import id.co.indocyber.bmmRmiCms.entity.MAddressType;
import id.co.indocyber.bmmRmiCms.entity.MEducationType;
import id.co.indocyber.bmmRmiCms.entity.MGender;
import id.co.indocyber.bmmRmiCms.entity.MMaritalStatus;
import id.co.indocyber.bmmRmiCms.entity.MReligion;
import id.co.indocyber.bmmRmiCms.entity.MobDriver;
import id.co.indocyber.bmmRmiCms.service.MUserSvc;
import id.co.indocyber.bmmRmiCms.service.MiscellaneousSvc;
import id.co.indocyber.bmmRmiCms.service.MobDriverSvc;
import id.co.indocyber.bmmRmiCms.tools.DeepCopy;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

import java.sql.Timestamp;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.Period;
import java.time.ZoneOffset;
import java.util.List;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Window;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class MDSupirVmd extends BaseVmd {
	@WireVariable
	MobDriverSvc mobDriverSvc;
	@WireVariable
	MUserSvc mUserSvc;
	@WireVariable
	MiscellaneousSvc miscellaneousSvc;

	String urlIndex = "/master/supir/index.zul";
	String refresh = "/master/supir/ref.zul";
	
	private MUserDto user;

	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;
	
	private int editIndex=-1;

	private MobDriverDto editDriver;
	private List<MobDriverDto> driverList;

	private List<MGender> genders;
	private List<MReligion> religions;
	private List<MEducationType> educationLevels;
	private List<MAddressType> addressTypes;
	private List<MMaritalStatus> maritalStatuses;
	
	private boolean freezeId;
	private Boolean editMode;
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private Integer recordCount = 1;
	private String arg, sArg;
	
	private String sortBy = "";
	private boolean sortAsc = false;
	private boolean firstLoad = true;
	
	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public MobDriverDto getEditDriver() {
		return editDriver;
	}

	public void setEditDriver(MobDriverDto editDriver) {
		this.editDriver = editDriver;
	}

	public List<MobDriverDto> getDriverList() {
		return driverList;
	}

	public void setDriverList(List<MobDriverDto> driverList) {
		this.driverList = driverList;
	}

	public List<MGender> getGenders() {
		return genders;
	}

	public void setGenders(List<MGender> genders) {
		this.genders = genders;
	}

	public List<MReligion> getReligions() {
		return religions;
	}

	public void setReligions(List<MReligion> religions) {
		this.religions = religions;
	}

	public List<MEducationType> getEducationLevels() {
		return educationLevels;
	}

	public void setEducationLevels(List<MEducationType> educationLevels) {
		this.educationLevels = educationLevels;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public String getArg() {
		return arg;
	}

	public void setArg(String arg) {
		this.arg = arg;
	}
	
	public List<MAddressType> getAddressTypes() {
		return addressTypes;
	}

	public void setAddressTypes(List<MAddressType> addressTypes) {
		this.addressTypes = addressTypes;
	}

	public List<MMaritalStatus> getMaritalStatuses() {
		return maritalStatuses;
	}

	public void setMaritalStatuses(List<MMaritalStatus> maritalStatuses) {
		this.maritalStatuses = maritalStatuses;
	}

	@Init
	@NotifyChange({ "btnAdd", "btnDel", "btnEdit", "btnDl" })
	public void load() {
		editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
		genders = miscellaneousSvc.getGenders();
		user = (MUserDto) Sessions.getCurrent().getAttribute("user");
		//superAdmin = (Integer) Sessions.getCurrent().getAttribute("superAdmin") == 1;
		driverList = mobDriverSvc.findAll(pageNo, "id", false);
		recordCount = mobDriverSvc.countAll();
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
		
		for (MMenuRoleDto x : user.getRolePrivs()) {
			if ("MD-2".equals(x.getMenuCode())) {
				btnAdd = !x.getIsAdd();
				btnDel = !x.getIsDelete();
				btnDl = !x.getIsDownload();
				btnEdit = !x.getIsEdit();
				break;
			}
		}
	}

	@Command("back")
	public void back() {
		if (activeCheck()) {
			editMode=false;
		}
	}

	@Command("add")
	@NotifyChange({"editMode","editDriver","genders",
		"religions","educationLevels","maritalStatuses","addressTypes"})
	public void add() {
		if(activeCheck()){
			editMode = true;
			editDriver = new MobDriverDto();
			editIndex = -1;
			
			if(firstLoad){
				firstLoad = false;
				religions = miscellaneousSvc.getReligions();
				educationLevels = miscellaneousSvc.getEducationTypes();
				maritalStatuses = miscellaneousSvc.getMaritalStatuses();
				addressTypes = miscellaneousSvc.getAddressTypes();
			}
		}
	}

	@Command("edit")
	@NotifyChange({"editMode","editDriver","genders",
		"religions","educationLevels","maritalStatuses","addressTypes"})
	public void edit(@BindingParam("item") MobDriverDto edit) {
		if(activeCheck()){
			if (edit.getId() == null)
				Messagebox.show("Pilih data yang akan dirubah!", "Message", 0, null);
			else {
				editMode = true;
				editDriver = (MobDriverDto)DeepCopy.deepCopy(edit);
				editIndex = driverList.indexOf(edit);
				
				if(firstLoad){
					firstLoad = false;
					religions = miscellaneousSvc.getReligions();
					educationLevels = miscellaneousSvc.getEducationTypes();
					maritalStatuses = miscellaneousSvc.getMaritalStatuses();
					addressTypes = miscellaneousSvc.getAddressTypes();
				}
			}
		}
	}

	@Command("save")
	@NotifyChange({"editDriver","editMode","driverList","maxPage","recordCount"})
	public void save(@BindingParam("btn")Component btn) {
		if(activeCheck()){
			// Database design is not final, but one does what one can...
			/*
			boolean existence;
			try {
				existence = mobDriverSvc.existence(editDriver.getId());
			} catch (Exception e) {
				existence = false;
			}
			*/
			String fullErrMsg = StringConstants.MSG_FORM_TEMPLATE;
			int error = 0;
			if (StringUtils.isEmpty(editDriver.getFullname())) {
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Nama Pengemudi");
			} else if(mobDriverSvc.existsName(editDriver.getId(), editDriver.getFullname())){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_ID_EXISTS, "Nama Pengemudi");
			}
			if(editDriver.getCreatedDate()==null && (
					editDriver.getBirthday()!=null && 
					Period.between(editDriver.getBirthday().toLocalDate(), LocalDate.now()).getYears()<17)){
				error++;
				fullErrMsg += StringConstants.MSG_FORM_FIELD_AGE_RESTRICTION;
			}
			if(!StringUtils.isEmpty(editDriver.getPhoneNum()) && 
					(!editDriver.getPhoneNum().matches("[0-9]+") || 
							! editDriver.getPhoneNum().startsWith("08")) ) {
				error++;
				fullErrMsg += String.format(
						StringConstants.MSG_FORM_PHONE_NUMERIC_INVALID,
						"Mobile Phone Number");
			}
			if (editDriver.getGender()==null){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Gender");
			}
			if (editDriver.getReligion()==null){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Agama");
			}
			if(!StringUtils.isEmpty(editDriver.getEmail()) && !(editDriver.getEmail().
					matches("[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})"))){
				error++;
				fullErrMsg += StringConstants.MSG_FORM_EMAIL_INVALID;
			}
			else if(!StringUtils.isEmpty(editDriver.getEmail()) && 
					mobDriverSvc.existsEmail(editDriver.getId(), editDriver.getEmail())){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_ID_EXISTS, "E-mail");
			}
			if (editDriver.getMaritalStatus()==null){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Status Perkawinan");
			}
			if (StringUtils.isEmpty(editDriver.getDriverType())){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Jenis Supir");
			}
			if (error > 0) {
				Messagebox.show(fullErrMsg, "Message", 0, null);
			} else {
				if(editDriver.getId()==null){
					editDriver.setIsActive(true);
					if(driverList.size()==25){
						driverList.remove(24);
					}
					
					recordCount++;
					maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
					editDriver.setCreatedBy(user.getUsername());
					editDriver.setCreatedDate(Timestamp.from(
							OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
				}
				if(editIndex!=-1){
					driverList.remove(editIndex);
				}
				driverList.add(0,editDriver);
				
				editDriver.setModifiedBy(user.getUsername());
				editDriver.setModifiedDate(Timestamp.from(
						OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
				MobDriver saved = mobDriverSvc.save(editDriver);
				
				driverList.get(0).setId(saved.getId());
				if(editDriver.getId()!=null){
					mUserSvc.updateDriverPhone(editDriver.getId(), editDriver.getPhoneNum());
				}
				
				editMode = false;
				if(editIndex==-1){
					showSuccessMsgBox("E004");
				}
				else{
					showSuccessMsgBox("E100");
				}
				
				load();
			}
		}
	}

	/*
	private boolean validatePassword(String passwd) {
		int chr = 0, spec = 0, digit = 0, length;
		length = passwd.length();
		for (char y : passwd.toCharArray()) {
			if (Character.isAlphabetic(y)) {
				chr++;
			} else if (Character.isDigit(y)) {
				digit++;
			} else
				spec++;
		}
		return chr >= minChar && spec >= minSpec && digit >= minDigit
				&& length >= minLength;
	}
	*/

	@Command("src")
	@NotifyChange({ "arg", "driverList", "pageNo", "maxPage", "recordCount"})
	public void search() {
		if(activeCheck()){
			search = true;
			pageNo = 1;
	
			sArg = String.valueOf(StringUtils.isEmpty(arg)?"":arg);
			driverList = mobDriverSvc.findByArg(sArg, pageNo,
					StringUtils.isEmpty(sortBy) ? "id" : sortBy, sortAsc);
			recordCount = mobDriverSvc.countByArg(sArg);
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
			arg="";
		}
	}
	
	@Command("del")
	public void delete(@BindingParam("item") final MobDriverDto del) {
		if(activeCheck()){
			if (del.getId() == null)
				Messagebox.show("Pilih data yang akan dihapus", "Message", 0, null);
			else {
				if (!btnDel) {
					Messagebox.show(
						"Apakah anda ingin menonaktifkan driver "+del.getFullname()+" ?", "Perhatian",
						new Button[] { Button.YES, Button.NO }, Messagebox.QUESTION, Button.NO,
						new EventListener<Messagebox.ClickEvent>() {
							@Override
							public void onEvent(ClickEvent event)throws Exception {
								if (Messagebox.ON_YES.equals(event.getName())) {
									if(activeCheck()){
										mobDriverSvc.disable(del.getId());
										driverList.get(driverList.indexOf(del)).setIsActive(false);
										BindUtils.postNotifyChange(null, null, MDSupirVmd.this, "driverList");
										Clients.showNotification("Parameter berhasil dinonaktifkan",
											Clients.NOTIFICATION_TYPE_INFO, null, null, 5000);
									}
								}
							}
						}
					);
				} else {
					Clients.alert(StringConstants.MSG_NO_DEL_ACCESS, "Warning", "");
				}
			}
		}
	}

	@Command("switchPage")
	@NotifyChange({ "pageNo", "driverList" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}

			if (search) {
				driverList = mobDriverSvc.findByArg(sArg, pageNo,
					StringUtils.isEmpty(sortBy) ? "id" : sortBy, sortAsc);
			} else {
				driverList = mobDriverSvc.findAll(pageNo, 
					StringUtils.isEmpty(sortBy) ? "id" : sortBy, sortAsc);
			}
		}
	}

	@Command("sort")
	@NotifyChange({"driverList"})
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}

			if (search) {
				driverList = mobDriverSvc.findByArg(sArg, pageNo, sortBy, sortAsc);
			} else {
				driverList = mobDriverSvc.findAll(pageNo, sortBy, sortAsc);
			}
		}
	}
	
	@Command("reload")
	@NotifyChange({"recordCount","driverList","maxPage"})
	public void reload(){
		if (search) {
			recordCount = mobDriverSvc.countByArg(sArg);
			driverList = mobDriverSvc.findByArg(sArg, pageNo, 
					StringUtils.isEmpty(sortBy) ? "id" : sortBy, sortAsc);
		} else {
			recordCount = mobDriverSvc.countAll();
			driverList = mobDriverSvc.findAll(pageNo, 
					StringUtils.isEmpty(sortBy) ? "id" : sortBy, sortAsc);
		}
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
	}
	
	@Command
	@NotifyChange("flagIconWarning")
	public void ubahStatusAktif(@BindingParam("dto") MobDriverDto dto){
		showConfirm("E002");
		setFlagIconWarning(true);
		editDriver = new MobDriverDto();
		editDriver = dto;
		editDriver.setIsActive(false);
		editDriver.setModifiedBy(user.getFullname());
		editDriver.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange("flagIconWarning")
	public void ubahStatusNonAktif(@BindingParam("dto") MobDriverDto dto){
		showConfirm("E003");
		setFlagIconWarning(true);
		editDriver = new MobDriverDto();
		editDriver = dto;
		editDriver.setIsActive(true);
		editDriver.setModifiedBy(user.getFullname());
		editDriver.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}

	@Override
	@Command
	@NotifyChange({"recordCount","driverList","maxPage","maxScore"})
	public void yesConfirm(){
		try{
			mobDriverSvc.save(editDriver);
			flagConfirmMsg = false;
			showSuccessMsgBox("E000");
			//reload list
			driverList = mobDriverSvc.findAll(pageNo, "id", false);
			recordCount = mobDriverSvc.countAll();
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);

			BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
		}catch(Exception e){
			e.printStackTrace();
			showErrorMsgBox("E001");			
		}
	}

}
