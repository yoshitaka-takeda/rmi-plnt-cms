package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MobActivitiesEntryDto;
import id.co.indocyber.bmmRmiCms.dto.MobEmployeeDto;
import id.co.indocyber.bmmRmiCms.service.GlobalSvc;
import id.co.indocyber.bmmRmiCms.service.MiscellaneousSvc;
import id.co.indocyber.bmmRmiCms.service.MobActivitiesEntrySvc;
import id.co.indocyber.bmmRmiCms.service.MobEmployeeSvc;
import id.co.indocyber.bmmRmiCms.tools.DeepCopy;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.List;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Textbox;
import org.zkoss.zul.Window;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VSActivitiesEntryVmd extends BaseVmd {
	@WireVariable
	MobActivitiesEntrySvc mobActivitiesEntrySvc;
	@WireVariable
	MobEmployeeSvc mobEmployeeSvc;
	@WireVariable
	GlobalSvc globalSvc;
	@WireVariable
	MiscellaneousSvc miscellaneousSvc;
	
	String urlIndex = "/visit/activity/index.zul";
	String refresh = "/visit/activity/ref.zul";
	
	private MUserDto user;

	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;

	private MobActivitiesEntryDto viewEntry;
	private List<MobActivitiesEntryDto> entryList;
	private List<String> activities;

	private boolean freezeId;
	private Boolean editMode;
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private Integer recordCount = 1;
	private String arg, sArg;
	
	private String sortBy = "";
	private boolean sortAsc = true;

	private List<Integer> intList;
	
	private List<MobEmployeeDto> empList, fEmpList;
	
	private int editIndex=-1;
	
	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}
	
	public MobActivitiesEntryDto getViewEntry() {
		return viewEntry;
	}

	public void setViewEntry(MobActivitiesEntryDto viewEntry) {
		this.viewEntry = viewEntry;
	}

	public List<MobActivitiesEntryDto> getEntryList() {
		return entryList;
	}

	public void setEntryList(List<MobActivitiesEntryDto> entryList) {
		this.entryList = entryList;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public String getArg() {
		return arg;
	}

	public void setArg(String arg) {
		this.arg = arg;
	}
	
	public List<MobEmployeeDto> getfEmpList() {
		return fEmpList;
	}

	public void setfEmpList(List<MobEmployeeDto> fEmpList) {
		this.fEmpList = fEmpList;
	}
	
	public List<String> getActivities() {
		return activities;
	}

	public void setActivities(List<String> activities) {
		this.activities = activities;
	}

	@Init
	@NotifyChange({ "btnAdd", "btnDel", "btnEdit", "btnDl" })
	public void load() {
		editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
		user = (MUserDto) Sessions.getCurrent().getAttribute("user");
		//superAdmin = (Integer) Sessions.getCurrent().getAttribute("superAdmin") == 1;
		
		activities = new ArrayList<>(miscellaneousSvc.getOptions("activity_type"));
		activities.add(0,null);
		
		if(user.isAdmin()){
			intList = mobEmployeeSvc.findAllIds();
		}
		else{
			intList = globalSvc.getAllMyTeam(user.getPersonId());
		}
		
		empList = mobEmployeeSvc.findAllById(intList);
		
		entryList = mobActivitiesEntrySvc.findAllById(intList, "", pageNo, "createdDate", true);
		recordCount = mobActivitiesEntrySvc.countAllById(intList, "");
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);

		for (MMenuRoleDto x : user.getRolePrivs()) {
			if ("VS-3".equals(x.getMenuCode())) {
				btnAdd = !x.getIsAdd();
				btnDel = !x.getIsDelete();
				btnDl = !x.getIsDownload();
				btnEdit = !x.getIsEdit();
				break;
			}
		}
	}

	@Command("back")
	public void back() {
		if (activeCheck()) {
			editMode=false;
		}
	}

	@Command("add")
	@NotifyChange({"editMode","viewEntry"})
	public void add() {
		if(activeCheck()){
			editMode = true;
			viewEntry = new MobActivitiesEntryDto();
			editIndex = -1;
		}
	}
	
	@Command("edit")
	@NotifyChange({"editMode","viewEntry"})
	public void edit(@BindingParam("item") MobActivitiesEntryDto edit) {
		if(activeCheck()){
			if (edit.getId() == null) {
				org.zkoss.zul.Messagebox.show("Pilih data yang akan dilihat!", "Message", 0, null);
			}
			else {
				editMode = true;
				viewEntry = (MobActivitiesEntryDto)DeepCopy.deepCopy(edit);
				editIndex = entryList.indexOf(edit);
			}
		}
	}
	
	@Command("src")
	@NotifyChange({ "arg", "entryList", "pageNo", "maxPage", "recordCount"})
	public void search() {
		if(activeCheck()){
			search = true;
			pageNo = 1;
			
			sArg = String.valueOf(StringUtils.isEmpty(arg)?"":arg);
			if(user.isAdmin()){
				entryList = mobActivitiesEntrySvc.findAll(sArg, pageNo, StringUtils.isEmpty(sortBy) ? "createdDate" : sortBy, true);
				recordCount = mobActivitiesEntrySvc.countAll(sArg);
			}
			else{
				entryList = mobActivitiesEntrySvc.findAllById(intList, sArg, pageNo, StringUtils.isEmpty(sortBy) ? "createdDate" : sortBy, true);
				recordCount = mobActivitiesEntrySvc.countAllById(intList, sArg);
			}
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
			arg = "";
		}
	}

	@Command("reload")
	@NotifyChange({"recordCount","entryList","maxPage"})
	public void reload(){
		if(user.isAdmin()){
			entryList = mobActivitiesEntrySvc.findAll(StringUtils.isEmpty(sArg)?"":sArg, pageNo, StringUtils.isEmpty(sortBy) ? "createdDate" : sortBy, true);
			recordCount = mobActivitiesEntrySvc.countAll(StringUtils.isEmpty(sArg)?"":sArg);
		}
		else{
			entryList = mobActivitiesEntrySvc.findAllById(intList, StringUtils.isEmpty(sArg)?"":sArg, pageNo, StringUtils.isEmpty(sortBy) ? "createdDate" : sortBy, true);
			recordCount = mobActivitiesEntrySvc.countAllById(intList, StringUtils.isEmpty(sArg)?"":sArg);
		}
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
	}
	
	@Command("switchPage")
	@NotifyChange({ "pageNo", "entryList" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}

			if(user.isAdmin()){
				entryList = mobActivitiesEntrySvc.findAll(StringUtils.isEmpty(sArg)?"":sArg, pageNo, StringUtils.isEmpty(sortBy) ? "createdDate" : sortBy, true);
			}
			else{
				entryList = mobActivitiesEntrySvc.findAllById(intList, StringUtils.isEmpty(sArg)?"":sArg, pageNo, StringUtils.isEmpty(sortBy) ? "createdDate" : sortBy, true);
			}
			
		}
	}

	@Command("sort")
	@NotifyChange({"entryList"})
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}

			if(user.isAdmin()){
				entryList = mobActivitiesEntrySvc.findAll(
					StringUtils.isEmpty(sArg)?"":sArg, pageNo, sortBy, true);
			}
			else{
				entryList = mobActivitiesEntrySvc.findAllById(intList, 
					StringUtils.isEmpty(sArg)?"":sArg, pageNo, sortBy, true);
			}
		}
	}
	
	@Command("refEmp")
	@NotifyChange({ "fEmpList" })
	public void refEmp(@BindingParam("src")String src){
		if (activeCheck()) {
			if(fEmpList!=null)fEmpList.clear();
			if(src!=null){
				for(MobEmployeeDto x: empList){
					if(x.getFullname().toLowerCase().contains(src.toLowerCase()) || 
							x.getId().toString().contains(src.toLowerCase()) ||
							(StringUtils.isEmpty(x.getEmployeePosition())?"":x.getEmployeePosition()).toLowerCase().contains(src.toLowerCase()) || 
							(x.getManager()==null?"":x.getManager().getFullname()).toLowerCase().contains(src.toLowerCase())){
						fEmpList.add(x);
					}
				}
			}
		}
	}
	
	@Command
	public void loadListProfile(@BindingParam("tb")Textbox x){
		if(fEmpList == null) fEmpList = new ArrayList<MobEmployeeDto>();
		fEmpList.clear();
		fEmpList.addAll(empList);
		x.focus();x.setValue("");
		BindUtils.postNotifyChange(null, null, this, "fEmpList");
	}
	
	@Command
	@NotifyChange("flagIconWarning")
	public void ubahStatusAktif(@BindingParam("dto") MobActivitiesEntryDto dto){
		showConfirm("E002");
		setFlagIconWarning(true);
		viewEntry = new MobActivitiesEntryDto();
		viewEntry = dto;
		viewEntry.setIsActive(false);
	}
	
	@Command
	@NotifyChange("flagIconWarning")
	public void ubahStatusNonAktif(@BindingParam("dto") MobActivitiesEntryDto dto){
		showConfirm("E003");
		setFlagIconWarning(true);
		viewEntry = new MobActivitiesEntryDto();
		viewEntry = dto;
		viewEntry.setIsActive(true);
	}

	@Override
	@Command
	@NotifyChange({"recordCount","entryList","maxPage","maxScore"})
	public void yesConfirm(){
		try{
			mobActivitiesEntrySvc.save(viewEntry);
			flagConfirmMsg = false;
			showSuccessMsgBox("E000");
			//reload list
			entryList = mobActivitiesEntrySvc.findAll(StringUtils.isEmpty(sArg)?"":sArg, pageNo, StringUtils.isEmpty(sortBy) ? "createdDate" : sortBy, true);
			recordCount = mobActivitiesEntrySvc.countAllById(intList, StringUtils.isEmpty(sArg)?"":sArg);
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
			
			BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
		}catch(Exception e){
			e.printStackTrace();
			showErrorMsgBox("E001");			
		}
	}
	
	@Command("save")
	@NotifyChange({"viewEntry","editMode","entryList","maxPage","recordCount","maxScore"})
	public void save(@BindingParam("btn")Component btn) {
		if(activeCheck()){
			// Database design is not final, but one does what one can...
			/*
			boolean existence;
			try {
				existence = mobActivitiesEntrySvc.existence(viewEntry.getParamCode());
			} catch (Exception e) {
				existence = false;
			}
			*/
			String fullErrMsg = StringConstants.MSG_FORM_TEMPLATE;
			int error = 0;
			if(viewEntry.getEmployee()==null){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Petugas");
			}
			
			if (error > 0) {
				org.zkoss.zul.Messagebox.show(fullErrMsg, "Message", 0, null);
			} else {
				if(viewEntry.getCreatedDate()==null){
					viewEntry.setIsActive(true);
					viewEntry.setCreatedBy(user.getUsername());
					viewEntry.setCreatedDate(Timestamp.from(
							OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
					
					if(entryList.size()==25){
						entryList.remove(24);
					}
					recordCount++;
					maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
				}
				if(editIndex!=-1){
					entryList.remove(editIndex);
				}
				entryList.add(0,viewEntry);
				
				viewEntry.setModifiedBy(user.getUsername());
				viewEntry.setModifiedDate(Timestamp.from(
						OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
				
				mobActivitiesEntrySvc.save(viewEntry);
				
				editMode = false;
				((Window)btn.getParent().getParent()).setVisible(false);
				if(editIndex==-1){
					showSuccessMsgBox("E107");
				}
				else{
					showSuccessMsgBox("E100");
				}
				
				load();
			}
		}
	}
}
