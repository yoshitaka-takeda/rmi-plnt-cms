package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.entity.MSugarcaneParamScoring;
import id.co.indocyber.bmmRmiCms.service.MSugarcaneParamScoringSvc;
import id.co.indocyber.bmmRmiCms.tools.DeepCopy;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.List;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Window;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class MDPenilaianTebuVmd extends BaseVmd {
	@WireVariable
	MSugarcaneParamScoringSvc mSugarcaneParamScoringSvc;

	String urlIndex = "/master/tebu/index.zul";
	String refresh = "/master/tebu/ref.zul";
	
	private MUserDto user;

	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;
	
	private int editIndex=-1;

	private MSugarcaneParamScoring editParam;
	private List<MSugarcaneParamScoring> paramList;

	private boolean freezeId;
	private Boolean editMode;
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private Integer recordCount = 1;
	private String arg,sArg;
	
	private String sortBy = "";
	private boolean sortAsc = false;
	
	private Double maxScore = 0d, prevValue = 0d, currentSum = 0d;
	
	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public MSugarcaneParamScoring getEditParam() {
		return editParam;
	}

	public void setEditParam(MSugarcaneParamScoring editParam) {
		this.editParam = editParam;
	}

	public List<MSugarcaneParamScoring> getParamList() {
		return paramList;
	}

	public void setParamList(List<MSugarcaneParamScoring> paramList) {
		this.paramList = paramList;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public String getArg() {
		return arg;
	}

	public void setArg(String arg) {
		this.arg = arg;
	}
	
	public Double getMaxScore() {
		return maxScore;
	}

	public void setMaxScore(Double maxScore) {
		this.maxScore = maxScore;
	}

	public Double getCurrentSum() {
		return currentSum;
	}

	public void setCurrentSum(Double currentSum) {
		this.currentSum = currentSum;
	}

	@Init
	@NotifyChange({ "btnAdd", "btnDel", "btnEdit", "btnDl" })
	public void load() {
		editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
		user = (MUserDto) Sessions.getCurrent().getAttribute("user");
		//superAdmin = (Integer) Sessions.getCurrent().getAttribute("superAdmin") == 1;
		paramList = mSugarcaneParamScoringSvc.findAll(pageNo, "processedDate", false);
		recordCount = mSugarcaneParamScoringSvc.countAll();
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
		maxScore = mSugarcaneParamScoringSvc.getSum();
		
		for (MMenuRoleDto x : user.getRolePrivs()) {
			if ("MD-1".equals(x.getMenuCode())) {
				btnAdd = !x.getIsAdd();
				btnDel = !x.getIsDelete();
				btnDl = !x.getIsDownload();
				btnEdit = !x.getIsEdit();
				break;
			}
		}
	}

	@Command("back")
	public void back() {
		if (activeCheck()) {
			editMode=false;
		}
	}

	@Command("add")
	@NotifyChange({"editMode","editParam","currentSum"})
	public void add() {
		if(activeCheck()){
			editMode = true;
			editParam = new MSugarcaneParamScoring();
			editParam.setIsActive(true);
			editIndex = -1;
			prevValue = 0d;
			reCalc();
		}
	}

	@Command("edit")
	@NotifyChange({"editMode","editParam","currentSum"})
	public void edit(@BindingParam("item") MSugarcaneParamScoring edit) {
		if(activeCheck()){
			if (edit.getParamCode() == null)
				Messagebox.show("Pilih data yang akan dirubah!", "Message", 0, null);
			else {
				editMode = true;
				editParam = (MSugarcaneParamScoring)DeepCopy.deepCopy(edit);
				editIndex = paramList.indexOf(edit);
				prevValue = paramList.get(editIndex).getWeight() * (paramList.get(editIndex).getIsActive()?1:0);
				reCalc();
			}
		}
	}

	@Command("save")
	@NotifyChange({"editParam","editMode","paramList","maxPage","recordCount","maxScore"})
	public void save(@BindingParam("btn")Component btn) {
		if(activeCheck()){
			// Database design is not final, but one does what one can...
			/*
			boolean existence;
			try {
				existence = mSugarcaneParamScoringSvc.existence(editParam.getParamCode());
			} catch (Exception e) {
				existence = false;
			}
			*/
			String fullErrMsg = StringConstants.MSG_FORM_TEMPLATE;
			int error = 0;
			if(editParam.getWeight().compareTo(0d)<0){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_NUMERIC_NEGATIVE, "Bobot");
			}
			else{
				// Refresh sum
				maxScore = mSugarcaneParamScoringSvc.getSum();
				currentSum = maxScore - prevValue + 
					(editParam.getWeight()==null?0d:editParam.getWeight())*(
						editParam.getIsActive()==null?0:editParam.getIsActive()?1:0);
				if (currentSum.compareTo(100d)>0){
					error++;
					fullErrMsg += String.format(StringConstants.MSG_FORM_NUMERIC_OVERFLOW, 
						"Jumlah Bobot", "100");
				}
				else{
					maxScore = currentSum;
				}
			}
			if (error > 0) {
				Messagebox.show(fullErrMsg, "Message", 0, null);
			} else {
				if(editParam.getCreatedDate()==null){
					editParam.setIsActive(true);
					editParam.setCreatedBy(user.getUsername());
					editParam.setCreatedDate(Timestamp.from(
							OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
					
					if(paramList.size()==25){
						paramList.remove(24);
					}
					recordCount++;
					maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
				}
				if(editIndex!=-1){
					paramList.remove(editIndex);
				}
				paramList.add(0,editParam);
				
				editParam.setProcessedBy(user.getUsername());
				editParam.setProcessedDate(Timestamp.from(
						OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
				
				MSugarcaneParamScoring save = mSugarcaneParamScoringSvc.save(editParam);
				
				if(editIndex==-1){
					paramList.get(0).setParamCode(save.getParamCode());
				}
				
				editMode = false;
				((Window)btn.getParent().getParent()).setVisible(false);
				if(editIndex==-1){
					showSuccessMsgBox("E103");
				}
				else{
					showSuccessMsgBox("E100");
				}
				
				load();
			}
		}
	}


	@Command("reCalc")
	@NotifyChange({"currentSum"})
	public void reCalc(){
		maxScore = mSugarcaneParamScoringSvc.getSum();
		currentSum = maxScore - prevValue + 
			(editParam.getWeight()==null?0d:editParam.getWeight())*(
				editParam.getIsActive()==null?0:editParam.getIsActive()?1:0);
	}

	@Command("src")
	@NotifyChange({ "arg", "paramList", "pageNo", "maxPage", "recordCount"})
	public void search() {
		if(activeCheck()){
			search = true;
			pageNo = 1;
			
			sArg = String.valueOf(StringUtils.isEmpty(arg)?"":arg);
			paramList = mSugarcaneParamScoringSvc.findByArg(sArg, pageNo,
					StringUtils.isEmpty(sortBy) ? "processedDate" : sortBy, sortAsc);
			recordCount = mSugarcaneParamScoringSvc.countByArg(sArg);
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
			arg = "";
		}
	}
	
	@Command
	@NotifyChange("flagIconWarning")
	public void ubahStatusAktif(@BindingParam("dto") MSugarcaneParamScoring dto){
		showConfirm("E002");
		setFlagIconWarning(true);
		editParam = new MSugarcaneParamScoring();
		editParam = dto;
		editParam.setIsActive(false);
	}
	
	@Command
	@NotifyChange("flagIconWarning")
	public void ubahStatusNonAktif(@BindingParam("dto") MSugarcaneParamScoring dto){
		showConfirm("E003");
		setFlagIconWarning(true);
		editParam = new MSugarcaneParamScoring();
		editParam = dto;
		editParam.setIsActive(true);
	}

	@Override
	@Command
	@NotifyChange({"recordCount","paramList","maxPage","maxScore"})
	public void yesConfirm(){
		try{
			mSugarcaneParamScoringSvc.save(editParam);
			flagConfirmMsg = false;
			showSuccessMsgBox("E000");
			//reload list
			paramList = mSugarcaneParamScoringSvc.findAll(pageNo, "processedDate", false);
			recordCount = mSugarcaneParamScoringSvc.countAll();
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
			maxScore = mSugarcaneParamScoringSvc.getSum();
			
			BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
		}catch(Exception e){
			e.printStackTrace();
			showErrorMsgBox("E001");			
		}
	}
	
	

	
	@Command("del")
	public void delete(@BindingParam("item") final MSugarcaneParamScoring del) {
		if(activeCheck()){
			if (del.getParamCode() == null)
				Messagebox.show("Pilih data yang akan dihapus", "Message", 0, null);
			else {
				if (!btnDel) {
					Messagebox.show(
						"Apakah anda ingin menonaktifkan parameter?", "Perhatian",
						new Button[] { Button.YES, Button.NO }, Messagebox.QUESTION, Button.NO,
						new EventListener<Messagebox.ClickEvent>() {
							@Override
							public void onEvent(ClickEvent event)throws Exception {
								if (Messagebox.ON_YES.equals(event.getName())) {
									if(activeCheck()){
										maxScore -= del.getWeight();
										mSugarcaneParamScoringSvc.disable(del.getParamCode());
										paramList.get(paramList.indexOf(del)).setIsActive(false);
										BindUtils.postNotifyChange(null, null, MDPenilaianTebuVmd.this, "paramList");
										Clients.showNotification("Parameter berhasil dinonaktifkan",
											Clients.NOTIFICATION_TYPE_INFO, null, null, 5000);
									}
								}
							}
						}
					);
				} else {
					Clients.alert(StringConstants.MSG_NO_DEL_ACCESS, "Warning", "");
				}
			}
		}
	}

	@Command("switchPage")
	@NotifyChange({ "pageNo", "paramList" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}

			if (search) {
				paramList = mSugarcaneParamScoringSvc.findByArg(sArg, pageNo,
					StringUtils.isEmpty(sortBy) ? "processedDate" : sortBy, sortAsc);
			} else {
				paramList = mSugarcaneParamScoringSvc.findAll(pageNo, 
					StringUtils.isEmpty(sortBy) ? "processedDate" : sortBy, sortAsc);
			}
		}
	}

	@Command("sort")
	@NotifyChange({"paramList"})
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}

			if (search) {
				paramList = mSugarcaneParamScoringSvc.findByArg(sArg, pageNo, sortBy, sortAsc);
			} else {
				paramList = mSugarcaneParamScoringSvc.findAll(pageNo, sortBy, sortAsc);
			}
		}
	}
	
	@Command("reload")
	@NotifyChange({"recordCount","paramList","maxPage"})
	public void reload(){
		if (search) {
			recordCount = mSugarcaneParamScoringSvc.countByArg(sArg);
			paramList = mSugarcaneParamScoringSvc.findByArg(sArg, pageNo, 
					StringUtils.isEmpty(sortBy) ? "processedDate" : sortBy, sortAsc);
		} else {
			recordCount = mSugarcaneParamScoringSvc.countAll();
			paramList = mSugarcaneParamScoringSvc.findAll(pageNo, 
					StringUtils.isEmpty(sortBy) ? "processedDate" : sortBy, sortAsc);
		}
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
	}
}
