package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.entity.MRole;
import id.co.indocyber.bmmRmiCms.service.MMenuRoleSvc;
import id.co.indocyber.bmmRmiCms.service.MRoleSvc;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

import java.sql.Timestamp;
import java.time.OffsetDateTime;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class UMRoleMenuVmd extends BaseVmd {
	@WireVariable
	MRoleSvc mRoleSvc;

	@WireVariable
	MMenuRoleSvc mMenuRoleSvc;

	String urlIndex = "/userManagement/role_menu/index.zul";
	String urlEdit = "/userManagement/role_menu/edit.zul";
	String refresh = "/userManagement/role_menu/ref.zul";

	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;

	private MUserDto user;
	private List<MRole> roleList;

	private MRole editRole;
	private MRole selectedRole;
	private List<List<MMenuRoleDto>> rolePrivs;
	private List<MMenuRoleDto> fRolePrivs;

	private boolean freezeId;
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private Integer recordCount = 1;

	private String sortBy = "";
	private boolean sortAsc = false;
	
	private String arg="",sArg;
	private Boolean editMode;

	private Integer moduleIdx, editIndex;
	private String defModulIdx;
	private List<String> moduleList;
	
	
	public String getDefModulIdx() {
		return defModulIdx;
	}

	public void setDefModulIdx(String defModulIdx) {
		this.defModulIdx = defModulIdx;
	}

	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public List<MRole> getRoleList() {
		return roleList;
	}

	public void setRoleList(List<MRole> roleList) {
		this.roleList = roleList;
	}

	public MRole getEditRole() {
		return editRole;
	}

	public void setEditRole(MRole editRole) {
		this.editRole = editRole;
	}

	public List<List<MMenuRoleDto>> getRolePrivs() {
		return rolePrivs;
	}

	public void setRolePrivs(List<List<MMenuRoleDto>> rolePrivs) {
		this.rolePrivs = rolePrivs;
	}

	public List<MMenuRoleDto> getfRolePrivs() {
		return fRolePrivs;
	}

	public void setfRolePrivs(List<MMenuRoleDto> fRolePrivs) {
		this.fRolePrivs = fRolePrivs;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public String getArg() {
		return arg;
	}

	public void setArg(String arg) {
		this.arg = arg;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}
	
	public Integer getModuleIdx() {
		return moduleIdx;
	}

	public void setModuleIdx(Integer moduleIdx) {
		this.moduleIdx = moduleIdx;
	}
	
	public List<String> getModuleList() {
		return moduleList;
	}

	public void setModuleList(List<String> moduleList) {
		this.moduleList = moduleList;
	}

	public MRole getSelectedRole() {
		return selectedRole;
	}

	public void setSelectedRole(MRole selectedRole) {
		this.selectedRole = selectedRole;
	}

	@Init
	@NotifyChange({ "btnAdd", "btnDel", "btnEdit", "btnDl" })
	public void load() {
		editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
		user = (MUserDto) Sessions.getCurrent().getAttribute("user");
		editRole = (MRole) Sessions.getCurrent().getAttribute("editRole");
		
		moduleList = Arrays.asList(new String[]{"Mobile","Web"});
		
		roleList = mRoleSvc.findAll(pageNo, "modifiedDate", sortAsc);
		recordCount = mRoleSvc.countAll();
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);

		for (MMenuRoleDto x : user.getRolePrivs()) {
			if ("UM-2".equals(x.getMenuCode())) {
				btnAdd = !x.getIsAdd();
				btnDel = !x.getIsDelete();
				btnDl = !x.getIsDownload();
				btnEdit = !x.getIsEdit();
				break;
			}
		}
	}
	
	@Command
	@NotifyChange("selectedRole")
	public void ubahStatusAktif(@BindingParam("dto") MRole dto){
		showConfirm("E002");
		selectedRole = new MRole();
		selectedRole = dto;
		selectedRole.setIsActive(false);
		selectedRole.setModifiedBy(user.getFullname());
		selectedRole.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange("selectedRole")
	public void ubahStatusNonAktif(@BindingParam("dto") MRole dto){
		showConfirm("E003");
		selectedRole = new MRole();
		selectedRole = dto;
		selectedRole.setIsActive(true);
		selectedRole.setModifiedBy(user.getFullname());
		selectedRole.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Override
	@Command
	@NotifyChange({"recordCount","roleList","maxPage"})
	public void yesConfirm(){
		try{
			mRoleSvc.save(selectedRole);
			flagConfirmMsg = false;
			showSuccessMsgBox("E000");
			//reload list
			if (search) {
				recordCount = mRoleSvc.countByArg(sArg);
				roleList = mRoleSvc.findByArg(sArg, pageNo, 
						StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			} else {
				recordCount = mRoleSvc.countAll();
				roleList = mRoleSvc.findAll(pageNo, 
						StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			}
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
			BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
		}catch(Exception e){
			e.printStackTrace();
		}
	}
	

	@Command("back")
	public void back() {
		if (activeCheck()) {
			Messagebox.show("Data tidak akan disimpan. Apakah Anda yakin?", "Perhatian",
				new Button[] { Button.YES, Button.NO },
				Messagebox.QUESTION, Button.NO,
				new EventListener<Messagebox.ClickEvent>() {
					@Override
					public void onEvent(ClickEvent event) throws Exception {
						if (Messagebox.ON_YES.equals(event.getName())) {
							Sessions.getCurrent().removeAttribute("editRole");
							navigate(urlIndex);
						}
					}
				}
			);
		}
	}

	@Command("add")
	@NotifyChange({"editMode","editRole","fRolePrivs","freezeId","pageTitle","moduleIdx","defModulIdx"})
	public void add() {
		if(activeCheck()){
			editMode = true;
			editRole = new MRole();
			editIndex = -1;
			
			rolePrivs = new ArrayList<List<MMenuRoleDto>>();
			
			for (String x: moduleList) {
				rolePrivs.add(mMenuRoleSvc.getAccessPrivsForMaster(editRole.getId(), x));
			}
			freezeId = false;
			pageTitle = "Create Role";
			
			if(defModulIdx == null){
				defModulIdx = "Web";
				moduleIdx = 1;
			}
			fRolePrivs = new ArrayList<>(rolePrivs.get(moduleIdx));
//			if(fRolePrivs!=null) fRolePrivs.clear();
//			moduleIdx=null;
		}
	}

	@Command("edit")
	@NotifyChange({"editMode","editRole","fRolePrivs","freezeId","pageTitle","moduleIdx","defModulIdx"})
	public void edit(@BindingParam("item") MRole edit) {
		if(activeCheck()){
			if (edit==null || edit.getId() == null)
				Messagebox.show("Pilih data yang akan dirubah!", "Message", 0, null);
			else {
				editMode = true;
				editRole = edit;
				editIndex = roleList.indexOf(edit);
								
				rolePrivs = new ArrayList<List<MMenuRoleDto>>();
				if(defModulIdx == null){
					defModulIdx = "Web";
					moduleIdx = 1;
				}
				
				for (String x: moduleList) {
					rolePrivs.add(mMenuRoleSvc.getAccessPrivsForMaster(editRole.getId(), x));
				}
				
				freezeId = true;
				pageTitle = "Update Role";
				
				fRolePrivs = new ArrayList<>(rolePrivs.get(moduleIdx));
				
//				if(fRolePrivs!=null) 
//				fRolePrivs.clear();
//				moduleIdx=null;
			}
		}
	}

	@Command("save")
	@NotifyChange({ "editRole", "rolePrivs", "maxPage", "recordCount", "roleList","editMode" })
	public void save(@BindingParam("btn")Component btn) {
		if(activeCheck()){
			boolean existence;
			try {
				existence = mRoleSvc.findOne(editRole.getId()) != null ? true : false;
			} catch (Exception e) {
				existence = false;
			}
			String fullErrMsg = StringConstants.MSG_FORM_TEMPLATE;
			int error = 0;
			/*if (StringUtils.isEmpty(editRole.getIdRole())) {
				error++;
				fullErrMsg += String.format(
						StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Role Id");
			} else */if (existence
					&& editRole.getCreatedDate()==null) {
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_ID_EXISTS,"Role Id");
			}
			if (!editRole.getRoleName().matches("[a-zA-Z0-9 ]+")) {
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_ILLEGAL_CHAR, "Role Name");
			} else if (existence==false && mRoleSvc.existsName(
					editRole.getId(),editRole.getRoleName())){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_ID_EXISTS, "Role Name");
			}
			/*
			if (moduleIdx == null) {
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY,"Application Name");
			}*/
			if (error > 0) {
				Messagebox.show(fullErrMsg, "Message", 0, null);
			} else {
				Timestamp time = Timestamp.from(OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant());
				if(editRole.getCreatedDate()==null){
					editRole.setCreatedBy(user.getUsername());
					editRole.setCreatedDate(time);
					for (List<MMenuRoleDto> x : rolePrivs) {
						for (MMenuRoleDto y : x) {
							y.setCreated(user.getUsername());
							y.setCreatedDate(time);
						}
					}
					
					if(roleList.size()==25){
						roleList.remove(24);
					}
					
					recordCount++;
					maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
				}

				if(editIndex!=-1) roleList.remove(editIndex);
				roleList.add(0,editRole);
				
				editRole.setModifiedBy(user.getUsername());
				editRole.setModifiedDate(time);
				for (List<MMenuRoleDto> x : rolePrivs) {
					for (MMenuRoleDto y : x) {
						y.setModified(user.getUsername());
						y.setModifiedDate(time);
					}
				}
	
				if(editIndex!=-1){
					roleList.get(editIndex).setRoleName(editRole.getRoleName());
					roleList.get(editIndex).setDescription(editRole.getDescription());
				}
				
				MRole saved = mRoleSvc.save(editRole);
				
				if (editIndex==-1) {
					for (List<MMenuRoleDto> x : rolePrivs) {
						for (MMenuRoleDto y : x) {
							y.setRoleId(saved.getId());
						}
					}
				}
	
				for (List<MMenuRoleDto> x : rolePrivs) {
					mMenuRoleSvc.save(x);
				}
	
				editMode = false;
				if(editIndex==-1){
					showSuccessMsgBox("E004");
				}
				else{
					showSuccessMsgBox("E100");
				}
				
				load();
				
//				((Window)btn.getParent().getParent()).setVisible(false);
//				Clients.showNotification("Data sukses disimpan", 
//						Clients.NOTIFICATION_TYPE_INFO, null, null, 5000);
			}	
		}
	}

	@Command("del")
	public void delete(@BindingParam("item") final MRole del) {
		if(activeCheck()){
			if (del==null || del.getId() == null)
				Messagebox.show("Pilih data yang akan dihapus", "Message", 0, null);
			else {
				if (!btnDel) {
					Messagebox.show("Apakah anda ingin menonaktifkan Role "+del.getRoleName()+" ?", "Perhatian",
						new Button[] { Button.YES, Button.NO },
						Messagebox.QUESTION, Button.NO,
						new EventListener<Messagebox.ClickEvent>() {
							@Override
							public void onEvent(ClickEvent event) throws Exception {
								if (Messagebox.ON_YES.equals(event.getName())) {
									mRoleSvc.disable(del.getId());
									roleList.get(roleList.indexOf(del)).setIsActive(false);
									BindUtils.postNotifyChange(null, null, UMRoleMenuVmd.this, "roleList");
									Clients.showNotification(del.getRoleName() + " berhasil dinonaktifkan", 
										Clients.NOTIFICATION_TYPE_INFO, null, null, 5000);
								}
							}
						});
				} else {
					Clients.alert(StringConstants.MSG_NO_DEL_ACCESS, "Warning", "");
				}
			}
		}
	}
	
	@Command("reload")
	@NotifyChange({"recordCount","roleList","maxPage"})
	public void reload(){
		if (search) {
			recordCount = mRoleSvc.countByArg(sArg);
			roleList = mRoleSvc.findByArg(sArg, pageNo, 
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
		} else {
			recordCount = mRoleSvc.countAll();
			roleList = mRoleSvc.findAll(pageNo, 
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
		}
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
	}

	@Command("src")
	@NotifyChange({"arg", "roleList", "pageNo", "maxPage", "recordCount" })
	public void search() {
		if(activeCheck()){
			search = true;
			pageNo = 1;
			sArg = String.valueOf(StringUtils.isEmpty(arg)?"":arg);
			roleList = mRoleSvc.findByArg(sArg, pageNo, 
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			recordCount = mRoleSvc.countByArg(sArg);
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
			arg="";
		}
	}

	@Command("switchPage")
	@NotifyChange({ "pageNo", "roleList" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try{
					pageNo = Integer.parseInt(action);
				}catch(NumberFormatException e){
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}
	
			if (search) {
				roleList = mRoleSvc.findByArg(sArg, pageNo, 
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			} else {
				roleList = mRoleSvc.findAll(pageNo, "modifiedDate", sortAsc);
			}
		}
	}

	@Command("sort")
	@NotifyChange("roleList")
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}
			
			if (search) {
				roleList = mRoleSvc.findByArg(sArg, pageNo, 
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			} else {
				roleList = mRoleSvc.findAll(pageNo, "modifiedDate", sortAsc);
			}
		}
	}
	
	/*
	@Command("dl")
	public void download() {
		if(activeCheck()){
			List<MRole> ents;
			List<TbMRoleHExportDto> dtos = new ArrayList<>();
			if (aiosearch) {
				ents = tbMRoleHSvc.findAllSelectedSimple(aios, recordCount,
						StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			} else if (search) {
				ents = tbMRoleHSvc.findAllSelected(id, selectState, roleName, selectModule, recordCount,
						StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			} else {
				ents = tbMRoleHSvc.findAllDto(StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			}
			for(MRole x: ents){
				dtos.add(new TbMRoleHExportDto(x));
			}
			serve(Sessions.getCurrent().getAttribute("saveTempDir") + "Roles", dtos.toArray());
		}
	}
	*/
	
	@Command("notifyF")
	@NotifyChange("fRolePrivs")
	public void notifyF(){
		if(activeCheck()){
			fRolePrivs = new ArrayList<>(rolePrivs.get(moduleIdx));
		}
	}
	
	@Command("filter")
	@NotifyChange("fRolePrivs")
	public void filter(@BindingParam("src")String src){
		if(activeCheck()){
			if(fRolePrivs!=null)fRolePrivs.clear();
			if(src!=null && moduleIdx!=null){
				for(MMenuRoleDto x: rolePrivs.get(moduleIdx)){
					if(x.getMenuName().toLowerCase().contains(src.toLowerCase())||
							x.getMenuCode().toLowerCase().contains(src.toLowerCase())){
						fRolePrivs.add(x);
					}
				}
			}
		}
	}
	
}
