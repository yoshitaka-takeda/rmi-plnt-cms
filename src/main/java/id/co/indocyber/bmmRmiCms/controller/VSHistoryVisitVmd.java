package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MobScheduleVisitAppraisementDto;
import id.co.indocyber.bmmRmiCms.dto.MobScheduleVisitScoringDto;
import id.co.indocyber.bmmRmiCms.dto.MobScheduleVisitToFarmerHistoryDto;
import id.co.indocyber.bmmRmiCms.dto.MobScheduleVisitToFarmerResultsDto;
import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitOtherActivities;
import id.co.indocyber.bmmRmiCms.entity.MobStand;
import id.co.indocyber.bmmRmiCms.service.GlobalSvc;
import id.co.indocyber.bmmRmiCms.service.MSugarcaneParamScoringSvc;
import id.co.indocyber.bmmRmiCms.service.MobScheduleVisitSvc;
import id.co.indocyber.bmmRmiCms.tools.ExcelExport;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

import java.sql.Date;
import java.time.LocalDate;
import java.time.format.TextStyle;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;
import java.util.Locale;

import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zul.Filedownload;
import org.zkoss.zk.ui.Executions;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class VSHistoryVisitVmd extends BaseVmd {
	@WireVariable
	MobScheduleVisitSvc mobScheduleVisitSvc;
	@WireVariable
	MSugarcaneParamScoringSvc mSugarcaneParamScoringSvc;
	@WireVariable
	GlobalSvc globalSvc;
	
	String urlIndex = "/visit/history/index.zul";
	String refresh = "/visit/history/ref.zul";
	
	private static final String urlGoogleMaps = "https://www.google.com/maps/place/";
	private List<Integer> listId = new ArrayList<>();
	
	private MUserDto user;
	
	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;

	private List<MobScheduleVisitToFarmerResultsDto> resultList, viewResultList;
	
	private boolean freezeId;
	private Boolean editMode, activityWindow, vHist;
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private Integer recordCount = 1;
	
	private MobScheduleVisitToFarmerResultsDto reportData;
	private MobScheduleVisitScoringDto reportDetails;
	private List<MobScheduleVisitOtherActivities> otherActivities;
	private List<MobScheduleVisitToFarmerHistoryDto> visitHistory;
	
	private List<MobScheduleVisitAppraisementDto> viewTaksasi;
	private List<MobStand> viewTegakan;
	
	private Date startDate, sStart, endDate, sEnd;
	private boolean search;
	
	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public List<MobScheduleVisitToFarmerResultsDto> getViewResultList() {
		return viewResultList;
	}

	public void setViewResultList(
			List<MobScheduleVisitToFarmerResultsDto> viewResultList) {
		this.viewResultList = viewResultList;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}
	
	public MobScheduleVisitToFarmerResultsDto getReportData() {
		return reportData;
	}

	public void setReportData(MobScheduleVisitToFarmerResultsDto reportData) {
		this.reportData = reportData;
	}
	
	public MobScheduleVisitScoringDto getReportDetails() {
		return reportDetails;
	}

	public void setReportDetails(MobScheduleVisitScoringDto reportDetails) {
		this.reportDetails = reportDetails;
	}
	
	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}
	
	public List<MobScheduleVisitOtherActivities> getOtherActivities() {
		return otherActivities;
	}

	public void setOtherActivities(
			List<MobScheduleVisitOtherActivities> otherActivities) {
		this.otherActivities = otherActivities;
	}
	
	public Boolean getActivityWindow() {
		return activityWindow;
	}

	public void setActivityWindow(Boolean activityWindow) {
		this.activityWindow = activityWindow;
	}
	
	public Boolean getvHist() {
		return vHist;
	}

	public void setvHist(Boolean vHist) {
		this.vHist = vHist;
	}

	public List<MobScheduleVisitToFarmerHistoryDto> getVisitHistory() {
		return visitHistory;
	}

	public void setVisitHistory(
			List<MobScheduleVisitToFarmerHistoryDto> visitHistory) {
		this.visitHistory = visitHistory;
	}
	
	public List<MobScheduleVisitAppraisementDto> getViewTaksasi() {
		return viewTaksasi;
	}

	public void setViewTaksasi(List<MobScheduleVisitAppraisementDto> viewTaksasi) {
		this.viewTaksasi = viewTaksasi;
	}

	public List<MobStand> getViewTegakan() {
		return viewTegakan;
	}

	public void setViewTegakan(List<MobStand> viewTegakan) {
		this.viewTegakan = viewTegakan;
	}

	@Init
	@NotifyChange({ "btnAdd", "btnDel", "btnEdit", "btnDl" })
	public void load() {
		editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
		user = (MUserDto) Sessions.getCurrent().getAttribute("user");
		
		//menampilkan jumlah anggota team
		listId = globalSvc.getAllMyTeam(user.getPersonId());
		
		startDate = new Date(new java.util.Date().getTime());
		endDate = new Date(startDate.getTime());
		
		for (MMenuRoleDto x : user.getRolePrivs()) {
			if ("VS-2".equals(x.getMenuCode())) {
				btnAdd = !x.getIsAdd();
				btnDel = !x.getIsDelete();
				btnDl = !x.getIsDownload();
				btnEdit = !x.getIsEdit();
				break;
			}
		}
	}

	@Command("back")
	public void back() {
		if (activeCheck()) {
			editMode=false;
		}
	}

	@Command("src")
	@NotifyChange({ "viewResultList", "pageNo", "maxPage", "recordCount", "reportData"})
	public void search() {
		if(activeCheck()){
			if(startDate==null || endDate==null){
				Messagebox.show("Tanggal awal dan akhir pencarian harus diisi.", "Message", 0, null);
			}
			else if(startDate.after(endDate)){
				Messagebox.show("Tanggal awal pencarian tidak boleh setelah tanggal akhir pencarian.", "Message", 0, null);
			}
			else {
				search = true;
				pageNo = 1;
		
				sStart = Date.valueOf(startDate.toLocalDate());
				sEnd = Date.valueOf(endDate.toLocalDate());
				
				if(user.isAdmin()){
					resultList = mobScheduleVisitSvc.getResultsAdmin(sStart,sEnd);
				}
				else{
					if(listId.size()>0)
						resultList = mobScheduleVisitSvc.getResults(sStart,sEnd,listId);
					else{
						Messagebox.show("Tidak ada anggota.", "Message", 0, null);
					}
				}
				recordCount = resultList.size();
				if(recordCount>0){
					viewResultList = new ArrayList<>(resultList.subList((pageNo - 1) * 25,
							pageNo * 25 > recordCount ? recordCount : pageNo * 25));
				}
				else{
					if(viewResultList!=null)viewResultList.clear();
				}
					
				maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
				reportData = null;
			}
		}
	}
	
	@Command("reload")
	@NotifyChange({"recordCount","viewResultList","maxPage", "reportData"})
	public void reload(){
		if(search){
			resultList = mobScheduleVisitSvc.getResults(sStart,sEnd,listId);
			recordCount = resultList.size();
			if(recordCount>0){
				viewResultList = new ArrayList<>(resultList.subList((pageNo - 1) * 25,
						pageNo * 25 > recordCount ? recordCount : pageNo * 25));
			}
			else{
				if(viewResultList!=null)viewResultList.clear();
			}
				
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
			reportData = null;
		}
	}

	@Command("switchPage")
	@NotifyChange({ "pageNo", "viewResultList", "reportData" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}

			if (resultList!=null && recordCount>0) {
				viewResultList = new ArrayList<>(resultList.subList((pageNo - 1) * 25,
					pageNo * 25 > recordCount ? recordCount : pageNo * 25));
			}
			
			reportData = null;
		}
	}
	
	@Command("viewReport")
	@NotifyChange({"reportData","viewTaksasi","viewTegakan","visitHistory","otherActivities"})
	public void viewFields(@BindingParam("report")MobScheduleVisitToFarmerResultsDto selectReport){
		if(activeCheck()){
			reportData = selectReport;
			viewTaksasi = mobScheduleVisitSvc.getAppraisement(selectReport.getVisitDetailIds());
			MobStand data = mobScheduleVisitSvc.getTegakan(selectReport.getVisitId());
			if(data!=null){
				viewTegakan = new ArrayList<>();
				viewTegakan.add(data);
			}
			visitHistory = mobScheduleVisitSvc.getHistory(selectReport.getVisitId());
			otherActivities = mobScheduleVisitSvc.getOtherActivities(selectReport.getVisitId());
			//System.out.println(viewTegakan==null);
		}
	}
	
	@Command("viewDetail")
	@NotifyChange({"reportDetails","editMode"})
	public void viewDetail(@BindingParam("detail")MobScheduleVisitScoringDto selectDetail){
		if(activeCheck()){
			editMode = true;
			reportDetails = selectDetail;
		}
	}
	
	@Command("viewOtherActivities")
	@NotifyChange({"otherActivities", "activityWindow"})
	public void viewOtherActivities(
			@BindingParam("report")MobScheduleVisitToFarmerResultsDto selectReport){
		if(activeCheck()){
			activityWindow = true;
			otherActivities = mobScheduleVisitSvc.getOtherActivities(selectReport.getVisitId());
		}
	}
	
	@Command("export")
	public void export(@BindingParam("report")MobScheduleVisitToFarmerResultsDto selectReport){
		if(activeCheck()){
			ExcelExport exp = new ExcelExport();
			Filedownload.save(exp.generate( 
					mSugarcaneParamScoringSvc.findAllActive(), selectReport),null,
							"Eight Scoring "+selectReport.getVisitId().replaceAll("/", "")+".xlsx");
		}
	}
	
	@Command("exportT")
	public void exportT(@BindingParam("report")MobScheduleVisitToFarmerResultsDto selectReport){
		if(activeCheck()){
			if(selectReport.getVisitDetailIds().size()>0){
				LocalDate currentDate = LocalDate.now();
				List<MobScheduleVisitAppraisementDto> taksasi = mobScheduleVisitSvc.getAppraisement(selectReport.getVisitDetailIds());
				if(taksasi.size()>0){
					ExcelExport exp = new ExcelExport();
					Filedownload.save(exp.generateTaksasi(
						currentDate.getMonth().getDisplayName(TextStyle.FULL, Locale.forLanguageTag("id-ID")), 
						Integer.toString(currentDate.getYear()), taksasi), null,
						"Taksasi "+
						currentDate.getMonth().getDisplayName(TextStyle.FULL, Locale.forLanguageTag("id-ID"))+" "+
						Integer.toString(currentDate.getYear())+" "+selectReport.getVisitId().replaceAll("/", "")+".xlsx");
				}
				else{
					Messagebox.show(StringConstants.MSG_NOTHING_TO_EXPORT, "Message", 0, null);
				}
			}
			else{
				Messagebox.show(StringConstants.MSG_NOTHING_TO_EXPORT, "Message", 0, null);
			}
		}
	}
	
	@Command
	@NotifyChange({"visitHistory","vHist","reportData"})
	public void history(@BindingParam("item") final MobScheduleVisitToFarmerResultsDto hdr) {
		if(activeCheck()){
			reportData = hdr;
			visitHistory = mobScheduleVisitSvc.getHistory(hdr.getVisitId());
			vHist = true;
		}
	}
	
	@Command
	public void checkArea(){
		Executions.getCurrent().sendRedirect(urlGoogleMaps+reportData.getLatitude()+","+reportData.getLongitude(), "_blank");
	}
}
