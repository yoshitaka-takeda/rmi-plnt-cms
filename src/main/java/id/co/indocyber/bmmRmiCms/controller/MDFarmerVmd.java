package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MobFarmerDto;
import id.co.indocyber.bmmRmiCms.service.MobFarmerSvc;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

import java.sql.Timestamp;
import java.util.List;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class MDFarmerVmd extends BaseVmd {
	@WireVariable
	MobFarmerSvc mobFarmerSvc;

	String urlIndex = "/master/supir/index.zul";
	String refresh = "/master/supir/ref.zul";
	
	private MUserDto user;

	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;
	
	private MobFarmerDto editFarmer;
	private List<MobFarmerDto> farmerList;

	private boolean freezeId;
	private Boolean editMode;
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private Integer recordCount = 1;
	private String arg, sArg;
	
	private String sortBy = "";
	private boolean sortAsc = false;
	
	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public MobFarmerDto getEditFarmer() {
		return editFarmer;
	}

	public void setEditFarmer(MobFarmerDto editFarmer) {
		this.editFarmer = editFarmer;
	}

	public List<MobFarmerDto> getFarmerList() {
		return farmerList;
	}

	public void setFarmerList(List<MobFarmerDto> farmerList) {
		this.farmerList = farmerList;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public String getArg() {
		return arg;
	}

	public void setArg(String arg) {
		this.arg = arg;
	}

	@Init
	@NotifyChange({ "btnAdd", "btnDel", "btnEdit", "btnDl" })
	public void load() {
		editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
		user = (MUserDto) Sessions.getCurrent().getAttribute("user");
		//superAdmin = (Integer) Sessions.getCurrent().getAttribute("superAdmin") == 1;
		farmerList = mobFarmerSvc.findAllDto(pageNo, "id", false);
		recordCount = mobFarmerSvc.countAll();
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
		
		for (MMenuRoleDto x : user.getRolePrivs()) {
			if ("MD-5".equals(x.getMenuCode())) {
				btnAdd = !x.getIsAdd();
				btnDel = !x.getIsDelete();
				btnDl = !x.getIsDownload();
				btnEdit = !x.getIsEdit();
				break;
			}
		}
	}

	@Command("back")
	public void back() {
		if (activeCheck()) {
			editMode=false;
		}
	}

	@Command("view")
	@NotifyChange({"editMode","editFarmer","genders",
		"religions","educationLevels","maritalStatuses","addressTypes"})
	public void edit(@BindingParam("item") MobFarmerDto edit) {
		if(activeCheck()){
			if (edit.getId() == null)
				Messagebox.show("Pilih data yang akan dilihat!", "Message", 0, null);
			else {
				editMode = true;
				editFarmer = edit;
			}
		}
	}

	@Command("reload")
	@NotifyChange({"recordCount","farmerList","maxPage"})
	public void reload(){
		if (search) {
			recordCount = mobFarmerSvc.countAllByArg(sArg);
			farmerList = mobFarmerSvc.findDtoByArg(sArg, pageNo, 
					StringUtils.isEmpty(sortBy) ? "id" : sortBy, sortAsc);
		} else {
			recordCount = mobFarmerSvc.countAll();
			farmerList = mobFarmerSvc.findAllDto(pageNo, 
					StringUtils.isEmpty(sortBy) ? "id" : sortBy, sortAsc);
		}
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
	}

	@Command("src")
	@NotifyChange({ "arg","farmerList", "pageNo", "maxPage", "recordCount"})
	public void search() {
		if(activeCheck()){
			search = true;
			pageNo = 1;
			
			sArg = String.valueOf(StringUtils.isEmpty(arg)?"":arg);
			farmerList = mobFarmerSvc.findDtoByArg(sArg, pageNo,
					StringUtils.isEmpty(sortBy) ? "id" : sortBy, sortAsc);
			recordCount = mobFarmerSvc.countAllByArg(sArg);
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
			arg="";
		}
	}
	
	@Command("del")
	public void delete(@BindingParam("item") final MobFarmerDto del) {
		if(activeCheck()){
			if (del.getId() == null)
				Messagebox.show("Pilih data yang akan dihapus", "Message", 0, null);
			else {
				if (!btnDel) {
					Messagebox.show(
						"Apakah anda ingin menonaktifkan parameter?", "Perhatian",
						new Button[] { Button.YES, Button.NO }, Messagebox.QUESTION, Button.NO,
						new EventListener<Messagebox.ClickEvent>() {
							@Override
							public void onEvent(ClickEvent event)throws Exception {
								if (Messagebox.ON_YES.equals(event.getName())) {
									if(activeCheck()){
										mobFarmerSvc.disable(del.getId());
										farmerList.get(farmerList.indexOf(del)).setIsActive(false);
										BindUtils.postNotifyChange(null, null, MDFarmerVmd.this, "farmerList");
										Clients.showNotification("Parameter berhasil dinonaktifkan",
											Clients.NOTIFICATION_TYPE_INFO, null, null, 5000);
									}
								}
							}
						}
					);
				} else {
					Clients.alert(StringConstants.MSG_NO_DEL_ACCESS, "Warning", "");
				}
			}
		}
	}

	@Command("switchPage")
	@NotifyChange({ "pageNo", "farmerList" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}

			if (search) {
				farmerList = mobFarmerSvc.findDtoByArg(sArg, pageNo,
					StringUtils.isEmpty(sortBy) ? "id" : sortBy, sortAsc);
			} else {
				farmerList = mobFarmerSvc.findAllDto(pageNo, 
					StringUtils.isEmpty(sortBy) ? "id" : sortBy, sortAsc);
			}
		}
	}

	@Command("sort")
	@NotifyChange({"farmerList"})
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}

			if (search) {
				farmerList = mobFarmerSvc.findDtoByArg(sArg, pageNo, sortBy, sortAsc);
			} else {
				farmerList = mobFarmerSvc.findAllDto(pageNo, sortBy, sortAsc);
			}
		}
	}
	
	@Command
	@NotifyChange("flagIconWarning")
	public void ubahStatusAktif(@BindingParam("dto") MobFarmerDto dto){
		showConfirm("E002");
		setFlagIconWarning(true);
		editFarmer = new MobFarmerDto();
		editFarmer = dto;
		editFarmer.setIsActive(false);
		editFarmer.setModifiedBy(user.getFullname());
		editFarmer.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange("flagIconWarning")
	public void ubahStatusNonAktif(@BindingParam("dto") MobFarmerDto dto){
		showConfirm("E003");
		setFlagIconWarning(true);
		editFarmer = new MobFarmerDto();
		editFarmer = dto;
		editFarmer.setIsActive(true);
		editFarmer.setModifiedBy(user.getFullname());
		editFarmer.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}

	@Override
	@Command
	@NotifyChange({"recordCount","farmerList","maxPage","maxScore"})
	public void yesConfirm(){
		try{
			mobFarmerSvc.save(editFarmer);
			flagConfirmMsg = false;
			showSuccessMsgBox("E000");
			//reload list
			farmerList = mobFarmerSvc.findAllDto(pageNo, "id", false);
			recordCount = mobFarmerSvc.countAll();
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
			BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
		}catch(Exception e){
			e.printStackTrace();
			showErrorMsgBox("E001");			
		}
	}
}
