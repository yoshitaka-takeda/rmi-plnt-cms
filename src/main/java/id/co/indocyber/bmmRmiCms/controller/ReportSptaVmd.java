package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MobSptaDto;
import id.co.indocyber.bmmRmiCms.service.MobSptaSvc;

import java.sql.Date;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.util.List;

import org.springframework.util.StringUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zul.Messagebox;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ReportSptaVmd extends BaseVmd {
	@WireVariable
	MobSptaSvc mobSptaSvc;

	String urlIndex = "/spta/index.zul";
	String refresh = "/spta/ref.zul";
	
	private MUserDto user;

	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;

	private MobSptaDto viewSpta;
	private List<MobSptaDto> sptaList;

	private boolean freezeId;
	private Boolean editMode;
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private Integer recordCount = 1;
	private String arg, sArg;
	
	private String sortBy = "";
	private boolean sortAsc = true;

	private Date startDate, sStart, endDate, sEnd;
	private SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy");
	
	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public MobSptaDto getViewSpta() {
		return viewSpta;
	}

	public void setViewSpta(MobSptaDto viewSpta) {
		this.viewSpta = viewSpta;
	}

	public List<MobSptaDto> getSptaList() {
		return sptaList;
	}

	public void setSptaList(List<MobSptaDto> sptaList) {
		this.sptaList = sptaList;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public String getArg() {
		return arg;
	}

	public void setArg(String arg) {
		this.arg = arg;
	}

	public Date getStartDate() {
		return startDate;
	}

	public void setStartDate(Date startDate) {
		this.startDate = startDate;
	}

	public Date getEndDate() {
		return endDate;
	}

	public void setEndDate(Date endDate) {
		this.endDate = endDate;
	}

	@Init
	@NotifyChange({ "btnAdd", "btnDel", "btnEdit", "btnDl" })
	public void load() {
		editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
		user = (MUserDto) Sessions.getCurrent().getAttribute("user");
		//superAdmin = (Integer) Sessions.getCurrent().getAttribute("superAdmin") == 1;
		sptaList = mobSptaSvc.findAll(pageNo, "sptaNum", true);
		recordCount = mobSptaSvc.countAll();
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);

		startDate = new Date(new java.util.Date().getTime());
		endDate = new Date(startDate.getTime());
		
		for (MMenuRoleDto x : user.getRolePrivs()) {
			if ("SP-1".equals(x.getMenuCode())) {
				btnAdd = !x.getIsAdd();
				btnDel = !x.getIsDelete();
				btnDl = !x.getIsDownload();
				btnEdit = !x.getIsEdit();
				break;
			}
		}
	}

	@Command("back")
	public void back() {
		if (activeCheck()) {
			editMode=false;
		}
	}

	@Command("view")
	@NotifyChange({"editMode","viewSpta"})
	public void edit(@BindingParam("item") MobSptaDto edit) {
		if(activeCheck()){
			if (edit.getSptaNum() == null)
				Messagebox.show("Pilih data yang akan dilihat!", "Message", 0, null);
			else {
				editMode = true;
				viewSpta = edit;
			}
		}
	}

	@Command("src")
	@NotifyChange({ "arg", "sptaList", "pageNo", "maxPage", "recordCount"})
	public void search() throws ParseException {
		if(activeCheck()){
			if(startDate==null || endDate==null){
				Messagebox.show("Tanggal awal dan akhir pencarian harus diisi.", "Message", 0, null);
			}
			else if(startDate.after(endDate)){
				Messagebox.show("Tanggal awal pencarian tidak boleh setelah tanggal akhir pencarian.", "Message", 0, null);
			}
			else{
				search = true;
				pageNo = 1;
				
				sStart = Date.valueOf(startDate.toLocalDate());
				sEnd = Date.valueOf(endDate.toLocalDate());
				
				sArg = String.valueOf(StringUtils.isEmpty(arg)?"":arg);
				sptaList = mobSptaSvc.findByArg(sArg, sStart, sEnd, pageNo,
						StringUtils.isEmpty(sortBy) ? "sptaNum" : sortBy, sortAsc);
				recordCount = mobSptaSvc.countByArg(sArg, sStart, sEnd);
				maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
				arg = "";
			}
		}
	}

	@Command("reload")
	@NotifyChange({"recordCount","sptaList","maxPage"})
	public void reload() throws ParseException{
		if (search) {
			recordCount = mobSptaSvc.countByArg(sArg, sStart==null?new Date(sdf.parse("01-01-1970").getTime()):sStart, 
					sEnd==null?new Date(sdf.parse("31-12-9999").getTime()):sEnd);
			sptaList = mobSptaSvc.findByArg(sArg, sStart==null?new Date(sdf.parse("01-01-1970").getTime()):sStart, 
					sEnd==null?new Date(sdf.parse("31-12-9999").getTime()):sEnd, pageNo, 
					StringUtils.isEmpty(sortBy) ? "sptaNum" : sortBy, sortAsc);
		} else {
			recordCount = mobSptaSvc.countAll();
			sptaList = mobSptaSvc.findAll(pageNo, 
					StringUtils.isEmpty(sortBy) ? "sptaNum" : sortBy, sortAsc);
		}
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
	}
	
	@Command("switchPage")
	@NotifyChange({ "pageNo", "sptaList" })
	public void switchPage(@BindingParam("action") String action) throws ParseException {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}

			if (search) {
				sptaList = mobSptaSvc.findByArg(sArg, sStart==null?new Date(sdf.parse("01-01-1970").getTime()):sStart, 
						sEnd==null?new Date(sdf.parse("31-12-9999").getTime()):sEnd, pageNo,
					StringUtils.isEmpty(sortBy) ? "sptaNum" : sortBy, sortAsc);
			} else {
				sptaList = mobSptaSvc.findAll(pageNo, 
					StringUtils.isEmpty(sortBy) ? "sptaNum" : sortBy, sortAsc);
			}
		}
	}

	@Command("sort")
	@NotifyChange({"sptaList"})
	public void sort(@BindingParam("col") String column) throws ParseException {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}

			if (search) {
				sptaList = mobSptaSvc.findByArg(sArg, sStart==null?new Date(sdf.parse("01-01-1970").getTime()):sStart, 
						sEnd==null?new Date(sdf.parse("31-12-9999").getTime()):sEnd, pageNo, sortBy, sortAsc);
			} else {
				sptaList = mobSptaSvc.findAll(pageNo, sortBy, sortAsc);
			}
		}
	}
}
