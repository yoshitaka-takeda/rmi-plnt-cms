package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.dto.MobEmployeeDto;
import id.co.indocyber.bmmRmiCms.entity.MAddressType;
import id.co.indocyber.bmmRmiCms.entity.MEducationType;
import id.co.indocyber.bmmRmiCms.entity.MGender;
import id.co.indocyber.bmmRmiCms.entity.MMaritalStatus;
import id.co.indocyber.bmmRmiCms.entity.MReligion;
import id.co.indocyber.bmmRmiCms.entity.MobEmployee;
import id.co.indocyber.bmmRmiCms.service.MUserSvc;
import id.co.indocyber.bmmRmiCms.service.MiscellaneousSvc;
import id.co.indocyber.bmmRmiCms.service.MobEmployeeSvc;
import id.co.indocyber.bmmRmiCms.tools.DeepCopy;
import id.co.indocyber.bmmRmiCms.tools.StringConstants;

import java.sql.Timestamp;
import java.text.ParseException;
import java.time.LocalDate;
import java.time.OffsetDateTime;
import java.time.Period;
import java.time.ZoneOffset;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import org.springframework.util.StringUtils;
import org.zkoss.bind.BindUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Component;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.event.EventListener;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;
import org.zkoss.zk.ui.util.Clients;
import org.zkoss.zul.Messagebox;
import org.zkoss.zul.Messagebox.Button;
import org.zkoss.zul.Messagebox.ClickEvent;
import org.zkoss.zul.Window;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class MDEmployeeVmd extends BaseVmd {
	@WireVariable
	MobEmployeeSvc mobEmployeeSvc;
	@WireVariable
	MUserSvc mUserSvc;
	@WireVariable
	MiscellaneousSvc miscellaneousSvc;

	String urlIndex = "/master/supir/index.zul";
	String refresh = "/master/supir/ref.zul";
	
	private MUserDto user;

	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;
	
	private int editIndex=-1;

	private MobEmployeeDto editEmployee;
	private List<MobEmployeeDto> empList;

	private List<MGender> genders;
	private List<MReligion> religions;
	private List<MEducationType> educationLevels;
	private List<MAddressType> addressTypes;
	private List<MMaritalStatus> maritalStatuses;
	private List<MobEmployee> managerList, fManagerList;
	private List<String> areaNames;
	
	private boolean freezeId;
	private Boolean editMode;
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private Integer recordCount = 1;
	private String arg,sArg;
	
	private String sortBy = "";
	private boolean sortAsc = false;
	private boolean firstLoad = true;
	
	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public MobEmployeeDto getEditEmployee() {
		return editEmployee;
	}

	public void setEditEmployee(MobEmployeeDto editEmployee) {
		this.editEmployee = editEmployee;
	}

	public List<MobEmployeeDto> getEmpList() {
		return empList;
	}

	public void setEmpList(List<MobEmployeeDto> empList) {
		this.empList = empList;
	}

	public List<MGender> getGenders() {
		return genders;
	}

	public void setGenders(List<MGender> genders) {
		this.genders = genders;
	}

	public List<MReligion> getReligions() {
		return religions;
	}

	public void setReligions(List<MReligion> religions) {
		this.religions = religions;
	}

	public List<MEducationType> getEducationLevels() {
		return educationLevels;
	}

	public void setEducationLevels(List<MEducationType> educationLevels) {
		this.educationLevels = educationLevels;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public String getArg() {
		return arg;
	}

	public void setArg(String arg) {
		this.arg = arg;
	}
	
	public List<MAddressType> getAddressTypes() {
		return addressTypes;
	}

	public void setAddressTypes(List<MAddressType> addressTypes) {
		this.addressTypes = addressTypes;
	}

	public List<MMaritalStatus> getMaritalStatuses() {
		return maritalStatuses;
	}

	public void setMaritalStatuses(List<MMaritalStatus> maritalStatuses) {
		this.maritalStatuses = maritalStatuses;
	}
	
	public List<MobEmployee> getfManagerList() {
		return fManagerList;
	}

	public void setfManagerList(List<MobEmployee> fManagerList) {
		this.fManagerList = fManagerList;
	}
	
	public List<String> getAreaNames() {
		return areaNames;
	}

	public void setAreaNames(List<String> areaNames) {
		this.areaNames = areaNames;
	}

	@Init
	@NotifyChange({ "btnAdd", "btnDel", "btnEdit", "btnDl" })
	public void load() {
		editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
		genders = miscellaneousSvc.getGenders();
		user = (MUserDto) Sessions.getCurrent().getAttribute("user");
		
		//superAdmin = (Integer) Sessions.getCurrent().getAttribute("superAdmin") == 1;
		
		empList = mobEmployeeSvc.findAllDto(pageNo, "modifiedDate", false);
		recordCount = mobEmployeeSvc.countAll();
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
		
		areaNames = new ArrayList<>(miscellaneousSvc.getOptions("area_name"));
		areaNames.add(0,null);
		
		for (MMenuRoleDto x : user.getRolePrivs()) {
			if ("MD-4".equals(x.getMenuCode())) {
				btnAdd = !x.getIsAdd();
				btnDel = !x.getIsDelete();
				btnDl = !x.getIsDownload();
				btnEdit = !x.getIsEdit();
				break;
			}
		}
	}

	@Command("back")
	public void back() {
		if (activeCheck()) {
			editMode=false;
		}
	}

	@Command("add")
	@NotifyChange({"editMode","editEmployee","genders",
		"religions","educationLevels","maritalStatuses","addressTypes"})
	public void add() {
		if(activeCheck()){
			editMode = true;
			editEmployee = new MobEmployeeDto();
			editIndex = -1;
			
			if(firstLoad){
				firstLoad = false;
				religions = miscellaneousSvc.getReligions();
				educationLevels = miscellaneousSvc.getEducationTypes();
				maritalStatuses = miscellaneousSvc.getMaritalStatuses();
				addressTypes = miscellaneousSvc.getAddressTypes();
			}
			
			managerList = mobEmployeeSvc.findAll();
		}
	}

	@Command("edit")
	@NotifyChange({"editMode","editEmployee","genders",
		"religions","educationLevels","maritalStatuses","addressTypes"})
	public void edit(@BindingParam("item") MobEmployeeDto edit) {
		if(activeCheck()){
			if (edit.getId() == null)
				Messagebox.show("Pilih data yang akan dirubah!", "Message", 0, null);
			else {
				editMode = true;
				editEmployee = (MobEmployeeDto)DeepCopy.deepCopy(edit);
				editIndex = empList.indexOf(edit);
				
				if(firstLoad){
					firstLoad = false;
					religions = miscellaneousSvc.getReligions();
					educationLevels = miscellaneousSvc.getEducationTypes();
					maritalStatuses = miscellaneousSvc.getMaritalStatuses();
					addressTypes = miscellaneousSvc.getAddressTypes();
				}
				
				managerList = mobEmployeeSvc.findAll();
			}
		}
	}

	@Command("reload")
	@NotifyChange({"recordCount","empList","maxPage"})
	public void reload(){
		if (search) {
			recordCount = mobEmployeeSvc.countAllByArg(sArg);
			empList = mobEmployeeSvc.findDtoByArg(sArg, pageNo, 
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
		} else {
			recordCount = mobEmployeeSvc.countAll();
			empList = mobEmployeeSvc.findAllDto(pageNo, 
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
		}
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
	}
	
	@Command("save")
	@NotifyChange({"editEmployee","editMode","empList","maxPage","recordCount"})
	public void save(@BindingParam("btn")Component btn) {
		if(activeCheck()){
			// Database design is not final, but one does what one can...
			String fullErrMsg = StringConstants.MSG_FORM_TEMPLATE;
			
			int error = 0;
			if(mobEmployeeSvc.existsName(editEmployee.getId(), editEmployee.getFullname())){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_ID_EXISTS, "Nama Lengkap");
			}
			if(Period.between(editEmployee.getBirthday().toLocalDate(), LocalDate.now()).getYears()<17){
				error++;
				fullErrMsg += StringConstants.MSG_FORM_FIELD_AGE_RESTRICTION;
			}
			if (editEmployee.getGender()==null){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_FIELD_IS_EMPTY, "Gender");
			}
			if(!editEmployee.getPhoneNum().matches("[0-9]+") || !editEmployee.getPhoneNum().startsWith("08")){
				error++;
				fullErrMsg += String.format(
						StringConstants.MSG_FORM_PHONE_NUMERIC_INVALID,
						"Mobile Phone Number");
			}
			else if(mobEmployeeSvc.existsPhone(editEmployee.getId(), editEmployee.getPhoneNum())){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_ID_EXISTS, "Nomor HP");
			}
			if (!editEmployee.getEmail()
					.matches("[\\w-\\+]+(\\.[\\w]+)*@[\\w-]+(\\.[\\w]+)*(\\.[a-z]{2,})")) {
				error++;
				fullErrMsg += StringConstants.MSG_FORM_EMAIL_INVALID;
			} 
			else if(mobEmployeeSvc.existsEmail(
					editEmployee.getId(), editEmployee.getEmail())){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_ID_EXISTS, "E-mail");
			}
			if(editEmployee.getJointDate()==null){
				try{
					editEmployee.setJointDate("01-01-1970");
				}
				catch(ParseException e){
					e.printStackTrace();
				}
			}
			if(editEmployee.getEndDate()==null){
				try{
					editEmployee.setEndDate("31-12-9999");
				}
				catch(ParseException e){
					e.printStackTrace();
				}
			}
			if (editEmployee.getJointDate().after(editEmployee.getEndDate())){
				error++;
				fullErrMsg += String.format(StringConstants.MSG_FORM_DATEDIFF_NEGATIVE, "Tanggal Kerja", "Tanggal Akhir Kerja");
			}
			if (error > 0) {
				Messagebox.show(fullErrMsg, "Message", 0, null);
			} else {
				if(editEmployee.getId()==null){
					editEmployee.setIsActive(true);
					editEmployee.setPrevManager(editEmployee.getManager()==null?0:
						editEmployee.getManager().getId());
					if(empList.size()==25){
						empList.remove(24);
					}	
					recordCount++;
					maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
					editEmployee.setCreatedBy(user.getUsername());
					editEmployee.setCreatedDate(Timestamp.from(
							OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
				}
				if(editIndex!=-1){
					empList.remove(editIndex);
				}
				
				empList.add(0,editEmployee);
				editEmployee.setModifiedBy(user.getUsername());
				editEmployee.setModifiedDate(Timestamp.from(
					OffsetDateTime.now(ZoneOffset.ofHours(7)).toInstant()));
				MobEmployee saved = mobEmployeeSvc.save(editEmployee);
				
				if(editEmployee.getId()!=null){
					mUserSvc.updateEmployeePhone(editEmployee.getId(), editEmployee.getPhoneNum());
				}
				
				if(editEmployee.getId()==null){
					empList.get(0).setId(saved.getId());
				}
				
				editMode = false;
				((Window)btn.getParent().getParent()).setVisible(false);
				
				if(editIndex==-1){
					showSuccessMsgBox("E102");
				}
				else{
					showSuccessMsgBox("E100");
				}
				
				load();
			}
		}
	}

	@Command("src")
	@NotifyChange({ "arg","empList", "pageNo", "maxPage", "recordCount"})
	public void search() {
		if(activeCheck()){
			search = true;
			pageNo = 1;
			
			sArg = String.valueOf(StringUtils.isEmpty(arg)?"":arg);
			empList = mobEmployeeSvc.findDtoByArg(sArg, pageNo,
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			recordCount = mobEmployeeSvc.countAllByArg(sArg);
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
			arg="";
		}
	}
	
	@Command("del")
	public void delete(@BindingParam("item") final MobEmployeeDto del) {
		if(activeCheck()){
			if (del.getId() == null)
				Messagebox.show("Pilih data yang akan dihapus", "Message", 0, null);
			else {
				if (!btnDel) {
					Messagebox.show(
						"Apakah Anda yakin ingin menonaktifkan employee "+del.getFullname()+" ?", "Perhatian",
						new Button[] { Button.YES, Button.NO }, Messagebox.QUESTION, Button.NO,
						new EventListener<Messagebox.ClickEvent>() {
							@Override
							public void onEvent(ClickEvent event)throws Exception {
								if (Messagebox.ON_YES.equals(event.getName())) {
									if(activeCheck()){
										mobEmployeeSvc.disable(del.getId());
										empList.get(empList.indexOf(del)).setIsActive(false);
										BindUtils.postNotifyChange(null, null, MDEmployeeVmd.this, "empList");
										Clients.showNotification("Parameter berhasil dinonaktifkan",
											Clients.NOTIFICATION_TYPE_INFO, null, null, 5000);
									}
								}
							}
						}
					);
				} else {
					Clients.alert(StringConstants.MSG_NO_DEL_ACCESS, "Warning", "");
				}
			}
		}
	}

	@Command("switchPage")
	@NotifyChange({ "pageNo", "empList" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}

			if (search) {
				empList = mobEmployeeSvc.findDtoByArg(sArg, pageNo,
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			} else {
				empList = mobEmployeeSvc.findAllDto(pageNo, 
					StringUtils.isEmpty(sortBy) ? "modifiedDate" : sortBy, sortAsc);
			}
		}
	}

	@Command("sort")
	@NotifyChange({"empList"})
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}

			if (search) {
				empList = mobEmployeeSvc.findDtoByArg(sArg, pageNo, sortBy, sortAsc);
			} else {
				empList = mobEmployeeSvc.findAllDto(pageNo, sortBy, sortAsc);
			}
		}
	}
	
	@Command("refManager")
	@NotifyChange("fManagerList")
	public void refManager(@BindingParam("src")String src){
		if(fManagerList==null) fManagerList = new ArrayList<>();
		fManagerList.clear();
		for(MobEmployee x: managerList){
			if(x.getFullname().toLowerCase().contains(src)){
				fManagerList.add(x);
			}
		}
	}
	
	@Command
	@NotifyChange("flagIconWarning")
	public void ubahStatusAktif(@BindingParam("dto") MobEmployeeDto dto){
		showConfirm("E002");
		setFlagIconWarning(true);
		editEmployee = new MobEmployeeDto();
		editEmployee = dto;
		editEmployee.setIsActive(false);
		editEmployee.setModifiedBy(user.getFullname());
		editEmployee.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}
	
	@Command
	@NotifyChange("flagIconWarning")
	public void ubahStatusNonAktif(@BindingParam("dto") MobEmployeeDto dto){
		showConfirm("E003");
		setFlagIconWarning(true);
		editEmployee = new MobEmployeeDto();
		editEmployee = dto;
		editEmployee.setIsActive(true);
		editEmployee.setModifiedBy(user.getFullname());
		editEmployee.setModifiedDate(new Timestamp(System.currentTimeMillis()));
	}

	@Override
	@Command
	@NotifyChange({"recordCount","empList","maxPage","maxScore"})
	public void yesConfirm(){
		try{
			mobEmployeeSvc.save(editEmployee);
			flagConfirmMsg = false;
			showSuccessMsgBox("E000");
			//reload list
			empList = mobEmployeeSvc.findAllDto(pageNo, "modifiedDate", false);
			recordCount = mobEmployeeSvc.countAll();
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);

			BindUtils.postNotifyChange(null, null, this, "flagConfirmMsg");
		}catch(Exception e){
			e.printStackTrace();
			showErrorMsgBox("E001");			
		}
	}
}
