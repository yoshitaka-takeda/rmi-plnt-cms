package id.co.indocyber.bmmRmiCms.controller;

import id.co.indocyber.bmmRmiCms.dto.MMenuRoleDto;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.entity.MobRequestForCancel;
import id.co.indocyber.bmmRmiCms.service.MobSptaSvc;

import java.util.List;

import org.springframework.util.StringUtils;
import org.zkoss.bind.annotation.BindingParam;
import org.zkoss.bind.annotation.Command;
import org.zkoss.bind.annotation.Init;
import org.zkoss.bind.annotation.NotifyChange;
import org.zkoss.zk.ui.Sessions;
import org.zkoss.zk.ui.select.annotation.VariableResolver;
import org.zkoss.zk.ui.select.annotation.WireVariable;

@VariableResolver(org.zkoss.zkplus.spring.DelegatingVariableResolver.class)
public class ReportCancelSptaVmd extends BaseVmd {
	@WireVariable
	MobSptaSvc mobSptaSvc;

	String urlIndex = "/cancelspta/index.zul";
	String refresh = "/cancelspta/ref.zul";
	
	private MUserDto user;

	private boolean btnAdd;
	private boolean btnDel;
	private boolean btnEdit;
	private boolean btnDl;

	private MobRequestForCancel viewSpta;
	private List<MobRequestForCancel> sptaList;

	private boolean freezeId;
	private Boolean editMode;
	private String pageTitle;

	// paging variables
	private Integer pageNo = 1;
	private Integer maxPage = 1;
	private boolean search = false;
	private Integer recordCount = 1;
	private String arg, sArg;
	
	private String sortBy = "";
	private boolean sortAsc = true;

	public boolean isBtnAdd() {
		return btnAdd;
	}

	public void setBtnAdd(boolean btnAdd) {
		this.btnAdd = btnAdd;
	}

	public boolean isBtnDel() {
		return btnDel;
	}

	public void setBtnDel(boolean btnDel) {
		this.btnDel = btnDel;
	}

	public boolean isBtnEdit() {
		return btnEdit;
	}

	public void setBtnEdit(boolean btnEdit) {
		this.btnEdit = btnEdit;
	}

	public boolean isBtnDl() {
		return btnDl;
	}

	public void setBtnDl(boolean btnDl) {
		this.btnDl = btnDl;
	}

	public MobRequestForCancel getViewSpta() {
		return viewSpta;
	}

	public void setViewSpta(MobRequestForCancel viewSpta) {
		this.viewSpta = viewSpta;
	}

	public List<MobRequestForCancel> getSptaList() {
		return sptaList;
	}

	public void setSptaList(List<MobRequestForCancel> sptaList) {
		this.sptaList = sptaList;
	}

	public boolean isFreezeId() {
		return freezeId;
	}

	public void setFreezeId(boolean freezeId) {
		this.freezeId = freezeId;
	}

	public Boolean getEditMode() {
		return editMode;
	}

	public void setEditMode(Boolean editMode) {
		this.editMode = editMode;
	}

	public String getPageTitle() {
		return pageTitle;
	}

	public void setPageTitle(String pageTitle) {
		this.pageTitle = pageTitle;
	}

	public Integer getPageNo() {
		return pageNo;
	}

	public void setPageNo(Integer pageNo) {
		this.pageNo = pageNo;
	}

	public Integer getMaxPage() {
		return maxPage;
	}

	public void setMaxPage(Integer maxPage) {
		this.maxPage = maxPage;
	}

	public boolean isSearch() {
		return search;
	}

	public void setSearch(boolean search) {
		this.search = search;
	}

	public Integer getRecordCount() {
		return recordCount;
	}

	public void setRecordCount(Integer recordCount) {
		this.recordCount = recordCount;
	}

	public String getArg() {
		return arg;
	}

	public void setArg(String arg) {
		this.arg = arg;
	}

	@Init
	@NotifyChange({ "btnAdd", "btnDel", "btnEdit", "btnDl" })
	public void load() {
		editMode = Boolean.valueOf((Boolean) Sessions.getCurrent().getAttribute("editMode"));
		user = (MUserDto) Sessions.getCurrent().getAttribute("user");
		//superAdmin = (Integer) Sessions.getCurrent().getAttribute("superAdmin") == 1;
		sptaList = mobSptaSvc.findCancelRequest("", pageNo, "sptaNum", true);
		recordCount = mobSptaSvc.countCancelRequest("");
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);

		for (MMenuRoleDto x : user.getRolePrivs()) {
			if ("SP-1R".equals(x.getMenuCode())) {
				btnAdd = !x.getIsAdd();
				btnDel = !x.getIsDelete();
				btnDl = !x.getIsDownload();
				btnEdit = !x.getIsEdit();
				break;
			}
		}
	}

	@Command("back")
	public void back() {
		if (activeCheck()) {
			editMode=false;
		}
	}

	/*
	@Command("view")
	@NotifyChange({"editMode","viewSpta"})
	public void edit(@BindingParam("item") MobRequestForCancel edit) {
		if(activeCheck()){
			if (edit.getSptaNum() == null)
				Messagebox.show("Pilih data yang akan dilihat!", "Message", 0, null);
			else {
				editMode = true;
				viewSpta = edit;
			}
		}
	}
	*/
	
	@Command("src")
	@NotifyChange({ "arg", "sptaList", "pageNo", "maxPage", "recordCount"})
	public void search() {
		if(activeCheck()){
			search = true;
			pageNo = 1;
			
			sArg = String.valueOf(StringUtils.isEmpty(arg)?"":arg);
			sptaList = mobSptaSvc.findCancelRequest(sArg, pageNo,
					StringUtils.isEmpty(sortBy) ? "sptaNum" : sortBy, sortAsc);
			recordCount = mobSptaSvc.countCancelRequest(sArg);
			maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
			arg = "";
		}
	}

	@Command("reload")
	@NotifyChange({"recordCount","sptaList","maxPage"})
	public void reload(){
		recordCount = mobSptaSvc.countCancelRequest(StringUtils.isEmpty(sArg)?"":sArg);
		sptaList = mobSptaSvc.findCancelRequest(StringUtils.isEmpty(sArg)?"":sArg, pageNo,
				StringUtils.isEmpty(sortBy) ? "sptaNum" : sortBy, sortAsc);
		maxPage = (recordCount / 25) + (recordCount % 25 == 0 ? 0 : 1);
	}
	
	@Command("switchPage")
	@NotifyChange({ "pageNo", "sptaList" })
	public void switchPage(@BindingParam("action") String action) {
		if(activeCheck()){
			switch (action) {
			case "first":
				pageNo = 1;
				break;
			case "prev":
				if (pageNo > 1)
					pageNo--;
				break;
			case "next":
				if (pageNo < maxPage)
					pageNo++;
				break;
			case "last":
				pageNo = maxPage;
				break;
			default:
				try {
					pageNo = Integer.parseInt(action);
				} catch (NumberFormatException e) {
					pageNo = 1;
				}
				if (pageNo < 1)
					pageNo = 1;
				else if (pageNo > maxPage)
					pageNo = maxPage;
				break;
			}

			sptaList = mobSptaSvc.findCancelRequest(StringUtils.isEmpty(sArg)?"":sArg, pageNo,
				StringUtils.isEmpty(sortBy) ? "sptaNum" : sortBy, sortAsc);
			
		}
	}

	@Command("sort")
	@NotifyChange({"sptaList"})
	public void sort(@BindingParam("col") String column) {
		if (activeCheck()) {
			if (sortBy.equals(column)) {
				sortAsc = !sortAsc;
			} else {
				sortBy = column;
				sortAsc = true;
			}

			sptaList = mobSptaSvc.findCancelRequest(StringUtils.isEmpty(sArg)?"":sArg, 
					pageNo, sortBy, sortAsc);
		}
	}
}
