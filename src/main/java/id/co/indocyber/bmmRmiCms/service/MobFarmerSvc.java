package id.co.indocyber.bmmRmiCms.service;

import id.co.indocyber.bmmRmiCms.dto.MobFarmerDto;
import id.co.indocyber.bmmRmiCms.entity.MobFarmer;

import java.sql.Timestamp;
import java.util.List;

public interface MobFarmerSvc {
	public List<MobFarmer> findAll();

	public MobFarmer findOne(Integer id);

	public List<MobFarmerDto> findAllDto(Integer pageNo, String colName, boolean sortAsc);

	public Integer countAll();

	public Integer countAllByArg(String arg);

	public List<MobFarmerDto> findDtoByArg(String arg, Integer pageNo,
			String colName, boolean sortAsc);

	public void disable(Integer id);

	public List<MobFarmer> findAllNoUser();
	
	public List<MobFarmer> deepFindManagedFarmers(List<Integer> listId);
	
	// MAPPING FUNCTIONS HERE
	public List<MobFarmer> findAllUnassigned();
	public List<MobFarmer> findAllUnassignedFromHead(int id, int empId);

	public List<MobFarmer> findAssignedFarmers(Integer employeeId);

	public void saveAssignments(Integer employeeId, List<Integer> farmerIds, 
			String createdBy, Timestamp createdDate);
	
	public MobFarmer save(MobFarmerDto dto);
}
