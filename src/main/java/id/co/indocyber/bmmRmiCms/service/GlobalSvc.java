package id.co.indocyber.bmmRmiCms.service;

import java.util.List;

import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.entity.MUserRole;

public interface GlobalSvc {
	
	public abstract List<Integer> getAllMyTeam(Integer personId);
	public abstract List<MUserDto> listTesting();
	public MUserRole findUserLogin(String username);

}
