package id.co.indocyber.bmmRmiCms.service.impl;

import java.math.BigDecimal;
import java.sql.Date;
import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.Collections;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MobEmployeeDao;
import id.co.indocyber.bmmRmiCms.dao.MobFarmerDao;
import id.co.indocyber.bmmRmiCms.dao.MobScheduleVisitAppraisementDao;
import id.co.indocyber.bmmRmiCms.dao.MobScheduleVisitOtherActivitiesDao;
import id.co.indocyber.bmmRmiCms.dao.MobScheduleVisitToFarmerDetailDao;
import id.co.indocyber.bmmRmiCms.dao.MobScheduleVisitToFarmerHeaderDao;
import id.co.indocyber.bmmRmiCms.dao.MobScheduleVisitToFarmerHistoryDao;
import id.co.indocyber.bmmRmiCms.dao.MobStandDao;
import id.co.indocyber.bmmRmiCms.dto.MobEmployeeDto;
import id.co.indocyber.bmmRmiCms.dto.MobScheduleVisitAppraisementDto;
import id.co.indocyber.bmmRmiCms.dto.MobScheduleVisitScoringDto;
import id.co.indocyber.bmmRmiCms.dto.MobScheduleVisitToFarmerHeaderDto;
import id.co.indocyber.bmmRmiCms.dto.MobScheduleVisitToFarmerHistoryDto;
import id.co.indocyber.bmmRmiCms.dto.MobScheduleVisitToFarmerResultsDto;
import id.co.indocyber.bmmRmiCms.entity.MobEmployee;
import id.co.indocyber.bmmRmiCms.entity.MobPaddyField;
import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitOtherActivities;
import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitScoring;
import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitToFarmerDetail;
import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitToFarmerHeader;
import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitToFarmerHistory;
import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitToFarmerHistoryPK;
import id.co.indocyber.bmmRmiCms.entity.MobStand;
import id.co.indocyber.bmmRmiCms.service.MobFarmSvc;
import id.co.indocyber.bmmRmiCms.service.MobPaddyFieldSvc;
import id.co.indocyber.bmmRmiCms.service.MobScheduleVisitSvc;
import ma.glasnost.orika.MapperFacade;

@Service("mobScheduleVisitSvc")
@Transactional
public class MobScheduleVisitSvcImpl extends SortGenerator implements MobScheduleVisitSvc{
	@Autowired
	MobScheduleVisitToFarmerHeaderDao mobScheduleVisitToFarmerHeaderDao;
	
	@Autowired
	MobScheduleVisitAppraisementDao mobScheduleVisitAppraisementDao;
	
	@Autowired
	MobScheduleVisitOtherActivitiesDao mobScheduleVisitOtherActivitiesDao;
	
	@Autowired
	MobScheduleVisitToFarmerDetailDao mobScheduleVisitToFarmerDetailDao;
	
	@Autowired
	MobScheduleVisitToFarmerHistoryDao mobScheduleVisitToFarmerHistoryDao;
	
	@Autowired
	MobFarmerDao mobFarmerDao;
	
	@Autowired
	MobFarmSvc mobFarmSvc;
	
	@Autowired
	MobStandDao mobStandDao;
	
	@Autowired
	MobPaddyFieldSvc mobPaddyFieldSvc;
	
	@Autowired
	MobEmployeeDao mobEmployeeDao;

	@Autowired
	MapperFacade mapperFacade;
	
	private MobScheduleVisitToFarmerHeader convertDtoToEntity(
			MobScheduleVisitToFarmerHeaderDto editHeader) {
		MobScheduleVisitToFarmerHeader entity = new MobScheduleVisitToFarmerHeader();
		entity.setAssignTo(editHeader.getAssignedEmployee().getId());
		entity.setCreatedBy(editHeader.getCreatedBy());
		entity.setCreatedDate(editHeader.getCreatedDate());
		entity.setEstimateHarvestTime(editHeader.getEstimateHarvestTime());
		entity.setEvaluation(editHeader.getEvaluation());
		entity.setFarmerId(editHeader.getFarmer().getId());
		entity.setIsActive(editHeader.getIsActive());
		entity.setModifiedBy(editHeader.getModifiedBy());
		entity.setModifiedDate(editHeader.getModifiedDate());
		entity.setNotes(editHeader.getNotes());
		entity.setPlanText(editHeader.getPlanText());
		entity.setTaskActivity(editHeader.getTaskActivity());
		entity.setVisitDate(editHeader.getVisitDate());
		entity.setVisitExpiredDate(editHeader.getVisitExpiredDate());
		entity.setVisitNo(editHeader.getVisitNo());
		return entity;
	}

	private void saveHistory(MobScheduleVisitToFarmerHeader entity){
		MobScheduleVisitToFarmerHistory hist = new MobScheduleVisitToFarmerHistory();
		hist.setAssignDate(entity.getModifiedDate());
		hist.setAssignTo(entity.getAssignTo());
		hist.setCreatedBy(entity.getModifiedBy());
		hist.setCreatedDate(entity.getModifiedDate());
		hist.setFarmerId(entity.getFarmerId());
		hist.setIsActive(true);
		hist.setTaskAction(entity.getTaskActivity());
		hist.setVisitNo(entity.getVisitNo());
		mobScheduleVisitToFarmerHistoryDao.save(hist);
	}
		
	public MobScheduleVisitToFarmerHeaderDto convertEntityToDto(MobScheduleVisitToFarmerHeader x){
		MobScheduleVisitToFarmerHeaderDto y = new MobScheduleVisitToFarmerHeaderDto(x);
		List<MobScheduleVisitToFarmerDetail> details = mobScheduleVisitToFarmerDetailDao.findActiveVisitDetail(x.getVisitNo());
		
		List<String> paddyFieldCodes = new ArrayList<>();
		for(MobScheduleVisitToFarmerDetail detail: details){
			if(detail.getIsActive())
				paddyFieldCodes.add(detail.getPaddyFieldCode());
		}
		
		if(x.getFarmerId()!=null) y.setFarmer(mobFarmerDao.findOne(x.getFarmerId()));
		if(x.getAssignTo()!=null){
			MobEmployee m = mobEmployeeDao.findOne(x.getAssignTo());
			y.setAssignedEmployee(mapperFacade.map(m, MobEmployeeDto.class));
		}
		if(details!=null && details.size()>0){ 
			y.setFarm(mobFarmSvc.findOne(y.getFarmer().getId(),details.get(0).getFarmCode()));
			y.setPaddyFields(mobPaddyFieldSvc.findSelectedPaddyFields(
				y.getFarmer().getId(), y.getFarm()==null?details.get(0).getFarmCode():y.getFarm().getFarmCode(), paddyFieldCodes));
		}
		
		y.setAdminDelegable(!mobScheduleVisitToFarmerHistoryDao.hasBeenDelegated(y.getVisitNo()));
		
		return y;
	}

	// VISIT SCHEDULING
	@Override
	public List<MobScheduleVisitToFarmerHeader> findAllVisitHeaders() {
		return mobScheduleVisitToFarmerHeaderDao.findAll();
	}

	@Override
	public List<MobScheduleVisitToFarmerHeaderDto> findAllVisitHeaders(
			List<Integer> listFarmerId, Integer pageNo, String colName, boolean sortAsc) {
		
		List<MobScheduleVisitToFarmerHeader> listObj = mobScheduleVisitToFarmerHeaderDao.findAllPending(
				listFarmerId, new PageRequest(pageNo - 1, 25, 
						generateSort("isActive", false).and(generateSort(colName,sortAsc))));
		List<MobScheduleVisitToFarmerHeaderDto> res = new ArrayList<>();
		
		for(MobScheduleVisitToFarmerHeader x: listObj){
			res.add(convertEntityToDto(x));
//			MobScheduleVisitToFarmerHeaderDto dto = new MobScheduleVisitToFarmerHeaderDto();		
//			dto.setVisitNo(x.getVisitNo());
//			dto.setVisitDate(x.getVisitDate());
//			dto.setVisitExpiredDate(x.getVisitExpiredDate());
//			dto.setFarmerId(x.getFarmerId());
//			dto.setAssignTo(x.getAssignTo());
//			dto.setNotes(x.getNotes());
//			dto.setEstimateHarvestTime(x.getEstimateHarvestTime());
//			dto.setPlanText(x.getPlanText());
//			dto.setEvaluation(x.getEvaluation());
//			dto.setTaskActivity(x.getTaskActivity());
//			dto.setIsActive(x.getIsActive());
//			dto.setCreatedBy(x.getCreatedBy());
//			dto.setCreatedDate(x.getCreatedDate());
//			dto.setModifiedBy(x.getModifiedBy());
//			dto.setModifiedDate(x.getModifiedDate());
		}
		return res;
	}

	@Override
	public Integer countAllVisitHeaders(List<Integer> listId) {
		return mobScheduleVisitToFarmerHeaderDao.countAllPending(listId).intValue();
	}

	@Override
	public List<MobScheduleVisitToFarmerHeaderDto> findVisitHeadersByArg(
			List<Integer> listId, String arg, Integer pageNo, String colName, boolean sortAsc) {
		List<MobScheduleVisitToFarmerHeaderDto> res = new ArrayList<>();
		for(MobScheduleVisitToFarmerHeader x: 
			mobScheduleVisitToFarmerHeaderDao.findAllByArg(listId, arg,
				new PageRequest(pageNo - 1, 25, 
						generateSort("isActive", false).and(generateSort(colName,sortAsc))))){
			res.add(convertEntityToDto(x));
		}
		return res;
	}

	@Override
	public Integer countVisitHeadersByArg(List<Integer> listId, String arg) {
		return mobScheduleVisitToFarmerHeaderDao.countAllByArg(listId, arg).intValue();
	}

	@Override
	public void save(MobScheduleVisitToFarmerHeaderDto editHeader) {
		MobScheduleVisitToFarmerHeader entity = convertDtoToEntity(editHeader);
		saveHistory(mobScheduleVisitToFarmerHeaderDao.save(entity));
		
		// Handle Edits (Apabila field yang akan dikunjungi berubah)
		List<MobScheduleVisitToFarmerDetail> details = 
			new ArrayList<>(mobScheduleVisitToFarmerDetailDao.findVisitDetail(entity.getVisitNo()));
		for(MobScheduleVisitToFarmerDetail x : details){
			x.setIsActive(false);
			x.setTaskActivity("01");
		}
		
		for(MobPaddyField x : editHeader.getPaddyFields()){
			boolean found = false;
			MobScheduleVisitToFarmerDetail y = null;
			for(MobScheduleVisitToFarmerDetail z: details){
				if(z.getPaddyFieldCode().equals(x.getPaddyFieldCode()) 
						&& z.getFarmCode().equals(x.getFarmCode())){
					y = z;
					found = true;
					break;
				}
			}
			if(y==null) y = new MobScheduleVisitToFarmerDetail();
			y.setCreatedBy(entity.getCreatedBy());
			y.setCreatedDate(entity.getCreatedDate());
			y.setFarmCode(editHeader.getFarm().getFarmCode());
			y.setIsActive(true);
			y.setLatitude(x.getLatitude());
			y.setLongitude(x.getLongitude());
			y.setPaddyFieldCode(x.getPaddyFieldCode());
			y.setVisitEndDate(entity.getVisitExpiredDate());
			y.setVisitStartDate(entity.getVisitDate());
			y.setVisitNo(entity.getVisitNo());
			y.setTaskActivity("02");
			if(!found) details.add(y);
		}
		mobScheduleVisitToFarmerDetailDao.save(details);
	}
	
	@Override
	public void saveAmbil(MobScheduleVisitToFarmerHeaderDto editHeader) {
		MobScheduleVisitToFarmerHeader entity = convertDtoToEntity(editHeader);
		saveHistory(mobScheduleVisitToFarmerHeaderDao.save(entity));
		
		// Handle Edits (Apabila field yang akan dikunjungi berubah)
		List<MobScheduleVisitToFarmerDetail> details = 
			new ArrayList<>(mobScheduleVisitToFarmerDetailDao.findVisitDetail(entity.getVisitNo()));
		for(MobScheduleVisitToFarmerDetail x : details){
			x.setIsActive(false);
			x.setTaskActivity("01");
		}
		
		for(MobPaddyField x : editHeader.getPaddyFields()){
			boolean found = false;
			MobScheduleVisitToFarmerDetail y = null;
			for(MobScheduleVisitToFarmerDetail z: details){
				if(z.getPaddyFieldCode().equals(x.getPaddyFieldCode()) 
						&& z.getFarmCode().equals(x.getFarmCode())){
					y = z;
					found = true;
					break;
				}
			}
			if(y==null) y = new MobScheduleVisitToFarmerDetail();
			y.setCreatedBy(entity.getCreatedBy());
			y.setCreatedDate(entity.getCreatedDate());
			y.setFarmCode(editHeader.getFarm().getFarmCode());
			y.setIsActive(true);
			y.setLatitude(x.getLatitude());
			y.setLongitude(x.getLongitude());
			y.setPaddyFieldCode(x.getPaddyFieldCode());
			y.setVisitEndDate(entity.getVisitExpiredDate());
			y.setVisitStartDate(entity.getVisitDate());
			y.setVisitNo(entity.getVisitNo());
			y.setTaskActivity("03");
			if(!found) details.add(y);
		}
		mobScheduleVisitToFarmerDetailDao.save(details);
	}

	@Override
	public void cancel(MobScheduleVisitToFarmerHeaderDto editHeader) {
		MobScheduleVisitToFarmerHeader entity = mobScheduleVisitToFarmerHeaderDao.findOne(editHeader.getVisitNo());
		entity.setTaskActivity("01");
		entity.setNotes(editHeader.getNotes());
		entity.setIsActive(false);
		mobScheduleVisitToFarmerHeaderDao.save(entity);
		
		List<MobScheduleVisitToFarmerDetail> details = 
				new ArrayList<>(mobScheduleVisitToFarmerDetailDao.findVisitDetail(entity.getVisitNo()));
			for(MobScheduleVisitToFarmerDetail x : details){
				x.setIsActive(false);
				x.setTaskActivity("01");
			}
			
		mobScheduleVisitToFarmerDetailDao.save(details);
		
		MobScheduleVisitToFarmerHistoryPK pk = new MobScheduleVisitToFarmerHistoryPK();
		pk.setAssignTo(entity.getAssignTo());
		pk.setFarmerId(entity.getFarmerId());
		pk.setVisitNo(entity.getVisitNo());
		
		MobScheduleVisitToFarmerHistory prev = mobScheduleVisitToFarmerHistoryDao.findOne(pk);
		
		if(prev!=null){
			prev.setTaskAction("01");
			prev.setCreatedDate(entity.getModifiedDate());
		}
		
		mobScheduleVisitToFarmerHistoryDao.save(prev);
	}

	@Override
	public Integer getVisitCount() {
		return mobScheduleVisitToFarmerHeaderDao.getCurrentMonthYearVisitCount();
	}

	@Override
	public List<MobScheduleVisitToFarmerDetail> findDetails(String visitNo) {
		return mobScheduleVisitToFarmerDetailDao.findVisitDetail(visitNo);
	}

	/*
	@Override
	public void reAssignSave(MobScheduleVisitToFarmerHeaderDto x, Integer previous) {
		MobScheduleVisitToFarmerHeader entity = convertDtoToEntity(x);
		
		MobScheduleVisitToFarmerHistoryPK pk = new MobScheduleVisitToFarmerHistoryPK();
		pk.setAssignTo(previous);
		pk.setFarmerId(entity.getFarmerId());
		pk.setVisitNo(entity.getVisitNo());
		
		MobScheduleVisitToFarmerHistory prev = mobScheduleVisitToFarmerHistoryDao.findOne(pk);
		
		if(prev!=null){
			prev.setTaskAction("04");
			prev.setCreatedDate(entity.getCreatedDate());
		}
		
		mobScheduleVisitToFarmerHistoryDao.save(prev);
		
		saveHistory(mobScheduleVisitToFarmerHeaderDao.save(entity));
	}
	*/

	// VISIT HISTORY
	@Override
	public List<MobScheduleVisitToFarmerResultsDto> getResults(Date startDate,
			Date endDate, List<Integer> employees) {
		List<Object[]> list = mobScheduleVisitToFarmerDetailDao.findResults(startDate, endDate, employees);
		List<MobScheduleVisitToFarmerResultsDto> res = new ArrayList<>();
		Set<Integer> visitIds = new HashSet<>();
		String prevId = null;
		Integer prevFarmer = null;
		String prevEmployee = null;
		String prevVillage = null;
		String prevFarm = null;
		Integer index = 0;
		Integer innerIndex = 0;
		
		MobScheduleVisitToFarmerResultsDto result = null;
		MobScheduleVisitScoringDto det = null;
		MobScheduleVisitScoring score = null;
		for(Object[] x: list){
			// new report entry untuk employee berbeda / farmer berbeda
			if((prevId==null || prevId.compareToIgnoreCase((String)x[0])!=0)
					||(prevFarmer==null || prevFarmer.compareTo((Integer)x[1])!=0)
					||(prevEmployee==null || prevEmployee.compareToIgnoreCase((String)x[2])!=0)){
				if(index>0){
					// process ranking & scoring
					det = result.getReports().get(innerIndex-1);
					for(MobScheduleVisitScoring y : det.getReportDetails()){
						det.setWeightSum(det.getWeightSum()+y.getScore());
					}
					
					int rank=1;
					result = res.get(index-1);
					Collections.sort(result.getReports());
					for(MobScheduleVisitScoringDto z: result.getReports()){
						z.setRank(rank++);
					}
					result.setVisitDetailIds(new HashSet<>(visitIds));
					visitIds.clear();
				}
				prevId = (String)x[0];
				prevFarmer = (Integer)x[1];
				prevEmployee = (String)x[2];
				result = new MobScheduleVisitToFarmerResultsDto();
				result.setFarmer(mobFarmerDao.findOne((Integer)x[1]));
				result.setStaffOnFarm((String)x[2]);
				result.setVillage((String)x[3]);
				result.setVisitDate((Date)x[21]);
				result.setVisitEndDate((Date)x[22]);
				result.setFarmCount(mobFarmSvc.farmCount((Integer)x[1]));
				result.setVisitId((String)x[0]);
				result.setTaskActivity((String)x[17]);
				result.setNotes((String)x[18]);
				result.setLatitude(x[19]==null?null:((BigDecimal)x[19]).doubleValue());
				result.setLongitude(x[20]==null?null:((BigDecimal)x[20]).doubleValue());
				result.setCoordString(
						(result.getLatitude()==null?"":result.getLatitude().toString()).concat(",").concat(
						(result.getLongitude()==null?"":result.getLongitude().toString())) );
				res.add(index++,result);
				innerIndex=0;
				
				prevVillage = null;
				prevFarm = null;
			}
			result = res.get(index-1);
			// new scoring detail entry untuk farm berbeda
			if((prevVillage==null || prevVillage.compareToIgnoreCase((String)x[3])!=0)
					||(prevFarm==null || prevFarm.compareToIgnoreCase((String)x[5])!=0)){
				if(innerIndex>0){
					det = result.getReports().get(innerIndex-1);
					for(MobScheduleVisitScoring y : det.getReportDetails()){
						det.setWeightSum(det.getWeightSum()+y.getScore());
					}
				}
				prevVillage = (String)x[3];
				prevFarm = (String)x[5];
				
				det = new MobScheduleVisitScoringDto();
				det.setFarmVille((String)x[5]);
				det.setFieldArea(mobPaddyFieldSvc.getFieldArea((Integer)x[1],(String)x[4],(String)x[5]));
				det.setReportDate(x[16]==null ? null : new Date(((Timestamp)x[16]).getTime()));
				result.getReports().add(innerIndex++,det);
				if(x[6]!=null) visitIds.add((Integer)x[6]);
			}
			det = result.getReports().get(innerIndex-1);
			if(!("01".equals(x[17]))){
				if(x[7]!=null){
					score = new MobScheduleVisitScoring();
					score.setScheduleVisitDetailId((Integer)x[6]);
					score.setParamCode((String)x[8]);
					score.setParamValue((String)x[9]);
					score.setCriteria((String)x[10]);
					score.setWeight(x[11]==null?0:((BigDecimal)x[11]).doubleValue());
					score.setScore(x[12]==null?0:((BigDecimal)x[12]).doubleValue());
					score.setNotes((String)x[13]);
					score.setIsActive((Boolean)x[14]);
					score.setCreatedBy((String)x[15]);
					score.setCreatedDate((Timestamp)x[16]);
					
					det.getReportDetails().add(score);
				}
			}
		}
		
		// JUST IN CASE GA ADA DATA YANG DI RETURN
		if(det!=null){
			for(MobScheduleVisitScoring y : det.getReportDetails()){
				det.setWeightSum(det.getWeightSum()+y.getScore());
			}
		}
		
		if(result!=null){
			int rank=1;
			Collections.sort(result.getReports());
			for(MobScheduleVisitScoringDto z: result.getReports()){
				z.setRank(rank++);
			}

			result.setVisitDetailIds(visitIds);
		}
		
		return res;
	}

	@Override
	public List<MobScheduleVisitToFarmerResultsDto> getResultsAdmin(Date startDate,Date endDate) {
		List<Object[]> list = mobScheduleVisitToFarmerDetailDao.findResultsAdmin(startDate, endDate);
		List<MobScheduleVisitToFarmerResultsDto> res = new ArrayList<>();
		Set<Integer> visitIds = new HashSet<>();
		String prevId = null;
		Integer prevFarmer = null;
		String prevEmployee = null;
		String prevVillage = null;
		String prevFarm = null;
		Integer index = 0;
		Integer innerIndex = 0;
		
		MobScheduleVisitToFarmerResultsDto result = null;
		MobScheduleVisitScoringDto det = null;
		MobScheduleVisitScoring score = null;
		for(Object[] x: list){
			// new report entry untuk employee berbeda / farmer berbeda
			if((prevId==null || prevId.compareToIgnoreCase((String)x[0])!=0)
					||(prevFarmer==null || prevFarmer.compareTo((Integer)x[1])!=0)
					||(prevEmployee==null || prevEmployee.compareToIgnoreCase((String)x[2])!=0)){
				if(index>0){
					// process ranking & scoring
					det = result.getReports().get(innerIndex-1);
					for(MobScheduleVisitScoring y : det.getReportDetails()){
						det.setWeightSum(det.getWeightSum()+y.getScore());
					}
					
					int rank=1;
					result = res.get(index-1);
					Collections.sort(result.getReports());
					for(MobScheduleVisitScoringDto z: result.getReports()){
						z.setRank(rank++);
					}
					result.setVisitDetailIds(new HashSet<>(visitIds));
					visitIds.clear();
				}
				prevId = (String)x[0];
				prevFarmer = (Integer)x[1];
				prevEmployee = (String)x[2];
				result = new MobScheduleVisitToFarmerResultsDto();
				result.setFarmer(mobFarmerDao.findOne((Integer)x[1]));
				result.setStaffOnFarm((String)x[2]);
				result.setVillage((String)x[3]);
				result.setVisitDate((Date)x[21]);
				result.setVisitEndDate((Date)x[22]);
				result.setFarmCount(mobFarmSvc.farmCount((Integer)x[1]));
				result.setVisitId((String)x[0]);
				result.setTaskActivity((String)x[17]);
				result.setNotes((String)x[18]);
				result.setLatitude(x[19]==null?null:((BigDecimal)x[19]).doubleValue());
				result.setLongitude(x[20]==null?null:((BigDecimal)x[20]).doubleValue());
				result.setCoordString(
						(result.getLatitude()==null?"":result.getLatitude().toString()).concat(",").concat(
						(result.getLongitude()==null?"":result.getLongitude().toString())) );
				res.add(index++,result);
				innerIndex=0;
				
				prevVillage = null;
				prevFarm = null;
			}
			result = res.get(index-1);
			// new scoring detail entry untuk farm berbeda
			if((prevVillage==null || prevVillage.compareToIgnoreCase((String)x[3])!=0)
					||(prevFarm==null || prevFarm.compareToIgnoreCase((String)x[5])!=0)){
				if(innerIndex>0){
					det = result.getReports().get(innerIndex-1);
					for(MobScheduleVisitScoring y : det.getReportDetails()){
						det.setWeightSum(det.getWeightSum()+y.getScore());
					}
				}
				prevVillage = (String)x[3];
				prevFarm = (String)x[5];
				
				det = new MobScheduleVisitScoringDto();
				det.setFarmVille((String)x[5]);
				det.setFieldArea(mobPaddyFieldSvc.getFieldArea((Integer)x[1],(String)x[4],(String)x[5]));
				det.setReportDate(x[16]==null ? null : new Date(((Timestamp)x[16]).getTime()));
				result.getReports().add(innerIndex++,det);
				if(x[6]!=null) visitIds.add((Integer)x[6]);
			}
			det = result.getReports().get(innerIndex-1);
			if(!("01".equals(x[17]))){
				if(x[7]!=null){
					score = new MobScheduleVisitScoring();
					score.setScheduleVisitDetailId((Integer)x[6]);
					score.setParamCode((String)x[8]);
					score.setParamValue((String)x[9]);
					score.setCriteria((String)x[10]);
					score.setWeight(x[11]==null?0:((BigDecimal)x[11]).doubleValue());
					score.setScore(x[12]==null?0:((BigDecimal)x[12]).doubleValue());
					score.setNotes((String)x[13]);
					score.setIsActive((Boolean)x[14]);
					score.setCreatedBy((String)x[15]);
					score.setCreatedDate((Timestamp)x[16]);
				
					det.getReportDetails().add(score);
				}
			}
		}
		
		// JUST IN CASE GA ADA DATA YANG DI RETURN
		if(det!=null){
			for(MobScheduleVisitScoring y : det.getReportDetails()){
				det.setWeightSum(det.getWeightSum()+y.getScore());
			}
		}
		
		if(result!=null){
			int rank=1;
			Collections.sort(result.getReports());
			for(MobScheduleVisitScoringDto z: result.getReports()){
				z.setRank(rank++);
			}

			result.setVisitDetailIds(visitIds);
		}
		
		return res;
	}
	
	//APPRAISEMENT
	@Override
	public List<MobScheduleVisitAppraisementDto> getAppraisement(Set<Integer> visitIds) {
		List<MobScheduleVisitAppraisementDto> res = new ArrayList<>();
		for(Object[] x: mobScheduleVisitAppraisementDao.getAppraisement(visitIds)) {
			int idx=0;
			MobScheduleVisitAppraisementDto y = new MobScheduleVisitAppraisementDto();
			y.setSubDistrict((String)x[idx++]);
			y.setDistrict((String)x[idx++]);
			y.setFieldArea(((BigDecimal)x[idx++]).doubleValue());
			y.setFarmer((String)x[idx++]);
			y.setPaddyFieldCode((String)x[idx++]);
			y.setPeriodName((String)x[idx++]);
			y.setCurrentSugarcaneHeight(((BigDecimal)x[idx++]).doubleValue());
			y.setCutdownSugarcaneHeight(((BigDecimal)x[idx++]).doubleValue());
			y.setSugarcaneStemWeight(((BigDecimal)x[idx++]).doubleValue());
			y.setWeightPerSugarcane(((BigDecimal)x[idx++]).doubleValue());
			y.setNumberOfStemPerLeng((Integer)x[idx++]);
			y.setLengFactorPerHa((Integer)x[idx++]);
			y.setNumberOfSugarcanePerHa(((BigDecimal)x[idx++]).doubleValue());
			y.setProdPerHectarKu(((BigDecimal)x[idx++]).doubleValue());
			y.setAmountOfProduction(((BigDecimal)x[idx++]).doubleValue());
			y.setCuttingSchedule((String)x[idx++]);
			y.setAvgBrixNumber(((BigDecimal)x[idx++]).doubleValue());
			y.setRendemenEstimation(((BigDecimal)x[idx++]).doubleValue());
			y.setKuHablurPerHa(((BigDecimal)x[idx++]).doubleValue());
			y.setAmountOfHablur(((BigDecimal)x[idx++]).doubleValue());
			y.setLatitude(((BigDecimal)x[idx++]).doubleValue());
			y.setLongitude(((BigDecimal)x[idx++]).doubleValue());
			res.add(y);
		}
		return res;
	}

	@Override
	public void saveReAssign(MobScheduleVisitToFarmerHeaderDto editHeader, Integer previous) {
		MobScheduleVisitToFarmerHeader header = new MobScheduleVisitToFarmerHeader();
		header.setVisitNo(editHeader.getVisitNo());
		header.setVisitDate(editHeader.getVisitDate());
		header.setVisitExpiredDate(editHeader.getVisitExpiredDate());
		header.setFarmerId(editHeader.getFarmerId());
		header.setAssignTo(editHeader.getAssignTo());
		header.setNotes(editHeader.getNotes());
		header.setEstimateHarvestTime(editHeader.getEstimateHarvestTime());
		header.setPlanText(editHeader.getPlanText());
		header.setEvaluation(editHeader.getEvaluation());
		header.setTaskActivity(editHeader.getTaskActivity());
		header.setIsActive(editHeader.getIsActive());
		header.setCreatedBy(editHeader.getCreatedBy());
		header.setCreatedDate(editHeader.getCreatedDate());
		header.setModifiedBy(editHeader.getModifiedBy());
		header.setModifiedDate(editHeader.getModifiedDate());
		
		MobScheduleVisitToFarmerHistoryPK pk = new MobScheduleVisitToFarmerHistoryPK();
		pk.setAssignTo(previous);
		pk.setFarmerId(header.getFarmerId());
		pk.setVisitNo(header.getVisitNo());
		
		MobScheduleVisitToFarmerHistory prev = mobScheduleVisitToFarmerHistoryDao.findOne(pk);
		
		if(prev!=null){
			prev.setTaskAction("04");
			prev.setCreatedDate(header.getCreatedDate());
		}
		
		mobScheduleVisitToFarmerHistoryDao.save(prev);
		
		saveHistory(mobScheduleVisitToFarmerHeaderDao.save(header));
	}
	
	// OTHER ACTIVITIES
	@Override
	public List<MobScheduleVisitOtherActivities> getOtherActivities(
			String visitId) {
		return mobScheduleVisitOtherActivitiesDao.findActivities(visitId);
	}
	
	@Override
	public List<MobScheduleVisitToFarmerHistoryDto> getHistory(String visitNo){
		List<MobScheduleVisitToFarmerHistoryDto> res = new ArrayList<>();
		for(Object[] x: 
			mobScheduleVisitToFarmerHistoryDao.getHistoryWithName(visitNo)){
			MobScheduleVisitToFarmerHistoryDto y = 
					mapperFacade.map(x[0], MobScheduleVisitToFarmerHistoryDto.class);
			y.setAssignedName((String)x[1]);
			res.add(y);
		}
		return res;
	}
	
	// STAND (Tegakkan)
	@Override
	public MobStand getTegakan(String visitNo){
		return mobStandDao.findOne(visitNo);
	}
}
