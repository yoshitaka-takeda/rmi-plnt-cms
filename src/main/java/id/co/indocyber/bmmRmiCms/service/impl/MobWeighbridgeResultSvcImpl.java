package id.co.indocyber.bmmRmiCms.service.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MobDriverDao;
import id.co.indocyber.bmmRmiCms.dao.MobWeighbridgeResultDao;
import id.co.indocyber.bmmRmiCms.dto.MobWeighbridgeResultDto;
import id.co.indocyber.bmmRmiCms.entity.MVehicleType;
import id.co.indocyber.bmmRmiCms.entity.MobWeighbridgeResult;
import id.co.indocyber.bmmRmiCms.service.MiscellaneousSvc;
import id.co.indocyber.bmmRmiCms.service.MobWeighbridgeResultSvc;

@Service("mobWeighbridgeResultSvc")
@Transactional
public class MobWeighbridgeResultSvcImpl extends SortGenerator implements MobWeighbridgeResultSvc{
	@Autowired
	MobWeighbridgeResultDao mobWeighbridgeResultDao;
	@Autowired
	MobDriverDao mobDriverDao;
	@Autowired
	MiscellaneousSvc miscellaneousSvc;
	@Autowired
	MapperFacade mapperFacade;
	
	@Override
	public List<MobWeighbridgeResultDto> findAll(Integer pageNo, String colName,
			boolean sortAsc) {
		List<MobWeighbridgeResult> entities = 
			mobWeighbridgeResultDao.findAll(
					new PageRequest(pageNo-1, 25, generateSort(colName, sortAsc))).getContent();
		List<MobWeighbridgeResultDto> res = new ArrayList<>();
		for(MobWeighbridgeResult x: entities){
			res.add(convertEntityToDto(x));
		}
		return res;
	}

	private MobWeighbridgeResultDto convertEntityToDto(MobWeighbridgeResult x) {
		// TODO Auto-generated method stub
		MobWeighbridgeResultDto y = mapperFacade.map(x, MobWeighbridgeResultDto.class);
		MVehicleType z = miscellaneousSvc.getVehicleType(Integer.parseInt(y.getVehicleType()));
		y.setVehicleType(z==null?y.getVehicleType():z.getVehicleTypeName());
		return y;
	}

	@Override
	public Integer countAll() {
		return ((Long)mobWeighbridgeResultDao.count()).intValue();
	}

	@Override
	public List<MobWeighbridgeResultDto> findByArg(String arg, Date sStart, Date sEnd, Integer pageNo,
			String colName, boolean sortAsc) {
		List<MobWeighbridgeResultDto> res = new ArrayList<>();
		for(MobWeighbridgeResult x: mobWeighbridgeResultDao.findByArg(
				arg,sStart,sEnd, new PageRequest(pageNo-1, 25, generateSort(colName, sortAsc)))){
			res.add(convertEntityToDto(x));
		}
		return res;
	}

	@Override
	public Integer countByArg(String arg, Date sStart, Date sEnd) {
		return mobWeighbridgeResultDao.countByArg(arg,sStart,sEnd).intValue();
	}
}
