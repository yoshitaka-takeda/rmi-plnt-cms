package id.co.indocyber.bmmRmiCms.service;

import id.co.indocyber.bmmRmiCms.dto.MobWeighbridgeResultDto;

import java.sql.Date;
import java.util.List;

public interface MobWeighbridgeResultSvc {
	public List<MobWeighbridgeResultDto> findAll(Integer pageNo, String colName, boolean sortAsc);
	public Integer countAll();
	public List<MobWeighbridgeResultDto> findByArg(String arg, Date sStart, Date sEnd, Integer pageNo, String colName, boolean sortAsc);
	public Integer countByArg(String arg, Date sStart, Date sEnd);
}
