package id.co.indocyber.bmmRmiCms.service.impl;

import org.springframework.data.domain.Sort;

public abstract class SortGenerator {
	public Sort generateSort(String columnName, boolean ascending) {
		return new Sort(ascending ? Sort.Direction.ASC : Sort.Direction.DESC,
				columnName);
	}
}
