package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import id.co.indocyber.bmmRmiCms.dao.MobFarmDao;
import id.co.indocyber.bmmRmiCms.dto.MobFarmDto;
import id.co.indocyber.bmmRmiCms.entity.MobFarm;
import id.co.indocyber.bmmRmiCms.entity.MobFarmPK;
import id.co.indocyber.bmmRmiCms.service.MiscellaneousSvc;
import id.co.indocyber.bmmRmiCms.service.MobFarmSvc;

@Service("mobFarmSvc")
@Transactional
public class MobFarmSvcImpl extends SortGenerator implements MobFarmSvc{
	@Autowired
	MobFarmDao mobFarmDao;
	@Autowired
	MiscellaneousSvc miscellaneousSvc;
	
	@Override
	public List<MobFarm> findAll() {
		return mobFarmDao.findAll();
	}
	
	@Override
	public List<MobFarm> findFarmByFarmer(Integer farmerId) {
		return mobFarmDao.findFarmByFarmer(farmerId);
	}

	@Override
	public MobFarm findOne(Integer farmerId, String farmCode) {
		return mobFarmDao.findOne(new MobFarmPK(farmerId,farmCode));
	}

	@Override
	public List<MobFarmDto> findFarmDtoByFarmer(Integer farmerId) {
		List<MobFarmDto> res = new ArrayList<>();
		for(MobFarm x: findFarmByFarmer(farmerId)){
			res.add(convertEntityToDto(x));
		}
		return res;
	}

	private MobFarmDto convertEntityToDto(MobFarm x) {
		MobFarmDto y = new MobFarmDto(x);
		if(!StringUtils.isEmpty(x.getFarmMainType()))
			y.setFarmMainType(miscellaneousSvc.getFarmType(x.getFarmMainType()));
		if(!StringUtils.isEmpty(x.getFarmSubType()))
			y.setFarmSubType(miscellaneousSvc.getSugarcaneFarmType(x.getFarmSubType()));
		return y;
	}

	@Override
	public Integer farmCount(Integer farmerId) {
		return mobFarmDao.farmCount(farmerId).intValue();
	}
}
