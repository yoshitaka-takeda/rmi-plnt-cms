package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.ArrayList;
import java.util.List;

import ma.glasnost.orika.MapperFacade;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MobActivitiesEntryDao;
import id.co.indocyber.bmmRmiCms.dto.MobActivitiesEntryDto;
import id.co.indocyber.bmmRmiCms.dto.MobEmployeeDto;
import id.co.indocyber.bmmRmiCms.entity.MobActivitiesEntry;
import id.co.indocyber.bmmRmiCms.service.MobActivitiesEntrySvc;

@Service("mobActivitiesEntrySvc")
@Transactional
public class MobActivitiesEntrySvcImpl extends SortGenerator implements
		MobActivitiesEntrySvc {
	@Autowired
	MapperFacade mapperFacade;

	@Autowired
	MobActivitiesEntryDao mobActivitiesEntryDao;

	@Override
	public List<MobActivitiesEntryDto> findAll(String arg, Integer pageNo,
			String colName, boolean sortAsc) {
		List<MobActivitiesEntryDto> res = new ArrayList<>();
		for (Object[] x : mobActivitiesEntryDao.findAll(arg,
				new PageRequest(pageNo - 1, 25, 
					generateSort("isActive", false).and(generateSort(colName, sortAsc))))) {
			MobActivitiesEntryDto y = mapperFacade.map(x[0], MobActivitiesEntryDto.class);
			y.setEmployee(mapperFacade.map(x[1], MobEmployeeDto.class));
			res.add(y);
		}
		return res;
	}

	@Override
	public List<MobActivitiesEntryDto> findAllById(List<Integer> ids,
			String arg, Integer pageNo, String colName, boolean sortAsc) {
		List<MobActivitiesEntryDto> res = new ArrayList<>();
		for (Object[] x : mobActivitiesEntryDao.findAllInMyTeam(ids,arg,
				new PageRequest(pageNo - 1, 25, 
					generateSort("isActive", false).and(generateSort(colName, sortAsc))))) {
			MobActivitiesEntryDto y = mapperFacade.map(x[0], MobActivitiesEntryDto.class);
			y.setEmployee(mapperFacade.map(x[1], MobEmployeeDto.class));
			res.add(y);
		}
		return res;
	}

	@Override
	public Integer countAll(String arg) {
		return mobActivitiesEntryDao.countAll(arg).intValue();
	}

	@Override
	public Integer countAllById(List<Integer> ids, String arg) {
		return mobActivitiesEntryDao.countAllInMyTeam(ids,arg).intValue();
	}

	@Override
	public void save(MobActivitiesEntryDto viewEntry) {
		MobActivitiesEntry entity = new MobActivitiesEntry();
		entity.setActivityInfo(viewEntry.getActivityInfo());
		entity.setActivityType(viewEntry.getActivityType());
		entity.setCreatedBy(viewEntry.getCreatedBy());
		entity.setCreatedDate(viewEntry.getCreatedDate());
		entity.setEmployeeId(viewEntry.getEmployee().getId());
		entity.setId(viewEntry.getId());
		entity.setIsActive(viewEntry.getIsActive());
		entity.setLocation(viewEntry.getLocation());
		entity.setModifiedBy(viewEntry.getModifiedBy());
		entity.setModifiedDate(viewEntry.getModifiedDate());
		mobActivitiesEntryDao.save(entity);
	}

}
