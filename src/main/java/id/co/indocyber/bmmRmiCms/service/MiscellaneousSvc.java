package id.co.indocyber.bmmRmiCms.service;

import id.co.indocyber.bmmRmiCms.entity.MAddressType;
import id.co.indocyber.bmmRmiCms.entity.MEducationType;
import id.co.indocyber.bmmRmiCms.entity.MFarmFieldsType;
import id.co.indocyber.bmmRmiCms.entity.MFarmerType;
import id.co.indocyber.bmmRmiCms.entity.MFundType;
import id.co.indocyber.bmmRmiCms.entity.MGender;
import id.co.indocyber.bmmRmiCms.entity.MMaritalStatus;
import id.co.indocyber.bmmRmiCms.entity.MPlantingPeriod;
import id.co.indocyber.bmmRmiCms.entity.MReligion;
import id.co.indocyber.bmmRmiCms.entity.MSugarcaneFarmType;
import id.co.indocyber.bmmRmiCms.entity.MSugarcaneType;
import id.co.indocyber.bmmRmiCms.entity.MVehicleType;

import java.util.List;

public interface MiscellaneousSvc {
	public List<String> getOptions(String key);
	public List<MGender> getGenders();
	public List<MReligion> getReligions();
	public List<MEducationType> getEducationTypes();
	public List<MAddressType> getAddressTypes();
	public List<MMaritalStatus> getMaritalStatuses();
	public List<MVehicleType> getVehicleTypes();
	public List<MFarmerType> getFarmerTypes();
	public List<MFarmFieldsType> getFarmTypes();
	
	public MGender getGender(String code);
	public MReligion getReligion(String code);
	public MEducationType getEducationType(String code);
	public MAddressType getAddressType(String code);
	public MMaritalStatus getMaritalStatus(String code);
	public MVehicleType getVehicleType(Integer code);
	public MFarmerType getFarmerType(String code);
	public MFarmFieldsType getFarmType(String code);
	public MFundType getFundType(String code);
	public MSugarcaneFarmType getSugarcaneFarmType(String code);
	public MSugarcaneType getSugarcaneType(String code);
	public MPlantingPeriod getPlantingPeriod(String code);
}
