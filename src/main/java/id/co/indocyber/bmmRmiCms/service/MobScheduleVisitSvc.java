package id.co.indocyber.bmmRmiCms.service;

import id.co.indocyber.bmmRmiCms.dto.MobScheduleVisitAppraisementDto;
import id.co.indocyber.bmmRmiCms.dto.MobScheduleVisitToFarmerHeaderDto;
import id.co.indocyber.bmmRmiCms.dto.MobScheduleVisitToFarmerHistoryDto;
import id.co.indocyber.bmmRmiCms.dto.MobScheduleVisitToFarmerResultsDto;
import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitOtherActivities;
import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitToFarmerDetail;
import id.co.indocyber.bmmRmiCms.entity.MobScheduleVisitToFarmerHeader;
import id.co.indocyber.bmmRmiCms.entity.MobStand;

import java.sql.Date;
import java.util.List;
import java.util.Set;

public interface MobScheduleVisitSvc {
	// VISIT SCHEDULES
	public List<MobScheduleVisitToFarmerHeader> findAllVisitHeaders();

	public List<MobScheduleVisitToFarmerHeaderDto> findAllVisitHeaders(
			List<Integer> listFarmerId, Integer pageNo, String colName, boolean sortAsc);

	public Integer countAllVisitHeaders(List<Integer> newListId);

	public List<MobScheduleVisitToFarmerHeaderDto> findVisitHeadersByArg(
			List<Integer> listId, String arg, Integer pageNo, String colName, boolean sortAsc);

	public Integer countVisitHeadersByArg(List<Integer> listFarmerId, String arg);

	public void save(MobScheduleVisitToFarmerHeaderDto editHeader);
	public void saveReAssign(MobScheduleVisitToFarmerHeaderDto editHeader, Integer previous);
	public void saveAmbil(MobScheduleVisitToFarmerHeaderDto editHeader);
	
	public void cancel(MobScheduleVisitToFarmerHeaderDto editHeader);

	public Integer getVisitCount();

	public List<MobScheduleVisitToFarmerDetail> findDetails(String visitNo);

	//public void reAssignSave(MobScheduleVisitToFarmerHeaderDto x, Integer previous);

	// VISIT HISTORY
	public List<MobScheduleVisitToFarmerResultsDto> getResults(Date startDate, Date endDate, List<Integer> employees);
	public List<MobScheduleVisitToFarmerResultsDto> getResultsAdmin(Date sStart, Date sEnd);
	
	// APPRAISE (Untuk Taksasi)
	public List<MobScheduleVisitAppraisementDto> getAppraisement(Set<Integer> visitIds);

	// OTHER ACTIVITIES
	public List<MobScheduleVisitOtherActivities> getOtherActivities(
			String visitId);

	public List<MobScheduleVisitToFarmerHistoryDto> getHistory(String visitNo);
	
	// STAND (Tegakkan)
	public MobStand getTegakan(String visitNo);
}
