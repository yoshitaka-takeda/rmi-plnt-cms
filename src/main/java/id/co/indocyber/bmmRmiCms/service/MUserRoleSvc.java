package id.co.indocyber.bmmRmiCms.service;

import java.util.List;

import id.co.indocyber.bmmRmiCms.entity.MUserRole;

public interface MUserRoleSvc {
	// asumsi 1 user bisa multiple role di multiple module...
	public MUserRole findUserRole(String email);
	public List<MUserRole> findAll();
	
	public MUserRole cmsAccess(String email);
	public void save(MUserRole uR);
	
	public void remove(MUserRole uR);
}
