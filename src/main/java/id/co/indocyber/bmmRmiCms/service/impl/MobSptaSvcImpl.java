package id.co.indocyber.bmmRmiCms.service.impl;

import java.sql.Date;
import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MobDriverDao;
import id.co.indocyber.bmmRmiCms.dao.MobFarmerDao;
import id.co.indocyber.bmmRmiCms.dao.MobRequestForCancelDao;
import id.co.indocyber.bmmRmiCms.dao.MobSptaDao;
import id.co.indocyber.bmmRmiCms.dto.MobSptaDto;
import id.co.indocyber.bmmRmiCms.entity.MobRequestForCancel;
import id.co.indocyber.bmmRmiCms.entity.MobSpta;
import id.co.indocyber.bmmRmiCms.service.MiscellaneousSvc;
import id.co.indocyber.bmmRmiCms.service.MobSptaSvc;

@Service("mobSptaSvc")
@Transactional
public class MobSptaSvcImpl extends SortGenerator implements MobSptaSvc{
	@Autowired
	MobSptaDao mobSptaDao;
	@Autowired
	MobRequestForCancelDao mobRequestForCancelDao;
	@Autowired
	MiscellaneousSvc miscellaneousSvc;
	@Autowired
	MobFarmerDao mobFarmerDao;
	@Autowired
	MobDriverDao mobDriverDao;

	@Override
	public List<MobSptaDto> findAll(Integer pageNo, String colName,
			boolean sortAsc) {
		List<MobSpta> entities = mobSptaDao.findAll(new PageRequest(pageNo-1, 25, generateSort(colName, sortAsc))).getContent();
		List<MobSptaDto> res = new ArrayList<>();
		for(MobSpta x: entities){
			res.add(convertEntityToDto(x));
		}
		return res;
	}

	private MobSptaDto convertEntityToDto(MobSpta x) {
		MobSptaDto y = new MobSptaDto(x);
		if(x.getVehicleType()!=null) y.setVehicleType(miscellaneousSvc.getVehicleType(x.getVehicleType()));
		if(x.getDriverId()!=null) y.setDriver(mobDriverDao.findOne(x.getDriverId()));
		if(x.getFarmerCode()!=null) y.setFarmer(mobFarmerDao.findByReference(x.getFarmerCode()));
		return y;
	}

	@Override
	public Integer countAll() {
		return ((Long)mobSptaDao.count()).intValue();
	}

	@Override
	public List<MobSptaDto> findByArg(String arg, Date date, Date date2, Integer pageNo,
			String colName, boolean sortAsc) {
		List<MobSptaDto> res = new ArrayList<>();
		for(MobSpta x: mobSptaDao.findByArg(arg,date,date2,new PageRequest(pageNo-1, 25, generateSort(colName, sortAsc)))){
			res.add(convertEntityToDto(x));
		}
		return res;
	}

	@Override
	public Integer countByArg(String arg, Date date, Date date2) {
		return mobSptaDao.countByArg(arg,date,date2).intValue();
	}
	
	@Override
	public List<MobRequestForCancel> findCancelRequest(
			String arg, Integer pageNo, String colName, boolean sortAsc){
		return mobRequestForCancelDao.findAllByArg(arg,new PageRequest(pageNo-1, 25, generateSort(colName, sortAsc)));
	}
	
	@Override
	public Integer countCancelRequest(String arg){
		return mobRequestForCancelDao.countAllByArg(arg).intValue();
	}
}
