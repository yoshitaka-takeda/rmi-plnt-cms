package id.co.indocyber.bmmRmiCms.service.impl;

import java.sql.Timestamp;
import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import id.co.indocyber.bmmRmiCms.dao.MobEmployeeDao;
import id.co.indocyber.bmmRmiCms.dao.MobFarmerDao;
import id.co.indocyber.bmmRmiCms.dao.MobMappingFarmerEmployeeDao;
import id.co.indocyber.bmmRmiCms.dto.MobFarmerDto;
import id.co.indocyber.bmmRmiCms.entity.MobFarmer;
import id.co.indocyber.bmmRmiCms.entity.MobMappingFarmerEmployee;
import id.co.indocyber.bmmRmiCms.service.GlobalSvc;
import id.co.indocyber.bmmRmiCms.service.MiscellaneousSvc;
import id.co.indocyber.bmmRmiCms.service.MobFarmerSvc;

@Service("mobFarmerSvc")
@Transactional
public class MobFarmerSvcImpl extends SortGenerator implements MobFarmerSvc{
	@Autowired
	MobFarmerDao mobFarmerDao;
	@Autowired
	MobEmployeeDao mobEmployeeDao;
	@Autowired
	MobMappingFarmerEmployeeDao mobMappingFarmerEmployeeDao;
	@Autowired
	MiscellaneousSvc miscellaneousSvc;
	@Autowired
	GlobalSvc globalSvc;
	
	@Override
	public List<MobFarmer> findAll() {
		return mobFarmerDao.findAll();
	}

	@Override
	public MobFarmer findOne(Integer id) {
		return mobFarmerDao.findOne(id);
	}

	@Override
	public List<MobFarmerDto> findAllDto(Integer pageNo, String colName,
			boolean sortAsc) {
		List<MobFarmerDto> res = new ArrayList<>();
		for(MobFarmer x: mobFarmerDao.findAll(new PageRequest(pageNo-1,10,
				generateSort("isActive", false).and(generateSort(colName, sortAsc))))){
			res.add(convertEntityToDto(x));
		}
		return res;
	}

	private MobFarmerDto convertEntityToDto(MobFarmer x) {
		MobFarmerDto y = new MobFarmerDto(x);
		if(!StringUtils.isEmpty(x.getAddressType())) 
			y.setAddressType(miscellaneousSvc.getAddressType(x.getAddressType()));
		if(!StringUtils.isEmpty(x.getFarmerType())) 
			y.setFarmerType(miscellaneousSvc.getFarmerType(x.getFarmerType()));
		if(!StringUtils.isEmpty(x.getFarmType())) 
			y.setFarmType(miscellaneousSvc.getFarmType(x.getFarmType()));
		if(!StringUtils.isEmpty(x.getFundType())) 
			y.setFundType(miscellaneousSvc.getFundType(x.getFundType()));
		if(!StringUtils.isEmpty(x.getReligion())) 
			y.setReligion(miscellaneousSvc.getReligion(x.getReligion()));
		if(!StringUtils.isEmpty(x.getLastEducation())) 
			y.setLastEducation(miscellaneousSvc.getEducationType(x.getLastEducation()));
		if(!StringUtils.isEmpty(x.getGender())) 
			y.setGender(miscellaneousSvc.getGender(x.getGender()));
		return y;
	}

	@Override
	public Integer countAll() {
		return ((Long)mobFarmerDao.count()).intValue();
	}

	@Override
	public Integer countAllByArg(String arg) {
		return mobFarmerDao.countByArg(arg).intValue();
	}

	@Override
	public List<MobFarmerDto> findDtoByArg(String arg, Integer pageNo,
			String colName, boolean sortAsc) {
		List<MobFarmerDto> res = new ArrayList<>();
		for(MobFarmer x: mobFarmerDao.findByArg(arg, 
				new PageRequest(pageNo-1,10,
					generateSort("isActive", false).and(generateSort(colName, sortAsc))))){
			res.add(convertEntityToDto(x));
		}
		return res;
	}

	@Override
	public void disable(Integer id) {
		MobFarmer x = mobFarmerDao.findOne(id);
		x.setIsActive(false);
		mobFarmerDao.save(x);
	}
	
	@Override
	public List<MobFarmer> deepFindManagedFarmers(List<Integer> listId) {
		Set<MobFarmer> allFarmers = new HashSet<>();
		for(Integer x: listId){
			allFarmers.addAll( 
				mobFarmerDao.findAll(mobMappingFarmerEmployeeDao.findAssignedFarmerIds(x)) );
		}
		return new ArrayList<>(allFarmers);
	}
	
	// MAPPING FUNCTIONS HERE
	@Override 
	public List<MobFarmer> findAllUnassigned(){
		return new ArrayList<>(mobFarmerDao.findAllUnmapped());
	}
	
	@Override
	public List<MobFarmer> findAssignedFarmers(Integer employeeId){
		List<Integer> managedEmployees = globalSvc.getAllMyTeam(employeeId);
		managedEmployees.add(employeeId);
		return new ArrayList<>(
				mobFarmerDao.findAll(mobMappingFarmerEmployeeDao.findAssignedFarmerIdsMultiple(managedEmployees)));
	}
	
	@Override
	public void saveAssignments(Integer employeeId, List<Integer> farmerIds,
			String createdBy, Timestamp createdDate){
		List<MobMappingFarmerEmployee> prev = 
				mobMappingFarmerEmployeeDao.findExisting(employeeId);
		
		for(MobMappingFarmerEmployee x: prev){
			x.setIsActive(false);
		}
		
		for(Integer x: farmerIds){
			MobMappingFarmerEmployee map = null;
			for(MobMappingFarmerEmployee y: prev){
				if(x.equals(y.getFarmerId())){
					map = y;
					y.setIsActive(true);
					break;
				}
			}
			if(map==null){
				map = new MobMappingFarmerEmployee();
				map.setEmployeeId(employeeId);
				map.setFarmerId(x);
				map.setIsActive(true);
				map.setCreatedBy(createdBy);
				map.setCreatedDate(createdDate);
				prev.add(map);
			}
			else{
				continue;
			}
		}
		
		mobMappingFarmerEmployeeDao.save(prev);
		
		// Then add new mappings ABOVE ME to the TOP of the hierarchy!
		Integer myManager = mobEmployeeDao.getMyManager(employeeId);
		while(myManager!=null && !myManager.equals(0)){
			List<MobMappingFarmerEmployee> mMap = new ArrayList<>();
			for(Integer x: farmerIds){
				MobMappingFarmerEmployee map = new MobMappingFarmerEmployee();
				map.setEmployeeId(employeeId);
				map.setFarmerId(x);
				map.setIsActive(true);
				map.setCreatedBy(createdBy);
				map.setCreatedDate(createdDate);
				prev.add(map);
			}
			mobMappingFarmerEmployeeDao.save(mMap);
			myManager = mobEmployeeDao.getMyManager(myManager);
		}
	}

	@Override
	public List<MobFarmer> findAllNoUser() {
		return mobFarmerDao.findAllNoUser();
	}

	@Override
	public MobFarmer save(MobFarmerDto dto) {
		MobFarmer x = new MobFarmer();
		x.setAddress(dto.getAddress());
		x.setAddressType(dto.getAddressType()==null?"":dto.getAddressType().getCode());
		x.setBirthday(dto.getBirthday());
		x.setBusinessPartnerCode(dto.getBusinessPartnerCode());
		x.setCity(dto.getCity());
		x.setDistrict(dto.getDistrict());
		x.setEmail(dto.getEmail());
		x.setFarmerType(dto.getFarmerType()==null?"":dto.getFarmerType().getCode());
		x.setFarmType(dto.getFarmType()==null?"":dto.getFarmType().getCode());
		x.setFieldTotal(dto.getFieldTotal());
		x.setFullname(dto.getFullname());
		x.setFundType(dto.getFundType()==null?"":dto.getFundType().getCode());
		x.setGender(dto.getGender()==null?"":dto.getGender().getCode());
		x.setId(dto.getId());
		x.setIsActive(dto.getIsActive());
		x.setKtp(dto.getKtp());
		x.setLastEducation(dto.getLastEducation()==null?"":dto.getLastEducation().getCode());
		x.setNpwp(dto.getNpwp());
		x.setPhoneNum(dto.getPhoneNum());
		x.setPlaceOfBirthday(dto.getPlaceOfBirthday());
		x.setProcessedBy(dto.getProcessedBy());
		x.setProcessedDate(dto.getProcessedDate());
		x.setProvince(dto.getProvince());
		x.setReferenceCode(dto.getReferenceCode());
		x.setReligion(dto.getReligion()==null?"":dto.getReligion().getCode());
		x.setRtrw(dto.getRtrw());
		x.setStatus(dto.getStatus()==null?"":dto.getStatus().getCode());
		x.setSubDistrict(dto.getSubDistrict());
		x.setVillage(dto.getVillage());
		x.setVillage(dto.getVillage());
		x.setZipcode(dto.getZipcode());
		x.setModifiedBy(dto.getModifiedBy());
		x.setModifiedDate(dto.getModifiedDate());
		return mobFarmerDao.save(x);
	}

	@Override
	public List<MobFarmer> findAllUnassignedFromHead(int id, int empId) {
		return new ArrayList<>(mobFarmerDao.findAllUnmappedFromHead(id,empId));
	}
}
