package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import id.co.indocyber.bmmRmiCms.dao.MobDriverDao;
import id.co.indocyber.bmmRmiCms.dto.MobDriverDto;
import id.co.indocyber.bmmRmiCms.entity.MobDriver;
import id.co.indocyber.bmmRmiCms.service.MiscellaneousSvc;
import id.co.indocyber.bmmRmiCms.service.MobDriverSvc;

@Service("mobDriverSvc")
@Transactional
public class MobDriverSvcImpl extends SortGenerator implements MobDriverSvc {
	@Autowired
	MobDriverDao mobDriverDao;

	@Autowired
	MiscellaneousSvc miscellaneousSvc;
	
	public MobDriverDto convertEntityToDto(MobDriver x) {
		MobDriverDto y = new MobDriverDto(x);

		if (!StringUtils.isEmpty(x.getAddressType()))
			y.setAddressType(miscellaneousSvc.getAddressType(x.getAddressType()));
		if (!StringUtils.isEmpty(x.getLastEducation()))
			y.setLastEducation(miscellaneousSvc.getEducationType(x.getLastEducation()));
		if (!StringUtils.isEmpty(x.getGender()))
			y.setGender(miscellaneousSvc.getGender(x.getGender()));
		if (!StringUtils.isEmpty(x.getReligion()))
			y.setReligion(miscellaneousSvc.getReligion(x.getReligion()));
		if (!StringUtils.isEmpty(x.getMaritalStatus()))
			y.setMaritalStatus(miscellaneousSvc.getMaritalStatus(x.getMaritalStatus()));

		return y;
	}

	@Override
	public List<MobDriver> findAll() {
		return mobDriverDao.findAll();
	}

	@Override
	public List<MobDriverDto> findAll(Integer pageNo, String colName,
			boolean sortAsc) {
		List<MobDriver> entities = mobDriverDao.findAll(
			new PageRequest(pageNo - 1, 25, generateSort("isActive", false).and(generateSort(colName, sortAsc)))).getContent();
		List<MobDriverDto> res = new ArrayList<>();

		for (MobDriver x : entities) {
			res.add(convertEntityToDto(x));
		}

		return res;
	}

	@Override
	public Integer countAll() {
		return ((Long) mobDriverDao.count()).intValue();
	}

	@Override
	public List<MobDriverDto> findByArg(String arg, Integer pageNo,
			String colName, boolean sortAsc) {
		List<MobDriver> entities = mobDriverDao.findByArg(arg, new PageRequest(
				pageNo - 1, 25, generateSort("isActive", false).and(generateSort(colName, sortAsc))));
		List<MobDriverDto> res = new ArrayList<>();

		for (MobDriver x : entities) {
			res.add(convertEntityToDto(x));
		}
		return res;
	}

	@Override
	public Integer countByArg(String arg) {
		return mobDriverDao.countByArg(arg).intValue();
	}

	@Override
	public MobDriver save(MobDriverDto dto) {
		MobDriver entity = new MobDriver();
		entity.setAddress(dto.getAddress());
		entity.setAddressType(dto.getAddressType() == null ? "" : dto.getAddressType().getCode());
		entity.setBirthday(dto.getBirthday());
		entity.setCity(dto.getCity());
		entity.setDistrict(dto.getDistrict());
		entity.setEmail(dto.getEmail());
		entity.setFullname(dto.getFullname());
		entity.setGender(dto.getGender() == null ? "" : dto.getGender().getCode());
		entity.setId(dto.getId());
		entity.setIsActive(dto.getIsActive());
		entity.setKtp(dto.getKtp());
		entity.setLastEducation(dto.getLastEducation() == null ? "" : dto.getLastEducation().getCode());
		entity.setMaritalStatus(dto.getMaritalStatus() == null ? "" : dto.getMaritalStatus().getCode());
		entity.setPhoneNum(StringUtils.isEmpty(dto.getPhoneNum()) ? null : 
			dto.getPhoneNum().startsWith("08")?dto.getPhoneNum().replaceFirst("08", "628"):
			dto.getPhoneNum());
		entity.setPlaceOfBirthday(dto.getPlaceOfBirthday());
		entity.setDriverType(dto.getDriverType());
		entity.setModifiedBy(dto.getModifiedBy());
		entity.setModifiedDate(dto.getModifiedDate());
		entity.setCreatedBy(dto.getCreatedBy());
		entity.setCreatedDate(dto.getCreatedDate());
		entity.setProvince(dto.getProvince());
		entity.setReferenceCode(dto.getReferenceCode());
		entity.setReligion(dto.getReligion() == null ? "" : dto.getReligion().getCode());
		entity.setRtrw(dto.getRtrw());
		entity.setSubDistrict(dto.getSubDistrict());
		entity.setVillage(dto.getVillage());
		entity.setZipcode(dto.getZipcode());
		return mobDriverDao.save(entity);
	}

	@Override
	public boolean existence(Integer id) {
		if (id == null)
			return false;
		return mobDriverDao.findOne(id) != null;
	}

	@Override
	public void disable(Integer id) {
		MobDriver x = mobDriverDao.findOne(id);
		x.setIsActive(false);
		mobDriverDao.save(x);
	}

	@Override
	public boolean existsName(Integer id, String fullname) {
		MobDriver x = id == null ? null : mobDriverDao.findOne(id);
		if (x != null) {
			if (x.getFullname() != null && x.getFullname().equals(fullname)) {
				// special case for unchanged email
				return false;
			} else {
				return mobDriverDao.findByName(fullname).size() != 0;
			}
		} else
			return mobDriverDao.findByName(fullname).size() != 0;
	}
	
	@Override
	public boolean existsEmail(Integer id, String email) {
		MobDriver x = id == null ? null : mobDriverDao.findOne(id);
		if (x != null) {
			if (x.getEmail() != null && x.getEmail().equals(email)) {
				// special case for unchanged email
				return false;
			} else {
				return mobDriverDao.findByEmail(email).size() != 0;
			}
		} else
			return mobDriverDao.findByEmail(email).size() != 0;
	}

	@Override
	public MobDriver findOne(Integer id) {
		return mobDriverDao.findOne(id);
	}

	@Override
	public List<MobDriver> findAllNoUser() {
		return mobDriverDao.findAllNoUser();
	}

	@Override
	public void updatePhone(Integer personId, String phoneNum) {
		MobDriver drv = findOne(personId);
		if(drv!=null){
			drv.setPhoneNum(phoneNum);
			mobDriverDao.save(drv);
		}
	}
}
