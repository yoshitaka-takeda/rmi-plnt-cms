package id.co.indocyber.bmmRmiCms.service;

import id.co.indocyber.bmmRmiCms.dto.MobSptaDto;
import id.co.indocyber.bmmRmiCms.entity.MobRequestForCancel;

import java.sql.Date;
import java.util.List;

public interface MobSptaSvc {
	public List<MobSptaDto> findAll(Integer pageNo, String colName, boolean sortAsc);
	public Integer countAll();
	public List<MobSptaDto> findByArg(String arg, Date date, Date date2, Integer pageNo, String colName, boolean sortAsc);
	public Integer countByArg(String arg, Date date, Date date2);
	
	public List<MobRequestForCancel> findCancelRequest(String arg, Integer pageNo, String colName, boolean sortAsc);
	public Integer countCancelRequest(String arg);
}
