package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MSugarcaneParamScoringDao;
import id.co.indocyber.bmmRmiCms.entity.MSugarcaneParamScoring;
import id.co.indocyber.bmmRmiCms.service.MSugarcaneParamScoringSvc;

@Service("mSugarcaneParamScoringSvc")
@Transactional
public class MSugarcaneParamScoringSvcImpl extends SortGenerator implements MSugarcaneParamScoringSvc{
	@Autowired
	MSugarcaneParamScoringDao mSugarcaneParamScoringDao;

	@Override
	public List<MSugarcaneParamScoring> findAll() {
		return mSugarcaneParamScoringDao.findAll();
	}
	
	@Override
	public List<MSugarcaneParamScoring> findAllActive() {
		return mSugarcaneParamScoringDao.findAllActive();
	}

	@Override
	public List<MSugarcaneParamScoring> findAll(Integer pageNo, String colName,
			boolean sortAsc) {
		return new ArrayList<>(mSugarcaneParamScoringDao.findAll(
			new PageRequest(pageNo-1, 25, 
					generateSort("isActive", false).and(generateSort(colName, sortAsc)))).getContent());
	}
	
	@Override
	public List<MSugarcaneParamScoring> findByArg(String arg, Integer pageNo, String colName,
			boolean sortAsc) {
		return new ArrayList<>(mSugarcaneParamScoringDao.findByArg(arg,
			new PageRequest(pageNo-1, 25, 
					generateSort("isActive", false).and(generateSort(colName, sortAsc)))));
	}

	@Override
	public MSugarcaneParamScoring save(MSugarcaneParamScoring entity) {
		entity.setIsActive(entity.getIsActive()==null?false:entity.getIsActive());
		return mSugarcaneParamScoringDao.save(entity);
	}

	@Override
	public Integer countAll() {
		return ((Long)mSugarcaneParamScoringDao.count()).intValue();
	}

	@Override
	public boolean existence(Integer paramCode) {
		if(paramCode==null) return false;
		return mSugarcaneParamScoringDao.findOne(paramCode)!=null;
	}

	@Override
	public Integer countByArg(String arg) {
		return mSugarcaneParamScoringDao.countByArg(arg).intValue();
	}

	@Override
	public void disable(Integer paramCode) {
		MSugarcaneParamScoring entity = mSugarcaneParamScoringDao.findOne(paramCode);
		entity.setIsActive(false);
		mSugarcaneParamScoringDao.save(entity);
	}

	@Override
	public Double getSum() {
		return mSugarcaneParamScoringDao.getSum();
	}
}
