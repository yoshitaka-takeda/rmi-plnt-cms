package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MobVehicleDao;
import id.co.indocyber.bmmRmiCms.dto.MobVehicleDto;
import id.co.indocyber.bmmRmiCms.entity.MobVehicle;
import id.co.indocyber.bmmRmiCms.service.MiscellaneousSvc;
import id.co.indocyber.bmmRmiCms.service.MobVehicleSvc;

@Service("mobVehicleSvc")
@Transactional
public class MobVehicleSvcImpl extends SortGenerator implements MobVehicleSvc{
	@Autowired
	MobVehicleDao mobVehicleDao;
	
	@Autowired
	MiscellaneousSvc miscellaneousSvc;
	
	public MobVehicleDto convertEntityToDto(MobVehicle x){
		MobVehicleDto y = new MobVehicleDto(x);
		if(x.getVehicleType()!=null) y.setVehicleType(miscellaneousSvc.getVehicleType(x.getVehicleType()));
		return y;
	}
	
	@Override
	public List<MobVehicle> findAll() {
		return mobVehicleDao.findAll();
	}

	@Override
	public List<MobVehicleDto> findAll(Integer pageNo, String colName, boolean sortAsc) {
		List<MobVehicle> entities = mobVehicleDao.findAll(
			new PageRequest(pageNo-1, 25, 
					generateSort("isActive", false).and(generateSort(colName, sortAsc)))).getContent();
		List<MobVehicleDto> res = new ArrayList<>();
		
		for(MobVehicle x: entities){
			res.add(convertEntityToDto(x));
		}
		
		return res;
	}

	@Override
	public Integer countAll() {
		return ((Long)mobVehicleDao.count()).intValue();
	}

	@Override
	public List<MobVehicleDto> findByArg(String arg, Integer pageNo,
			String colName, boolean sortAsc) {
		List<MobVehicle> entities = mobVehicleDao.findByArg(arg, 
				new PageRequest(pageNo-1, 25, 
						generateSort("isActive", false).and(generateSort(colName, sortAsc))));
		List<MobVehicleDto> res = new ArrayList<>();
		
		for(MobVehicle x: entities){
			res.add(convertEntityToDto(x));
		}
		return res;
	}

	@Override
	public Integer countByArg(String arg) {
		return mobVehicleDao.countByArg(arg).intValue();
	}

	@Override
	public void save(MobVehicleDto dto) {
		MobVehicle entity = new MobVehicle();
		entity.setCapacity(dto.getCapacity());
		entity.setCreatedBy(dto.getCreatedBy());
		entity.setCreatedDate(dto.getCreatedDate());
		entity.setIsActive(dto.getIsActive());
		entity.setModifiedBy(dto.getModifiedBy());
		entity.setModifiedDate(dto.getModifiedDate());
		entity.setProductionYear(dto.getProductionYear());
		entity.setVehicleNo(dto.getVehicleNo());
		entity.setVehicleType(dto.getVehicleType().getId());
		mobVehicleDao.save(entity);
	}

	@Override
	public boolean existence(String id) {
		if (id==null) return false;
		return mobVehicleDao.findOne(id)!=null;
	}

	@Override
	public void disable(String id) {
		MobVehicle x = mobVehicleDao.findOne(id);
		x.setIsActive(false);
		mobVehicleDao.save(x);
	}
}
