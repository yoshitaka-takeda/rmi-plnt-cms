package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.ArrayList;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import id.co.indocyber.bmmRmiCms.dao.MobEmployeeDao;
import id.co.indocyber.bmmRmiCms.dao.MobMappingFarmerEmployeeDao;
import id.co.indocyber.bmmRmiCms.dto.MobEmployeeDto;
import id.co.indocyber.bmmRmiCms.entity.MobEmployee;
import id.co.indocyber.bmmRmiCms.entity.MobMappingFarmerEmployee;
import id.co.indocyber.bmmRmiCms.service.MiscellaneousSvc;
import id.co.indocyber.bmmRmiCms.service.MobEmployeeSvc;
import ma.glasnost.orika.MapperFacade;

@Service("mobEmployeeSvc")
@Transactional
public class MobEmployeeSvcImpl extends SortGenerator implements MobEmployeeSvc {
	@Autowired
	MobEmployeeDao mobEmployeeDao;
	
	@Autowired
	MobMappingFarmerEmployeeDao mobMappingFarmerEmployeeDao;

	@Autowired
	MiscellaneousSvc miscellaneousSvc;
	
	@Autowired
	MapperFacade mapperFacade; 

	private MobEmployeeDto convertEntityToDto(MobEmployee x) {
		MobEmployeeDto y = new MobEmployeeDto(x);
		if (!StringUtils.isEmpty(x.getAddressType()))
			y.setAddressType(miscellaneousSvc.getAddressType(x.getAddressType()));
		if (!StringUtils.isEmpty(x.getGender()))
			y.setGender(miscellaneousSvc.getGender(x.getGender()));
		if (!StringUtils.isEmpty(x.getReligion()))
			y.setReligion(miscellaneousSvc.getReligion(x.getReligion()));
		if (!StringUtils.isEmpty(x.getMaritalStatus()))
			y.setMaritalStatus(miscellaneousSvc.getMaritalStatus(x.getMaritalStatus()));
		if (!StringUtils.isEmpty(x.getLastEducation()))
			y.setLastEducation(miscellaneousSvc.getEducationType(x.getLastEducation()));
		if (x.getManager() != null && !x.getManager().equals(0)){
			y.setPrevManager(x.getManager());
			y.setManager(mobEmployeeDao.findOne(x.getManager()));
		}
		y.setEmployeePosition(String.join("; ",mobEmployeeDao.getRoleName(y.getId())));
		return y;
	}
	
	private MobEmployeeDto convertEntityToDtoLite(MobEmployee x) {
		MobEmployeeDto y = new MobEmployeeDto(x);
		if (x.getManager() != null && !x.getManager().equals(0)){
			y.setPrevManager(x.getManager());
			y.setManager(mobEmployeeDao.findOne(x.getManager()));
		}
		y.setEmployeePosition(String.join("; ",mobEmployeeDao.getRoleName(y.getId())));
		return y;
	}

	@Override
	public List<MobEmployee> findAll() {
		return mobEmployeeDao.findAll();
	}
	
	@Override
	public List<MobEmployeeDto> findAllDto() {
		List<MobEmployeeDto> res = new ArrayList<>();
		for(MobEmployee x: mobEmployeeDao.findAll()){
			res.add(convertEntityToDtoLite(x));
		}
		return res;
	}
	
	@Override
	public List<MobEmployeeDto> findAllById(List<Integer> intList){
		List<MobEmployeeDto> res = new ArrayList<>();
		for(MobEmployee x: mobEmployeeDao.findAll(intList)){
			res.add(convertEntityToDtoLite(x));
		}
		return res;
	}

	@Override
	public List<MobEmployeeDto> findManagedEmployees(List<Integer> id, int farmerId) {
		List<MobEmployeeDto> listDto = new ArrayList<>();
		if(id!=null && id.size()>0){
			List<Object[]> listObj = mobEmployeeDao.findManagedEmployeesById(id, farmerId);
			for(Object[] o : listObj){
				MobEmployeeDto e = mapperFacade.map(o[0], MobEmployeeDto.class);
				e.setEmployeePosition((String) o[1]);
				listDto.add(e);
			}
	//		List<MobEmployee> iterManagerEmployees;
	
	//		List<Integer> employeeIds = new ArrayList<>();
	//		employeeIds.add(id);
	//		// Loop sampai bawahannya ngga ada yang di-manage
	//		while (employeeIds.size() > 0) {
	//			iterManagerEmployees = mobEmployeeDao.findManagedEmployees(employeeIds);
	//			managedEmployees.addAll(iterManagerEmployees);
	//
	//			employeeIds.clear();
	//			for (MobEmployee x : iterManagerEmployees) {
	//				employeeIds.add(x.getId());
	//			}
	//		}
		}
		return listDto;
	}

	@Override
	public List<MobEmployeeDto> findAllDto(Integer pageNo, String colName, boolean sortAsc) {
		List<MobEmployeeDto> res = new ArrayList<>();
		for(MobEmployee x: 
			mobEmployeeDao.findAll(new PageRequest(pageNo-1, 25, generateSort("isActive", false).and(generateSort(colName, sortAsc))))){
			res.add(convertEntityToDto(x));
		}
		return res;
	}
	
	@Override
	public List<Integer> findAllIds(){
		return mobEmployeeDao.findAllIds();
	}

	@Override
	public Integer countAll() {
		return ((Long)mobEmployeeDao.count()).intValue();
	}

	@Override
	public List<MobEmployeeDto> findDtoByArg(String arg, Integer pageNo, String colName, boolean sortAsc) {
		List<MobEmployeeDto> res = new ArrayList<>();
		for(MobEmployee x: mobEmployeeDao.findByArg(arg,
				new PageRequest(pageNo-1, 25, generateSort("isActive", false).and(generateSort(colName, sortAsc))))){
			res.add(convertEntityToDto(x));
		}
		return res;
	}

	@Override
	public List<MobEmployeeDto> findMyManager(Integer userPersonId, String roleName, Integer farmerId) {
		// Mapping hierarchy baru bisa menghasilkan adanya banyak employee menghandle 1 farmer, 
		// jadi store ke set.
		Set<MobEmployeeDto> res = new HashSet<>();
		List<MobEmployee> managers = 
				mobEmployeeDao.findAll(mobMappingFarmerEmployeeDao.findMyEmployees(farmerId));
		
		MobEmployee manager;
		
		for(MobEmployee x: managers){
			if(!x.getId().equals(userPersonId)){
				MobEmployeeDto y = convertEntityToDto(x);
				y.setEmployeePosition(String.join("; ",mobEmployeeDao.getRoleName(y.getId())));
				// Apabila derajat nya sama dengan pembuat, maka jangan add. 
				if(!y.getEmployeePosition().contains(roleName)){
					res.add(y);
				}
				
				manager = x;
				while(manager!=null){
					manager = mobEmployeeDao.findOne(manager.getManager());
					if(manager!=null && !manager.getId().equals(userPersonId)) {
						y = convertEntityToDto(manager);
						y.setEmployeePosition(String.join("; ",mobEmployeeDao.getRoleName(y.getId())));
						if(!y.getEmployeePosition().contains(roleName)){
							res.add(y);
						}
					}
					else break;
				}
			}
		}
		
		return new ArrayList<>(res);
	}
	
	@Override
	public Integer countAllByArg(String arg) {
		return mobEmployeeDao.countByArg(arg).intValue();
	}

	@Override
	public MobEmployee save(MobEmployeeDto editEmp) {
		MobEmployee x = new MobEmployee();
		x.setAddress(editEmp.getAddress());
		x.setAddressType(editEmp.getAddressType()==null?"":editEmp.getAddressType().getCode());
		x.setBirthday(editEmp.getBirthday());
		x.setCity(editEmp.getCity());
		x.setCreatedBy(editEmp.getCreatedBy());
		x.setCreatedDate(editEmp.getCreatedDate());
		x.setDistrict(editEmp.getDistrict());
		x.setEmail(editEmp.getEmail());
		x.setEndDate(editEmp.getEndDate());
		x.setFullname(editEmp.getFullname());
		x.setGender(editEmp.getGender()==null?"":editEmp.getGender().getCode());
		x.setId(editEmp.getId());
		x.setIsActive(editEmp.getIsActive());
		x.setJointDate(editEmp.getJointDate());
		x.setKtp(editEmp.getKtp());
		x.setLastEducation(editEmp.getLastEducation()==null?"":editEmp.getLastEducation().getCode());
		x.setManager(editEmp.getManager()==null?0:editEmp.getManager().getId());
		x.setMaritalStatus(editEmp.getMaritalStatus()==null?"":editEmp.getMaritalStatus().getCode());
		x.setModifiedBy(editEmp.getModifiedBy());
		x.setModifiedDate(editEmp.getModifiedDate());
		x.setNipeg(editEmp.getNipeg());
		x.setNpwp(editEmp.getNpwp());
		x.setPhoneNum(editEmp.getPhoneNum());
		x.setPlaceOfBirthday(editEmp.getPlaceOfBirthday());
		x.setProvince(editEmp.getProvince());
		x.setReferenceCode(editEmp.getReferenceCode());
		x.setReligion(editEmp.getReligion()==null?"":editEmp.getReligion().getCode());
		x.setRtrw(editEmp.getRtrw());
		x.setSubDistrict(editEmp.getSubDistrict());
		x.setVillage(editEmp.getVillage());
		x.setZipCode(editEmp.getZipCode());
		x.setAreaName(editEmp.getAreaName());
		
		if((editEmp.getPrevManager()!=null && !editEmp.getPrevManager().equals(0)) &&
				!editEmp.getManager().getId().equals(editEmp.getPrevManager())){
			//update bawahan-nya, hapus mapping petani sendiri
			mobEmployeeDao.updateManager(editEmp.getPrevManager(),x.getManager());
			mobMappingFarmerEmployeeDao.delete(mobMappingFarmerEmployeeDao.findExisting(x.getId()));
			
			//berikan mapping atasan
			List<MobMappingFarmerEmployee> newMap = 
					mobMappingFarmerEmployeeDao.findExisting(x.getManager());
			List<MobMappingFarmerEmployee> newMap2 = new ArrayList<>();
			
			for(MobMappingFarmerEmployee y: newMap){
				MobMappingFarmerEmployee z = new MobMappingFarmerEmployee();
				
				z.setCreatedBy(y.getCreatedBy());
				z.setCreatedDate(y.getCreatedDate());
				z.setEmployeeId(x.getId());
				z.setFarmerId(y.getFarmerId());
				z.setIsActive(y.getIsActive());
				
				newMap2.add(z);
			}
			mobMappingFarmerEmployeeDao.save(newMap2);
			
			//set bawahan manager jadi null
			List<Integer> listParam1 = new ArrayList<>();
			listParam1.add(editEmp.getId());
			List<MobEmployee> n = mobEmployeeDao.findManagedEmployees(listParam1);
			for(MobEmployee o : n){
				MobEmployee b = new MobEmployee();
				b = o;
				b.setManager(null);
				mobEmployeeDao.save(b);
			}
		}

		return mobEmployeeDao.save(x);
	}

	@Override
	public void disable(Integer empId) {
		MobEmployee x = mobEmployeeDao.findOne(empId);
		x.setIsActive(false);
		mobEmployeeDao.save(x);
	}

	@Override
	public boolean existsName(Integer id, String fullname) {
		MobEmployee x = id == null ? null : mobEmployeeDao.findOne(id);
		if (x != null) {
			if (x.getFullname() != null && x.getFullname().equals(fullname)) {
				// special case for unchanged name
				return false;
			} else {
				return mobEmployeeDao.findByName(fullname).size() != 0;
			}
		} else
			return mobEmployeeDao.findByName(fullname).size() != 0;
	}
	
	@Override
	public boolean existsEmail(Integer id, String email) {
		MobEmployee x = id == null ? null : mobEmployeeDao.findOne(id);
		if (x != null) {
			if (x.getEmail() != null && x.getEmail().equals(email)) {
				// special case for unchanged name
				return false;
			} else {
				return mobEmployeeDao.findByEmail(email).size() != 0;
			}
		} else
			return mobEmployeeDao.findByEmail(email).size() != 0;
	}
	
	@Override
	public boolean existsPhone(Integer id, String phone) {
		MobEmployee x = id == null ? null : mobEmployeeDao.findOne(id);
		if (x != null) {
			if (x.getPhoneNum() != null && x.getPhoneNum().equals(phone)) {
				// special case for unchanged name
				return false;
			} else {
				return mobEmployeeDao.findByPhone(phone).size() != 0;
			}
		} else
			return mobEmployeeDao.findByPhone(phone).size() != 0;
	}

	@Override
	public MobEmployee findOne(Integer id) {
		return mobEmployeeDao.findOne(id);
	}

	@Override
	public List<MobEmployee> findAllNoUser() {
		return mobEmployeeDao.findAllNoUser();
	}

	@Override
	public void updatePhone(Integer personId, String phoneNum) {
		MobEmployee emp = findOne(personId);
		if(emp!=null){
			emp.setPhoneNum(phoneNum);
			mobEmployeeDao.save(emp);
		}
	}
	
	@Override
	public List<String> findDirectManagedNames(Integer id){
		return mobEmployeeDao.findDirectManagedNames(id);
	}
}
