package id.co.indocyber.bmmRmiCms.service;

import id.co.indocyber.bmmRmiCms.dto.MobIdentityDto;

import java.util.List;

public interface MobIdentitySvc {
	List<MobIdentityDto> getIdentities(Integer id);

	MobIdentityDto findIdentity(Integer id, Integer personId);

	void updateIdentity(Integer role, Integer personId, String phoneNum);
}
