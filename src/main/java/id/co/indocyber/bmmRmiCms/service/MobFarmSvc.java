package id.co.indocyber.bmmRmiCms.service;

import id.co.indocyber.bmmRmiCms.dto.MobFarmDto;
import id.co.indocyber.bmmRmiCms.entity.MobFarm;

import java.util.List;

public interface MobFarmSvc {
	public List<MobFarm> findAll();

	public List<MobFarm> findFarmByFarmer(Integer farmerId);
	
	public List<MobFarmDto> findFarmDtoByFarmer(Integer farmerId);

	public MobFarm findOne(Integer farmerId, String farmCode);

	public Integer farmCount(Integer farmerId);
}
