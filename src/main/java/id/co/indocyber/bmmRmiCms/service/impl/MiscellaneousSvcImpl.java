package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MAddressTypeDao;
import id.co.indocyber.bmmRmiCms.dao.MEducationTypeDao;
import id.co.indocyber.bmmRmiCms.dao.MFarmFieldsTypeDao;
import id.co.indocyber.bmmRmiCms.dao.MFarmerTypeDao;
import id.co.indocyber.bmmRmiCms.dao.MFundTypeDao;
import id.co.indocyber.bmmRmiCms.dao.MGenderDao;
import id.co.indocyber.bmmRmiCms.dao.MGlobalParamDao;
import id.co.indocyber.bmmRmiCms.dao.MMaritalStatusDao;
import id.co.indocyber.bmmRmiCms.dao.MPlantingPeriodDao;
import id.co.indocyber.bmmRmiCms.dao.MReligionDao;
import id.co.indocyber.bmmRmiCms.dao.MSugarcaneFarmTypeDao;
import id.co.indocyber.bmmRmiCms.dao.MSugarcaneTypeDao;
import id.co.indocyber.bmmRmiCms.dao.MVehicleTypeDao;
import id.co.indocyber.bmmRmiCms.entity.MAddressType;
import id.co.indocyber.bmmRmiCms.entity.MEducationType;
import id.co.indocyber.bmmRmiCms.entity.MFarmFieldsType;
import id.co.indocyber.bmmRmiCms.entity.MFarmerType;
import id.co.indocyber.bmmRmiCms.entity.MFundType;
import id.co.indocyber.bmmRmiCms.entity.MGender;
import id.co.indocyber.bmmRmiCms.entity.MMaritalStatus;
import id.co.indocyber.bmmRmiCms.entity.MPlantingPeriod;
import id.co.indocyber.bmmRmiCms.entity.MReligion;
import id.co.indocyber.bmmRmiCms.entity.MSugarcaneFarmType;
import id.co.indocyber.bmmRmiCms.entity.MSugarcaneType;
import id.co.indocyber.bmmRmiCms.entity.MVehicleType;
import id.co.indocyber.bmmRmiCms.service.MiscellaneousSvc;

@Service("miscellaneousSvc")
@Transactional
public class MiscellaneousSvcImpl implements MiscellaneousSvc{
	@Autowired
	MMaritalStatusDao mMaritalStatusDao;
	@Autowired
	MGenderDao mGenderDao;
	@Autowired
	MEducationTypeDao mEducationTypeDao;
	@Autowired
	MAddressTypeDao mAddressTypeDao;
	@Autowired
	MReligionDao mReligionDao;
	@Autowired
	MVehicleTypeDao mVehicleTypeDao;
	@Autowired
	MFarmerTypeDao mFarmerTypeDao;
	@Autowired
	MFarmFieldsTypeDao mFarmFieldsTypeDao;
	@Autowired
	MFundTypeDao mFundTypeDao;
	@Autowired
	MSugarcaneFarmTypeDao mSugarcaneFarmTypeDao;
	@Autowired
	MSugarcaneTypeDao mSugarcaneTypeDao;
	@Autowired
	MPlantingPeriodDao mPlantingPeriodDao;
	@Autowired
	MGlobalParamDao mGlobalParamDao;
	
	@Override
	public List<MGender> getGenders() {
		return mGenderDao.findAll();
	}

	@Override
	public List<MReligion> getReligions() {
		return mReligionDao.findAll();
	}

	@Override
	public List<MEducationType> getEducationTypes() {
		return mEducationTypeDao.findAll();
	}

	@Override
	public List<MAddressType> getAddressTypes() {
		return mAddressTypeDao.findAll();
	}

	@Override
	public List<MMaritalStatus> getMaritalStatuses() {
		return mMaritalStatusDao.findAll();
	}

	@Override
	public List<MVehicleType> getVehicleTypes() {
		return mVehicleTypeDao.findAll();
	}

	@Override
	public List<MFarmerType> getFarmerTypes() {
		return mFarmerTypeDao.findAll();
	}

	@Override
	public List<MFarmFieldsType> getFarmTypes() {
		return mFarmFieldsTypeDao.findAll();
	}

	@Override
	public MGender getGender(String code) {
		return mGenderDao.findOne(code);
	}

	@Override
	public MReligion getReligion(String code) {
		return mReligionDao.findOne(code);
	}

	@Override
	public MEducationType getEducationType(String code) {
		return mEducationTypeDao.findOne(code);
	}

	@Override
	public MAddressType getAddressType(String code) {
		return mAddressTypeDao.findOne(code);
	}

	@Override
	public MMaritalStatus getMaritalStatus(String code) {
		return mMaritalStatusDao.findOne(code);
	}

	@Override
	public MVehicleType getVehicleType(Integer code) {
		return mVehicleTypeDao.findOne(code);
	}

	@Override
	public MFarmerType getFarmerType(String code) {
		return mFarmerTypeDao.findOne(code);
	}

	@Override
	public MFarmFieldsType getFarmType(String code) {
		return mFarmFieldsTypeDao.findOne(code);
	}

	@Override
	public MFundType getFundType(String code) {
		return mFundTypeDao.findOne(code);
	}

	@Override
	public MSugarcaneFarmType getSugarcaneFarmType(String code) {
		return mSugarcaneFarmTypeDao.findOne(code);
	}

	@Override
	public MSugarcaneType getSugarcaneType(String code) {
		return mSugarcaneTypeDao.findOne(code);
	}

	@Override
	public MPlantingPeriod getPlantingPeriod(String code) {
		return mPlantingPeriodDao.findOne(code);
	}
	
	@Override
	public List<String> getOptions(String key){
		return mGlobalParamDao.getOptions(key);	
	}
}
