package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.apache.commons.codec.digest.DigestUtils;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.PageRequest;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import id.co.indocyber.bmmRmiCms.dao.MUserDao;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.entity.MUser;
import id.co.indocyber.bmmRmiCms.entity.MobEmployee;
import id.co.indocyber.bmmRmiCms.service.MUserSvc;

@Service("mUserSvc")
@Transactional
public class MUserSvcImpl extends SortGenerator implements MUserSvc{
	@Autowired
	MUserDao mUserDao;
	
	public String hash(String username, String pass){
		String toHash = username+"rejoso"+pass+"manisindo";
		String debugvar = DigestUtils.sha1Hex(toHash);
		return debugvar;
	}
	
	@Override
	public boolean login(String usernameOrEmail, String password) {
		MUser user = findByUsernameOrEmail(usernameOrEmail);
		
		if(user == null) {
			return false;
		}
		else {
			if(hash(user.getUsername(),password).equals(user.getPassword()))
				return true;
			return false;
		}
	}
	
	@Override
	public boolean login(String username, String password, String hashed) {
		if(hash(username,password).equals(hashed))
			return true;
		return false;
	}
	
	@Override
	public MUser findByUsernameOrEmail(String usernameOrEmail){
		return mUserDao.findByUsernameOrEmail(usernameOrEmail);
	}

	@Override
	public List<MUser> findAll() {
		return mUserDao.findAll();
	}

	@Override
	public boolean existence(String usernameOrEmail) {
		return findByUsernameOrEmail(usernameOrEmail)!=null;
	}

	@Override
	public List<MUserDto> findAll(Integer pageNo, String colName,boolean sortAsc) {
		List<Object[]> entities = mUserDao.getAll(
			new PageRequest(pageNo-1, 25, 
				generateSort("isActive", false).and(generateSort(colName,sortAsc))));
		List<MUserDto> res = new ArrayList<>();
		for (Object[] x: entities){
			MUserDto y = new MUserDto((MUser)x[0]);
			y.setPrevEmail(y.getEmail());
			y.setRoleName((String)x[1]);
			res.add(y);
		}
		return res;
	}

	@Override
	public Integer countAll() {
		return ((Long)mUserDao.count()).intValue();
	}

	@Override
	public void save(MUserDto editUser) {
		MUser user = new MUser();
		
		user.setCreatedBy(editUser.getCreatedBy());
		user.setCreatedDate(editUser.getCreatedDate());
		user.setEmail(editUser.getEmail());
		user.setFullname(editUser.getFullname());
		user.setImei(editUser.getImei());
		user.setIsActive(editUser.getIsActive());
		user.setModifiedBy(editUser.getModifiedBy());
		user.setModifiedDate(editUser.getModifiedDate());
		user.setNipeg(editUser.getNipeg());
		
		if(editUser.isRehash()){
			user.setPassword(hash(editUser.getUsername(),editUser.getPassword()));
		}else{
			user.setPassword(editUser.getPassword());
		}
		
		user.setPswdDefault(hash(editUser.getUsername(),"rmi2019"));
		user.setPersonId(editUser.getPersonId());
		if(!StringUtils.isEmpty(user.getPhoneNum()) && user.getPhoneNum().startsWith("628")){
			user.setPhoneNum(editUser.getPhoneNum().replaceFirst("628", "08"));
		}
		else user.setPhoneNum(editUser.getPhoneNum());
		user.setPhotoText(editUser.getPhotoText());
		user.setReferenceCode(editUser.getReferenceCode());
		user.setUsername(editUser.getUsername());
		user.setToken(editUser.getToken());
		
		if(editUser.getPrevEmail()!=null && !editUser.getPrevEmail().equals(editUser.getEmail())){
			//deleted = mUserDao.findOne(editUser.getPrevEmail());
			mUserDao.deleteByUsername(editUser.getUsername());
		}
		System.err.print(user.getPassword().compareTo(user.getPswdDefault()));
		
		if(existence(editUser.getUsername()))
			mUserDao.updateEmail(editUser.getUsername(), editUser.getEmail());
		
		mUserDao.save(user);
	}

	@Override
	public boolean existsMobile(String username, String phoneNum) {
		MUser x = StringUtils.isEmpty(username) ? null : mUserDao.findByUsernameOrEmail(username);
		if (x != null) {
			if (x.getPhoneNum() != null && x.getPhoneNum().equals(phoneNum)) {
				// special case for unchanged name
				return false;
			} else {
				return mUserDao.findByUsernameOrEmail(phoneNum)!=null;
			}
		} else
			return mUserDao.findByUsernameOrEmail(phoneNum)!=null;
	}

	@Override
	public boolean existsEmail(String username, String email) {
		MUser x = StringUtils.isEmpty(username) ? null : mUserDao.findByUsernameOrEmail(username);
		if (x != null) {
			if (x.getEmail() != null && x.getEmail().equals(email)) {
				// special case for unchanged name
				return false;
			} else {
				return existence(email);
			}
		} else
			return existence(email);
		
	}

	@Override
	public void disable(String username) {
		MUser user = findByUsernameOrEmail(username);
		user.setIsActive(false);
		mUserDao.save(user);
	}

	@Override
	public List<MUserDto> findByArg(String arg, Integer pageNo, String colName,
			boolean sortAsc) {
		List<Object[]> entities = mUserDao.findByArg(arg,
			new PageRequest(pageNo-1, 25, 
				generateSort("isActive", false).and(generateSort(colName,sortAsc))));
		List<MUserDto> res = new ArrayList<>();
		for (Object[] x: entities){
			MUserDto y = new MUserDto((MUser)x[0]);
			y.setRoleName((String)x[1]);
			res.add(y);
		}
		return res;
	}

	@Override
	public Integer countByArg(String arg) {
		return mUserDao.countByArg(arg).intValue();
	}

	@Override
	public void updateEmployeePhone(Integer id, String phoneNum) {
		MUser user = mUserDao.findEmployee(id);
		if(user!=null){
			user.setPhoneNum(phoneNum);
			mUserDao.save(user);
		}
	}
	
	@Override
	public void updateDriverPhone(Integer id, String phoneNum) {
		MUser user = mUserDao.findDriver(id);
		if(user!=null){
			user.setPhoneNum(phoneNum);
			mUserDao.save(user);
		}
	}
	
	@Override
	public String getEmployeeToken(Integer id){
		return mUserDao.findEmployeeToken(id);
	}
}
