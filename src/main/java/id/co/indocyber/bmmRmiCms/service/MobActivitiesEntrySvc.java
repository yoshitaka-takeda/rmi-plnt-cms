package id.co.indocyber.bmmRmiCms.service;

import java.util.List;

import id.co.indocyber.bmmRmiCms.dto.MobActivitiesEntryDto;

public interface MobActivitiesEntrySvc {
	
	public abstract List<MobActivitiesEntryDto> findAll(String arg, Integer pageNo, String colName,
		boolean sortAsc);
	public abstract List<MobActivitiesEntryDto> findAllById(List<Integer>ids, 
		String arg, Integer pageNo, String colName, boolean sortAsc);

	public abstract Integer countAll(String arg);
	public abstract Integer countAllById(List<Integer>ids, String arg);
	
	public abstract void save(MobActivitiesEntryDto viewEntry);
}
