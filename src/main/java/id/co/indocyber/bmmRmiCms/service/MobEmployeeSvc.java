package id.co.indocyber.bmmRmiCms.service;

import id.co.indocyber.bmmRmiCms.dto.MobEmployeeDto;
import id.co.indocyber.bmmRmiCms.entity.MobEmployee;

import java.util.List;

public interface MobEmployeeSvc {
	public List<MobEmployee> findAll();
	
	public List<MobEmployeeDto> findAllDto(Integer pageNo, String colName, boolean sortAsc);
	public List<Integer> findAllIds();
	public Integer countAll();
	public List<MobEmployeeDto> findDtoByArg(String arg, Integer pageNo, String colName, boolean sortAsc);
	public Integer countAllByArg(String arg);

	public MobEmployee save(MobEmployeeDto editEmp);
	public void disable(Integer empId);
	
	public List<MobEmployeeDto> findManagedEmployees(List<Integer> id, int farmerId);
	public List<MobEmployeeDto> findMyManager(Integer userPersonId, String roleName, Integer farmerId);
	
	public boolean existsName(Integer id, String fullname);
	public boolean existsEmail(Integer id, String email);
	public boolean existsPhone(Integer id, String phone);
	
	public MobEmployee findOne(Integer id);

	public List<MobEmployee> findAllNoUser();

	public void updatePhone(Integer personId, String phoneNum);

	public List<MobEmployeeDto> findAllDto();
	public List<MobEmployeeDto> findAllById(List<Integer> intList);
	
	public List<String> findDirectManagedNames(Integer id);
}
