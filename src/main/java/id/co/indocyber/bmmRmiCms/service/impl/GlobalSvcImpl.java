package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.ArrayList;
import java.util.LinkedHashSet;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dao.MUserDao;
import id.co.indocyber.bmmRmiCms.dao.MobEmployeeDao;
import id.co.indocyber.bmmRmiCms.dto.MUserDto;
import id.co.indocyber.bmmRmiCms.entity.MUser;
import id.co.indocyber.bmmRmiCms.entity.MUserRole;
import id.co.indocyber.bmmRmiCms.entity.MobEmployee;
import id.co.indocyber.bmmRmiCms.service.GlobalSvc;
import ma.glasnost.orika.MapperFacade;

@Service("globalSvc")
@Transactional
public class GlobalSvcImpl implements GlobalSvc {
	
	@Autowired
	private MobEmployeeDao mobEmployeeDao;
	
	@Autowired
	private MapperFacade mapperFacade;
	
	@Autowired
	private MUserDao mUserDao;
	
	@Override
	public List<Integer> getAllMyTeam(Integer personId) {
		List<Integer> listParam1 = new ArrayList<>();
		listParam1.add(personId);
		List<MobEmployee> list1 = mobEmployeeDao.findManagedEmployees(listParam1);
		
		List<Integer> listId = new ArrayList<>();
		for(MobEmployee e : list1){
			listId.add(e.getId());
		}
		
		List<MobEmployee> list2 = new ArrayList<>();
		if(listId!=null && listId.size()>0)
		{
			for(int i = 1; i < 10 ; i++){
				list2 = mobEmployeeDao.findManagedEmployees(listId);
				if(list2.size() > 0){
					for(MobEmployee f : list2){
						listId.add(f.getId());
					}
				}
			}
		}
		
		listId.add(personId);
		
		LinkedHashSet<Integer> hashSet = new LinkedHashSet<>(listId);
		List<Integer> listOutput = new ArrayList<>(hashSet);
		return listOutput;
	}

	@Override
	public List<MUserDto> listTesting() {
		List<MUser> m = mUserDao.findAll();
		List<MUserDto> listDto = new ArrayList<>();
		for(MUser u : m){
			MUserDto dto = mapperFacade.map(u, MUserDto.class);
			listDto.add(dto);
		}
		return listDto;
	}
	
	@Override
	public MUserRole findUserLogin(String username) {
		return mUserDao.findUserLogin(username);
	}


}
