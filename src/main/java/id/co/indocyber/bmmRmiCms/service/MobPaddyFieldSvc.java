package id.co.indocyber.bmmRmiCms.service;

import id.co.indocyber.bmmRmiCms.dto.MobPaddyFieldDto;
import id.co.indocyber.bmmRmiCms.entity.MobPaddyField;

import java.util.List;

public interface MobPaddyFieldSvc {
	public List<MobPaddyField> findPaddyFieldByFarmCode(Integer farmerId, String farmCode);
	
	public List<MobPaddyFieldDto> findPaddyFieldDtoByFarmCode(Integer farmerId, String farmCode);

	public List<MobPaddyField> findSelectedPaddyFields(Integer farmerId, String farmCode, 
			List<String> paddyFieldCodes);

	public Double getFieldArea(Integer farmerId, String farmCode, String paddyFieldCode);
}
