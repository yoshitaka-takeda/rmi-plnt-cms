package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;

import id.co.indocyber.bmmRmiCms.dto.MobIdentityDto;
import id.co.indocyber.bmmRmiCms.entity.MobDriver;
import id.co.indocyber.bmmRmiCms.entity.MobEmployee;
import id.co.indocyber.bmmRmiCms.entity.MobFarmer;
import id.co.indocyber.bmmRmiCms.service.MobDriverSvc;
import id.co.indocyber.bmmRmiCms.service.MobEmployeeSvc;
import id.co.indocyber.bmmRmiCms.service.MobFarmerSvc;
import id.co.indocyber.bmmRmiCms.service.MobIdentitySvc;

@Service("mobIdentitySvc")
@Transactional
public class MobIdentitySvcImpl implements MobIdentitySvc{
	@Autowired
	MobEmployeeSvc mobEmployeeSvc;
	
	@Autowired
	MobFarmerSvc mobFarmerSvc;
	
	@Autowired
	MobDriverSvc mobDriverSvc;
	
	@Override
	public List<MobIdentityDto> getIdentities(Integer role) {
		List<MobIdentityDto> ids = new ArrayList<>();
		switch(role){
		case 1:
			for(MobEmployee x: mobEmployeeSvc.findAllNoUser()){
				ids.add(new MobIdentityDto(x));
			}
			return ids;
		case 2:
			for(MobFarmer x: mobFarmerSvc.findAllNoUser()){
				ids.add(new MobIdentityDto(x));
			}
			return ids;
		case 3:
			for(MobDriver x: mobDriverSvc.findAllNoUser()){
				ids.add(new MobIdentityDto(x));
			}
			return ids;
		default:
			for(MobEmployee x: mobEmployeeSvc.findAllNoUser()){
				ids.add(new MobIdentityDto(x));
			}
			return ids;
		}
	}

	@Override
	public MobIdentityDto findIdentity(Integer role, Integer personId) {
		MobEmployee emp;
		MobFarmer farmer;
		MobDriver drv;
		
		switch(role){
		case 1:
			emp = mobEmployeeSvc.findOne(personId);
			if(emp==null) return null;
			return new MobIdentityDto(emp);
		case 2:
			farmer = mobFarmerSvc.findOne(personId);
			if(farmer==null) return null;
			return new MobIdentityDto(farmer);
		case 3:
			drv = mobDriverSvc.findOne(personId);
			if(drv==null) return null;
			return new MobIdentityDto(drv);
		default:
			emp = mobEmployeeSvc.findOne(personId);
			if(emp==null) return null;
			return new MobIdentityDto(emp);
		}
	}
	
	@Override
	public void updateIdentity(Integer role, Integer personId, String phoneNum){
		switch(role){
		case 1:
			mobEmployeeSvc.updatePhone(personId,phoneNum);
			break;
		case 2:
			//do nothing
			break;
		case 3:
			mobDriverSvc.updatePhone(personId,phoneNum);
			break;
		default:
			mobEmployeeSvc.updatePhone(personId,phoneNum);
			break;
		}
	}
}
