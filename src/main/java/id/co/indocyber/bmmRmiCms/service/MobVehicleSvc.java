package id.co.indocyber.bmmRmiCms.service;

import id.co.indocyber.bmmRmiCms.dto.MobVehicleDto;
import id.co.indocyber.bmmRmiCms.entity.MobVehicle;

import java.util.List;

public interface MobVehicleSvc {
	public List<MobVehicle> findAll();
	public List<MobVehicleDto> findAll(Integer pageNo, String colName, boolean sortAsc);
	public Integer countAll();
	public List<MobVehicleDto> findByArg(String arg, Integer pageNo, String colName, boolean sortAsc);
	public Integer countByArg(String arg);
	
	public void save(MobVehicleDto dto);
	
	public boolean existence(String id);
	public void disable(String id);
}
