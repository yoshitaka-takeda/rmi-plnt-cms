package id.co.indocyber.bmmRmiCms.service;

import id.co.indocyber.bmmRmiCms.entity.MSugarcaneParamScoring;

import java.util.List;

public interface MSugarcaneParamScoringSvc {
	public List<MSugarcaneParamScoring> findAll();
	public List<MSugarcaneParamScoring> findAllActive();
	public List<MSugarcaneParamScoring> findAll(Integer pageNo, String colName, boolean sortAsc);
	public Integer countAll();
	public List<MSugarcaneParamScoring> findByArg(String arg, Integer pageNo, String colName, boolean sortAsc);
	public Integer countByArg(String arg);
	public Double getSum();
	
	public MSugarcaneParamScoring save(MSugarcaneParamScoring entity);
	
	public boolean existence(Integer paramCode);
	public void disable(Integer paramCode);
}
