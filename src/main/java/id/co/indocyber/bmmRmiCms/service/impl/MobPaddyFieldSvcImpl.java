package id.co.indocyber.bmmRmiCms.service.impl;

import java.util.ArrayList;
import java.util.List;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.StringUtils;

import id.co.indocyber.bmmRmiCms.dao.MobPaddyFieldDao;
import id.co.indocyber.bmmRmiCms.dto.MobPaddyFieldDto;
import id.co.indocyber.bmmRmiCms.entity.MobPaddyField;
import id.co.indocyber.bmmRmiCms.service.MiscellaneousSvc;
import id.co.indocyber.bmmRmiCms.service.MobPaddyFieldSvc;

@Service("mobPaddyFieldSvc")
@Transactional
public class MobPaddyFieldSvcImpl extends SortGenerator implements MobPaddyFieldSvc{
	@Autowired
	private MobPaddyFieldDao mobPaddyFieldDao;
	@Autowired
	private MiscellaneousSvc miscellaneousSvc;
	
	@Override
	public List<MobPaddyField> findPaddyFieldByFarmCode(Integer farmerId,
			String farmCode) {
		return mobPaddyFieldDao.findFieldsByFarmCode(farmerId, farmCode);
	}

	@Override
	public List<MobPaddyField> findSelectedPaddyFields(Integer farmerId,
			String farmCode, List<String> paddyFieldCodes) {
		return mobPaddyFieldDao.findSelectedPaddyFields(farmerId, farmCode, paddyFieldCodes);
	}

	@Override
	public List<MobPaddyFieldDto> findPaddyFieldDtoByFarmCode(Integer farmerId,
			String farmCode) {
		List<MobPaddyFieldDto> res = new ArrayList<>();
		for(MobPaddyField x : findPaddyFieldByFarmCode(farmerId, farmCode)){
			res.add(convertEntityToDto(x));
		}
		return res;
	}

	private MobPaddyFieldDto convertEntityToDto(MobPaddyField x) {
		MobPaddyFieldDto y = new MobPaddyFieldDto(x);
		if(!StringUtils.isEmpty(x.getSugarcaneType()))
			y.setSugarcaneType(miscellaneousSvc.getSugarcaneType(x.getSugarcaneType()));
		if(!StringUtils.isEmpty(x.getPlantingPeriod()))
			y.setPlantingPeriod(miscellaneousSvc.getPlantingPeriod(x.getPlantingPeriod()));
		return y;
	}

	@Override
	public Double getFieldArea(Integer farmerId, String farmCode,
			String paddyFieldCode) {
		return mobPaddyFieldDao.findFieldArea(farmerId,farmCode,paddyFieldCode);
	}
	
}
