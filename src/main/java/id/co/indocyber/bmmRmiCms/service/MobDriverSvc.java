package id.co.indocyber.bmmRmiCms.service;

import id.co.indocyber.bmmRmiCms.dto.MobDriverDto;
import id.co.indocyber.bmmRmiCms.entity.MobDriver;

import java.util.List;

public interface MobDriverSvc {
	public List<MobDriver> findAll();
	public List<MobDriverDto> findAll(Integer pageNo, String colName, boolean sortAsc);
	public Integer countAll();
	public List<MobDriverDto> findByArg(String arg, Integer pageNo, String colName, boolean sortAsc);
	public Integer countByArg(String arg);
	
	public MobDriver save(MobDriverDto dto);
	
	public boolean existence(Integer id);
	public void disable(Integer id);
	
	public boolean existsName(Integer id, String fullname);
	public boolean existsEmail(Integer id, String email);
	
	public MobDriver findOne(Integer id);
	public List<MobDriver> findAllNoUser();
	
	public void updatePhone(Integer personId, String phoneNum);
}
