package id.co.indocyber.bmmRmiCms.tools;

import java.io.Serializable;

public class RestResponse implements Serializable {

	private static final long serialVersionUID = 1L;
	
	private int status;	
	private String message; 
	private Object dataObject;
	
	public RestResponse(){
	}
	
	public RestResponse(int status, String message, Object dataObject){
		this.status = status;
		this.message = message;
		this.dataObject = dataObject;
	}
	
	public int getStatus() {
		return status;
	}
	
	public void setStatus(int status) {
		this.status = status;
	}
	
	public String getMessage() {
		return message;
	}
	
	public void setMessage(String message) {
		this.message = message;
	}

	public Object getDataObject() {
		return dataObject;
	}

	public void setDataObject(Object dataObject) {
		this.dataObject = dataObject;
	}
	
		
}
