package id.co.indocyber.bmmRmiCms.tools;

import id.co.indocyber.bmmRmiCms.dto.MobScheduleVisitAppraisementDto;
import id.co.indocyber.bmmRmiCms.dto.MobScheduleVisitScoringDto;
import id.co.indocyber.bmmRmiCms.dto.MobScheduleVisitToFarmerResultsDto;
import id.co.indocyber.bmmRmiCms.entity.MSugarcaneParamScoring;

import java.io.ByteArrayOutputStream;
import java.io.IOException;
import java.lang.reflect.Field;
import java.util.List;

import org.apache.poi.ss.usermodel.Cell;
import org.apache.poi.ss.usermodel.CellStyle;
import org.apache.poi.ss.usermodel.Font;
import org.apache.poi.ss.usermodel.IndexedColors;
import org.apache.poi.ss.usermodel.Row;
import org.apache.poi.ss.util.CellRangeAddress;
import org.apache.poi.xssf.usermodel.XSSFCellStyle;
import org.apache.poi.xssf.usermodel.XSSFSheet;
import org.apache.poi.xssf.usermodel.XSSFWorkbook;

public class ExcelExport {
	private XSSFWorkbook workbook = new XSSFWorkbook();

	private XSSFCellStyle generateTitleStyle() {
		XSSFCellStyle cs = workbook.createCellStyle();
		cs.setWrapText(true);
		cs.setAlignment(CellStyle.ALIGN_CENTER);
		cs.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		Font bold = workbook.createFont();
		bold.setBoldweight(Font.BOLDWEIGHT_BOLD);
		cs.setFont(bold);
		return cs;
	}

	private XSSFCellStyle generateTableHeaderStyle() {
		XSSFCellStyle cs = generateTitleStyle();
		cs.setBorderBottom(CellStyle.BORDER_THIN);
		cs.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		cs.setBorderTop(CellStyle.BORDER_THIN);
		cs.setTopBorderColor(IndexedColors.BLACK.getIndex());
		cs.setBorderLeft(CellStyle.BORDER_THIN);
		cs.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		cs.setBorderRight(CellStyle.BORDER_THIN);
		cs.setRightBorderColor(IndexedColors.BLACK.getIndex());
		return cs;
	}

	private XSSFCellStyle generateTableContentStyle() {
		XSSFCellStyle cs = workbook.createCellStyle();
		cs.setBorderBottom(CellStyle.BORDER_THIN);
		cs.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		cs.setBorderTop(CellStyle.BORDER_THIN);
		cs.setTopBorderColor(IndexedColors.BLACK.getIndex());
		cs.setBorderLeft(CellStyle.BORDER_THIN);
		cs.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		cs.setBorderRight(CellStyle.BORDER_THIN);
		cs.setRightBorderColor(IndexedColors.BLACK.getIndex());
		return cs;
	}

	private XSSFCellStyle generateTableContentStyleCenter() {
		XSSFCellStyle cs = workbook.createCellStyle();
		cs.setBorderBottom(CellStyle.BORDER_THIN);
		cs.setBottomBorderColor(IndexedColors.BLACK.getIndex());
		cs.setBorderTop(CellStyle.BORDER_THIN);
		cs.setTopBorderColor(IndexedColors.BLACK.getIndex());
		cs.setBorderLeft(CellStyle.BORDER_THIN);
		cs.setLeftBorderColor(IndexedColors.BLACK.getIndex());
		cs.setBorderRight(CellStyle.BORDER_THIN);
		cs.setRightBorderColor(IndexedColors.BLACK.getIndex());
		cs.setAlignment(CellStyle.ALIGN_CENTER);
		cs.setVerticalAlignment(CellStyle.VERTICAL_CENTER);
		return cs;
	}

	public byte[] generate(List<MSugarcaneParamScoring> questionList,
			MobScheduleVisitToFarmerResultsDto result) {
		String[] criteriaHeaders = new String[] { "NO", "URAIAN", "KRITERIA",
				"BOBOT" };
		String[] footerFields = new String[] { "RENCANA: ", "EVALUASI: " };
		String[] paramWeightFields = new String[] { "paramValue", "criteria",
				"weight" };
		String[] farmerFields = new String[] { "Nama Petani", "Alamat Petani",
				"Kepemilikan Luas Lahan", "Jumlah Kebun" };

		XSSFSheet sheet = workbook.createSheet("Sheet 1");

		CellStyle titleStyle = generateTitleStyle();
		CellStyle headerStyle = generateTableHeaderStyle();
		CellStyle contentStyle = generateTableContentStyle();
		CellStyle contentCenterStyle = generateTableContentStyleCenter();

		int rowNum = 0;

		// Row 1 (for Title)
		Row row = sheet.createRow(rowNum++);
		Cell titleCell = row.createCell(0);
		titleCell.setCellValue("EIGHT SCORE POLA TEBANGAN");
		titleCell.setCellStyle(titleStyle);
		sheet.addMergedRegion(new CellRangeAddress(0, // first row (0-based)
				0, // last row (0-based)
				0, // first column (0-based)
				9 // last column (0-based)
		));

		// Row 2 (for PG)
		row = sheet.createRow(rowNum++);
		Cell pgCell = row.createCell(9);
		pgCell.setCellValue("UNTUK PG");

		// Row 3-6 (farmer info)
		// TODO: lanjutkan pembuatan template saat datanya sudah ada.
		// speaking of which, struktur objectnya aja belum tahu...

		for (int i = 0; i < farmerFields.length; i++) {
			row = sheet.createRow(rowNum++);
			pgCell = row.createCell(0);
			pgCell.setCellValue(farmerFields[i]);
			sheet.addMergedRegion(new CellRangeAddress(rowNum - 1, // first row
																	// (0-based)
					rowNum - 1, // last row (0-based)
					0, // first column (0-based)
					1 // last column (0-based)
			));
			pgCell = row.createCell(2);
			switch (i) {
			case 0:
				pgCell.setCellValue(result.getFarmer().getFullname());
				break;
			case 1:
				pgCell.setCellValue(result.getFarmer().getAddress());
				break;
			case 2:
				pgCell.setCellValue(result.getFarmer().getFieldTotal()
						.toString()
						+ " Ha.");
				break;
			case 3:
				pgCell.setCellValue(result.getFarmCount());
				break;
			}
		}

		sheet.createRow(7);
		sheet.createRow(8);
		sheet.createRow(9);

		Cell critCell;
		for (int i = 0; i < criteriaHeaders.length; i++) {
			row = sheet.getRow(7);
			critCell = row.createCell(i);
			critCell.setCellValue(criteriaHeaders[i]);
			critCell.setCellStyle(headerStyle);
			row = sheet.getRow(8);
			critCell = row.createCell(i);
			critCell.setCellStyle(headerStyle);
			row = sheet.getRow(9);
			critCell = row.createCell(i);
			critCell.setCellStyle(headerStyle);
			sheet.addMergedRegion(new CellRangeAddress(7, // first row (0-based)
					9, // last row (0-based)
					i, // first column (0-based)
					i // last column (0-based)
			));
		}

		// Panjang dari bagian kebun bergantung pada jumlah data...
		row = sheet.getRow(7);
		critCell = row.createCell(4);
		critCell.setCellValue("KEBUN");
		critCell.setCellStyle(headerStyle);
		sheet.addMergedRegion(new CellRangeAddress(7, // first row (0-based)
				7, // last row (0-based)
				4, // first column (0-based)
				9 // last column (0-based)
		));
		for (int i = 0; i < 5; i++) {
			critCell = row.createCell(5 + i);
			critCell.setCellStyle(headerStyle);
		}

		int contentRow = 10;
		int questionCount = questionList.size();
		for (int i = 0; i < questionCount; i++) {
			row = sheet.createRow(contentRow + i);
			int col = 0;
			Cell contentCell;
			for (int j = 0; j < 4; j++) {
				contentCell = row.createCell(col++);
				if (j == 0) {
					contentCell.setCellStyle(headerStyle);
					contentCell.setCellValue(i + 1);
				} else {
					try {
						Field field = questionList.get(i).getClass()
								.getDeclaredField(paramWeightFields[j - 1]);
						field.setAccessible(true);
						try {
							contentCell.setCellValue(field.get(
									questionList.get(i)).toString());
						} catch (IllegalArgumentException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							break;
						} catch (IllegalAccessException e) {
							// TODO Auto-generated catch block
							e.printStackTrace();
							break;
						} catch (NullPointerException e) {
							contentCell.setCellValue("");
						} finally {
							if (j < 2)
								contentCell.setCellStyle(contentStyle);
							else
								contentCell.setCellStyle(contentCenterStyle);
						}
					} catch (NoSuchFieldException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						break;
					} catch (SecurityException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						break;
					}
				}
			}
			// print data untuk setiap farm disini
			for (int z = 0; z < result.getReports().size(); z++) {
				MobScheduleVisitScoringDto dto = result.getReports().get(z);
				contentCell = row.createCell(col++);
				if (i <= dto.getReportDetails().size() - 1) {
					contentCell.setCellValue(dto.getReportDetails().get(i)
							.getScore());
				}
				contentCell.setCellStyle(contentCenterStyle);
			}
		}

		for (int i = 0; i < result.getReports().size(); i++) {
			Row farms = sheet.getRow(8);
			Cell farmCell = farms.getCell(4 + i, Row.CREATE_NULL_AS_BLANK);
			farmCell.setCellValue(result.getReports().get(i).getFarmVille());
			farmCell.setCellStyle(contentCenterStyle);
			farms = sheet.getRow(9);
			farmCell = farms.getCell(4 + i, Row.CREATE_NULL_AS_BLANK);
			farmCell.setCellValue(result.getReports().get(i).getFieldArea()
					.toString()
					+ " Ha.");
			farmCell.setCellStyle(contentCenterStyle);
		}

		// Scoring & Ranking
		Row total = sheet.createRow(10 + questionCount);
		Cell resultCell = total.createCell(0);
		resultCell.setCellStyle(headerStyle);
		resultCell = total.createCell(1);
		resultCell.setCellStyle(headerStyle);
		resultCell.setCellValue("SCORING");
		resultCell = total.createCell(2);
		resultCell.setCellStyle(headerStyle);
		resultCell = total.createCell(3);
		resultCell.setCellStyle(headerStyle);
		resultCell.setCellValue(100);
		for (int i = 0; i < result.getReports().size(); i++) {
			resultCell = total.createCell(4 + i);
			resultCell.setCellStyle(headerStyle);
			resultCell.setCellValue(result.getReports().get(i).getWeightSum());
		}
		total = sheet.createRow(11 + questionCount);
		resultCell = total.createCell(0);
		resultCell.setCellStyle(headerStyle);
		resultCell = total.createCell(1);
		resultCell.setCellStyle(headerStyle);
		resultCell.setCellValue("RANKING");
		resultCell = total.createCell(2);
		resultCell.setCellStyle(headerStyle);
		resultCell = total.createCell(3);
		resultCell.setCellStyle(headerStyle);
		for (int i = 0; i < result.getReports().size(); i++) {
			resultCell = total.createCell(4 + i);
			resultCell.setCellStyle(headerStyle);
			resultCell.setCellValue(result.getReports().get(i).getRank());
		}

		// Footer
		int footerRow = 13 + questionCount;
		for (int i = 0; i < footerFields.length; i++) {
			row = sheet.createRow(footerRow++);
			pgCell = row.createCell(0);
			pgCell.setCellValue(footerFields[i]);
			sheet.addMergedRegion(new CellRangeAddress(footerRow - 1, // first
																		// row
																		// (0-based)
					footerRow - 1, // last row (0-based)
					0, // first column (0-based)
					1 // last column (0-based)
			));
		}

		footerRow += 2;
		row = sheet.createRow(footerRow);
		pgCell = row.createCell(1);
		pgCell.setCellValue("Dibuat");
		pgCell = row.createCell(4);
		pgCell.setCellValue("Diketahui");
		pgCell = row.createCell(8);
		pgCell.setCellValue("Disetujui");

		for (int i = 0; i < 10; i++) {
			sheet.autoSizeColumn(i, true);
			if (i >= 5)
				sheet.setColumnWidth(i, sheet.getColumnWidth(4));
		}

		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			workbook.write(baos);
			return baos.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}

	public byte[] generateTaksasi(String month, String year,
			List<MobScheduleVisitAppraisementDto> dataTaksasi) {
		String title = "BLANGKO TAKSASI " + month.toUpperCase() + " " + year;

		String[] headers = new String[] { "No.", "Kecamatan", "Desa / Kel",
				"Luas (Ha)", "Nama Pemilik", "Nomor Register", "Bulan Tanam",
				"Tinggi Batang (m)","Berat Batang per Meter (kg)",
				"Berat per Batang (kg)","Jumlah Batang tiap Leng",
				"Jumlah Leng per Ha", "Jumlah Batang per Ha",
				"Prod per Hektar (Ku)", "Jumlah Produksi (Ku)","Periode Tebang",
				"Rerata Angka Brix","Estimasi Rendemen","Ku Hablur Per Ha",
				"Jumlah Hablur", "Koordinat"};

		String[] tebang = new String[] { "Sekarang", "Ditebang" };
		String[] koordinat = new String[] { "Latitude", "Longitude" };

		String[] fieldNames = new String[] { "subDistrict", "district",
				"fieldArea", "farmer", "paddyFieldCode", "periodName",
				"currentSugarcaneHeight", "cutdownSugarcaneHeight",
				"sugarcaneStemWeight", "weightPerSugarcane",
				"numberOfStemPerLeng", "lengFactorPerHa",
				"numberOfSugarcanePerHa", "prodPerHectarKu",
				"amountOfProduction", "cuttingSchedule", "avgBrixNumber",
				"rendemenEstimation", "kuHablurPerHa", "amountOfHablur",
				"latitude", "longitude" };

		XSSFSheet sheet = workbook.createSheet("Sheet 1");

		CellStyle titleStyle = generateTitleStyle();
		CellStyle headerStyle = generateTableHeaderStyle();
		CellStyle contentStyle = generateTableContentStyle();
		CellStyle contentCenterStyle = generateTableContentStyleCenter();

		Row titleRow;
		Cell titleCell;

		// Row 1 (for Title)
		Row row = sheet.createRow(0);
		titleCell = row.createCell(0);
		titleCell.setCellValue(title);
		titleCell.setCellStyle(titleStyle);
		sheet.addMergedRegion(new CellRangeAddress(0, // first row (0-based)
				0, // last row (0-based)
				0, // first column (0-based)
				9 // last column (0-based)
		));

		// Row 5-6 (column headers)
		sheet.createRow(4);
		sheet.createRow(5);
		for (int i = 0; i < 7; i++) {
			for (int j = 0; j < 2; j++) {
				titleRow = sheet.getRow(4 + j);
				titleCell = titleRow.createCell(i);
				if (j == 0)
					titleCell.setCellValue(headers[i]);
				titleCell.setCellStyle(headerStyle);
			}
			sheet.addMergedRegion(new CellRangeAddress(4, // first row (0-based)
					5, // last row (0-based)
					i, // first column (0-based)
					i // last column (0-based)
			));
		}

		titleRow = sheet.getRow(4);
		for (int i = 0; i < 2; i++) {
			titleCell = titleRow.createCell(7 + i);
			if (i == 0)
				titleCell.setCellValue(headers[7]);
			titleCell.setCellStyle(headerStyle);
		}
		sheet.addMergedRegion(new CellRangeAddress(4, // first row (0-based)
				4, // last row (0-based)
				7, // first column (0-based)
				8 // last column (0-based)
		));
		
		for (int i = 0; i < 12; i++) {
			for (int j = 0; j < 2; j++) {
				titleRow = sheet.getRow(4 + j);
				titleCell = titleRow.createCell(9 + i);
				if (j == 0)
					titleCell.setCellValue(headers[8 + i]);
				titleCell.setCellStyle(headerStyle);
			}
			sheet.addMergedRegion(new CellRangeAddress(4, // first row (0-based)
					5, // last row (0-based)
					9+i, // first column (0-based)
					9+i // last column (0-based)
			));
		}

		titleRow = sheet.getRow(4);
		for (int i = 0; i < 2; i++) {
			titleCell = titleRow.createCell(21 + i);
			if (i == 0)
				titleCell.setCellValue(headers[20]);
			titleCell.setCellStyle(headerStyle);
		}
		sheet.addMergedRegion(new CellRangeAddress(4, // first row (0-based)
				4, // last row (0-based)
				21, // first column (0-based)
				22 // last column (0-based)
		));

		// Row 6 (Hasil Tebang & Koordinat)
		titleRow = sheet.getRow(5);
		for (int i = 0; i < 2; i++) {
			titleCell = titleRow.createCell(7 + i);
			titleCell.setCellValue(tebang[i]);
			titleCell.setCellStyle(headerStyle);
		}
		
		for (int i = 0; i < 2; i++) {
			titleCell = titleRow.createCell(21 + i);
			titleCell.setCellValue(koordinat[i]);
			titleCell.setCellStyle(headerStyle);
		}

		int rowNum = 1;
		Row contentRow;
		Cell contentCell;
		// Print Data here!!
		for (MobScheduleVisitAppraisementDto x : dataTaksasi) {
			contentRow = sheet.createRow(5 + rowNum);
			contentCell = contentRow.createCell(0);
			contentCell.setCellValue(rowNum++);
			contentCell.setCellStyle(contentStyle);
			for (int i = 0; i < fieldNames.length; i++) {
				contentCell = contentRow.createCell(i + 1);
				try {
					Field field = x.getClass().getDeclaredField(fieldNames[i]);
					field.setAccessible(true);
					try {
						contentCell.setCellValue(field.get(x).toString());
					} catch (IllegalArgumentException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						break;
					} catch (IllegalAccessException e) {
						// TODO Auto-generated catch block
						e.printStackTrace();
						break;
					} catch (NullPointerException e) {
						contentCell.setCellValue("");
					} finally {
						contentCell.setCellStyle(contentStyle);

					}
				} catch (NoSuchFieldException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					break;
				} catch (SecurityException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
					break;
				}
			}
		}

		for (int i = 0; i < 23; i++) {
			sheet.autoSizeColumn(i, true);
		}

		try {
			ByteArrayOutputStream baos = new ByteArrayOutputStream();
			workbook.write(baos);
			return baos.toByteArray();
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
			return null;
		}
	}
}
