package id.co.indocyber.bmmRmiCms.tools;

import org.zkoss.zk.ui.WrongValueException;
import org.zkoss.zul.Radio;
import org.zkoss.zul.Radiogroup;

public class CustomRadiogroup extends Radiogroup {
	/**
	 * 
	 */
	private static final long serialVersionUID = 8433398023296329516L;
	private String _constraint;
	public void setConstraint (String constraint) {
		_constraint = constraint;
	}
	// overload instead of override since
	// getSelectedItem () used in original Radiogroup
	public Radio getSelectedItem (boolean checkConstraint) throws WrongValueException {
		Radio radio = super.getSelectedItem();
		System.out.println(radio);
		if (radio == null) {
			if (_constraint != null && _constraint.contains("no empty")) {
				String msg = "Jenis kelamin harus dipilih.";
				System.out.println(" constraint ");
				throw new WrongValueException(this, msg);
			}
		}
		return radio;
	}
}